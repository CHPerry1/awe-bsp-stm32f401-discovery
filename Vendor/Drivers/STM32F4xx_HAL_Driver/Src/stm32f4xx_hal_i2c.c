/**
  ******************************************************************************
  * @file    stm32f4xx_hal_i2c.c
  * @author  MCD Application Team
  * @version V1.4.4
  * @date    22-January-2016
  * @brief   I2C HAL module driver.
  *          This file provides firmware functions to manage the following
  *          functionalities of the Inter Integrated Circuit (I2C) peripheral:
  *           + Initialization and de-initialization functions
  *           + IO operation functions
  *           + Peripheral Control functions
  *           + Peripheral State functions
  *
  @verbatim
  ==============================================================================
                        ##### How to use this driver #####
  ==============================================================================
  [..]
    The I2C HAL driver can be used as follows:

    (#) Declare a I2C_HandleTypeDef handle structure, for example:
        I2C_HandleTypeDef  hI2C;

    (#)Initialize the I2C low level resources by implement the HAL_I2C_MspInit() API:
        (##) Enable the I2Cx interface clock
        (##) I2C pins configuration
            (+++) Enable the clock for the I2C GPIOs
            (+++) Configure I2C pins as alternate function open-drain
        (##) NVIC configuration if you need to use interrupt process
            (+++) Configure the I2Cx interrupt priority
            (+++) Enable the NVIC I2C IRQ Channel
        (##) DMA Configuration if you need to use DMA process
            (+++) Declare a DMA_HandleTypeDef handle structure for the transmit or receive stream
            (+++) Enable the DMAx interface clock using
            (+++) Configure the DMA handle parameters
            (+++) Configure the DMA Tx or Rx Stream
            (+++) Associate the initialized DMA handle to the hI2C DMA Tx or Rx handle
            (+++) Configure the priority and enable the NVIC for the transfer complete interrupt on 
                  the DMA Tx or Rx Stream

    (#) Configure the Communication Speed, Duty cycle, Addressing mode, Own Address1,
        Dual Addressing mode, Own Address2, General call and Nostretch mode in the hI2C Init structure.

    (#) Initialize the I2C registers by calling the HAL_I2C_Init(), configures also the low level Hardware 
        (GPIO, CLOCK, NVIC...etc) by calling the customized HAL_I2C_MspInit(&hI2C) API.

    (#) To check if target device is ready for communication, use the function HAL_I2C_IsDeviceReady()

    (#) For I2C IO and IO MEM operations, three operation modes are available within this driver :

    *** Polling mode IO operation ***
    =================================
    [..]
      (+) Transmit in master mode an amount of data in blocking mode using HAL_I2C_Master_Transmit()
      (+) Receive in master mode an amount of data in blocking mode using HAL_I2C_Master_Receive()
      (+) Transmit in slave mode an amount of data in blocking mode using HAL_I2C_Slave_Transmit()
      (+) Receive in slave mode an amount of data in blocking mode using HAL_I2C_Slave_Receive()

    *** Polling mode IO MEM operation ***
    =====================================
    [..]
      (+) Write an amount of data in blocking mode to a specific memory address using HAL_I2C_Mem_Write()
      (+) Read an amount of data in blocking mode from a specific memory address using HAL_I2C_Mem_Read()


    *** Interrupt mode IO operation ***
    ===================================
    [..]
      (+) Transmit in master mode an amount of data in non blocking mode using HAL_I2C_Master_Transmit_IT()
      (+) At transmission end of transfer HAL_I2C_MasterTxCpltCallback is executed and user can
           add his own code by customization of function pointer HAL_I2C_MasterTxCpltCallback
      (+) Receive in master mode an amount of data in non blocking mode using HAL_I2C_Master_Receive_IT()
      (+) At reception end of transfer HAL_I2C_MasterRxCpltCallback is executed and user can
           add his own code by customization of function pointer HAL_I2C_MasterRxCpltCallback
      (+) Transmit in slave mode an amount of data in non blocking mode using HAL_I2C_Slave_Transmit_IT()
      (+) At transmission end of transfer HAL_I2C_SlaveTxCpltCallback is executed and user can
           add his own code by customization of function pointer HAL_I2C_SlaveTxCpltCallback
      (+) Receive in slave mode an amount of data in non blocking mode using HAL_I2C_Slave_Receive_IT()
      (+) At reception end of transfer HAL_I2C_SlaveRxCpltCallback is executed and user can
           add his own code by customization of function pointer HAL_I2C_SlaveRxCpltCallback
      (+) In case of transfer Error, HAL_I2C_ErrorCallback() function is executed and user can
           add his own code by customization of function pointer HAL_I2C_ErrorCallback

    *** Interrupt mode IO sequential operation ***
    ==============================================
    [..]
      (+@) These interfaces allow to manage a sequential transfer with a repeated start condition
          when a direction change during transfer
      (+) A specific option manage the different steps of a sequential transfer
      (+) Differents steps option I2C_XferOptions_definition are listed below :
      (++) I2C_FIRST_AND_LAST_FRAME: No sequential usage, functionnal is same as associated interfaces in no sequential mode 
      (++) I2C_FIRST_FRAME: Sequential usage, this option allow to manage a start condition with data to transfer without a final stop condition 
      (++) I2C_NEXT_FRAME: Sequential usage, this option allow to manage a restart condition with new data to transfer if the direction change or 
             manage only the new data to transfer if no direction change and without a final stop condition in both cases
      (++) I2C_LAST_FRAME: Sequential usage, this option allow to manage a restart condition with new data to transfer if the direction change or 
             manage only the new data to transfer if no direction change and with a final stop condition in both cases

      (+) Sequential transmit in master I2C mode an amount of data in non-blocking mode using HAL_I2C_Master_Sequential_Transmit_IT()
      (++) At transmission end of current frame transfer, HAL_I2C_MasterTxCpltCallback() is executed and user can
           add his own code by customization of function pointer HAL_I2C_MasterTxCpltCallback()
      (+) Sequential receive in master I2C mode an amount of data in non-blocking mode using HAL_I2C_Master_Sequential_Receive_IT()
      (++) At reception end of current frame transfer, HAL_I2C_MasterRxCpltCallback() is executed and user can
           add his own code by customization of function pointer HAL_I2C_MasterRxCpltCallback()
      (+) Abort a master I2C process communication with Interrupt using HAL_I2C_Master_Abort_IT()
      (++) The associated previous transfer callback is called at the end of abort process
      (++) mean HAL_I2C_MasterTxCpltCallback() in case of previous state was master transmit
      (++) mean HAL_I2c_MasterRxCpltCallback() in case of previous state was master receive
      (+) Enable/disable the Address listen mode in slave I2C mode
           using HAL_I2C_EnableListen_IT() HAL_I2C_DisableListen_IT()
      (++) When address slave I2C match, HAL_I2C_AddrCallback() is executed and user can
           add his own code to check the Address Match Code and the transmission direction request by master (Write/Read).
      (++) At Listen mode end HAL_I2C_ListenCpltCallback() is executed and user can
           add his own code by customization of function pointer HAL_I2C_ListenCpltCallback()
      (+) Sequential transmit in slave I2C mode an amount of data in non-blocking mode using HAL_I2C_Slave_Sequential_Transmit_IT()
      (++) At transmission end of current frame transfer, HAL_I2C_SlaveTxCpltCallback() is executed and user can
           add his own code by customization of function pointer HAL_I2C_SlaveTxCpltCallback()
      (+) Sequential receive in slave I2C mode an amount of data in non-blocking mode using HAL_I2C_Slave_Sequential_Receive_IT()
      (++) At reception end of current frame transfer, HAL_I2C_SlaveRxCpltCallback() is executed and user can
           add his own code by customization of function pointer HAL_I2C_SlaveRxCpltCallback()


    *** Interrupt mode IO MEM operation ***
    =======================================
    [..]
      (+) Write an amount of data in no-blocking mode with Interrupt to a specific memory address using
          HAL_I2C_Mem_Write_IT()
      (+) At MEM end of write transfer HAL_I2C_MemTxCpltCallback is executed and user can
           add his own code by customization of function pointer HAL_I2C_MemTxCpltCallback
      (+) Read an amount of data in no-blocking mode with Interrupt from a specific memory address using
          HAL_I2C_Mem_Read_IT()
      (+) At MEM end of read transfer HAL_I2C_MemRxCpltCallback is executed and user can
           add his own code by customization of function pointer HAL_I2C_MemRxCpltCallback
      (+) In case of transfer Error, HAL_I2C_ErrorCallback() function is executed and user can
           add his own code by customization of function pointer HAL_I2C_ErrorCallback

    *** DMA mode IO operation ***
    ==============================
    [..]
      (+) Transmit in master mode an amount of data in non blocking mode (DMA) using
          HAL_I2C_Master_Transmit_DMA()
      (+) At transmission end of transfer HAL_I2C_MasterTxCpltCallback is executed and user can
           add his own code by customization of function pointer HAL_I2C_MasterTxCpltCallback
      (+) Receive in master mode an amount of data in non blocking mode (DMA) using
          HAL_I2C_Master_Receive_DMA()
      (+) At reception end of transfer HAL_I2C_MasterRxCpltCallback is executed and user can
           add his own code by customization of function pointer HAL_I2C_MasterRxCpltCallback
      (+) Transmit in slave mode an amount of data in non blocking mode (DMA) using
          HAL_I2C_Slave_Transmit_DMA()
      (+) At transmission end of transfer HAL_I2C_SlaveTxCpltCallback is executed and user can
           add his own code by customization of function pointer HAL_I2C_SlaveTxCpltCallback
      (+) Receive in slave mode an amount of data in non blocking mode (DMA) using
          HAL_I2C_Slave_Receive_DMA()
      (+) At reception end of transfer HAL_I2C_SlaveRxCpltCallback is executed and user can
           add his own code by customization of function pointer HAL_I2C_SlaveRxCpltCallback
      (+) In case of transfer Error, HAL_I2C_ErrorCallback() function is executed and user can
           add his own code by customization of function pointer HAL_I2C_ErrorCallback

    *** DMA mode IO MEM operation ***
    =================================
    [..]
      (+) Write an amount of data in no-blocking mode with DMA to a specific memory address using
          HAL_I2C_Mem_Write_DMA()
      (+) At MEM end of write transfer HAL_I2C_MemTxCpltCallback is executed and user can
           add his own code by customization of function pointer HAL_I2C_MemTxCpltCallback
      (+) Read an amount of data in no-blocking mode with DMA from a specific memory address using
          HAL_I2C_Mem_Read_DMA()
      (+) At MEM end of read transfer HAL_I2C_MemRxCpltCallback is executed and user can
           add his own code by customization of function pointer HAL_I2C_MemRxCpltCallback
      (+) In case of transfer Error, HAL_I2C_ErrorCallback() function is executed and user can
           add his own code by customization of function pointer HAL_I2C_ErrorCallback


     *** I2C HAL driver macros list ***
     ==================================
     [..]
       Below the list of most used macros in I2C HAL driver.

      (+) __HAL_I2C_ENABLE: Enable the I2C peripheral
      (+) __HAL_I2C_DISABLE: Disable the I2C peripheral
      (+) __HAL_I2C_GET_FLAG: Checks whether the specified I2C flag is set or not
      (+) __HAL_I2C_CLEAR_FLAG: Clear the specified I2C pending flag
      (+) __HAL_I2C_ENABLE_IT: Enable the specified I2C interrupt
      (+) __HAL_I2C_DISABLE_IT: Disable the specified I2C interrupt

     [..]
       (@) You can refer to the I2C HAL driver header file for more useful macros


  @endverbatim
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2016 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/** @addtogroup STM32F4xx_HAL_Driver
  * @{
  */

/** @defgroup I2C I2C
  * @brief I2C HAL module driver
  * @{
  */

#ifdef HAL_I2C_MODULE_ENABLED

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/** @addtogroup I2C_Private_Constants
  * @{
  */    
#define I2C_TIMEOUT_FLAG          ((uint32_t)35U)         /*!< Timeout 35 ms             */
#define I2C_TIMEOUT_ADDR_SLAVE    ((uint32_t)10000U)      /*!< Timeout 10 s              */
#define I2C_TIMEOUT_BUSY_FLAG     ((uint32_t)10000U)      /*!< Timeout 10 s              */
#define I2C_NO_OPTION_FRAME       ((uint32_t)0xFFFF0000U) /*!< XferOptions default value */

/* Private define for @ref PreviousState usage */
#define I2C_STATE_MSK             ((uint32_t)((HAL_I2C_STATE_BUSY_TX | HAL_I2C_STATE_BUSY_RX) & (~(uint32_t)HAL_I2C_STATE_READY))) /*!< Mask State define, keep only RX and TX bits            */
#define I2C_STATE_NONE            ((uint32_t)(HAL_I2C_MODE_NONE))                                                        /*!< Default Value                                          */
#define I2C_STATE_MASTER_BUSY_TX  ((uint32_t)((HAL_I2C_STATE_BUSY_TX & I2C_STATE_MSK) | HAL_I2C_MODE_MASTER))            /*!< Master Busy TX, combinaison of State LSB and Mode enum */
#define I2C_STATE_MASTER_BUSY_RX  ((uint32_t)((HAL_I2C_STATE_BUSY_RX & I2C_STATE_MSK) | HAL_I2C_MODE_MASTER))            /*!< Master Busy RX, combinaison of State LSB and Mode enum */
#define I2C_STATE_SLAVE_BUSY_TX   ((uint32_t)((HAL_I2C_STATE_BUSY_TX & I2C_STATE_MSK) | HAL_I2C_MODE_SLAVE))             /*!< Slave Busy TX, combinaison of State LSB and Mode enum  */
#define I2C_STATE_SLAVE_BUSY_RX   ((uint32_t)((HAL_I2C_STATE_BUSY_RX & I2C_STATE_MSK) | HAL_I2C_MODE_SLAVE))             /*!< Slave Busy RX, combinaison of State LSB and Mode enum  */

/**
  * @}
  */

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/** @addtogroup I2C_Private_Functions
  * @{
  */
static void I2C_DMAMasterTransmitCplt(DMA_HandleTypeDef *hdma);
static void I2C_DMAMasterReceiveCplt(DMA_HandleTypeDef *hdma);
static void I2C_DMASlaveTransmitCplt(DMA_HandleTypeDef *hdma);
static void I2C_DMASlaveReceiveCplt(DMA_HandleTypeDef *hdma);
static void I2C_DMAMemTransmitCplt(DMA_HandleTypeDef *hdma);
static void I2C_DMAMemReceiveCplt(DMA_HandleTypeDef *hdma);
static void I2C_DMAError(DMA_HandleTypeDef *hdma);

static HAL_StatusTypeDef I2C_MasterRequestWrite(I2C_HandleTypeDef *hI2C, uint16_t DevAddress, uint32_t Timeout);
static HAL_StatusTypeDef I2C_MasterRequestRead(I2C_HandleTypeDef *hI2C, uint16_t DevAddress, uint32_t Timeout);
static HAL_StatusTypeDef I2C_RequestMemoryWrite(I2C_HandleTypeDef *hI2C, uint16_t DevAddress, uint16_t MemAddress, uint16_t MemAddSize, uint32_t Timeout);
static HAL_StatusTypeDef I2C_RequestMemoryRead(I2C_HandleTypeDef *hI2C, uint16_t DevAddress, uint16_t MemAddress, uint16_t MemAddSize, uint32_t Timeout);
static HAL_StatusTypeDef I2C_WaitOnFlagUntilTimeout(I2C_HandleTypeDef *hI2C, uint32_t Flag, FlagStatus Status, uint32_t Timeout);
static HAL_StatusTypeDef I2C_WaitOnMasterAddressFlagUntilTimeout(I2C_HandleTypeDef *hI2C, uint32_t Flag, uint32_t Timeout);
static HAL_StatusTypeDef I2C_WaitOnTXEFlagUntilTimeout(I2C_HandleTypeDef *hI2C, uint32_t Timeout);
static HAL_StatusTypeDef I2C_WaitOnBTFFlagUntilTimeout(I2C_HandleTypeDef *hI2C, uint32_t Timeout);
static HAL_StatusTypeDef I2C_WaitOnRXNEFlagUntilTimeout(I2C_HandleTypeDef *hI2C, uint32_t Timeout);
static HAL_StatusTypeDef I2C_WaitOnSTOPFlagUntilTimeout(I2C_HandleTypeDef *hI2C, uint32_t Timeout);
static HAL_StatusTypeDef I2C_IsAcknowledgeFailed(I2C_HandleTypeDef *hI2C);

static HAL_StatusTypeDef I2C_MasterTransmit_TXE(I2C_HandleTypeDef *hI2C);
static HAL_StatusTypeDef I2C_MasterTransmit_BTF(I2C_HandleTypeDef *hI2C);
static HAL_StatusTypeDef I2C_MasterReceive_RXNE(I2C_HandleTypeDef *hI2C);
static HAL_StatusTypeDef I2C_MasterReceive_BTF(I2C_HandleTypeDef *hI2C);

static HAL_StatusTypeDef I2C_SlaveTransmit_TXE(I2C_HandleTypeDef *hI2C);
static HAL_StatusTypeDef I2C_SlaveTransmit_BTF(I2C_HandleTypeDef *hI2C);
static HAL_StatusTypeDef I2C_SlaveReceive_RXNE(I2C_HandleTypeDef *hI2C);
static HAL_StatusTypeDef I2C_SlaveReceive_BTF(I2C_HandleTypeDef *hI2C);
static HAL_StatusTypeDef I2C_Slave_ADDR(I2C_HandleTypeDef *hI2C);
static HAL_StatusTypeDef I2C_Slave_STOPF(I2C_HandleTypeDef *hI2C);
static HAL_StatusTypeDef I2C_Slave_AF(I2C_HandleTypeDef *hI2C);
/**
  * @}
  */

/* Exported functions --------------------------------------------------------*/
/** @defgroup I2C_Exported_Functions I2C Exported Functions
  * @{
  */

/** @defgroup I2C_Exported_Functions_Group1 Initialization and de-initialization functions
 *  @brief    Initialization and Configuration functions
 *
@verbatim
 ===============================================================================
              ##### Initialization and de-initialization functions #####
 ===============================================================================
    [..]  This subsection provides a set of functions allowing to initialize and
          de-initialize the I2Cx peripheral:

      (+) User must Implement HAL_I2C_MspInit() function in which he configures
          all related peripherals resources (CLOCK, GPIO, DMA, IT and NVIC).

      (+) Call the function HAL_I2C_Init() to configure the selected device with
          the selected configuration:
        (++) Communication Speed
        (++) Duty cycle
        (++) Addressing mode
        (++) Own Address 1
        (++) Dual Addressing mode
        (++) Own Address 2
        (++) General call mode
        (++) Nostretch mode

      (+) Call the function HAL_I2C_DeInit() to restore the default configuration
          of the selected I2Cx peripheral.

@endverbatim
  * @{
  */

/**
  * @brief  Initializes the I2C according to the specified parameters
  *         in the I2C_InitTypeDef and create the associated handle.
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @retval HAL status
  */
HAL_StatusTypeDef HAL_I2C_Init(I2C_HandleTypeDef *hI2C)
{
  uint32_t freqrange = 0U;
  uint32_t pclk1 = 0U;

  /* Check the I2C handle allocation */
  if (hI2C == NULL)
  {
    return HAL_ERROR;
  }

  /* Check the parameters */
  assert_param(IS_I2C_ALL_INSTANCE(hI2C->Instance));
  assert_param(IS_I2C_CLOCK_SPEED(hI2C->Init.ClockSpeed));
  assert_param(IS_I2C_DUTY_CYCLE(hI2C->Init.DutyCycle));
  assert_param(IS_I2C_OWN_ADDRESS1(hI2C->Init.OwnAddress1));
  assert_param(IS_I2C_ADDRESSING_MODE(hI2C->Init.AddressingMode));
  assert_param(IS_I2C_DUAL_ADDRESS(hI2C->Init.DualAddressMode));
  assert_param(IS_I2C_OWN_ADDRESS2(hI2C->Init.OwnAddress2));
  assert_param(IS_I2C_GENERAL_CALL(hI2C->Init.GeneralCallMode));
  assert_param(IS_I2C_NO_STRETCH(hI2C->Init.NoStretchMode));

  if (hI2C->State == HAL_I2C_STATE_RESET)
  {
    /* Allocate lock resource and initialize it */
    hI2C->Lock = HAL_UNLOCKED;
      
    /* Init the low level hardware : GPIO, CLOCK, NVIC */
    HAL_I2C_MspInit(hI2C);
  }

  hI2C->State = HAL_I2C_STATE_BUSY;

  /* Disable the selected I2C peripheral */
  __HAL_I2C_DISABLE(hI2C);

  /* Get PCLK1 frequency */
  pclk1 = HAL_RCC_GetPCLK1Freq();

  /* Calculate frequency range */
  freqrange = I2C_FREQRANGE(pclk1);

  /*---------------------------- I2Cx CR2 Configuration ----------------------*/
  /* Configure I2Cx: Frequency range */
  hI2C->Instance->CR2 = freqrange;

  /*---------------------------- I2Cx TRISE Configuration --------------------*/
  /* Configure I2Cx: Rise Time */
  hI2C->Instance->TRISE = I2C_RISE_TIME(freqrange, hI2C->Init.ClockSpeed);

  /*---------------------------- I2Cx CCR Configuration ----------------------*/
  /* Configure I2Cx: Speed */
  hI2C->Instance->CCR = I2C_SPEED(pclk1, hI2C->Init.ClockSpeed, hI2C->Init.DutyCycle);

  /*---------------------------- I2Cx CR1 Configuration ----------------------*/
  /* Configure I2Cx: Generalcall and NoStretch mode */
  hI2C->Instance->CR1 = (hI2C->Init.GeneralCallMode | hI2C->Init.NoStretchMode);

  /*---------------------------- I2Cx OAR1 Configuration ---------------------*/
  /* Configure I2Cx: Own Address1 and addressing mode */
  hI2C->Instance->OAR1 = (hI2C->Init.AddressingMode | hI2C->Init.OwnAddress1);

  /*---------------------------- I2Cx OAR2 Configuration ---------------------*/
  /* Configure I2Cx: Dual mode and Own Address2 */
  hI2C->Instance->OAR2 = (hI2C->Init.DualAddressMode | hI2C->Init.OwnAddress2);

  /* Enable the selected I2C peripheral */
  __HAL_I2C_ENABLE(hI2C);

  hI2C->ErrorCode = HAL_I2C_ERROR_NONE;
  hI2C->State = HAL_I2C_STATE_READY;
  hI2C->PreviousState = I2C_STATE_NONE;
  hI2C->Mode = HAL_I2C_MODE_NONE;

  return HAL_OK;
}

/**
  * @brief  DeInitializes the I2C peripheral.
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @retval HAL status
  */
HAL_StatusTypeDef HAL_I2C_DeInit(I2C_HandleTypeDef *hI2C)
{
  /* Check the I2C handle allocation */
  if (hI2C == NULL)
  {
    return HAL_ERROR;
  }

  /* Check the parameters */
  assert_param(IS_I2C_ALL_INSTANCE(hI2C->Instance));

  hI2C->State = HAL_I2C_STATE_BUSY;

  /* Disable the I2C Peripheral Clock */
  __HAL_I2C_DISABLE(hI2C);

  /* DeInit the low level hardware: GPIO, CLOCK, NVIC */
  HAL_I2C_MspDeInit(hI2C);

  hI2C->ErrorCode = HAL_I2C_ERROR_NONE;
  hI2C->State = HAL_I2C_STATE_RESET;
  hI2C->PreviousState = I2C_STATE_NONE;
  hI2C->Mode = HAL_I2C_MODE_NONE;
    
  /* Release Lock */
  __HAL_UNLOCK(hI2C);

  return HAL_OK;
}

/**
  * @brief I2C MSP Init.
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @retval None
  */
 __weak void HAL_I2C_MspInit(I2C_HandleTypeDef *hI2C)
{
  /* Prevent unused argument(s) compilation warning */
  UNUSED(hI2C);
  /* NOTE : This function Should not be modified, when the callback is needed,
            the HAL_I2C_MspInit could be implemented in the user file
   */
}

/**
  * @brief I2C MSP DeInit
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @retval None
  */
 __weak void HAL_I2C_MspDeInit(I2C_HandleTypeDef *hI2C)
{
  /* Prevent unused argument(s) compilation warning */
  UNUSED(hI2C);
  /* NOTE : This function Should not be modified, when the callback is needed,
            the HAL_I2C_MspDeInit could be implemented in the user file
   */
}

/**
  * @}
  */

/** @defgroup I2C_Exported_Functions_Group2 IO operation functions
 *  @brief   Data transfers functions
 *
@verbatim
 ===============================================================================
                      ##### IO operation functions #####
 ===============================================================================
    [..]
    This subsection provides a set of functions allowing to manage the I2C data
    transfers.

    (#) There are two modes of transfer:
       (++) Blocking mode : The communication is performed in the polling mode.
            The status of all data processing is returned by the same function
            after finishing transfer.
       (++) No-Blocking mode : The communication is performed using Interrupts
            or DMA. These functions return the status of the transfer startup.
            The end of the data processing will be indicated through the
            dedicated I2C IRQ when using Interrupt mode or the DMA IRQ when
            using DMA mode.

    (#) Blocking mode functions are :
        (++) HAL_I2C_Master_Transmit()
        (++) HAL_I2C_Master_Receive()
        (++) HAL_I2C_Slave_Transmit()
        (++) HAL_I2C_Slave_Receive()
        (++) HAL_I2C_Mem_Write()
        (++) HAL_I2C_Mem_Read()
        (++) HAL_I2C_IsDeviceReady()

    (#) No-Blocking mode functions with Interrupt are :
        (++) HAL_I2C_Master_Transmit_IT()
        (++) HAL_I2C_Master_Receive_IT()
        (++) HAL_I2C_Slave_Transmit_IT()
        (++) HAL_I2C_Slave_Receive_IT()
        (++) HAL_I2C_Mem_Write_IT()
        (++) HAL_I2C_Mem_Read_IT()

    (#) No-Blocking mode functions with DMA are :
        (++) HAL_I2C_Master_Transmit_DMA()
        (++) HAL_I2C_Master_Receive_DMA()
        (++) HAL_I2C_Slave_Transmit_DMA()
        (++) HAL_I2C_Slave_Receive_DMA()
        (++) HAL_I2C_Mem_Write_DMA()
        (++) HAL_I2C_Mem_Read_DMA()

    (#) A set of Transfer Complete Callbacks are provided in non Blocking mode:
        (++) HAL_I2C_MemTxCpltCallback()
        (++) HAL_I2C_MemRxCpltCallback()
        (++) HAL_I2C_MasterTxCpltCallback()
        (++) HAL_I2C_MasterRxCpltCallback()
        (++) HAL_I2C_SlaveTxCpltCallback()
        (++) HAL_I2C_SlaveRxCpltCallback()
        (++) HAL_I2C_ErrorCallback()

@endverbatim
  * @{
  */

/**
  * @brief  Transmits in master mode an amount of data in blocking mode.
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @param  DevAddress Target device address
  * @param  pData Pointer to data buffer
  * @param  Size Amount of data to be sent
  * @param  Timeout Timeout duration
  * @retval HAL status
  */
HAL_StatusTypeDef HAL_I2C_Master_Transmit(I2C_HandleTypeDef *hI2C, uint16_t DevAddress, uint8_t *pData, uint16_t Size, uint32_t Timeout)
{
  if (hI2C->State == HAL_I2C_STATE_READY)
  {
    /* Wait until BUSY flag is reset */
    if (I2C_WaitOnFlagUntilTimeout(hI2C, I2C_FLAG_BUSY, SET, I2C_TIMEOUT_BUSY_FLAG) != HAL_OK)
    {
      return HAL_BUSY;
    }

    /* Process Locked */
    __HAL_LOCK(hI2C);
    
    /* Disable Pos */
    hI2C->Instance->CR1 &= ~I2C_CR1_POS;

    hI2C->State = HAL_I2C_STATE_BUSY_TX;
    hI2C->Mode = HAL_I2C_MODE_MASTER;
    hI2C->ErrorCode = HAL_I2C_ERROR_NONE;
    hI2C->XferOptions = I2C_NO_OPTION_FRAME;
    
    /* Send Slave Address */
    if (I2C_MasterRequestWrite(hI2C, DevAddress, Timeout) != HAL_OK)
    {
      if (hI2C->ErrorCode == HAL_I2C_ERROR_AF)
      {
        /* Process Unlocked */
        __HAL_UNLOCK(hI2C);
        return HAL_ERROR;
      }
      else
      {
        /* Process Unlocked */
        __HAL_UNLOCK(hI2C);
        return HAL_TIMEOUT;
      }
    }

    /* Clear ADDR flag */
    __HAL_I2C_CLEAR_ADDRFLAG(hI2C);

    while(Size > 0U)
    {
      /* Wait until TXE flag is set */
      if (I2C_WaitOnTXEFlagUntilTimeout(hI2C, Timeout) != HAL_OK)
      {
        if (hI2C->ErrorCode == HAL_I2C_ERROR_AF)
        {
          /* Generate Stop */
          hI2C->Instance->CR1 |= I2C_CR1_STOP;
          return HAL_ERROR;
        }
        else
        {
          return HAL_TIMEOUT;
        }
      }

      /* Write data to DR */
      hI2C->Instance->DR = (*pData++);
      Size--;

      if ((__HAL_I2C_GET_FLAG(hI2C, I2C_FLAG_BTF) == SET) && (Size != 0U))
      {
        /* Write data to DR */
        hI2C->Instance->DR = (*pData++);
        Size--;
      }
      
      /* Wait until BTF flag is set */
      if (I2C_WaitOnBTFFlagUntilTimeout(hI2C, Timeout) != HAL_OK)
      {
        if (hI2C->ErrorCode == HAL_I2C_ERROR_AF)
        {
          /* Generate Stop */
          hI2C->Instance->CR1 |= I2C_CR1_STOP;
          return HAL_ERROR;
        }
        else
        {
          return HAL_TIMEOUT;
        }
      }
    }

    /* Generate Stop */
    hI2C->Instance->CR1 |= I2C_CR1_STOP;

    hI2C->State = HAL_I2C_STATE_READY;
    hI2C->Mode = HAL_I2C_MODE_NONE;
    
    /* Process Unlocked */
    __HAL_UNLOCK(hI2C);

    return HAL_OK;
  }
  else
  {
    return HAL_BUSY;
  }
}

/**
  * @brief  Receives in master mode an amount of data in blocking mode.
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @param  DevAddress Target device address
  * @param  pData Pointer to data buffer
  * @param  Size Amount of data to be sent
  * @param  Timeout Timeout duration
  * @retval HAL status
  */
HAL_StatusTypeDef HAL_I2C_Master_Receive(I2C_HandleTypeDef *hI2C, uint16_t DevAddress, uint8_t *pData, uint16_t Size, uint32_t Timeout)
{
  if (hI2C->State == HAL_I2C_STATE_READY)
  {
    /* Wait until BUSY flag is reset */
    if (I2C_WaitOnFlagUntilTimeout(hI2C, I2C_FLAG_BUSY, SET, I2C_TIMEOUT_BUSY_FLAG) != HAL_OK)
    {
      return HAL_BUSY;
    }
    
    /* Process Locked */
    __HAL_LOCK(hI2C);
    
    /* Disable Pos */
    hI2C->Instance->CR1 &= ~I2C_CR1_POS;

    hI2C->State = HAL_I2C_STATE_BUSY_RX;
    hI2C->Mode = HAL_I2C_MODE_MASTER;
    hI2C->ErrorCode = HAL_I2C_ERROR_NONE;
    hI2C->XferOptions = I2C_NO_OPTION_FRAME;
    
    /* Send Slave Address */
    if (I2C_MasterRequestRead(hI2C, DevAddress, Timeout) != HAL_OK)
    {
      if (hI2C->ErrorCode == HAL_I2C_ERROR_AF)
      {
        /* Process Unlocked */
        __HAL_UNLOCK(hI2C);
        return HAL_ERROR;
      }
      else
      {
        /* Process Unlocked */
        __HAL_UNLOCK(hI2C);
        return HAL_TIMEOUT;
      }
    }

    if (Size == 0U)
    {
      /* Clear ADDR flag */
      __HAL_I2C_CLEAR_ADDRFLAG(hI2C);
      
      /* Generate Stop */
      hI2C->Instance->CR1 |= I2C_CR1_STOP;
    }
    else if (Size == 1U)
    {
      /* Disable Acknowledge */
      hI2C->Instance->CR1 &= ~I2C_CR1_ACK;

      /* Clear ADDR flag */
      __HAL_I2C_CLEAR_ADDRFLAG(hI2C);

      /* Generate Stop */
      hI2C->Instance->CR1 |= I2C_CR1_STOP;
    }
    else if (Size == 2U)
    {
      /* Disable Acknowledge */
      hI2C->Instance->CR1 &= ~I2C_CR1_ACK;

      /* Enable Pos */
      hI2C->Instance->CR1 |= I2C_CR1_POS;

      /* Clear ADDR flag */
      __HAL_I2C_CLEAR_ADDRFLAG(hI2C);
    }
    else
    {
      /* Enable Acknowledge */
      hI2C->Instance->CR1 |= I2C_CR1_ACK;

      /* Clear ADDR flag */
      __HAL_I2C_CLEAR_ADDRFLAG(hI2C);
    }

    while(Size > 0U)
    {
      if (Size <= 3U)
      {
        /* One byte */
        if (Size == 1U)
        {
          /* Wait until RXNE flag is set */
          if (I2C_WaitOnRXNEFlagUntilTimeout(hI2C, Timeout) != HAL_OK)      
          {
            if (hI2C->ErrorCode == HAL_I2C_ERROR_TIMEOUT)
            {
              return HAL_TIMEOUT;
            }
            else
            {
              return HAL_ERROR;
            }
          }

          /* Read data from DR */
          (*pData++) = hI2C->Instance->DR;
          Size--;
        }
        /* Two bytes */
        else if (Size == 2U)
        {
          /* Wait until BTF flag is set */
          if (I2C_WaitOnFlagUntilTimeout(hI2C, I2C_FLAG_BTF, RESET, Timeout) != HAL_OK)
          {
            return HAL_TIMEOUT;
          }

          /* Generate Stop */
          hI2C->Instance->CR1 |= I2C_CR1_STOP;

          /* Read data from DR */
          (*pData++) = hI2C->Instance->DR;
          Size--;

          /* Read data from DR */
          (*pData++) = hI2C->Instance->DR;
          Size--;
        }
        /* 3 Last bytes */
        else
        {
          /* Wait until BTF flag is set */
          if (I2C_WaitOnFlagUntilTimeout(hI2C, I2C_FLAG_BTF, RESET, Timeout) != HAL_OK)
          {
            return HAL_TIMEOUT;
          }

          /* Disable Acknowledge */
          hI2C->Instance->CR1 &= ~I2C_CR1_ACK;

          /* Read data from DR */
          (*pData++) = hI2C->Instance->DR;
          Size--;

          /* Wait until BTF flag is set */
          if (I2C_WaitOnFlagUntilTimeout(hI2C, I2C_FLAG_BTF, RESET, Timeout) != HAL_OK)
          {
            return HAL_TIMEOUT;
          }

          /* Generate Stop */
          hI2C->Instance->CR1 |= I2C_CR1_STOP;

          /* Read data from DR */
          (*pData++) = hI2C->Instance->DR;
          Size--;

          /* Read data from DR */
          (*pData++) = hI2C->Instance->DR;
          Size--;
        }
      }
      else
      {
        /* Wait until RXNE flag is set */
        if (I2C_WaitOnRXNEFlagUntilTimeout(hI2C, Timeout) != HAL_OK)      
        {
          if (hI2C->ErrorCode == HAL_I2C_ERROR_TIMEOUT)
          {
            return HAL_TIMEOUT;
          }
          else
          {
            return HAL_ERROR;
          }
        }

        /* Read data from DR */
        (*pData++) = hI2C->Instance->DR;
        Size--;

        if (__HAL_I2C_GET_FLAG(hI2C, I2C_FLAG_BTF) == SET)
        {
          /* Read data from DR */
          (*pData++) = hI2C->Instance->DR;
          Size--;
        }
      }
    }

    hI2C->State = HAL_I2C_STATE_READY;
    hI2C->Mode = HAL_I2C_MODE_NONE;

    /* Process Unlocked */
    __HAL_UNLOCK(hI2C);

    return HAL_OK;
  }
  else
  {
    return HAL_BUSY;
  }
}

/**
  * @brief  Transmits in slave mode an amount of data in blocking mode.
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @param  pData Pointer to data buffer
  * @param  Size Amount of data to be sent
  * @param  Timeout Timeout duration
  * @retval HAL status
  */
HAL_StatusTypeDef HAL_I2C_Slave_Transmit(I2C_HandleTypeDef *hI2C, uint8_t *pData, uint16_t Size, uint32_t Timeout)
{
  if (hI2C->State == HAL_I2C_STATE_READY)
  {
    if ((pData == NULL) || (Size == 0U))
    {
      return  HAL_ERROR;
    }

    /* Wait until BUSY flag is reset */
    if (I2C_WaitOnFlagUntilTimeout(hI2C, I2C_FLAG_BUSY, SET, I2C_TIMEOUT_BUSY_FLAG) != HAL_OK)
    {
      return HAL_BUSY;
    }

    /* Process Locked */
    __HAL_LOCK(hI2C);
    
    /* Disable Pos */
    hI2C->Instance->CR1 &= ~I2C_CR1_POS;

    hI2C->State = HAL_I2C_STATE_BUSY_TX;
    hI2C->Mode = HAL_I2C_MODE_SLAVE;
    hI2C->ErrorCode = HAL_I2C_ERROR_NONE;
    hI2C->XferOptions = I2C_NO_OPTION_FRAME;
    
    /* Enable Address Acknowledge */
    hI2C->Instance->CR1 |= I2C_CR1_ACK;

    /* Wait until ADDR flag is set */
    if (I2C_WaitOnFlagUntilTimeout(hI2C, I2C_FLAG_ADDR, RESET, Timeout) != HAL_OK)
    {
      return HAL_TIMEOUT;
    }

    /* Clear ADDR flag */
    __HAL_I2C_CLEAR_ADDRFLAG(hI2C);

    /* If 10bit addressing mode is selected */
    if (hI2C->Init.AddressingMode == I2C_ADDRESSINGMODE_10BIT)
    {
      /* Wait until ADDR flag is set */
      if (I2C_WaitOnFlagUntilTimeout(hI2C, I2C_FLAG_ADDR, RESET, Timeout) != HAL_OK)
      {
        return HAL_TIMEOUT;
      }

      /* Clear ADDR flag */
      __HAL_I2C_CLEAR_ADDRFLAG(hI2C);
    }

    while(Size > 0U)
    {
      /* Wait until TXE flag is set */
      if (I2C_WaitOnTXEFlagUntilTimeout(hI2C, Timeout) != HAL_OK)
      {
        /* Disable Address Acknowledge */
        hI2C->Instance->CR1 &= ~I2C_CR1_ACK;
        if (hI2C->ErrorCode == HAL_I2C_ERROR_AF)
        {
          return HAL_ERROR;
        }
        else
        {
          return HAL_TIMEOUT;
        }
      }

      /* Write data to DR */
      hI2C->Instance->DR = (*pData++);
      Size--;

      if ((__HAL_I2C_GET_FLAG(hI2C, I2C_FLAG_BTF) == SET) && (Size != 0U))
      {
        /* Write data to DR */
        hI2C->Instance->DR = (*pData++);
        Size--;
      }
    }

    /* Wait until AF flag is set */
    if (I2C_WaitOnFlagUntilTimeout(hI2C, I2C_FLAG_AF, RESET, Timeout) != HAL_OK)
    {
      return HAL_TIMEOUT;
    }

    /* Clear AF flag */
    __HAL_I2C_CLEAR_FLAG(hI2C, I2C_FLAG_AF);

    /* Disable Address Acknowledge */
    hI2C->Instance->CR1 &= ~I2C_CR1_ACK;

    hI2C->State = HAL_I2C_STATE_READY;
    hI2C->Mode = HAL_I2C_MODE_NONE;
    
    /* Process Unlocked */
    __HAL_UNLOCK(hI2C);

    return HAL_OK;
  }
  else
  {
    return HAL_BUSY;
  }
}

/**
  * @brief  Receive in slave mode an amount of data in blocking mode
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @param  pData Pointer to data buffer
  * @param  Size Amount of data to be sent
  * @param  Timeout Timeout duration
  * @retval HAL status
  */
HAL_StatusTypeDef HAL_I2C_Slave_Receive(I2C_HandleTypeDef *hI2C, uint8_t *pData, uint16_t Size, uint32_t Timeout)
{
  if (hI2C->State == HAL_I2C_STATE_READY)
  {
    if ((pData == NULL) || (Size == 0U))
    {
      return  HAL_ERROR;
    }

    /* Wait until BUSY flag is reset */
    if (I2C_WaitOnFlagUntilTimeout(hI2C, I2C_FLAG_BUSY, SET, I2C_TIMEOUT_BUSY_FLAG) != HAL_OK)
    {
      return HAL_BUSY;
    }

    /* Process Locked */
    __HAL_LOCK(hI2C);
    
    /* Disable Pos */
    hI2C->Instance->CR1 &= ~I2C_CR1_POS;

    hI2C->State = HAL_I2C_STATE_BUSY_RX;
    hI2C->Mode = HAL_I2C_MODE_SLAVE;
    hI2C->ErrorCode = HAL_I2C_ERROR_NONE;
    hI2C->XferOptions = I2C_NO_OPTION_FRAME;
    
    /* Enable Address Acknowledge */
    hI2C->Instance->CR1 |= I2C_CR1_ACK;

    /* Wait until ADDR flag is set */
    if (I2C_WaitOnFlagUntilTimeout(hI2C, I2C_FLAG_ADDR, RESET, Timeout) != HAL_OK)
    {
      return HAL_TIMEOUT;
    }

    /* Clear ADDR flag */
    __HAL_I2C_CLEAR_ADDRFLAG(hI2C);

    while(Size > 0U)
    {
      /* Wait until RXNE flag is set */
      if (I2C_WaitOnRXNEFlagUntilTimeout(hI2C, Timeout) != HAL_OK)      
      {
        /* Disable Address Acknowledge */
        hI2C->Instance->CR1 &= ~I2C_CR1_ACK;
        if (hI2C->ErrorCode == HAL_I2C_ERROR_TIMEOUT)
        {
          return HAL_TIMEOUT;
        }
        else
        {
          return HAL_ERROR;
        }
      }

      /* Read data from DR */
      (*pData++) = hI2C->Instance->DR;
      Size--;

      if ((__HAL_I2C_GET_FLAG(hI2C, I2C_FLAG_BTF) == SET) && (Size != 0U))
      {
        /* Read data from DR */
        (*pData++) = hI2C->Instance->DR;
        Size--;
      }
    }

    /* Wait until STOP flag is set */
    if (I2C_WaitOnSTOPFlagUntilTimeout(hI2C, I2C_TIMEOUT_FLAG) != HAL_OK)
    {
      /* Disable Address Acknowledge */
      hI2C->Instance->CR1 &= ~I2C_CR1_ACK;

      if (hI2C->ErrorCode == HAL_I2C_ERROR_AF)
      {
        return HAL_ERROR;
      }
      else
      {
        return HAL_TIMEOUT;
      }
    }

    /* Clear STOP flag */
    __HAL_I2C_CLEAR_STOPFLAG(hI2C);

    /* Disable Address Acknowledge */
    hI2C->Instance->CR1 &= ~I2C_CR1_ACK;

    hI2C->State = HAL_I2C_STATE_READY;
    hI2C->Mode = HAL_I2C_MODE_NONE;
    
    /* Process Unlocked */
    __HAL_UNLOCK(hI2C);

    return HAL_OK;
  }
  else
  {
    return HAL_BUSY;
  }
}

/**
  * @brief  Transmit in master mode an amount of data in no-blocking mode with Interrupt
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @param  DevAddress Target device address
  * @param  pData Pointer to data buffer
  * @param  Size Amount of data to be sent
  * @retval HAL status
  */
HAL_StatusTypeDef HAL_I2C_Master_Transmit_IT(I2C_HandleTypeDef *hI2C, uint16_t DevAddress, uint8_t *pData, uint16_t Size)
{
  if (hI2C->State == HAL_I2C_STATE_READY)
  {
    /* Wait until BUSY flag is reset */
    if (I2C_WaitOnFlagUntilTimeout(hI2C, I2C_FLAG_BUSY, SET, I2C_TIMEOUT_BUSY_FLAG) != HAL_OK)
    {
      return HAL_BUSY;
    }

    /* Process Locked */
    __HAL_LOCK(hI2C);
    
    /* Disable Pos */
    hI2C->Instance->CR1 &= ~I2C_CR1_POS;

    hI2C->State = HAL_I2C_STATE_BUSY_TX;
    hI2C->Mode = HAL_I2C_MODE_MASTER;
    hI2C->ErrorCode = HAL_I2C_ERROR_NONE;

    hI2C->pBuffPtr = pData;
    hI2C->XferSize = Size;
    hI2C->XferCount = Size;
    hI2C->XferOptions = I2C_NO_OPTION_FRAME;

    /* Send Slave Address */
    if (I2C_MasterRequestWrite(hI2C, DevAddress, I2C_TIMEOUT_FLAG) != HAL_OK)
    {
      if (hI2C->ErrorCode == HAL_I2C_ERROR_AF)
      {
        /* Process Unlocked */
        __HAL_UNLOCK(hI2C);
        return HAL_ERROR;
      }
      else
      {
        /* Process Unlocked */
        __HAL_UNLOCK(hI2C);
        return HAL_TIMEOUT;
      }
    }

    /* Clear ADDR flag */
    __HAL_I2C_CLEAR_ADDRFLAG(hI2C);

    if (hI2C->XferSize > 0U)
    {
    /* Process Unlocked */
    __HAL_UNLOCK(hI2C);

    /* Note : The I2C interrupts must be enabled after unlocking current process
              to avoid the risk of I2C interrupt handle execution before current
              process unlock */
    /* Enable EVT, BUF and ERR interrupt */
    __HAL_I2C_ENABLE_IT(hI2C, I2C_IT_EVT | I2C_IT_BUF | I2C_IT_ERR);
    }
    else
    {
      /* Wait until TXE flag is set */
      if (I2C_WaitOnFlagUntilTimeout(hI2C, I2C_FLAG_TXE, RESET, I2C_TIMEOUT_FLAG) != HAL_OK)
      {
        return HAL_TIMEOUT;
      }

      /* Generate Stop */
      hI2C->Instance->CR1 |= I2C_CR1_STOP;

      hI2C->State = HAL_I2C_STATE_READY;
      
      /* Process Unlocked */
      __HAL_UNLOCK(hI2C);
    }

    return HAL_OK;
  }
  else
  {
    return HAL_BUSY;
  }
}

/**
  * @brief  Receive in master mode an amount of data in no-blocking mode with Interrupt
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @param  DevAddress Target device address
  * @param  pData Pointer to data buffer
  * @param  Size Amount of data to be sent
  * @retval HAL status
  */
HAL_StatusTypeDef HAL_I2C_Master_Receive_IT(I2C_HandleTypeDef *hI2C, uint16_t DevAddress, uint8_t *pData, uint16_t Size)
{
  if (hI2C->State == HAL_I2C_STATE_READY)
  {
    /* Wait until BUSY flag is reset */
    if (I2C_WaitOnFlagUntilTimeout(hI2C, I2C_FLAG_BUSY, SET, I2C_TIMEOUT_BUSY_FLAG) != HAL_OK)
    {
      return HAL_BUSY;
    }

    /* Process Locked */
    __HAL_LOCK(hI2C);
    
    /* Disable Pos */
    hI2C->Instance->CR1 &= ~I2C_CR1_POS;

    hI2C->State = HAL_I2C_STATE_BUSY_RX;
    hI2C->Mode = HAL_I2C_MODE_MASTER;
    hI2C->ErrorCode = HAL_I2C_ERROR_NONE;

    hI2C->pBuffPtr = pData;
    hI2C->XferSize = Size;
    hI2C->XferCount = Size;
    hI2C->XferOptions = I2C_NO_OPTION_FRAME;

    /* Send Slave Address */
    if (I2C_MasterRequestRead(hI2C, DevAddress, I2C_TIMEOUT_FLAG) != HAL_OK)
    {
      if (hI2C->ErrorCode == HAL_I2C_ERROR_AF)
      {
        /* Process Unlocked */
        __HAL_UNLOCK(hI2C);
        return HAL_ERROR;
      }
      else
      {
        /* Process Unlocked */
        __HAL_UNLOCK(hI2C);
        return HAL_TIMEOUT;
      }
    }

    if (hI2C->XferCount == 0U)
    {
      /* Clear ADDR flag */
      __HAL_I2C_CLEAR_ADDRFLAG(hI2C);
      
      /* Generate Stop */
      hI2C->Instance->CR1 |= I2C_CR1_STOP;
    }
    else if (hI2C->XferCount == 1U)   
    {
      /* Disable Acknowledge */
      hI2C->Instance->CR1 &= ~I2C_CR1_ACK;
      
      /* Clear ADDR flag */
      __HAL_I2C_CLEAR_ADDRFLAG(hI2C);
      
      /* Generate Stop */
      hI2C->Instance->CR1 |= I2C_CR1_STOP;
    }
    else if (hI2C->XferCount == 2U)
    {
      /* Disable Acknowledge */
      hI2C->Instance->CR1 &= ~I2C_CR1_ACK;

      /* Enable Pos */
      hI2C->Instance->CR1 |= I2C_CR1_POS;

      /* Clear ADDR flag */
      __HAL_I2C_CLEAR_ADDRFLAG(hI2C);
    }
    else
    {
      /* Enable Acknowledge */
      hI2C->Instance->CR1 |= I2C_CR1_ACK;

      /* Clear ADDR flag */
      __HAL_I2C_CLEAR_ADDRFLAG(hI2C);
    }
        
    if (hI2C->XferCount == 0U)
    {
      hI2C->State = HAL_I2C_STATE_READY;
    }

    /* Process Unlocked */
    __HAL_UNLOCK(hI2C);

    if (hI2C->XferCount > 0U)
    {
    /* Note : The I2C interrupts must be enabled after unlocking current process
              to avoid the risk of I2C interrupt handle execution before current
              process unlock */

    /* Enable EVT, BUF and ERR interrupt */
    __HAL_I2C_ENABLE_IT(hI2C, I2C_IT_EVT | I2C_IT_BUF | I2C_IT_ERR);
    }
    return HAL_OK;
  }
  else
  {
    return HAL_BUSY;
  }
}

/**
  * @brief  Sequential transmit in master mode an amount of data in no-blocking mode with Interrupt
  * @note   This interface allow to manage repeated start condition when a direction change during transfer
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @param  DevAddress Target device address
  * @param  pData Pointer to data buffer
  * @param  Size Amount of data to be sent
  * @param  XferOptions Options of Transfer, value of @ref I2C_XferOptions_definition
  * @retval HAL status
  */
HAL_StatusTypeDef HAL_I2C_Master_Sequential_Transmit_IT(I2C_HandleTypeDef *hI2C, uint16_t DevAddress, uint8_t *pData, uint16_t Size, uint32_t XferOptions)
{
  /* Check the parameters */
  assert_param(IS_I2C_TRANSFER_OPTIONS_REQUEST(XferOptions));

  if (hI2C->State == HAL_I2C_STATE_READY)
  {
    /* Check Busy Flag only if FIRST call of Master interface */
    if ((XferOptions == I2C_FIRST_AND_LAST_FRAME) || (XferOptions == I2C_FIRST_FRAME))
    {
      /* Wait until BUSY flag is reset */
      if (I2C_WaitOnFlagUntilTimeout(hI2C, I2C_FLAG_BUSY, SET, I2C_TIMEOUT_BUSY_FLAG) != HAL_OK)
      {
        return HAL_BUSY;
      }
    }

    /* Process Locked */
    __HAL_LOCK(hI2C);

    /* Disable Pos */
    hI2C->Instance->CR1 &= ~I2C_CR1_POS;

    hI2C->State = HAL_I2C_STATE_BUSY_TX;
    hI2C->Mode = HAL_I2C_MODE_MASTER;
    hI2C->ErrorCode = HAL_I2C_ERROR_NONE;

    hI2C->pBuffPtr = pData;
    hI2C->XferSize = Size;
    hI2C->XferCount = Size;
    hI2C->XferOptions = XferOptions;
    
    if ((hI2C->PreviousState == I2C_STATE_MASTER_BUSY_RX) || (hI2C->PreviousState == I2C_STATE_NONE))
    {
      /* Send Slave Address */
      if (I2C_MasterRequestWrite(hI2C, DevAddress, I2C_TIMEOUT_FLAG) != HAL_OK)
      {
        if (hI2C->ErrorCode == HAL_I2C_ERROR_AF)
        {
          /* Process Unlocked */
          __HAL_UNLOCK(hI2C);
          return HAL_ERROR;
        }
        else
        {
          /* Process Unlocked */
          __HAL_UNLOCK(hI2C);
          return HAL_TIMEOUT;
        }
      }

      /* Clear ADDR flag */
      __HAL_I2C_CLEAR_ADDRFLAG(hI2C);
    }

    if (hI2C->XferSize > 0U)
    {
    /* Process Unlocked */
    __HAL_UNLOCK(hI2C);

    /* Note : The I2C interrupts must be enabled after unlocking current process
              to avoid the risk of I2C interrupt handle execution before current
              process unlock */

    /* Enable EVT, BUF and ERR interrupt */
    __HAL_I2C_ENABLE_IT(hI2C, I2C_IT_EVT | I2C_IT_BUF | I2C_IT_ERR);
    }
    else
    {
      /* Wait until TXE flag is set */
      if (I2C_WaitOnFlagUntilTimeout(hI2C, I2C_FLAG_TXE, RESET, I2C_TIMEOUT_FLAG) != HAL_OK)
      {
        return HAL_TIMEOUT;
      }
      
      /* Generate Stop */
      hI2C->Instance->CR1 |= I2C_CR1_STOP;
      
      hI2C->State = HAL_I2C_STATE_READY;
      /* Process Unlocked */
      __HAL_UNLOCK(hI2C);
    }
    
    return HAL_OK;
  }
  else
  {
    return HAL_BUSY;
  }
}

/**
  * @brief  Sequential receive in master mode an amount of data in no-blocking mode with Interrupt
  * @note   This interface allow to manage repeated start condition when a direction change during transfer
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @param  DevAddress Target device address
  * @param  pData Pointer to data buffer
  * @param  Size Amount of data to be sent
  * @param  XferOptions Options of Transfer, value of @ref I2C_XferOptions_definition
  * @retval HAL status
  */
HAL_StatusTypeDef HAL_I2C_Master_Sequential_Receive_IT(I2C_HandleTypeDef *hI2C, uint16_t DevAddress, uint8_t *pData, uint16_t Size, uint32_t XferOptions)
{
  /* Check the parameters */
  assert_param(IS_I2C_TRANSFER_OPTIONS_REQUEST(XferOptions));

  if (hI2C->State == HAL_I2C_STATE_READY)
  {
    /* Check Busy Flag only if FIRST call of Master interface */
    if ((XferOptions == I2C_FIRST_AND_LAST_FRAME) || (XferOptions == I2C_FIRST_FRAME))
    {
      /* Wait until BUSY flag is reset */
      if (I2C_WaitOnFlagUntilTimeout(hI2C, I2C_FLAG_BUSY, SET, I2C_TIMEOUT_BUSY_FLAG) != HAL_OK)
      {
        return HAL_BUSY;
      }
    }

    /* Process Locked */
    __HAL_LOCK(hI2C);

    /* Disable Pos */
    hI2C->Instance->CR1 &= ~I2C_CR1_POS;

    hI2C->State = HAL_I2C_STATE_BUSY_RX;
    hI2C->Mode = HAL_I2C_MODE_MASTER;
    hI2C->ErrorCode = HAL_I2C_ERROR_NONE;

    hI2C->pBuffPtr = pData;
    hI2C->XferSize = Size;
    hI2C->XferCount = Size;
    hI2C->XferOptions = XferOptions;

    if ((hI2C->PreviousState == I2C_STATE_MASTER_BUSY_TX) || (hI2C->PreviousState == I2C_STATE_NONE))
    {
      /* Send Slave Address */
      if (I2C_MasterRequestRead(hI2C, DevAddress, I2C_TIMEOUT_FLAG) != HAL_OK)
      {
        if (hI2C->ErrorCode == HAL_I2C_ERROR_AF)
        {
          /* Process Unlocked */
          __HAL_UNLOCK(hI2C);
          return HAL_ERROR;
        }
        else
        {
          /* Process Unlocked */
          __HAL_UNLOCK(hI2C);
          return HAL_TIMEOUT;
        }
      }
    }
    
    if (hI2C->XferCount == 0U)
    {
      /* Clear ADDR flag */
      __HAL_I2C_CLEAR_ADDRFLAG(hI2C);
      
      /* Generate Stop */
      hI2C->Instance->CR1 |= I2C_CR1_STOP;
      
      hI2C->State = HAL_I2C_STATE_READY;
    }
    else if (hI2C->XferCount == 1U)   
    {
      /* Prepare next transfer or stop current transfer */
      if ((hI2C->XferOptions != I2C_FIRST_AND_LAST_FRAME) && (hI2C->XferOptions != I2C_LAST_FRAME) && (hI2C->PreviousState != I2C_STATE_MASTER_BUSY_RX))
      {
        if (hI2C->XferOptions != I2C_NEXT_FRAME)
        {
          /* Disable Acknowledge */
          hI2C->Instance->CR1 &= ~I2C_CR1_ACK;
        }
        else
        {
          /* Enable Acknowledge */
          hI2C->Instance->CR1 |= I2C_CR1_ACK;
        }
        
        /* Clear ADDR flag */
        __HAL_I2C_CLEAR_ADDRFLAG(hI2C);
      }
      else
      {
        /* Disable Acknowledge */
        hI2C->Instance->CR1 &= ~I2C_CR1_ACK;

        /* Clear ADDR flag */
        __HAL_I2C_CLEAR_ADDRFLAG(hI2C);

        /* Generate Stop */
        hI2C->Instance->CR1 |= I2C_CR1_STOP;
      }
    }
    else if (hI2C->XferCount == 2U)
    {
      if (hI2C->XferOptions != I2C_NEXT_FRAME)
      {
        /* Disable Acknowledge */
        hI2C->Instance->CR1 &= ~I2C_CR1_ACK;

        /* Enable Pos */
        hI2C->Instance->CR1 |= I2C_CR1_POS;
      }
      else
      {
        /* Enable Acknowledge */
        hI2C->Instance->CR1 |= I2C_CR1_ACK;
      }
        
      /* Clear ADDR flag */
      __HAL_I2C_CLEAR_ADDRFLAG(hI2C);
    }
    else
    {
      /* Enable Acknowledge */
      hI2C->Instance->CR1 |= I2C_CR1_ACK;

      /* Clear ADDR flag */
      __HAL_I2C_CLEAR_ADDRFLAG(hI2C);
    }

    /* Process Unlocked */
    __HAL_UNLOCK(hI2C);

    if (hI2C->XferCount > 0U)
    {
    /* Note : The I2C interrupts must be enabled after unlocking current process
              to avoid the risk of I2C interrupt handle execution before current
              process unlock */

    /* Enable EVT, BUF and ERR interrupt */
    __HAL_I2C_ENABLE_IT(hI2C, I2C_IT_EVT | I2C_IT_BUF | I2C_IT_ERR);
    }
    return HAL_OK;
  }
  else
  {
    return HAL_BUSY;
  }
}

/**
  * @brief  Transmit in slave mode an amount of data in no-blocking mode with Interrupt
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @param  pData Pointer to data buffer
  * @param  Size Amount of data to be sent
  * @retval HAL status
  */
HAL_StatusTypeDef HAL_I2C_Slave_Transmit_IT(I2C_HandleTypeDef *hI2C, uint8_t *pData, uint16_t Size)
{
  if (hI2C->State == HAL_I2C_STATE_READY)
  {
    if ((pData == NULL) || (Size == 0U))
    {
      return  HAL_ERROR;
    }

    /* Wait until BUSY flag is reset */
    if (I2C_WaitOnFlagUntilTimeout(hI2C, I2C_FLAG_BUSY, SET, I2C_TIMEOUT_BUSY_FLAG) != HAL_OK)
    {
      return HAL_BUSY;
    }

    /* Process Locked */
    __HAL_LOCK(hI2C);
    
    /* Disable Pos */
    hI2C->Instance->CR1 &= ~I2C_CR1_POS;

    hI2C->State = HAL_I2C_STATE_BUSY_TX;
    hI2C->Mode = HAL_I2C_MODE_SLAVE;
    hI2C->ErrorCode = HAL_I2C_ERROR_NONE;

    hI2C->pBuffPtr = pData;
    hI2C->XferSize = Size;
    hI2C->XferCount = Size;
    hI2C->XferOptions = I2C_NO_OPTION_FRAME;

    /* Enable Address Acknowledge */
    hI2C->Instance->CR1 |= I2C_CR1_ACK;

    /* Process Unlocked */
    __HAL_UNLOCK(hI2C);

    /* Note : The I2C interrupts must be enabled after unlocking current process
              to avoid the risk of I2C interrupt handle execution before current
              process unlock */

    /* Enable EVT, BUF and ERR interrupt */
    __HAL_I2C_ENABLE_IT(hI2C, I2C_IT_EVT | I2C_IT_BUF | I2C_IT_ERR);

    return HAL_OK;
  }
  else
  {
    return HAL_BUSY;
  }
}

/**
  * @brief  Receive in slave mode an amount of data in no-blocking mode with Interrupt
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @param  pData Pointer to data buffer
  * @param  Size Amount of data to be sent
  * @retval HAL status
  */
HAL_StatusTypeDef HAL_I2C_Slave_Receive_IT(I2C_HandleTypeDef *hI2C, uint8_t *pData, uint16_t Size)
{
  if (hI2C->State == HAL_I2C_STATE_READY)
  {
    if ((pData == NULL) || (Size == 0U))
    {
      return  HAL_ERROR;
    }

    /* Wait until BUSY flag is reset */
    if (I2C_WaitOnFlagUntilTimeout(hI2C, I2C_FLAG_BUSY, SET, I2C_TIMEOUT_BUSY_FLAG) != HAL_OK)
    {
      return HAL_BUSY;
    }

    /* Process Locked */
    __HAL_LOCK(hI2C);
    
    /* Disable Pos */
    hI2C->Instance->CR1 &= ~I2C_CR1_POS;

    hI2C->State = HAL_I2C_STATE_BUSY_RX;
    hI2C->Mode = HAL_I2C_MODE_SLAVE;
    hI2C->ErrorCode = HAL_I2C_ERROR_NONE;

    hI2C->pBuffPtr = pData;
    hI2C->XferSize = Size;
    hI2C->XferCount = Size;
    hI2C->XferOptions = I2C_NO_OPTION_FRAME;

    /* Enable Address Acknowledge */
    hI2C->Instance->CR1 |= I2C_CR1_ACK;

    /* Process Unlocked */
    __HAL_UNLOCK(hI2C);

    /* Note : The I2C interrupts must be enabled after unlocking current process
              to avoid the risk of I2C interrupt handle execution before current
              process unlock */

    /* Enable EVT, BUF and ERR interrupt */
    __HAL_I2C_ENABLE_IT(hI2C, I2C_IT_EVT | I2C_IT_BUF | I2C_IT_ERR);

    return HAL_OK;
  }
  else
  {
    return HAL_BUSY;
  }
}

/**
  * @brief  Sequential transmit in slave mode an amount of data in no-blocking mode with Interrupt
  * @note   This interface allow to manage repeated start condition when a direction change during transfer
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @param  pData Pointer to data buffer
  * @param  Size Amount of data to be sent
  * @param  XferOptions Options of Transfer, value of @ref I2C_XferOptions_definition
  * @retval HAL status
  */
HAL_StatusTypeDef HAL_I2C_Slave_Sequential_Transmit_IT(I2C_HandleTypeDef *hI2C, uint8_t *pData, uint16_t Size, uint32_t XferOptions)
{
  /* Check the parameters */
  assert_param(IS_I2C_TRANSFER_OPTIONS_REQUEST(XferOptions));

  if (hI2C->State == HAL_I2C_STATE_LISTEN)
  {
    if ((pData == NULL) || (Size == 0U))
    {
      return  HAL_ERROR;
    }

    /* Process Locked */
    __HAL_LOCK(hI2C);

    /* Disable Pos */
    hI2C->Instance->CR1 &= ~I2C_CR1_POS;

    hI2C->State = HAL_I2C_STATE_BUSY_TX_LISTEN;
    hI2C->Mode = HAL_I2C_MODE_SLAVE;
    hI2C->ErrorCode = HAL_I2C_ERROR_NONE;

    hI2C->pBuffPtr = pData;
    hI2C->XferSize = Size;
    hI2C->XferCount = Size;
    hI2C->XferOptions = XferOptions;

    /* Clear ADDR flag */
    __HAL_I2C_CLEAR_ADDRFLAG(hI2C);
    
    /* Process Unlocked */
    __HAL_UNLOCK(hI2C);

    /* Note : The I2C interrupts must be enabled after unlocking current process
              to avoid the risk of I2C interrupt handle execution before current
              process unlock */

    /* Enable EVT, BUF and ERR interrupt */
    __HAL_I2C_ENABLE_IT(hI2C, I2C_IT_EVT | I2C_IT_BUF | I2C_IT_ERR);

    return HAL_OK;
  }
  else
  {
    return HAL_BUSY;
  }
}

/**
  * @brief  Sequential receive in slave mode an amount of data in no-blocking mode with Interrupt
  * @note   This interface allow to manage repeated start condition when a direction change during transfer
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @param  pData Pointer to data buffer
  * @param  Size Amount of data to be sent
  * @param  XferOptions Options of Transfer, value of @ref I2C_XferOptions_definition
  * @retval HAL status
  */
HAL_StatusTypeDef HAL_I2C_Slave_Sequential_Receive_IT(I2C_HandleTypeDef *hI2C, uint8_t *pData, uint16_t Size, uint32_t XferOptions)
{
  /* Check the parameters */
  assert_param(IS_I2C_TRANSFER_OPTIONS_REQUEST(XferOptions));

  if (hI2C->State == HAL_I2C_STATE_LISTEN)
  {
    if ((pData == NULL) || (Size == 0U))
    {
      return  HAL_ERROR;
    }

    /* Process Locked */
    __HAL_LOCK(hI2C);

    /* Disable Pos */
    hI2C->Instance->CR1 &= ~I2C_CR1_POS;

    hI2C->State = HAL_I2C_STATE_BUSY_RX_LISTEN;
    hI2C->Mode = HAL_I2C_MODE_SLAVE;
    hI2C->ErrorCode = HAL_I2C_ERROR_NONE;

    hI2C->pBuffPtr = pData;
    hI2C->XferSize = Size;
    hI2C->XferCount = Size;
    hI2C->XferOptions = XferOptions;

    /* Clear ADDR flag */
    __HAL_I2C_CLEAR_ADDRFLAG(hI2C);

    /* Process Unlocked */
    __HAL_UNLOCK(hI2C);

    /* Note : The I2C interrupts must be enabled after unlocking current process
              to avoid the risk of I2C interrupt handle execution before current
              process unlock */

    /* Enable EVT, BUF and ERR interrupt */
    __HAL_I2C_ENABLE_IT(hI2C, I2C_IT_EVT | I2C_IT_BUF | I2C_IT_ERR);

    return HAL_OK;
  }
  else
  {
    return HAL_BUSY;
  }
}

/**
  * @brief  Enable the Address listen mode with Interrupt.
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *                the configuration information for the specified I2C.
  * @retval HAL status
  */
HAL_StatusTypeDef HAL_I2C_EnableListen_IT(I2C_HandleTypeDef *hI2C)
{
  if (hI2C->State == HAL_I2C_STATE_READY)
  {
    hI2C->State = HAL_I2C_STATE_LISTEN;
    
    /* Enable Address Acknowledge */
    hI2C->Instance->CR1 |= I2C_CR1_ACK;

    /* Enable EVT and ERR interrupt */
    __HAL_I2C_ENABLE_IT(hI2C, I2C_IT_EVT | I2C_IT_ERR);
    
    return HAL_OK;
  }
  else
  {
    return HAL_BUSY;
  }
}

/**
  * @brief  Disable the Address listen mode with Interrupt.
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *                the configuration information for the specified I2C.
  * @retval HAL status
  */
HAL_StatusTypeDef HAL_I2C_DisableListen_IT(I2C_HandleTypeDef *hI2C)
{
  /* Declaration of tmp to prevent undefined behavior of volatile usage */
  uint32_t tmp;
  
  /* Disable Address listen mode only if a transfer is not ongoing */
  if (hI2C->State == HAL_I2C_STATE_LISTEN)
  {
    tmp = (uint32_t)(hI2C->State) & I2C_STATE_MSK;
    hI2C->PreviousState = tmp | (uint32_t)(hI2C->Mode);
    hI2C->State = HAL_I2C_STATE_READY;
    hI2C->Mode = HAL_I2C_MODE_NONE;
    
    /* Disable Address Acknowledge */
    hI2C->Instance->CR1 &= ~I2C_CR1_ACK;

    /* Disable EVT and ERR interrupt */
    __HAL_I2C_DISABLE_IT(hI2C, I2C_IT_EVT | I2C_IT_ERR);
  
    return HAL_OK;
  }
  else
  {
    return HAL_BUSY;
  }
}

/**
  * @brief  Transmit in master mode an amount of data in no-blocking mode with DMA
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @param  DevAddress Target device address
  * @param  pData Pointer to data buffer
  * @param  Size Amount of data to be sent
  * @retval HAL status
  */
HAL_StatusTypeDef HAL_I2C_Master_Transmit_DMA(I2C_HandleTypeDef *hI2C, uint16_t DevAddress, uint8_t *pData, uint16_t Size)
{
  if (hI2C->State == HAL_I2C_STATE_READY)
  {
    /* Wait until BUSY flag is reset */
    if (I2C_WaitOnFlagUntilTimeout(hI2C, I2C_FLAG_BUSY, SET, I2C_TIMEOUT_BUSY_FLAG) != HAL_OK)
    {
      return HAL_BUSY;
    }

    /* Process Locked */
    __HAL_LOCK(hI2C);
    
    /* Disable Pos */
    hI2C->Instance->CR1 &= ~I2C_CR1_POS;

    hI2C->State = HAL_I2C_STATE_BUSY_TX;
    hI2C->Mode = HAL_I2C_MODE_MASTER;
    hI2C->ErrorCode = HAL_I2C_ERROR_NONE;

    hI2C->pBuffPtr = pData;
    hI2C->XferSize = Size;
    hI2C->XferCount = Size;
    hI2C->XferOptions = I2C_NO_OPTION_FRAME;
    
    if (hI2C->XferSize > 0U)
    {
    /* Set the I2C DMA transfer complete callback */
    hI2C->hdmatx->XferCpltCallback = I2C_DMAMasterTransmitCplt;

    /* Set the DMA error callback */
    hI2C->hdmatx->XferErrorCallback = I2C_DMAError;

    /* Enable the DMA Stream */
    HAL_DMA_Start_IT(hI2C->hdmatx, (uint32_t)pData, (uint32_t)&hI2C->Instance->DR, Size);

    /* Send Slave Address */
    if (I2C_MasterRequestWrite(hI2C, DevAddress, I2C_TIMEOUT_FLAG) != HAL_OK)
    {
      if (hI2C->ErrorCode == HAL_I2C_ERROR_AF)
      {
        /* Process Unlocked */
        __HAL_UNLOCK(hI2C);
        return HAL_ERROR;
      }
      else
      {
        /* Process Unlocked */
        __HAL_UNLOCK(hI2C);
        return HAL_TIMEOUT;
      }
    }

    /* Enable DMA Request */
    hI2C->Instance->CR2 |= I2C_CR2_DMAEN;

    /* Clear ADDR flag */
    __HAL_I2C_CLEAR_ADDRFLAG(hI2C);
    }
    else
    {
      /* Send Slave Address */
      if (I2C_MasterRequestWrite(hI2C, DevAddress, I2C_TIMEOUT_FLAG) != HAL_OK)
      {
        if (hI2C->ErrorCode == HAL_I2C_ERROR_AF)
        {
          /* Process Unlocked */
          __HAL_UNLOCK(hI2C);
          return HAL_ERROR;
        }
        else
        {
          /* Process Unlocked */
          __HAL_UNLOCK(hI2C);
          return HAL_TIMEOUT;
        }
      }

      /* Clear ADDR flag */
      __HAL_I2C_CLEAR_ADDRFLAG(hI2C);

      /* Wait until TXE flag is set */
      if (I2C_WaitOnFlagUntilTimeout(hI2C, I2C_FLAG_TXE, RESET, I2C_TIMEOUT_FLAG) != HAL_OK)
      {
        return HAL_TIMEOUT;
      }

      /* Generate Stop */
      hI2C->Instance->CR1 |= I2C_CR1_STOP;

      hI2C->State = HAL_I2C_STATE_READY;
    }
    
    /* Process Unlocked */
    __HAL_UNLOCK(hI2C);

    return HAL_OK;
  }
  else
  {
    return HAL_BUSY;
  }
}

/**
  * @brief  Receive in master mode an amount of data in no-blocking mode with DMA
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @param  DevAddress Target device address
  * @param  pData Pointer to data buffer
  * @param  Size Amount of data to be sent
  * @retval HAL status
  */
HAL_StatusTypeDef HAL_I2C_Master_Receive_DMA(I2C_HandleTypeDef *hI2C, uint16_t DevAddress, uint8_t *pData, uint16_t Size)
{
  if (hI2C->State == HAL_I2C_STATE_READY)
  {
    /* Wait until BUSY flag is reset */
    if (I2C_WaitOnFlagUntilTimeout(hI2C, I2C_FLAG_BUSY, SET, I2C_TIMEOUT_BUSY_FLAG) != HAL_OK)
    {
      return HAL_BUSY;
    }

    /* Process Locked */
    __HAL_LOCK(hI2C);
    
    /* Disable Pos */
    hI2C->Instance->CR1 &= ~I2C_CR1_POS;

    hI2C->State = HAL_I2C_STATE_BUSY_RX;
    hI2C->Mode = HAL_I2C_MODE_MASTER;
    hI2C->ErrorCode = HAL_I2C_ERROR_NONE;

    hI2C->pBuffPtr = pData;
    hI2C->XferSize = Size;
    hI2C->XferCount = Size;
    hI2C->XferOptions = I2C_NO_OPTION_FRAME;

    if (hI2C->XferSize > 0U)
    {    
      /* Set the I2C DMA transfer complete callback */
      hI2C->hdmarx->XferCpltCallback = I2C_DMAMasterReceiveCplt;
      
      /* Set the DMA error callback */
      hI2C->hdmarx->XferErrorCallback = I2C_DMAError;
      
      /* Enable the DMA Stream */
      HAL_DMA_Start_IT(hI2C->hdmarx, (uint32_t)&hI2C->Instance->DR, (uint32_t)pData, Size);
      
      /* Send Slave Address */
      if (I2C_MasterRequestRead(hI2C, DevAddress, I2C_TIMEOUT_FLAG) != HAL_OK)
      {
        if (hI2C->ErrorCode == HAL_I2C_ERROR_AF)
        {
          /* Process Unlocked */
          __HAL_UNLOCK(hI2C);
          return HAL_ERROR;
        }
        else
        {
          /* Process Unlocked */
          __HAL_UNLOCK(hI2C);
          return HAL_TIMEOUT;
        }
      }
      
      if (Size == 1U)
      {
        /* Disable Acknowledge */
        hI2C->Instance->CR1 &= ~I2C_CR1_ACK;
      }
      else
      {
        /* Enable Last DMA bit */
        hI2C->Instance->CR2 |= I2C_CR2_LAST;
      }
      
      /* Enable DMA Request */
      hI2C->Instance->CR2 |= I2C_CR2_DMAEN;
      
      /* Clear ADDR flag */
      __HAL_I2C_CLEAR_ADDRFLAG(hI2C);
    }
    else
    {
      /* Send Slave Address */
      if (I2C_MasterRequestRead(hI2C, DevAddress, I2C_TIMEOUT_FLAG) != HAL_OK)
      {
        if (hI2C->ErrorCode == HAL_I2C_ERROR_AF)
        {
          /* Process Unlocked */
          __HAL_UNLOCK(hI2C);
          return HAL_ERROR;
        }
        else
        {
          /* Process Unlocked */
          __HAL_UNLOCK(hI2C);
          return HAL_TIMEOUT;
        }
      }

      /* Clear ADDR flag */
      __HAL_I2C_CLEAR_ADDRFLAG(hI2C);

      /* Generate Stop */
      hI2C->Instance->CR1 |= I2C_CR1_STOP;

      hI2C->State = HAL_I2C_STATE_READY;
    }
    
    /* Process Unlocked */
    __HAL_UNLOCK(hI2C);

    return HAL_OK;
  }
  else
  {
    return HAL_BUSY;
  }
}

/**
  * @brief  Abort a master I2C process communication with Interrupt.
  * @note   This abort can be called only if state is ready
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *                the configuration information for the specified I2C.
  * @param  DevAddress Target device address
  * @retval HAL status
  */
HAL_StatusTypeDef HAL_I2C_Master_Abort_IT(I2C_HandleTypeDef *hI2C, uint16_t DevAddress)
{
  /* Abort Master transfer during Receive or Transmit process    */
  /* Or after an error during during Receive or Transmit process */
  if ((hI2C->Mode == HAL_I2C_MODE_MASTER) || \
     ((hI2C->ErrorCode != HAL_I2C_ERROR_NONE) && ((hI2C->PreviousState & ((uint32_t)HAL_I2C_MODE_MASTER)) == HAL_I2C_MODE_MASTER)))
  {
    /* Process Locked */
    __HAL_LOCK(hI2C);
    
    hI2C->ErrorCode = HAL_I2C_ERROR_NONE;
    
    /* Disable Acknowledge */
    hI2C->Instance->CR1 &= ~I2C_CR1_ACK;

    /* Generate Stop */
    hI2C->Instance->CR1 |= I2C_CR1_STOP;
    
    hI2C->XferCount = 0U;
    
    /* Disable EVT, BUF and ERR interrupt */
    __HAL_I2C_DISABLE_IT(hI2C, I2C_IT_EVT | I2C_IT_BUF | I2C_IT_ERR);
    
    hI2C->PreviousState = I2C_STATE_NONE;
    hI2C->State = HAL_I2C_STATE_READY;

    /* Process Unlocked */
    __HAL_UNLOCK(hI2C); 

    /* Call Complete Callback */
    return HAL_OK;
  }
  else
  {
    /* Wrong usage of abort function */
    /* This function should be used only in case of abort monitored by master device */
    return HAL_ERROR;
  }
}

/**
  * @brief  Transmit in slave mode an amount of data in no-blocking mode with DMA
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @param  pData Pointer to data buffer
  * @param  Size Amount of data to be sent
  * @retval HAL status
  */
HAL_StatusTypeDef HAL_I2C_Slave_Transmit_DMA(I2C_HandleTypeDef *hI2C, uint8_t *pData, uint16_t Size)
{
  if (hI2C->State == HAL_I2C_STATE_READY)
  {
    if ((pData == NULL) || (Size == 0U))
    {
      return  HAL_ERROR;
    }

    /* Wait until BUSY flag is reset */
    if (I2C_WaitOnFlagUntilTimeout(hI2C, I2C_FLAG_BUSY, SET, I2C_TIMEOUT_BUSY_FLAG) != HAL_OK)
    {
      return HAL_BUSY;
    }

    /* Process Locked */
    __HAL_LOCK(hI2C);
    
    /* Disable Pos */
    hI2C->Instance->CR1 &= ~I2C_CR1_POS;

    hI2C->State = HAL_I2C_STATE_BUSY_TX;
    hI2C->Mode = HAL_I2C_MODE_SLAVE;
    hI2C->ErrorCode = HAL_I2C_ERROR_NONE;

    hI2C->pBuffPtr = pData;
    hI2C->XferSize = Size;
    hI2C->XferCount = Size;
    hI2C->XferOptions = I2C_NO_OPTION_FRAME;
    
    /* Set the I2C DMA transfer complete callback */
    hI2C->hdmatx->XferCpltCallback = I2C_DMASlaveTransmitCplt;

    /* Set the DMA error callback */
    hI2C->hdmatx->XferErrorCallback = I2C_DMAError;

    /* Enable the DMA Stream */
    HAL_DMA_Start_IT(hI2C->hdmatx, (uint32_t)pData, (uint32_t)&hI2C->Instance->DR, Size);

    /* Enable DMA Request */
    hI2C->Instance->CR2 |= I2C_CR2_DMAEN;

    /* Enable Address Acknowledge */
    hI2C->Instance->CR1 |= I2C_CR1_ACK;

    /* Wait until ADDR flag is set */
    if (I2C_WaitOnFlagUntilTimeout(hI2C, I2C_FLAG_ADDR, RESET, I2C_TIMEOUT_ADDR_SLAVE) != HAL_OK)
    {
      return HAL_TIMEOUT;
    }

    /* If 7bit addressing mode is selected */
    if (hI2C->Init.AddressingMode == I2C_ADDRESSINGMODE_7BIT)
    {
      /* Clear ADDR flag */
      __HAL_I2C_CLEAR_ADDRFLAG(hI2C);
    }
    else
    {
      /* Clear ADDR flag */
      __HAL_I2C_CLEAR_ADDRFLAG(hI2C);

      /* Wait until ADDR flag is set */
      if (I2C_WaitOnFlagUntilTimeout(hI2C, I2C_FLAG_ADDR, RESET, I2C_TIMEOUT_ADDR_SLAVE) != HAL_OK)
      {
        return HAL_TIMEOUT;
      }

      /* Clear ADDR flag */
      __HAL_I2C_CLEAR_ADDRFLAG(hI2C);
    }

    /* Process Unlocked */
    __HAL_UNLOCK(hI2C);

    return HAL_OK;
  }
  else
  {
    return HAL_BUSY;
  }
}

/**
  * @brief  Receive in slave mode an amount of data in no-blocking mode with DMA
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @param  pData Pointer to data buffer
  * @param  Size Amount of data to be sent
  * @retval HAL status
  */
HAL_StatusTypeDef HAL_I2C_Slave_Receive_DMA(I2C_HandleTypeDef *hI2C, uint8_t *pData, uint16_t Size)
{
  if (hI2C->State == HAL_I2C_STATE_READY)
  {
    if ((pData == NULL) || (Size == 0U))
    {
      return  HAL_ERROR;
    }

    /* Wait until BUSY flag is reset */
    if (I2C_WaitOnFlagUntilTimeout(hI2C, I2C_FLAG_BUSY, SET, I2C_TIMEOUT_BUSY_FLAG) != HAL_OK)
    {
      return HAL_BUSY;
    }

    /* Process Locked */
    __HAL_LOCK(hI2C);
    
    /* Disable Pos */
    hI2C->Instance->CR1 &= ~I2C_CR1_POS;

    hI2C->State = HAL_I2C_STATE_BUSY_RX;
    hI2C->Mode = HAL_I2C_MODE_SLAVE;
    hI2C->ErrorCode = HAL_I2C_ERROR_NONE;

    hI2C->pBuffPtr = pData;
    hI2C->XferSize = Size;
    hI2C->XferCount = Size;
    hI2C->XferOptions = I2C_NO_OPTION_FRAME;
    
    /* Set the I2C DMA transfer complete callback */
    hI2C->hdmarx->XferCpltCallback = I2C_DMASlaveReceiveCplt;

    /* Set the DMA error callback */
    hI2C->hdmarx->XferErrorCallback = I2C_DMAError;

    /* Enable the DMA Stream */
    HAL_DMA_Start_IT(hI2C->hdmarx, (uint32_t)&hI2C->Instance->DR, (uint32_t)pData, Size);

    /* Enable DMA Request */
    hI2C->Instance->CR2 |= I2C_CR2_DMAEN;

    /* Enable Address Acknowledge */
    hI2C->Instance->CR1 |= I2C_CR1_ACK;

    /* Wait until ADDR flag is set */
    if (I2C_WaitOnFlagUntilTimeout(hI2C, I2C_FLAG_ADDR, RESET, I2C_TIMEOUT_ADDR_SLAVE) != HAL_OK)
    {
      return HAL_TIMEOUT;
    }

    /* Clear ADDR flag */
    __HAL_I2C_CLEAR_ADDRFLAG(hI2C);

    /* Process Unlocked */
    __HAL_UNLOCK(hI2C);

    return HAL_OK;
  }
  else
  {
    return HAL_BUSY;
  }
}
/**
  * @brief  Write an amount of data in blocking mode to a specific memory address
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @param  DevAddress Target device address
  * @param  MemAddress Internal memory address
  * @param  MemAddSize Size of internal memory address
  * @param  pData Pointer to data buffer
  * @param  Size Amount of data to be sent
  * @param  Timeout Timeout duration
  * @retval HAL status
  */
HAL_StatusTypeDef HAL_I2C_Mem_Write(I2C_HandleTypeDef *hI2C, uint16_t DevAddress, uint16_t MemAddress, uint16_t MemAddSize, uint8_t *pData, uint16_t Size, uint32_t Timeout)
{
  /* Check the parameters */
  assert_param(IS_I2C_MEMADD_SIZE(MemAddSize));

  if (hI2C->State == HAL_I2C_STATE_READY)
  {
    /* Wait until BUSY flag is reset */
    if (I2C_WaitOnFlagUntilTimeout(hI2C, I2C_FLAG_BUSY, SET, I2C_TIMEOUT_BUSY_FLAG) != HAL_OK)
    {
      return HAL_BUSY;
    }

    /* Process Locked */
    __HAL_LOCK(hI2C);
    
    /* Disable Pos */
    hI2C->Instance->CR1 &= ~I2C_CR1_POS;

    hI2C->State = HAL_I2C_STATE_BUSY_TX;
    hI2C->Mode = HAL_I2C_MODE_MEM;
    hI2C->ErrorCode = HAL_I2C_ERROR_NONE;
    hI2C->XferOptions = I2C_NO_OPTION_FRAME;
    
    /* Send Slave Address and Memory Address */
    if (I2C_RequestMemoryWrite(hI2C, DevAddress, MemAddress, MemAddSize, Timeout) != HAL_OK)
    {
      if (hI2C->ErrorCode == HAL_I2C_ERROR_AF)
      {
        /* Process Unlocked */
        __HAL_UNLOCK(hI2C);
        return HAL_ERROR;
      }
      else
      {
        /* Process Unlocked */
        __HAL_UNLOCK(hI2C);
        return HAL_TIMEOUT;
      }
    }

    while(Size > 0U)
    {
      /* Wait until TXE flag is set */
      if (I2C_WaitOnTXEFlagUntilTimeout(hI2C, Timeout) != HAL_OK)
      {
        if (hI2C->ErrorCode == HAL_I2C_ERROR_AF)
        {
          /* Generate Stop */
          hI2C->Instance->CR1 |= I2C_CR1_STOP;
          return HAL_ERROR;
        }
        else
        {
          return HAL_TIMEOUT;
        }
      }

      /* Write data to DR */
      hI2C->Instance->DR = (*pData++);
      Size--;

      if ((__HAL_I2C_GET_FLAG(hI2C, I2C_FLAG_BTF) == SET) && (Size != 0))
      {
        /* Write data to DR */
        hI2C->Instance->DR = (*pData++);
        Size--;
      }
    }

    /* Wait until TXE flag is set */
    if (I2C_WaitOnTXEFlagUntilTimeout(hI2C, Timeout) != HAL_OK)
    {
      if (hI2C->ErrorCode == HAL_I2C_ERROR_AF)
      {
        /* Generate Stop */
        hI2C->Instance->CR1 |= I2C_CR1_STOP;
        return HAL_ERROR;
      }
      else
      {
        return HAL_TIMEOUT;
      }
    }

    /* Generate Stop */
    hI2C->Instance->CR1 |= I2C_CR1_STOP;

    hI2C->State = HAL_I2C_STATE_READY;
    hI2C->Mode = HAL_I2C_MODE_NONE;
    
    /* Process Unlocked */
    __HAL_UNLOCK(hI2C);

    return HAL_OK;
  }
  else
  {
    return HAL_BUSY;
  }
}

/**
  * @brief  Read an amount of data in blocking mode from a specific memory address
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @param  DevAddress Target device address
  * @param  MemAddress Internal memory address
  * @param  MemAddSize Size of internal memory address
  * @param  pData Pointer to data buffer
  * @param  Size Amount of data to be sent
  * @param  Timeout Timeout duration
  * @retval HAL status
  */
HAL_StatusTypeDef HAL_I2C_Mem_Read(I2C_HandleTypeDef *hI2C, uint16_t DevAddress, uint16_t MemAddress, uint16_t MemAddSize, uint8_t *pData, uint16_t Size, uint32_t Timeout)
{
  /* Check the parameters */
  assert_param(IS_I2C_MEMADD_SIZE(MemAddSize));

  if (hI2C->State == HAL_I2C_STATE_READY)
  {
    /* Wait until BUSY flag is reset */
    if (I2C_WaitOnFlagUntilTimeout(hI2C, I2C_FLAG_BUSY, SET, I2C_TIMEOUT_BUSY_FLAG) != HAL_OK)
    {
      return HAL_BUSY;
    }

    /* Process Locked */
    __HAL_LOCK(hI2C);
    
    /* Disable Pos */
    hI2C->Instance->CR1 &= ~I2C_CR1_POS;

    hI2C->State = HAL_I2C_STATE_BUSY_RX;
    hI2C->Mode = HAL_I2C_MODE_MEM;
    hI2C->ErrorCode = HAL_I2C_ERROR_NONE;
    hI2C->XferOptions = I2C_NO_OPTION_FRAME;
    
    /* Send Slave Address and Memory Address */
    if (I2C_RequestMemoryRead(hI2C, DevAddress, MemAddress, MemAddSize, Timeout) != HAL_OK)
    {
      if (hI2C->ErrorCode == HAL_I2C_ERROR_AF)
      {
        /* Process Unlocked */
        __HAL_UNLOCK(hI2C);
        return HAL_ERROR;
      }
      else
      {
        /* Process Unlocked */
        __HAL_UNLOCK(hI2C);
        return HAL_TIMEOUT;
      }
    }

    if (Size == 0U)
    {
      /* Clear ADDR flag */
      __HAL_I2C_CLEAR_ADDRFLAG(hI2C);
      
      /* Generate Stop */
      hI2C->Instance->CR1 |= I2C_CR1_STOP;
    }
    else if (Size == 1U)
    {
      /* Disable Acknowledge */
      hI2C->Instance->CR1 &= ~I2C_CR1_ACK;

      /* Clear ADDR flag */
      __HAL_I2C_CLEAR_ADDRFLAG(hI2C);

      /* Generate Stop */
      hI2C->Instance->CR1 |= I2C_CR1_STOP;
    }
    else if (Size == 2U)
    {
      /* Disable Acknowledge */
      hI2C->Instance->CR1 &= ~I2C_CR1_ACK;

      /* Enable Pos */
      hI2C->Instance->CR1 |= I2C_CR1_POS;

      /* Clear ADDR flag */
      __HAL_I2C_CLEAR_ADDRFLAG(hI2C);
    }
    else
    {
      /* Clear ADDR flag */
      __HAL_I2C_CLEAR_ADDRFLAG(hI2C);
    }

    while(Size > 0U)
    {
      if (Size <= 3U)
      {
        /* One byte */
        if (Size== 1U)
        {
          /* Wait until RXNE flag is set */
          if (I2C_WaitOnRXNEFlagUntilTimeout(hI2C, Timeout) != HAL_OK)      
          {
            if (hI2C->ErrorCode == HAL_I2C_ERROR_TIMEOUT)
            {
              return HAL_TIMEOUT;
            }
            else
            {
              return HAL_ERROR;
            }
          }

          /* Read data from DR */
          (*pData++) = hI2C->Instance->DR;
          Size--;
        }
        /* Two bytes */
        else if (Size == 2U)
        {
          /* Wait until BTF flag is set */
          if (I2C_WaitOnFlagUntilTimeout(hI2C, I2C_FLAG_BTF, RESET, Timeout) != HAL_OK)
          {
            return HAL_TIMEOUT;
          }

          /* Generate Stop */
          hI2C->Instance->CR1 |= I2C_CR1_STOP;

          /* Read data from DR */
          (*pData++) = hI2C->Instance->DR;
          Size--;

          /* Read data from DR */
          (*pData++) = hI2C->Instance->DR;
          Size--;
        }
        /* 3 Last bytes */
        else
        {
          /* Wait until BTF flag is set */
          if (I2C_WaitOnFlagUntilTimeout(hI2C, I2C_FLAG_BTF, RESET, Timeout) != HAL_OK)
          {
            return HAL_TIMEOUT;
          }

          /* Disable Acknowledge */
          hI2C->Instance->CR1 &= ~I2C_CR1_ACK;

          /* Read data from DR */
          (*pData++) = hI2C->Instance->DR;
          Size--;

          /* Wait until BTF flag is set */
          if (I2C_WaitOnFlagUntilTimeout(hI2C, I2C_FLAG_BTF, RESET, Timeout) != HAL_OK)
          {
            return HAL_TIMEOUT;
          }

          /* Generate Stop */
          hI2C->Instance->CR1 |= I2C_CR1_STOP;

          /* Read data from DR */
          (*pData++) = hI2C->Instance->DR;
          Size--;

          /* Read data from DR */
          (*pData++) = hI2C->Instance->DR;
          Size--;
        }
      }
      else
      {
        /* Wait until RXNE flag is set */
        if (I2C_WaitOnRXNEFlagUntilTimeout(hI2C, Timeout) != HAL_OK)      
        {
          if (hI2C->ErrorCode == HAL_I2C_ERROR_TIMEOUT)
          {
            return HAL_TIMEOUT;
          }
          else
          {
            return HAL_ERROR;
          }
        }

        /* Read data from DR */
        (*pData++) = hI2C->Instance->DR;
        Size--;

        if (__HAL_I2C_GET_FLAG(hI2C, I2C_FLAG_BTF) == SET)
        {
          /* Read data from DR */
          (*pData++) = hI2C->Instance->DR;
          Size--;
        }
      }
    }

    hI2C->State = HAL_I2C_STATE_READY;
    hI2C->Mode = HAL_I2C_MODE_NONE;
    
    /* Process Unlocked */
    __HAL_UNLOCK(hI2C);

    return HAL_OK;
  }
  else
  {
    return HAL_BUSY;
  }
}
/**
  * @brief  Write an amount of data in no-blocking mode with Interrupt to a specific memory address
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @param  DevAddress Target device address
  * @param  MemAddress Internal memory address
  * @param  MemAddSize Size of internal memory address
  * @param  pData Pointer to data buffer
  * @param  Size Amount of data to be sent
  * @retval HAL status
  */
HAL_StatusTypeDef HAL_I2C_Mem_Write_IT(I2C_HandleTypeDef *hI2C, uint16_t DevAddress, uint16_t MemAddress, uint16_t MemAddSize, uint8_t *pData, uint16_t Size)
{
  /* Check the parameters */
  assert_param(IS_I2C_MEMADD_SIZE(MemAddSize));

  if (hI2C->State == HAL_I2C_STATE_READY)
  {
    /* Wait until BUSY flag is reset */
    if (I2C_WaitOnFlagUntilTimeout(hI2C, I2C_FLAG_BUSY, SET, I2C_TIMEOUT_BUSY_FLAG) != HAL_OK)
    {
      return HAL_BUSY;
    }

    /* Process Locked */
    __HAL_LOCK(hI2C);
    
    /* Disable Pos */
    hI2C->Instance->CR1 &= ~I2C_CR1_POS;

    hI2C->State = HAL_I2C_STATE_BUSY_TX;
    hI2C->Mode = HAL_I2C_MODE_MEM;
    hI2C->ErrorCode = HAL_I2C_ERROR_NONE;

    hI2C->pBuffPtr = pData;
    hI2C->XferSize = Size;
    hI2C->XferCount = Size;
    hI2C->XferOptions = I2C_NO_OPTION_FRAME;

    /* Send Slave Address and Memory Address */
    if (I2C_RequestMemoryWrite(hI2C, DevAddress, MemAddress, MemAddSize, I2C_TIMEOUT_FLAG) != HAL_OK)
    {
      if (hI2C->ErrorCode == HAL_I2C_ERROR_AF)
      {
        /* Process Unlocked */
        __HAL_UNLOCK(hI2C);
        return HAL_ERROR;
      }
      else
      {
        /* Process Unlocked */
        __HAL_UNLOCK(hI2C);
        return HAL_TIMEOUT;
      }
    }
    
    if (hI2C->XferSize > 0U)
    {
      /* Process Unlocked */
      __HAL_UNLOCK(hI2C);
      
      /* Note : The I2C interrupts must be enabled after unlocking current process
      to avoid the risk of I2C interrupt handle execution before current
      process unlock */
      
      /* Enable EVT, BUF and ERR interrupt */
      __HAL_I2C_ENABLE_IT(hI2C, I2C_IT_EVT | I2C_IT_BUF | I2C_IT_ERR);
    }
    else
    {
      /* Wait until TXE flag is set */
      if (I2C_WaitOnFlagUntilTimeout(hI2C, I2C_FLAG_TXE, RESET, I2C_TIMEOUT_FLAG) != HAL_OK)
      {
        return HAL_TIMEOUT;
      }
      
      /* Generate Stop */
      hI2C->Instance->CR1 |= I2C_CR1_STOP;
      
      hI2C->State = HAL_I2C_STATE_READY;
      
      /* Process Unlocked */
      __HAL_UNLOCK(hI2C);
    }
    return HAL_OK;
  }
  else
  {
    return HAL_BUSY;
  }
}

/**
  * @brief  Read an amount of data in no-blocking mode with Interrupt from a specific memory address
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @param  DevAddress Target device address
  * @param  MemAddress Internal memory address
  * @param  MemAddSize Size of internal memory address
  * @param  pData Pointer to data buffer
  * @param  Size Amount of data to be sent
  * @retval HAL status
  */
HAL_StatusTypeDef HAL_I2C_Mem_Read_IT(I2C_HandleTypeDef *hI2C, uint16_t DevAddress, uint16_t MemAddress, uint16_t MemAddSize, uint8_t *pData, uint16_t Size)
{
  /* Check the parameters */
  assert_param(IS_I2C_MEMADD_SIZE(MemAddSize));

  if (hI2C->State == HAL_I2C_STATE_READY)
  {
    /* Wait until BUSY flag is reset */
    if (I2C_WaitOnFlagUntilTimeout(hI2C, I2C_FLAG_BUSY, SET, I2C_TIMEOUT_BUSY_FLAG) != HAL_OK)
    {
      return HAL_BUSY;
    }

    /* Process Locked */
    __HAL_LOCK(hI2C);
    
    /* Disable Pos */
    hI2C->Instance->CR1 &= ~I2C_CR1_POS;

    hI2C->State = HAL_I2C_STATE_BUSY_RX;
    hI2C->Mode = HAL_I2C_MODE_MEM;
    hI2C->ErrorCode = HAL_I2C_ERROR_NONE;

    hI2C->pBuffPtr = pData;
    hI2C->XferSize = Size;
    hI2C->XferCount = Size;
    hI2C->XferOptions = I2C_NO_OPTION_FRAME;

    /* Send Slave Address and Memory Address */
    if (I2C_RequestMemoryRead(hI2C, DevAddress, MemAddress, MemAddSize, I2C_TIMEOUT_FLAG) != HAL_OK)
    {
      if (hI2C->ErrorCode == HAL_I2C_ERROR_AF)
      {
        /* Process Unlocked */
        __HAL_UNLOCK(hI2C);
        return HAL_ERROR;
      }
      else
      {
        /* Process Unlocked */
        __HAL_UNLOCK(hI2C);
        return HAL_TIMEOUT;
      }
    }

    if (hI2C->XferCount == 0U)
    {
      /* Clear ADDR flag */
      __HAL_I2C_CLEAR_ADDRFLAG(hI2C);
      
      /* Generate Stop */
      hI2C->Instance->CR1 |= I2C_CR1_STOP;
    }
    else if (hI2C->XferCount == 1U)   
    {
      /* Disable Acknowledge */
      hI2C->Instance->CR1 &= ~I2C_CR1_ACK;

      /* Clear ADDR flag */
      __HAL_I2C_CLEAR_ADDRFLAG(hI2C);

      /* Generate Stop */
      hI2C->Instance->CR1 |= I2C_CR1_STOP;
    }
    else if (hI2C->XferCount == 2U)
    {
      /* Disable Acknowledge */
      hI2C->Instance->CR1 &= ~I2C_CR1_ACK;

      /* Enable Pos */
      hI2C->Instance->CR1 |= I2C_CR1_POS;

      /* Clear ADDR flag */
      __HAL_I2C_CLEAR_ADDRFLAG(hI2C);
    }
    else
    {
      /* Enable Acknowledge */
      hI2C->Instance->CR1 |= I2C_CR1_ACK;

      /* Clear ADDR flag */
      __HAL_I2C_CLEAR_ADDRFLAG(hI2C);
    }
    
    if (hI2C->XferCount == 0U)
    {
      hI2C->State = HAL_I2C_STATE_READY;
    }
    
    /* Process Unlocked */
    __HAL_UNLOCK(hI2C);

    if (hI2C->XferCount > 0U)
    {
      /* Note : The I2C interrupts must be enabled after unlocking current process
      to avoid the risk of I2C interrupt handle execution before current
      process unlock */
      
      /* Enable EVT, BUF and ERR interrupt */
      __HAL_I2C_ENABLE_IT(hI2C, I2C_IT_EVT | I2C_IT_BUF | I2C_IT_ERR);
    }
    return HAL_OK;
  }
  else
  {
    return HAL_BUSY;
  }
}
/**
  * @brief  Write an amount of data in no-blocking mode with DMA to a specific memory address
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @param  DevAddress Target device address
  * @param  MemAddress Internal memory address
  * @param  MemAddSize Size of internal memory address
  * @param  pData Pointer to data buffer
  * @param  Size Amount of data to be sent
  * @retval HAL status
  */
HAL_StatusTypeDef HAL_I2C_Mem_Write_DMA(I2C_HandleTypeDef *hI2C, uint16_t DevAddress, uint16_t MemAddress, uint16_t MemAddSize, uint8_t *pData, uint16_t Size)
{
  /* Check the parameters */
  assert_param(IS_I2C_MEMADD_SIZE(MemAddSize));

  if (hI2C->State == HAL_I2C_STATE_READY)
  {
    /* Wait until BUSY flag is reset */
    if (I2C_WaitOnFlagUntilTimeout(hI2C, I2C_FLAG_BUSY, SET, I2C_TIMEOUT_BUSY_FLAG) != HAL_OK)
    {
      return HAL_BUSY;
    }

    /* Process Locked */
    __HAL_LOCK(hI2C);
    
    /* Disable Pos */
    hI2C->Instance->CR1 &= ~I2C_CR1_POS;

    hI2C->State = HAL_I2C_STATE_BUSY_TX;
    hI2C->Mode = HAL_I2C_MODE_MEM;
    hI2C->ErrorCode = HAL_I2C_ERROR_NONE;

    hI2C->pBuffPtr = pData;
    hI2C->XferSize = Size;
    hI2C->XferCount = Size;
    hI2C->XferOptions = I2C_NO_OPTION_FRAME;

    if (hI2C->XferSize > 0U)
    {    
      /* Set the I2C DMA transfer complete callback */
      hI2C->hdmatx->XferCpltCallback = I2C_DMAMemTransmitCplt;
      
      /* Set the DMA error callback */
      hI2C->hdmatx->XferErrorCallback = I2C_DMAError;
      
      /* Enable the DMA Stream */
      HAL_DMA_Start_IT(hI2C->hdmatx, (uint32_t)pData, (uint32_t)&hI2C->Instance->DR, Size);
      
      /* Send Slave Address and Memory Address */
      if (I2C_RequestMemoryWrite(hI2C, DevAddress, MemAddress, MemAddSize, I2C_TIMEOUT_FLAG) != HAL_OK)
      {
        if (hI2C->ErrorCode == HAL_I2C_ERROR_AF)
        {
          /* Process Unlocked */
          __HAL_UNLOCK(hI2C);
          return HAL_ERROR;
        }
        else
        {
          /* Process Unlocked */
          __HAL_UNLOCK(hI2C);
          return HAL_TIMEOUT;
        }
      }
      
      /* Enable DMA Request */
      hI2C->Instance->CR2 |= I2C_CR2_DMAEN;
    }
    else
    {
      /* Send Slave Address and Memory Address */
      if (I2C_RequestMemoryRead(hI2C, DevAddress, MemAddress, MemAddSize, I2C_TIMEOUT_FLAG) != HAL_OK)
      {
        if (hI2C->ErrorCode == HAL_I2C_ERROR_AF)
        {
          /* Process Unlocked */
          __HAL_UNLOCK(hI2C);
          return HAL_ERROR;
        }
        else
        {
          /* Process Unlocked */
          __HAL_UNLOCK(hI2C);
          return HAL_TIMEOUT;
        }
      }

      /* Wait until TXE flag is set */
      if (I2C_WaitOnFlagUntilTimeout(hI2C, I2C_FLAG_TXE, RESET, I2C_TIMEOUT_FLAG) != HAL_OK)
      {
        return HAL_TIMEOUT;
      }

      /* Generate Stop */
      hI2C->Instance->CR1 |= I2C_CR1_STOP;

      hI2C->State = HAL_I2C_STATE_READY;
    }


    /* Process Unlocked */
    __HAL_UNLOCK(hI2C);

    return HAL_OK;
  }
  else
  {
    return HAL_BUSY;
  }
}

/**
  * @brief  Reads an amount of data in no-blocking mode with DMA from a specific memory address.
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @param  DevAddress Target device address
  * @param  MemAddress Internal memory address
  * @param  MemAddSize Size of internal memory address
  * @param  pData Pointer to data buffer
  * @param  Size Amount of data to be read
  * @retval HAL status
  */
HAL_StatusTypeDef HAL_I2C_Mem_Read_DMA(I2C_HandleTypeDef *hI2C, uint16_t DevAddress, uint16_t MemAddress, uint16_t MemAddSize, uint8_t *pData, uint16_t Size)
{
  /* Check the parameters */
  assert_param(IS_I2C_MEMADD_SIZE(MemAddSize));

  if (hI2C->State == HAL_I2C_STATE_READY)
  {
    /* Wait until BUSY flag is reset */
    if (I2C_WaitOnFlagUntilTimeout(hI2C, I2C_FLAG_BUSY, SET, I2C_TIMEOUT_BUSY_FLAG) != HAL_OK)
    {
      return HAL_BUSY;
    }

    /* Process Locked */
    __HAL_LOCK(hI2C);
    
    /* Disable Pos */
    hI2C->Instance->CR1 &= ~I2C_CR1_POS;

    hI2C->State = HAL_I2C_STATE_BUSY_RX;
    hI2C->Mode = HAL_I2C_MODE_MEM;
    hI2C->ErrorCode = HAL_I2C_ERROR_NONE;

    hI2C->pBuffPtr = pData;
    hI2C->XferSize = Size;
    hI2C->XferCount = Size;
    hI2C->XferOptions = I2C_NO_OPTION_FRAME;
    
    if (hI2C->XferSize > 0U)
    {      
      /* Set the I2C DMA transfer complete callback */
      hI2C->hdmarx->XferCpltCallback = I2C_DMAMemReceiveCplt;
      
      /* Set the DMA error callback */
      hI2C->hdmarx->XferErrorCallback = I2C_DMAError;
      
      /* Enable the DMA Stream */
      HAL_DMA_Start_IT(hI2C->hdmarx, (uint32_t)&hI2C->Instance->DR, (uint32_t)pData, Size);
      
      /* Send Slave Address and Memory Address */
      if (I2C_RequestMemoryRead(hI2C, DevAddress, MemAddress, MemAddSize, I2C_TIMEOUT_FLAG) != HAL_OK)
      {
        if (hI2C->ErrorCode == HAL_I2C_ERROR_AF)
        {
          /* Process Unlocked */
          __HAL_UNLOCK(hI2C);
          return HAL_ERROR;
        }
        else
        {
          /* Process Unlocked */
          __HAL_UNLOCK(hI2C);
          return HAL_TIMEOUT;
        }
      }
      
      if (Size == 1U)
      {
        /* Disable Acknowledge */
        hI2C->Instance->CR1 &= ~I2C_CR1_ACK;
      }
      else
      {
        /* Enable Last DMA bit */
        hI2C->Instance->CR2 |= I2C_CR2_LAST;
      }
      
      /* Enable DMA Request */
      hI2C->Instance->CR2 |= I2C_CR2_DMAEN;
      
      /* Clear ADDR flag */
      __HAL_I2C_CLEAR_ADDRFLAG(hI2C);
      
    }
    else
    {
      /* Send Slave Address and Memory Address */
      if (I2C_RequestMemoryRead(hI2C, DevAddress, MemAddress, MemAddSize, I2C_TIMEOUT_FLAG) != HAL_OK)
      {
        if (hI2C->ErrorCode == HAL_I2C_ERROR_AF)
        {
          /* Process Unlocked */
          __HAL_UNLOCK(hI2C);
          return HAL_ERROR;
        }
        else
        {
          /* Process Unlocked */
          __HAL_UNLOCK(hI2C);
          return HAL_TIMEOUT;
        }
      }
      
      /* Clear ADDR flag */
      __HAL_I2C_CLEAR_ADDRFLAG(hI2C);

      /* Generate Stop */
      hI2C->Instance->CR1 |= I2C_CR1_STOP;

      hI2C->State = HAL_I2C_STATE_READY;
    }
    
    /* Process Unlocked */
    __HAL_UNLOCK(hI2C);

    return HAL_OK;
  }
  else
  {
    return HAL_BUSY;
  }
}

/**
  * @brief  Checks if target device is ready for communication.
  * @note   This function is used with Memory devices
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @param  DevAddress Target device address
  * @param  Trials Number of trials
  * @param  Timeout Timeout duration
  * @retval HAL status
  */
HAL_StatusTypeDef HAL_I2C_IsDeviceReady(I2C_HandleTypeDef *hI2C, uint16_t DevAddress, uint32_t Trials, uint32_t Timeout)
{
  uint32_t tickstart = 0U, tmp1 = 0U, tmp2 = 0U, tmp3 = 0U, I2C_Trials = 1U;

  if (hI2C->State == HAL_I2C_STATE_READY)
  {
    /* Wait until BUSY flag is reset */
    if (I2C_WaitOnFlagUntilTimeout(hI2C, I2C_FLAG_BUSY, SET, I2C_TIMEOUT_BUSY_FLAG) != HAL_OK)
    {
      return HAL_BUSY;
    }

    /* Process Locked */
    __HAL_LOCK(hI2C);
    
    /* Disable Pos */
    hI2C->Instance->CR1 &= ~I2C_CR1_POS;

    hI2C->State = HAL_I2C_STATE_BUSY;
    hI2C->ErrorCode = HAL_I2C_ERROR_NONE;
    hI2C->XferOptions = I2C_NO_OPTION_FRAME;
    
    do
    {
      /* Generate Start */
      hI2C->Instance->CR1 |= I2C_CR1_START;

      /* Wait until SB flag is set */
      if (I2C_WaitOnFlagUntilTimeout(hI2C, I2C_FLAG_SB, RESET, Timeout) != HAL_OK)
      {
        return HAL_TIMEOUT;
      }

      /* Send slave address */
      hI2C->Instance->DR = I2C_7BIT_ADD_WRITE(DevAddress);

      /* Wait until ADDR or AF flag are set */
      /* Get tick */
      tickstart = HAL_GetTick();

      tmp1 = __HAL_I2C_GET_FLAG(hI2C, I2C_FLAG_ADDR);
      tmp2 = __HAL_I2C_GET_FLAG(hI2C, I2C_FLAG_AF);
      tmp3 = hI2C->State;
      while((tmp1 == RESET) && (tmp2 == RESET) && (tmp3 != HAL_I2C_STATE_TIMEOUT))
      {
        if ((Timeout == 0U)||((HAL_GetTick() - tickstart ) > Timeout))
        {
          hI2C->State = HAL_I2C_STATE_TIMEOUT;
        }
        tmp1 = __HAL_I2C_GET_FLAG(hI2C, I2C_FLAG_ADDR);
        tmp2 = __HAL_I2C_GET_FLAG(hI2C, I2C_FLAG_AF);
        tmp3 = hI2C->State;
      }

      hI2C->State = HAL_I2C_STATE_READY;

      /* Check if the ADDR flag has been set */
      if (__HAL_I2C_GET_FLAG(hI2C, I2C_FLAG_ADDR) == SET)
      {
        /* Generate Stop */
        hI2C->Instance->CR1 |= I2C_CR1_STOP;

        /* Clear ADDR Flag */
        __HAL_I2C_CLEAR_ADDRFLAG(hI2C);

        /* Wait until BUSY flag is reset */
        if (I2C_WaitOnFlagUntilTimeout(hI2C, I2C_FLAG_BUSY, SET, I2C_TIMEOUT_BUSY_FLAG) != HAL_OK)
        {
          return HAL_TIMEOUT;
        }

        hI2C->State = HAL_I2C_STATE_READY;

        /* Process Unlocked */
        __HAL_UNLOCK(hI2C);

        return HAL_OK;
      }
      else
      {
        /* Generate Stop */
        hI2C->Instance->CR1 |= I2C_CR1_STOP;

        /* Clear AF Flag */
        __HAL_I2C_CLEAR_FLAG(hI2C, I2C_FLAG_AF);

        /* Wait until BUSY flag is reset */
        if (I2C_WaitOnFlagUntilTimeout(hI2C, I2C_FLAG_BUSY, SET, I2C_TIMEOUT_BUSY_FLAG) != HAL_OK)
        {
          return HAL_TIMEOUT;
        }
      }
    }while(I2C_Trials++ < Trials);

    hI2C->State = HAL_I2C_STATE_READY;

    /* Process Unlocked */
    __HAL_UNLOCK(hI2C);

    return HAL_ERROR;
  }
  else
  {
    return HAL_BUSY;
  }
}

/**
  * @brief  This function handles I2C event interrupt request.
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @retval HAL status
  */
void HAL_I2C_EV_IRQHandler(I2C_HandleTypeDef *hI2C)
{
  uint32_t tmp1 = 0U, tmp2 = 0U, tmp3 = 0U, tmp4 = 0U;
  /* Master or Memory mode selected */
  if ((hI2C->Mode == HAL_I2C_MODE_MASTER) || \
     (hI2C->Mode == HAL_I2C_MODE_MEM))
  {
    /* I2C in mode Transmitter -----------------------------------------------*/
    if (__HAL_I2C_GET_FLAG(hI2C, I2C_FLAG_TRA) == SET)
    {
      tmp1 = __HAL_I2C_GET_FLAG(hI2C, I2C_FLAG_TXE);
      tmp2 = __HAL_I2C_GET_IT_SOURCE(hI2C, I2C_IT_BUF);
      tmp3 = __HAL_I2C_GET_FLAG(hI2C, I2C_FLAG_BTF);
      tmp4 = __HAL_I2C_GET_IT_SOURCE(hI2C, I2C_IT_EVT);
      /* TXE set and BTF reset -----------------------------------------------*/
      if ((tmp1 == SET) && (tmp2 == SET) && (tmp3 == RESET))
      {
        I2C_MasterTransmit_TXE(hI2C);
      }
      /* BTF set -------------------------------------------------------------*/
      else if ((tmp3 == SET) && (tmp4 == SET))
      {
        I2C_MasterTransmit_BTF(hI2C);
      }
    }
    /* I2C in mode Receiver --------------------------------------------------*/
    else
    {
      tmp1 = __HAL_I2C_GET_FLAG(hI2C, I2C_FLAG_RXNE);
      tmp2 = __HAL_I2C_GET_IT_SOURCE(hI2C, I2C_IT_BUF);
      tmp3 = __HAL_I2C_GET_FLAG(hI2C, I2C_FLAG_BTF);
      tmp4 = __HAL_I2C_GET_IT_SOURCE(hI2C, I2C_IT_EVT);
      /* RXNE set and BTF reset -----------------------------------------------*/
      if ((tmp1 == SET) && (tmp2 == SET) && (tmp3 == RESET))
      {
        I2C_MasterReceive_RXNE(hI2C);
      }
      /* BTF set -------------------------------------------------------------*/
      else if ((tmp3 == SET) && (tmp4 == SET))
      {
        I2C_MasterReceive_BTF(hI2C);
      }
    }
  }
  /* Slave mode selected */
  else
  {
    tmp1 = __HAL_I2C_GET_FLAG(hI2C, I2C_FLAG_ADDR);
    tmp2 = __HAL_I2C_GET_IT_SOURCE(hI2C, (I2C_IT_EVT));
    tmp3 = __HAL_I2C_GET_FLAG(hI2C, I2C_FLAG_STOPF);
    tmp4 = __HAL_I2C_GET_FLAG(hI2C, I2C_FLAG_TRA);
    /* ADDR set --------------------------------------------------------------*/
    if ((tmp1 == SET) && (tmp2 == SET))
    {
      I2C_Slave_ADDR(hI2C);
    }
    /* STOPF set --------------------------------------------------------------*/
    else if ((tmp3 == SET) && (tmp2 == SET))
    {
      I2C_Slave_STOPF(hI2C);
    }
    /* I2C in mode Transmitter -----------------------------------------------*/
    else if (tmp4 == SET)
    {
      tmp1 = __HAL_I2C_GET_FLAG(hI2C, I2C_FLAG_TXE);
      tmp2 = __HAL_I2C_GET_IT_SOURCE(hI2C, I2C_IT_BUF);
      tmp3 = __HAL_I2C_GET_FLAG(hI2C, I2C_FLAG_BTF);
      tmp4 = __HAL_I2C_GET_IT_SOURCE(hI2C, I2C_IT_EVT);
      /* TXE set and BTF reset -----------------------------------------------*/
      if ((tmp1 == SET) && (tmp2 == SET) && (tmp3 == RESET))
      {
        I2C_SlaveTransmit_TXE(hI2C);
      }
      /* BTF set -------------------------------------------------------------*/
      else if ((tmp3 == SET) && (tmp4 == SET))
      {
        I2C_SlaveTransmit_BTF(hI2C);
      }
    }
    /* I2C in mode Receiver --------------------------------------------------*/
    else
    {
      tmp1 = __HAL_I2C_GET_FLAG(hI2C, I2C_FLAG_RXNE);
      tmp2 = __HAL_I2C_GET_IT_SOURCE(hI2C, I2C_IT_BUF);
      tmp3 = __HAL_I2C_GET_FLAG(hI2C, I2C_FLAG_BTF);
      tmp4 = __HAL_I2C_GET_IT_SOURCE(hI2C, I2C_IT_EVT);
      /* RXNE set and BTF reset ----------------------------------------------*/
      if ((tmp1 == SET) && (tmp2 == SET) && (tmp3 == RESET))
      {
        I2C_SlaveReceive_RXNE(hI2C);
      }
      /* BTF set -------------------------------------------------------------*/
      else if ((tmp3 == SET) && (tmp4 == SET))
      {
        I2C_SlaveReceive_BTF(hI2C);
      }
    }
  }
}

/**
  * @brief  This function handles I2C error interrupt request.
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @retval HAL status
  */
void HAL_I2C_ER_IRQHandler(I2C_HandleTypeDef *hI2C)
{
  uint32_t tmp1 = 0U, tmp2 = 0U, tmp3 = 0U, tmp4 = 0U;

  tmp1 = __HAL_I2C_GET_FLAG(hI2C, I2C_FLAG_BERR);
  tmp2 = __HAL_I2C_GET_IT_SOURCE(hI2C, I2C_IT_ERR);
  /* I2C Bus error interrupt occurred ----------------------------------------*/
  if ((tmp1 == SET) && (tmp2 == SET))
  {
    hI2C->ErrorCode |= HAL_I2C_ERROR_BERR;

    /* Clear BERR flag */
    __HAL_I2C_CLEAR_FLAG(hI2C, I2C_FLAG_BERR);
  }

  tmp1 = __HAL_I2C_GET_FLAG(hI2C, I2C_FLAG_ARLO);
  tmp2 = __HAL_I2C_GET_IT_SOURCE(hI2C, I2C_IT_ERR);
  /* I2C Arbitration Loss error interrupt occurred ---------------------------*/
  if ((tmp1 == SET) && (tmp2 == SET))
  {
    hI2C->ErrorCode |= HAL_I2C_ERROR_ARLO;

    /* Clear ARLO flag */
    __HAL_I2C_CLEAR_FLAG(hI2C, I2C_FLAG_ARLO);
  }

  tmp1 = __HAL_I2C_GET_FLAG(hI2C, I2C_FLAG_AF);
  tmp2 = __HAL_I2C_GET_IT_SOURCE(hI2C, I2C_IT_ERR);
  /* I2C Acknowledge failure error interrupt occurred ------------------------*/
  if ((tmp1 == SET) && (tmp2 == SET))
  {
    tmp1 = hI2C->Mode;
    tmp2 = hI2C->XferCount;
    tmp3 = hI2C->State;
    tmp4 = hI2C->PreviousState;
    if ((tmp1 == HAL_I2C_MODE_SLAVE) && (tmp2 == 0U) && \
      ((tmp3 == HAL_I2C_STATE_BUSY_TX) || (tmp3 == HAL_I2C_STATE_BUSY_TX_LISTEN) || \
      ((tmp3 == HAL_I2C_STATE_LISTEN) && (tmp4 == I2C_STATE_SLAVE_BUSY_TX))))
    {
      I2C_Slave_AF(hI2C);
    }
    else
    {
      hI2C->ErrorCode |= HAL_I2C_ERROR_AF;

      /* Generate Stop */
      SET_BIT(hI2C->Instance->CR1,I2C_CR1_STOP);

      /* Clear AF flag */
      __HAL_I2C_CLEAR_FLAG(hI2C, I2C_FLAG_AF);
    }
  }

  tmp1 = __HAL_I2C_GET_FLAG(hI2C, I2C_FLAG_OVR);
  tmp2 = __HAL_I2C_GET_IT_SOURCE(hI2C, I2C_IT_ERR);
  /* I2C Over-Run/Under-Run interrupt occurred -------------------------------*/
  if ((tmp1 == SET) && (tmp2 == SET))
  {
    hI2C->ErrorCode |= HAL_I2C_ERROR_OVR;
    /* Clear OVR flag */
    __HAL_I2C_CLEAR_FLAG(hI2C, I2C_FLAG_OVR);
  }

  if (hI2C->ErrorCode != HAL_I2C_ERROR_NONE)
  {
    if ((hI2C->State == HAL_I2C_STATE_BUSY_TX_LISTEN) || (hI2C->State == HAL_I2C_STATE_BUSY_RX_LISTEN))
    {
      /* keep HAL_I2C_STATE_LISTEN */
      hI2C->PreviousState = I2C_STATE_NONE;
      hI2C->State = HAL_I2C_STATE_LISTEN;
    }
    else
    {
      hI2C->PreviousState = I2C_STATE_NONE;
      hI2C->State = HAL_I2C_STATE_READY;
    }
    
    /* Disable Pos bit in I2C CR1 when error occurred in Master/Mem Receive IT Process */
    hI2C->Instance->CR1 &= ~I2C_CR1_POS;
    
    HAL_I2C_ErrorCallback(hI2C);
    
    /* STOP Flag is not set after a NACK reception */
    /* So may inform upper layer that listen phase is stopped */
    /* during NACK error treatment */
    if ((hI2C->State == HAL_I2C_STATE_LISTEN) && ((hI2C->ErrorCode & HAL_I2C_ERROR_AF) == HAL_I2C_ERROR_AF))
    {
      hI2C->XferOptions = I2C_NO_OPTION_FRAME;
      hI2C->PreviousState = I2C_STATE_NONE;
      hI2C->State = HAL_I2C_STATE_READY;
      hI2C->Mode = HAL_I2C_MODE_NONE;
      
      /* Call the Listen Complete callback, to inform upper layer of the end of Listen usecase */
      HAL_I2C_ListenCpltCallback(hI2C);
    }
  }
}

/**
  * @brief  Master Tx Transfer completed callbacks.
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @retval None
  */
__weak void HAL_I2C_MasterTxCpltCallback(I2C_HandleTypeDef *hI2C)
{
  /* Prevent unused argument(s) compilation warning */
  UNUSED(hI2C);
  /* NOTE : This function should not be modified, when the callback is needed,
            the HAL_I2C_MasterTxCpltCallback can be implemented in the user file
   */
}

/**
  * @brief  Master Rx Transfer completed callbacks.
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @retval None
  */
__weak void HAL_I2C_MasterRxCpltCallback(I2C_HandleTypeDef *hI2C)
{
  /* Prevent unused argument(s) compilation warning */
  UNUSED(hI2C);
  /* NOTE : This function should not be modified, when the callback is needed,
            the HAL_I2C_MasterRxCpltCallback can be implemented in the user file
   */
}

/** @brief  Slave Tx Transfer completed callbacks.
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @retval None
  */
__weak void HAL_I2C_SlaveTxCpltCallback(I2C_HandleTypeDef *hI2C)
{
  /* Prevent unused argument(s) compilation warning */
  UNUSED(hI2C);
  /* NOTE : This function should not be modified, when the callback is needed,
            the HAL_I2C_SlaveTxCpltCallback can be implemented in the user file
   */
}

/**
  * @brief  Slave Rx Transfer completed callbacks.
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @retval None
  */
__weak void HAL_I2C_SlaveRxCpltCallback(I2C_HandleTypeDef *hI2C)
{
  /* Prevent unused argument(s) compilation warning */
  UNUSED(hI2C);
  /* NOTE : This function should not be modified, when the callback is needed,
            the HAL_I2C_SlaveRxCpltCallback can be implemented in the user file
   */
}

/**
  * @brief  Slave Address Match callback.
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *                the configuration information for the specified I2C.
  * @param  TransferDirection Master request Transfer Direction (Write/Read), value of @ref I2C_XferOptions_definition
  * @param  AddrMatchCode Address Match Code
  * @retval None
  */
__weak void HAL_I2C_AddrCallback(I2C_HandleTypeDef *hI2C, uint8_t TransferDirection, uint16_t AddrMatchCode)
{
  /* Prevent unused argument(s) compilation warning */
  UNUSED(hI2C);
  UNUSED(TransferDirection);
  UNUSED(AddrMatchCode);
  /* NOTE : This function should not be modified, when the callback is needed,
            the HAL_I2C_AddrCallback can be implemented in the user file
   */
}

/**
  * @brief  Listen Complete callback.
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *                the configuration information for the specified I2C.
  * @retval None
  */
__weak void HAL_I2C_ListenCpltCallback(I2C_HandleTypeDef *hI2C)
{
  /* Prevent unused argument(s) compilation warning */
  UNUSED(hI2C);
    /* NOTE : This function should not be modified, when the callback is needed,
            the HAL_I2C_ListenCpltCallback can be implemented in the user file
   */
}

/**
  * @brief  Memory Tx Transfer completed callbacks.
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @retval None
  */
__weak void HAL_I2C_MemTxCpltCallback(I2C_HandleTypeDef *hI2C)
{
  /* Prevent unused argument(s) compilation warning */
  UNUSED(hI2C);
  /* NOTE : This function should not be modified, when the callback is needed,
            the HAL_I2C_MemTxCpltCallback can be implemented in the user file
   */
}

/**
  * @brief  Memory Rx Transfer completed callbacks.
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @retval None
  */
__weak void HAL_I2C_MemRxCpltCallback(I2C_HandleTypeDef *hI2C)
{
  /* Prevent unused argument(s) compilation warning */
  UNUSED(hI2C);
  /* NOTE : This function should not be modified, when the callback is needed,
            the HAL_I2C_MemRxCpltCallback can be implemented in the user file
   */
}

/**
  * @brief  I2C error callbacks.
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @retval None
  */
__weak void HAL_I2C_ErrorCallback(I2C_HandleTypeDef *hI2C)
{
  /* Prevent unused argument(s) compilation warning */
  UNUSED(hI2C);
  /* NOTE : This function should not be modified, when the callback is needed,
            the HAL_I2C_ErrorCallback can be implemented in the user file
   */
}

/**
  * @}
  */

/** @defgroup I2C_Exported_Functions_Group3 Peripheral State and Errors functions
  *  @brief   Peripheral State and Errors functions
  *
@verbatim
 ===============================================================================
            ##### Peripheral State, Mode and Errors functions #####
 ===============================================================================
    [..]
    This subsection permits to get in run-time the status of the peripheral
    and the data flow.

@endverbatim
  * @{
  */

/**
  * @brief  Returns the I2C state.
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @retval HAL state
  */
HAL_I2C_StateTypeDef HAL_I2C_GetState(I2C_HandleTypeDef *hI2C)
{
  return hI2C->State;
}

/**
  * @brief  Returns the I2C Master, Slave, Memory or no mode.
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @retval HAL mode
  */
HAL_I2C_ModeTypeDef HAL_I2C_GetMode(I2C_HandleTypeDef *hI2C)
{
  return hI2C->Mode;
}

/**
  * @brief  Return the I2C error code
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *              the configuration information for the specified I2C.
  * @retval I2C Error Code
  */
uint32_t HAL_I2C_GetError(I2C_HandleTypeDef *hI2C)
{
  return hI2C->ErrorCode;
}

/**
  * @}
  */

/**
  * @brief  Handle TXE flag for Master
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @retval HAL status
  */
static HAL_StatusTypeDef I2C_MasterTransmit_TXE(I2C_HandleTypeDef *hI2C)
{
  if (hI2C->XferCount == 0U)
  {
    /* Disable BUF interrupt */
    __HAL_I2C_DISABLE_IT(hI2C, I2C_IT_BUF);
  }
  else
  {
    /* Write data to DR */
    hI2C->Instance->DR = (*hI2C->pBuffPtr++);
    hI2C->XferCount--;
  }
  
  return HAL_OK;
}

/**
  * @brief  Handle BTF flag for Master transmitter
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @retval HAL status
  */
static HAL_StatusTypeDef I2C_MasterTransmit_BTF(I2C_HandleTypeDef *hI2C)
{
  /* Declaration of tmp to prevent undefined behavior of volatile usage */
  uint32_t tmp;

  if (hI2C->XferCount != 0U)
  {
    /* Write data to DR */
    hI2C->Instance->DR = (*hI2C->pBuffPtr++);
    hI2C->XferCount--;
  }
  else
  {
    /* Call TxCpltCallback() directly if no stop mode is set */
    if ((hI2C->XferOptions != I2C_FIRST_AND_LAST_FRAME) && (hI2C->XferOptions != I2C_LAST_FRAME) && (hI2C->XferOptions != I2C_NO_OPTION_FRAME))
    {
      __HAL_I2C_DISABLE_IT(hI2C, I2C_IT_EVT | I2C_IT_BUF | I2C_IT_ERR);
      
      tmp = (uint32_t)(hI2C->State) & I2C_STATE_MSK;
      hI2C->PreviousState = tmp | (uint32_t)(hI2C->Mode);
      hI2C->Mode = HAL_I2C_MODE_NONE;
      hI2C->State = HAL_I2C_STATE_READY;

      HAL_I2C_MasterTxCpltCallback(hI2C);
    }
    else /* Generate Stop condition then Call TxCpltCallback() */
    {
      /* Disable EVT, BUF and ERR interrupt */
      __HAL_I2C_DISABLE_IT(hI2C, I2C_IT_EVT | I2C_IT_BUF | I2C_IT_ERR);

      /* Generate Stop */
      hI2C->Instance->CR1 |= I2C_CR1_STOP;

      if (hI2C->Mode == HAL_I2C_MODE_MEM)
      {
        hI2C->PreviousState = I2C_STATE_NONE;
        hI2C->State = HAL_I2C_STATE_READY;
        hI2C->Mode = HAL_I2C_MODE_NONE;

        HAL_I2C_MemTxCpltCallback(hI2C);
      }
      else
      {
        hI2C->PreviousState = I2C_STATE_NONE;
        hI2C->State = HAL_I2C_STATE_READY;
        hI2C->Mode = HAL_I2C_MODE_NONE;

        HAL_I2C_MasterTxCpltCallback(hI2C);
      }
    }
  }
  return HAL_OK;
}

/**
  * @brief  Handle RXNE flag for Master
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @retval HAL status
  */
static HAL_StatusTypeDef I2C_MasterReceive_RXNE(I2C_HandleTypeDef *hI2C)
{
  uint32_t tmp = 0U;

  tmp = hI2C->XferCount;
  if (tmp > 3U)
  {
    /* Read data from DR */
    (*hI2C->pBuffPtr++) = hI2C->Instance->DR;
    hI2C->XferCount--;
  }
  else if ((tmp == 2U) || (tmp == 3U))
  {
    if (hI2C->XferOptions != I2C_NEXT_FRAME)
    {
      /* Disable Acknowledge */
      hI2C->Instance->CR1 &= ~I2C_CR1_ACK;

      /* Enable Pos */
      hI2C->Instance->CR1 |= I2C_CR1_POS;
    }
    else
    {
      /* Enable Acknowledge */
      hI2C->Instance->CR1 |= I2C_CR1_ACK;
    }

    /* Disable BUF interrupt */
    __HAL_I2C_DISABLE_IT(hI2C, I2C_IT_BUF);
  }
  else
  {
    if (hI2C->XferOptions != I2C_NEXT_FRAME)
    {
      /* Disable Acknowledge */
      hI2C->Instance->CR1 &= ~I2C_CR1_ACK;
    }
    else
    {
      /* Enable Acknowledge */
      hI2C->Instance->CR1 |= I2C_CR1_ACK;
    }

    /* Disable EVT, BUF and ERR interrupt */
    __HAL_I2C_DISABLE_IT(hI2C, I2C_IT_EVT | I2C_IT_BUF | I2C_IT_ERR);

    /* Read data from DR */
    (*hI2C->pBuffPtr++) = hI2C->Instance->DR;
    hI2C->XferCount--;

    if (hI2C->Mode == HAL_I2C_MODE_MEM)
    {
      tmp = (uint32_t)(hI2C->State) & I2C_STATE_MSK;
      hI2C->PreviousState = tmp | (uint32_t)(hI2C->Mode);
      hI2C->State = HAL_I2C_STATE_READY;
      hI2C->Mode = HAL_I2C_MODE_NONE;

      HAL_I2C_MemRxCpltCallback(hI2C);
    }
    else
    {
      tmp = (uint32_t)(hI2C->State) & I2C_STATE_MSK;
      hI2C->PreviousState = tmp | (uint32_t)(hI2C->Mode);
      hI2C->State = HAL_I2C_STATE_READY;
      hI2C->Mode = HAL_I2C_MODE_NONE;

      HAL_I2C_MasterRxCpltCallback(hI2C);
    }
  }
  return HAL_OK;
}

/**
  * @brief  Handle BTF flag for Master receiver
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @retval HAL status
  */
static HAL_StatusTypeDef I2C_MasterReceive_BTF(I2C_HandleTypeDef *hI2C)
{
  /* Declaration of tmp to prevent undefined behavior of volatile usage */
  uint32_t tmp;

  if (hI2C->XferCount == 3U)
  {
    if ((hI2C->XferOptions == I2C_FIRST_AND_LAST_FRAME) || (hI2C->XferOptions == I2C_LAST_FRAME) || (hI2C->XferOptions == I2C_NO_OPTION_FRAME))
    {
      if (hI2C->XferOptions != I2C_NEXT_FRAME)
      {
        /* Disable Acknowledge */
        hI2C->Instance->CR1 &= ~I2C_CR1_ACK;
      }
      else
      {
        /* Enable Acknowledge */
        hI2C->Instance->CR1 |= I2C_CR1_ACK;
      }
    }

    /* Read data from DR */
    (*hI2C->pBuffPtr++) = hI2C->Instance->DR;
    hI2C->XferCount--;
  }
  else if (hI2C->XferCount == 2U)
  {
    /* Prepare next transfer or stop current transfer */
    if ((hI2C->XferOptions != I2C_FIRST_AND_LAST_FRAME) && (hI2C->XferOptions != I2C_LAST_FRAME) && (hI2C->XferOptions != I2C_NO_OPTION_FRAME))
    {
      if (hI2C->XferOptions != I2C_NEXT_FRAME)
      {
        /* Disable Acknowledge */
        hI2C->Instance->CR1 &= ~I2C_CR1_ACK;
      }
      else
      {
        /* Enable Acknowledge */
        hI2C->Instance->CR1 |= I2C_CR1_ACK;
      }
      tmp = (uint32_t)(hI2C->State) & I2C_STATE_MSK;
      hI2C->PreviousState = tmp | (uint32_t)(hI2C->Mode);
    }
    else
    {
      hI2C->PreviousState = I2C_STATE_MASTER_BUSY_RX;

      /* Generate Stop */
      hI2C->Instance->CR1 |= I2C_CR1_STOP;
    }

    /* Read data from DR */
    (*hI2C->pBuffPtr++) = hI2C->Instance->DR;
    hI2C->XferCount--;

    /* Read data from DR */
    (*hI2C->pBuffPtr++) = hI2C->Instance->DR;
    hI2C->XferCount--;

    /* Disable EVT and ERR interrupt */
    __HAL_I2C_DISABLE_IT(hI2C, I2C_IT_EVT | I2C_IT_ERR);

    if (hI2C->Mode == HAL_I2C_MODE_MEM)
    {
      hI2C->State = HAL_I2C_STATE_READY;
      hI2C->Mode = HAL_I2C_MODE_NONE;

      HAL_I2C_MemRxCpltCallback(hI2C);
    }
    else
    {
      hI2C->State = HAL_I2C_STATE_READY;
      hI2C->Mode = HAL_I2C_MODE_NONE;

      HAL_I2C_MasterRxCpltCallback(hI2C);
    }
  }
  else
  {
    /* Read data from DR */
    (*hI2C->pBuffPtr++) = hI2C->Instance->DR;
    hI2C->XferCount--;
  }
  return HAL_OK;
}

/**
  * @brief  Handle TXE flag for Slave
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @retval HAL status
  */
static HAL_StatusTypeDef I2C_SlaveTransmit_TXE(I2C_HandleTypeDef *hI2C)
{
  /* Declaration of tmp to prevent undefined behavior of volatile usage */
  uint32_t tmp;

  if (hI2C->XferCount != 0U)
  {
    /* Write data to DR */
    hI2C->Instance->DR = (*hI2C->pBuffPtr++);
    hI2C->XferCount--;

    if ((hI2C->XferCount == 0U) && (hI2C->State == HAL_I2C_STATE_BUSY_TX_LISTEN))
    {
      /* Last Byte is received, disable Interrupt */
      __HAL_I2C_DISABLE_IT(hI2C, I2C_IT_BUF);
      
      /* Set state at HAL_I2C_STATE_LISTEN */
      tmp = (uint32_t)(hI2C->State) & I2C_STATE_MSK;
      hI2C->PreviousState = tmp | (uint32_t)(hI2C->Mode);
      hI2C->State = HAL_I2C_STATE_LISTEN;
      
      /* Call the Tx complete callback to inform upper layer of the end of receive process */
      HAL_I2C_SlaveTxCpltCallback(hI2C);
    }
  }
  return HAL_OK;
}

/**
  * @brief  Handle BTF flag for Slave transmitter
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @retval HAL status
  */
static HAL_StatusTypeDef I2C_SlaveTransmit_BTF(I2C_HandleTypeDef *hI2C)
{
  if (hI2C->XferCount != 0U)
  {
    /* Write data to DR */
    hI2C->Instance->DR = (*hI2C->pBuffPtr++);
    hI2C->XferCount--;
  }
  return HAL_OK;
}

/**
  * @brief  Handle RXNE flag for Slave
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @retval HAL status
  */
static HAL_StatusTypeDef I2C_SlaveReceive_RXNE(I2C_HandleTypeDef *hI2C)
{
  /* Declaration of tmp to prevent undefined behavior of volatile usage */
  uint32_t tmp;

  if (hI2C->XferCount != 0U)
  {
    /* Read data from DR */
    (*hI2C->pBuffPtr++) = hI2C->Instance->DR;
    hI2C->XferCount--;

    if ((hI2C->XferCount == 0U) && (hI2C->State == HAL_I2C_STATE_BUSY_RX_LISTEN))
    {
      /* Last Byte is received, disable Interrupt */
      __HAL_I2C_DISABLE_IT(hI2C, I2C_IT_BUF);
      
      /* Set state at HAL_I2C_STATE_LISTEN */
      tmp = (uint32_t)(hI2C->State) & I2C_STATE_MSK;
      hI2C->PreviousState = tmp | (uint32_t)(hI2C->Mode);
      hI2C->State = HAL_I2C_STATE_LISTEN;
      
      /* Call the Rx complete callback to inform upper layer of the end of receive process */
      HAL_I2C_SlaveRxCpltCallback(hI2C);
    }
  }
  return HAL_OK;
}

/**
  * @brief  Handle BTF flag for Slave receiver
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @retval HAL status
  */
static HAL_StatusTypeDef I2C_SlaveReceive_BTF(I2C_HandleTypeDef *hI2C)
{
  if (hI2C->XferCount != 0U)
  {
    /* Read data from DR */
    (*hI2C->pBuffPtr++) = hI2C->Instance->DR;
    hI2C->XferCount--;
  }
  return HAL_OK;
}

/**
  * @brief  Handle ADD flag for Slave
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @retval HAL status
  */
static HAL_StatusTypeDef I2C_Slave_ADDR(I2C_HandleTypeDef *hI2C)
{
  uint8_t TransferDirection = I2C_DIRECTION_RECEIVE;
  uint16_t SlaveAddrCode = 0U;

  /* Transfer Direction requested by Master */
  if (__HAL_I2C_GET_FLAG(hI2C, I2C_FLAG_TRA) == RESET)
  {
    TransferDirection = I2C_DIRECTION_TRANSMIT;
  }
  
  if (__HAL_I2C_GET_FLAG(hI2C, I2C_FLAG_DUALF) == RESET)
  {
    SlaveAddrCode = hI2C->Init.OwnAddress1;
  }
  else
  {
    SlaveAddrCode = hI2C->Init.OwnAddress2;
  }

  /* Call Slave Addr callback */
  HAL_I2C_AddrCallback(hI2C, TransferDirection, SlaveAddrCode);

  return HAL_OK;
}

/**
  * @brief  Handle STOPF flag for Slave
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @retval HAL status
  */
static HAL_StatusTypeDef I2C_Slave_STOPF(I2C_HandleTypeDef *hI2C)
{
  /* Disable EVT, BUF and ERR interrupt */
  __HAL_I2C_DISABLE_IT(hI2C, I2C_IT_EVT | I2C_IT_BUF | I2C_IT_ERR);

  /* Clear STOPF flag */
  __HAL_I2C_CLEAR_STOPFLAG(hI2C);

  /* Disable Acknowledge */
  hI2C->Instance->CR1 &= ~I2C_CR1_ACK;

  if ((hI2C->State & HAL_I2C_STATE_LISTEN)== HAL_I2C_STATE_LISTEN)
  {
    hI2C->XferOptions = I2C_NO_OPTION_FRAME;
    hI2C->PreviousState = I2C_STATE_NONE;
    hI2C->State = HAL_I2C_STATE_READY;
    hI2C->Mode = HAL_I2C_MODE_NONE;
    
    /* Call the Listen Complete callback, to inform upper layer of the end of Listen usecase */
    HAL_I2C_ListenCpltCallback(hI2C);
  }
  else
  {
    if ((hI2C->PreviousState  == I2C_STATE_SLAVE_BUSY_RX) || \
       (hI2C->State == HAL_I2C_STATE_BUSY_RX))
    {
      hI2C->PreviousState = I2C_STATE_NONE;
      hI2C->State = HAL_I2C_STATE_READY;
      hI2C->Mode = HAL_I2C_MODE_NONE;
      
      HAL_I2C_SlaveRxCpltCallback(hI2C);
    }
  }

  return HAL_OK;
}

/**
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @retval HAL status
  */
static HAL_StatusTypeDef I2C_Slave_AF(I2C_HandleTypeDef *hI2C)
{
  /* Declaration of tmp to prevent undefined behavior of volatile usage */
  uint32_t tmp;

  if (((hI2C->XferOptions ==  I2C_FIRST_AND_LAST_FRAME) || (hI2C->XferOptions == I2C_LAST_FRAME)) && \
          (hI2C->State == HAL_I2C_STATE_LISTEN))
  {
    hI2C->XferOptions = I2C_NO_OPTION_FRAME;

    /* Disable EVT, BUF and ERR interrupt */
    __HAL_I2C_DISABLE_IT(hI2C, I2C_IT_EVT | I2C_IT_BUF | I2C_IT_ERR);

    /* Clear AF flag */
    __HAL_I2C_CLEAR_FLAG(hI2C, I2C_FLAG_AF);

    /* Disable Acknowledge */
    hI2C->Instance->CR1 &= ~I2C_CR1_ACK;

    hI2C->PreviousState = I2C_STATE_NONE;
    hI2C->State = HAL_I2C_STATE_READY;
    hI2C->Mode = HAL_I2C_MODE_NONE;
      
    /* Call the Listen Complete callback, to inform upper layer of the end of Listen usecase */
    HAL_I2C_ListenCpltCallback(hI2C);
  }
  else if (hI2C->State == HAL_I2C_STATE_BUSY_TX)
  {
    hI2C->XferOptions = I2C_NO_OPTION_FRAME;
    tmp = (uint32_t)(hI2C->State) & I2C_STATE_MSK;
    hI2C->PreviousState = tmp | (uint32_t)(hI2C->Mode);
    hI2C->State = HAL_I2C_STATE_READY;
    hI2C->Mode = HAL_I2C_MODE_NONE;
    
    /* Disable EVT, BUF and ERR interrupt */
    __HAL_I2C_DISABLE_IT(hI2C, I2C_IT_EVT | I2C_IT_BUF | I2C_IT_ERR);

    /* Clear AF flag */
    __HAL_I2C_CLEAR_FLAG(hI2C, I2C_FLAG_AF);

    /* Disable Acknowledge */
    hI2C->Instance->CR1 &= ~I2C_CR1_ACK;

    HAL_I2C_SlaveTxCpltCallback(hI2C);
  }
  else
  {
    /* Clear AF flag only */
    /* State Listen, but XferOptions == FIRST or NEXT */
    __HAL_I2C_CLEAR_FLAG(hI2C, I2C_FLAG_AF);
  }
  
  return HAL_OK;
}

/**
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @param  DevAddress Target device address
  * @param  Timeout Timeout duration
  * @retval HAL status
  */
static HAL_StatusTypeDef I2C_MasterRequestWrite(I2C_HandleTypeDef *hI2C, uint16_t DevAddress, uint32_t Timeout)
{
  /* Generate Start condition if first transfer */
  if ((hI2C->XferOptions == I2C_FIRST_AND_LAST_FRAME) || (hI2C->XferOptions == I2C_FIRST_FRAME) || (hI2C->XferOptions == I2C_NO_OPTION_FRAME))
  {
    /* Generate Start */
    hI2C->Instance->CR1 |= I2C_CR1_START;
  }
  else if (hI2C->PreviousState == I2C_STATE_MASTER_BUSY_RX)
  {

    /* Generate ReStart */
    hI2C->Instance->CR1 |= I2C_CR1_START;
  }

  /* Wait until SB flag is set */
  if (I2C_WaitOnFlagUntilTimeout(hI2C, I2C_FLAG_SB, RESET, Timeout) != HAL_OK)
  {
    return HAL_TIMEOUT;
  }

  if (hI2C->Init.AddressingMode == I2C_ADDRESSINGMODE_7BIT)
  {
    /* Send slave address */
    hI2C->Instance->DR = I2C_7BIT_ADD_WRITE(DevAddress);
  }
  else
  {
    /* Send header of slave address */
    hI2C->Instance->DR = I2C_10BIT_HEADER_WRITE(DevAddress);

    /* Wait until ADD10 flag is set */
    if (I2C_WaitOnMasterAddressFlagUntilTimeout(hI2C, I2C_FLAG_ADD10, Timeout) != HAL_OK)
    {
      if (hI2C->ErrorCode == HAL_I2C_ERROR_AF)
      {
        return HAL_ERROR;
      }
      else
      {
        return HAL_TIMEOUT;
      }
    }

    /* Send slave address */
    hI2C->Instance->DR = I2C_10BIT_ADDRESS(DevAddress);
  }

  /* Wait until ADDR flag is set */
  if (I2C_WaitOnMasterAddressFlagUntilTimeout(hI2C, I2C_FLAG_ADDR, Timeout) != HAL_OK)
  {
    if (hI2C->ErrorCode == HAL_I2C_ERROR_AF)
    {
      return HAL_ERROR;
    }
    else
    {
      return HAL_TIMEOUT;
    }
  }

  return HAL_OK;
}

/**
  * @brief  Master sends target device address for read request.
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @param  DevAddress Target device address
  * @param  Timeout Timeout duration
  * @retval HAL status
  */
static HAL_StatusTypeDef I2C_MasterRequestRead(I2C_HandleTypeDef *hI2C, uint16_t DevAddress, uint32_t Timeout)
{
  /* Generate Start condition if first transfer */
  if ((hI2C->XferOptions == I2C_FIRST_AND_LAST_FRAME) || (hI2C->XferOptions == I2C_FIRST_FRAME)  || (hI2C->XferOptions == I2C_NO_OPTION_FRAME))
  {
    /* Enable Acknowledge */
    hI2C->Instance->CR1 |= I2C_CR1_ACK;

    /* Generate Start */
    hI2C->Instance->CR1 |= I2C_CR1_START;
  }
  else if (hI2C->PreviousState == I2C_STATE_MASTER_BUSY_TX)
  {
    /* Enable Acknowledge */
    hI2C->Instance->CR1 |= I2C_CR1_ACK;

    /* Generate ReStart */
    hI2C->Instance->CR1 |= I2C_CR1_START;
  }

  /* Wait until SB flag is set */
  if (I2C_WaitOnFlagUntilTimeout(hI2C, I2C_FLAG_SB, RESET, Timeout) != HAL_OK)
  {
    return HAL_TIMEOUT;
  }

  if (hI2C->Init.AddressingMode == I2C_ADDRESSINGMODE_7BIT)
  {
    /* Send slave address */
    hI2C->Instance->DR = I2C_7BIT_ADD_READ(DevAddress);
  }
  else
  {
    /* Send header of slave address */
    hI2C->Instance->DR = I2C_10BIT_HEADER_WRITE(DevAddress);

    /* Wait until ADD10 flag is set */
    if (I2C_WaitOnMasterAddressFlagUntilTimeout(hI2C, I2C_FLAG_ADD10, Timeout) != HAL_OK)
    {
      if (hI2C->ErrorCode == HAL_I2C_ERROR_AF)
      {
        return HAL_ERROR;
      }
      else
      {
        return HAL_TIMEOUT;
      }
    }

    /* Send slave address */
    hI2C->Instance->DR = I2C_10BIT_ADDRESS(DevAddress);

    /* Wait until ADDR flag is set */
    if (I2C_WaitOnMasterAddressFlagUntilTimeout(hI2C, I2C_FLAG_ADDR, Timeout) != HAL_OK)
    {
      if (hI2C->ErrorCode == HAL_I2C_ERROR_AF)
      {
        return HAL_ERROR;
      }
      else
      {
        return HAL_TIMEOUT;
      }
    }

    /* Clear ADDR flag */
    __HAL_I2C_CLEAR_ADDRFLAG(hI2C);

    /* Generate Restart */
    hI2C->Instance->CR1 |= I2C_CR1_START;

    /* Wait until SB flag is set */
    if (I2C_WaitOnFlagUntilTimeout(hI2C, I2C_FLAG_SB, RESET, Timeout) != HAL_OK)
    {
      return HAL_TIMEOUT;
    }

    /* Send header of slave address */
    hI2C->Instance->DR = I2C_10BIT_HEADER_READ(DevAddress);
  }

  /* Wait until ADDR flag is set */
  if (I2C_WaitOnMasterAddressFlagUntilTimeout(hI2C, I2C_FLAG_ADDR, Timeout) != HAL_OK)
  {
    if (hI2C->ErrorCode == HAL_I2C_ERROR_AF)
    {
      return HAL_ERROR;
    }
    else
    {
      return HAL_TIMEOUT;
    }
  }

  return HAL_OK;
}

/**
  * @brief  Master sends target device address followed by internal memory address for write request.
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @param  DevAddress Target device address
  * @param  MemAddress Internal memory address
  * @param  MemAddSize Size of internal memory address
  * @param  Timeout Timeout duration
  * @retval HAL status
  */
static HAL_StatusTypeDef I2C_RequestMemoryWrite(I2C_HandleTypeDef *hI2C, uint16_t DevAddress, uint16_t MemAddress, uint16_t MemAddSize, uint32_t Timeout)
{
  /* Generate Start */
  hI2C->Instance->CR1 |= I2C_CR1_START;

  /* Wait until SB flag is set */
  if (I2C_WaitOnFlagUntilTimeout(hI2C, I2C_FLAG_SB, RESET, Timeout) != HAL_OK)
  {
    return HAL_TIMEOUT;
  }

  /* Send slave address */
  hI2C->Instance->DR = I2C_7BIT_ADD_WRITE(DevAddress);

  /* Wait until ADDR flag is set */
  if (I2C_WaitOnMasterAddressFlagUntilTimeout(hI2C, I2C_FLAG_ADDR, Timeout) != HAL_OK)
  {
    if (hI2C->ErrorCode == HAL_I2C_ERROR_AF)
    {
      return HAL_ERROR;
    }
    else
    {
      return HAL_TIMEOUT;
    }
  }

  /* Clear ADDR flag */
  __HAL_I2C_CLEAR_ADDRFLAG(hI2C);

  /* Wait until TXE flag is set */
  if (I2C_WaitOnTXEFlagUntilTimeout(hI2C, Timeout) != HAL_OK)
  {
    if (hI2C->ErrorCode == HAL_I2C_ERROR_AF)
    {
      /* Generate Stop */
      hI2C->Instance->CR1 |= I2C_CR1_STOP;
      return HAL_ERROR;
    }
    else
    {
      return HAL_TIMEOUT;
    }
  }

  /* If Memory address size is 8Bit */
  if (MemAddSize == I2C_MEMADD_SIZE_8BIT)
  {
    /* Send Memory Address */
    hI2C->Instance->DR = I2C_MEM_ADD_LSB(MemAddress);
  }
  /* If Memory address size is 16Bit */
  else
  {
    /* Send MSB of Memory Address */
    hI2C->Instance->DR = I2C_MEM_ADD_MSB(MemAddress);

    /* Wait until TXE flag is set */
    if (I2C_WaitOnTXEFlagUntilTimeout(hI2C, Timeout) != HAL_OK)
    {
      if (hI2C->ErrorCode == HAL_I2C_ERROR_AF)
      {
        /* Generate Stop */
        hI2C->Instance->CR1 |= I2C_CR1_STOP;
        return HAL_ERROR;
      }
      else
      {
        return HAL_TIMEOUT;
      }
    }

    /* Send LSB of Memory Address */
    hI2C->Instance->DR = I2C_MEM_ADD_LSB(MemAddress);
  }

  return HAL_OK;
}

/**
  * @brief  Master sends target device address followed by internal memory address for read request.
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @param  DevAddress Target device address
  * @param  MemAddress Internal memory address
  * @param  MemAddSize Size of internal memory address
  * @param  Timeout Timeout duration
  * @retval HAL status
  */
static HAL_StatusTypeDef I2C_RequestMemoryRead(I2C_HandleTypeDef *hI2C, uint16_t DevAddress, uint16_t MemAddress, uint16_t MemAddSize, uint32_t Timeout)
{
  /* Enable Acknowledge */
  hI2C->Instance->CR1 |= I2C_CR1_ACK;

  /* Generate Start */
  hI2C->Instance->CR1 |= I2C_CR1_START;

  /* Wait until SB flag is set */
  if (I2C_WaitOnFlagUntilTimeout(hI2C, I2C_FLAG_SB, RESET, Timeout) != HAL_OK)
  {
    return HAL_TIMEOUT;
  }

  /* Send slave address */
  hI2C->Instance->DR = I2C_7BIT_ADD_WRITE(DevAddress);

  /* Wait until ADDR flag is set */
  if (I2C_WaitOnMasterAddressFlagUntilTimeout(hI2C, I2C_FLAG_ADDR, Timeout) != HAL_OK)
  {
    if (hI2C->ErrorCode == HAL_I2C_ERROR_AF)
    {
      return HAL_ERROR;
    }
    else
    {
      return HAL_TIMEOUT;
    }
  }

  /* Clear ADDR flag */
  __HAL_I2C_CLEAR_ADDRFLAG(hI2C);

  /* Wait until TXE flag is set */
  if (I2C_WaitOnTXEFlagUntilTimeout(hI2C, Timeout) != HAL_OK)
  {
    if (hI2C->ErrorCode == HAL_I2C_ERROR_AF)
    {
      /* Generate Stop */
      hI2C->Instance->CR1 |= I2C_CR1_STOP;
      return HAL_ERROR;
    }
    else
    {
      return HAL_TIMEOUT;
    }
  }

  /* If Memory address size is 8Bit */
  if (MemAddSize == I2C_MEMADD_SIZE_8BIT)
  {
    /* Send Memory Address */
    hI2C->Instance->DR = I2C_MEM_ADD_LSB(MemAddress);
  }
  /* If Memory address size is 16Bit */
  else
  {
    /* Send MSB of Memory Address */
    hI2C->Instance->DR = I2C_MEM_ADD_MSB(MemAddress);

    /* Wait until TXE flag is set */
    if (I2C_WaitOnTXEFlagUntilTimeout(hI2C, Timeout) != HAL_OK)
    {
      if (hI2C->ErrorCode == HAL_I2C_ERROR_AF)
      {
        /* Generate Stop */
        hI2C->Instance->CR1 |= I2C_CR1_STOP;
        return HAL_ERROR;
      }
      else
      {
        return HAL_TIMEOUT;
      }
    }

    /* Send LSB of Memory Address */
    hI2C->Instance->DR = I2C_MEM_ADD_LSB(MemAddress);
  }

  /* Wait until TXE flag is set */
  if (I2C_WaitOnTXEFlagUntilTimeout(hI2C, Timeout) != HAL_OK)
  {
    if (hI2C->ErrorCode == HAL_I2C_ERROR_AF)
    {
      /* Generate Stop */
      hI2C->Instance->CR1 |= I2C_CR1_STOP;
      return HAL_ERROR;
    }
    else
    {
      return HAL_TIMEOUT;
    }
  }

  /* Generate Restart */
  hI2C->Instance->CR1 |= I2C_CR1_START;

  /* Wait until SB flag is set */
  if (I2C_WaitOnFlagUntilTimeout(hI2C, I2C_FLAG_SB, RESET, Timeout) != HAL_OK)
  {
    return HAL_TIMEOUT;
  }

  /* Send slave address */
  hI2C->Instance->DR = I2C_7BIT_ADD_READ(DevAddress);

  /* Wait until ADDR flag is set */
  if (I2C_WaitOnMasterAddressFlagUntilTimeout(hI2C, I2C_FLAG_ADDR, Timeout) != HAL_OK)
  {
    if (hI2C->ErrorCode == HAL_I2C_ERROR_AF)
    {
      return HAL_ERROR;
    }
    else
    {
      return HAL_TIMEOUT;
    }
  }

  return HAL_OK;
}

/**
  * @brief  DMA I2C master transmit process complete callback.
  * @param  hdma DMA handle
  * @retval None
  */
static void I2C_DMAMasterTransmitCplt(DMA_HandleTypeDef *hdma)
{
  I2C_HandleTypeDef* hI2C = (I2C_HandleTypeDef*)((DMA_HandleTypeDef*)hdma)->Parent;

  /* Wait until BTF flag is reset */
  if (I2C_WaitOnFlagUntilTimeout(hI2C, I2C_FLAG_BTF, RESET, I2C_TIMEOUT_FLAG) != HAL_OK)
  {
    hI2C->ErrorCode |= HAL_I2C_ERROR_TIMEOUT;
  }

  /* Generate Stop */
  hI2C->Instance->CR1 |= I2C_CR1_STOP;

  /* Disable DMA Request */
  hI2C->Instance->CR2 &= ~I2C_CR2_DMAEN;

  hI2C->XferCount = 0U;

  hI2C->State = HAL_I2C_STATE_READY;
  hI2C->Mode = HAL_I2C_MODE_NONE;
  
  /* Check if Errors has been detected during transfer */
  if (hI2C->ErrorCode != HAL_I2C_ERROR_NONE)
  {
    HAL_I2C_ErrorCallback(hI2C);
  }
  else
  {
    HAL_I2C_MasterTxCpltCallback(hI2C);
  }
}

/**
  * @brief  DMA I2C slave transmit process complete callback.
  * @param  hdma DMA handle
  * @retval None
  */
static void I2C_DMASlaveTransmitCplt(DMA_HandleTypeDef *hdma)
{
  I2C_HandleTypeDef* hI2C = (I2C_HandleTypeDef*)((DMA_HandleTypeDef*)hdma)->Parent;

  /* Wait until AF flag is reset */
  if (I2C_WaitOnFlagUntilTimeout(hI2C, I2C_FLAG_AF, RESET, I2C_TIMEOUT_FLAG) != HAL_OK)
  {
    hI2C->ErrorCode |= HAL_I2C_ERROR_TIMEOUT;
  }

  /* Clear AF flag */
  __HAL_I2C_CLEAR_FLAG(hI2C, I2C_FLAG_AF);

  /* Disable Address Acknowledge */
  hI2C->Instance->CR1 &= ~I2C_CR1_ACK;

  /* Disable DMA Request */
  hI2C->Instance->CR2 &= ~I2C_CR2_DMAEN;

  hI2C->XferCount = 0U;

  hI2C->State = HAL_I2C_STATE_READY;
  hI2C->Mode = HAL_I2C_MODE_NONE;
  
  /* Check if Errors has been detected during transfer */
  if (hI2C->ErrorCode != HAL_I2C_ERROR_NONE)
  {
    HAL_I2C_ErrorCallback(hI2C);
  }
  else
  {
    HAL_I2C_SlaveTxCpltCallback(hI2C);
  }
}

/**
  * @brief  DMA I2C master receive process complete callback
  * @param  hdma DMA handle
  * @retval None
  */
static void I2C_DMAMasterReceiveCplt(DMA_HandleTypeDef *hdma)
{
  I2C_HandleTypeDef* hI2C = (I2C_HandleTypeDef*)((DMA_HandleTypeDef*)hdma)->Parent;

  /* Disable Acknowledge */
  hI2C->Instance->CR1 &= ~I2C_CR1_ACK;
  
  /* Generate Stop */
  hI2C->Instance->CR1 |= I2C_CR1_STOP;

  /* Disable Last DMA */
  hI2C->Instance->CR2 &= ~I2C_CR2_LAST;

  /* Disable DMA Request */
  hI2C->Instance->CR2 &= ~I2C_CR2_DMAEN;

  hI2C->XferCount = 0U;

  hI2C->State = HAL_I2C_STATE_READY;
  hI2C->Mode = HAL_I2C_MODE_NONE;
  
  /* Check if Errors has been detected during transfer */
  if (hI2C->ErrorCode != HAL_I2C_ERROR_NONE)
  {
    HAL_I2C_ErrorCallback(hI2C);
  }
  else
  {
    HAL_I2C_MasterRxCpltCallback(hI2C);
  }
}

/**
  * @brief  DMA I2C slave receive process complete callback.
  * @param  hdma DMA handle
  * @retval None
  */
static void I2C_DMASlaveReceiveCplt(DMA_HandleTypeDef *hdma)
{
  I2C_HandleTypeDef* hI2C = (I2C_HandleTypeDef*)((DMA_HandleTypeDef*)hdma)->Parent;

  /* Wait until STOPF flag is reset */
  if (I2C_WaitOnSTOPFlagUntilTimeout(hI2C, I2C_TIMEOUT_FLAG) != HAL_OK)
  {
    if (hI2C->ErrorCode == HAL_I2C_ERROR_AF)
    {
      hI2C->ErrorCode |= HAL_I2C_ERROR_AF;
    }
    else
    {
      hI2C->ErrorCode |= HAL_I2C_ERROR_TIMEOUT;
    }
  }

  /* Clear STOPF flag */
  __HAL_I2C_CLEAR_STOPFLAG(hI2C);

  /* Disable Address Acknowledge */
  hI2C->Instance->CR1 &= ~I2C_CR1_ACK;

  /* Disable DMA Request */
  hI2C->Instance->CR2 &= ~I2C_CR2_DMAEN;

  hI2C->XferCount = 0U;

  hI2C->State = HAL_I2C_STATE_READY;
  hI2C->Mode = HAL_I2C_MODE_NONE;
  
  /* Check if Errors has been detected during transfer */
  if (hI2C->ErrorCode != HAL_I2C_ERROR_NONE)
  {
    HAL_I2C_ErrorCallback(hI2C);
  }
  else
  {
    HAL_I2C_SlaveRxCpltCallback(hI2C);
  }
}

/**
  * @brief  DMA I2C Memory Write process complete callback
  * @param  hdma DMA handle
  * @retval None
  */
static void I2C_DMAMemTransmitCplt(DMA_HandleTypeDef *hdma)
{
  I2C_HandleTypeDef* hI2C = (I2C_HandleTypeDef*)((DMA_HandleTypeDef*)hdma)->Parent;

  /* Wait until BTF flag is reset */
  if (I2C_WaitOnFlagUntilTimeout(hI2C, I2C_FLAG_BTF, RESET, I2C_TIMEOUT_FLAG) != HAL_OK)
  {
    hI2C->ErrorCode |= HAL_I2C_ERROR_TIMEOUT;
  }

  /* Generate Stop */
  hI2C->Instance->CR1 |= I2C_CR1_STOP;

  /* Disable DMA Request */
  hI2C->Instance->CR2 &= ~I2C_CR2_DMAEN;

  hI2C->XferCount = 0U;

  hI2C->State = HAL_I2C_STATE_READY;
  hI2C->Mode = HAL_I2C_MODE_NONE;
  
  /* Check if Errors has been detected during transfer */
  if (hI2C->ErrorCode != HAL_I2C_ERROR_NONE)
  {
    HAL_I2C_ErrorCallback(hI2C);
  }
  else
  {
    HAL_I2C_MemTxCpltCallback(hI2C);
  }
}

/**
  * @brief  DMA I2C Memory Read process complete callback
  * @param  hdma DMA handle
  * @retval None
  */
static void I2C_DMAMemReceiveCplt(DMA_HandleTypeDef *hdma)
{
  I2C_HandleTypeDef* hI2C = (I2C_HandleTypeDef*)((DMA_HandleTypeDef*)hdma)->Parent;

  /* Disable Acknowledge */
  hI2C->Instance->CR1 &= ~I2C_CR1_ACK;
  
  /* Generate Stop */
  hI2C->Instance->CR1 |= I2C_CR1_STOP;

  /* Disable Last DMA */
  hI2C->Instance->CR2 &= ~I2C_CR2_LAST;

  /* Disable DMA Request */
  hI2C->Instance->CR2 &= ~I2C_CR2_DMAEN;

  hI2C->XferCount = 0U;

  hI2C->State = HAL_I2C_STATE_READY;
  hI2C->Mode = HAL_I2C_MODE_NONE;
  
  /* Check if Errors has been detected during transfer */
  if (hI2C->ErrorCode != HAL_I2C_ERROR_NONE)
  {
    HAL_I2C_ErrorCallback(hI2C);
  }
  else
  {
    HAL_I2C_MemRxCpltCallback(hI2C);
  }
}

/**
  * @brief  DMA I2C communication error callback.
  * @param  hdma DMA handle
  * @retval None
  */
static void I2C_DMAError(DMA_HandleTypeDef *hdma)
{
  I2C_HandleTypeDef* hI2C = (I2C_HandleTypeDef*)((DMA_HandleTypeDef*)hdma)->Parent;

  /* Disable Acknowledge */
  hI2C->Instance->CR1 &= ~I2C_CR1_ACK;

  hI2C->XferCount = 0U;

  hI2C->State = HAL_I2C_STATE_READY;
  hI2C->Mode = HAL_I2C_MODE_NONE;
  
  hI2C->ErrorCode |= HAL_I2C_ERROR_DMA;

  HAL_I2C_ErrorCallback(hI2C);
}

/**
  * @brief  This function handles I2C Communication Timeout.
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @param  Flag specifies the I2C flag to check.
  * @param  Status The new Flag status (SET or RESET).
  * @param  Timeout Timeout duration
  * @retval HAL status
  */
static HAL_StatusTypeDef I2C_WaitOnFlagUntilTimeout(I2C_HandleTypeDef *hI2C, uint32_t Flag, FlagStatus Status, uint32_t Timeout)
{
  uint32_t tickstart = 0U;

  /* Get tick */
  tickstart = HAL_GetTick();

  /* Wait until flag is set */
  if (Status == RESET)
  {
    while(__HAL_I2C_GET_FLAG(hI2C, Flag) == RESET)
    {
      /* Check for the Timeout */
      if (Timeout != HAL_MAX_DELAY)
      {
        if ((Timeout == 0U)||((HAL_GetTick() - tickstart ) > Timeout))
        {
          hI2C->PreviousState = I2C_STATE_NONE;
          hI2C->State= HAL_I2C_STATE_READY;

          /* Process Unlocked */
          __HAL_UNLOCK(hI2C);

          return HAL_TIMEOUT;
        }
      }
    }
  }
  else
  {
    while(__HAL_I2C_GET_FLAG(hI2C, Flag) != RESET)
    {
      /* Check for the Timeout */
      if (Timeout != HAL_MAX_DELAY)
      {
        if ((Timeout == 0U)||((HAL_GetTick() - tickstart ) > Timeout))
        {
          hI2C->PreviousState = I2C_STATE_NONE;
          hI2C->State= HAL_I2C_STATE_READY;

          /* Process Unlocked */
          __HAL_UNLOCK(hI2C);

          return HAL_TIMEOUT;
        }
      }
    }
  }
  return HAL_OK;
}

/**
  * @brief  This function handles I2C Communication Timeout for Master addressing phase.
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *         the configuration information for I2C module
  * @param  Flag specifies the I2C flag to check.
  * @param  Timeout Timeout duration
  * @retval HAL status
  */
static HAL_StatusTypeDef I2C_WaitOnMasterAddressFlagUntilTimeout(I2C_HandleTypeDef *hI2C, uint32_t Flag, uint32_t Timeout)
{
  uint32_t tickstart = 0U;

  /* Get tick */
  tickstart = HAL_GetTick();

  while(__HAL_I2C_GET_FLAG(hI2C, Flag) == RESET)
  {
    if (__HAL_I2C_GET_FLAG(hI2C, I2C_FLAG_AF) == SET)
    {
      /* Generate Stop */
      hI2C->Instance->CR1 |= I2C_CR1_STOP;

      /* Clear AF Flag */
      __HAL_I2C_CLEAR_FLAG(hI2C, I2C_FLAG_AF);

      hI2C->ErrorCode = HAL_I2C_ERROR_AF;
      hI2C->PreviousState = I2C_STATE_NONE;
      hI2C->State= HAL_I2C_STATE_READY;

      /* Process Unlocked */
      __HAL_UNLOCK(hI2C);

      return HAL_ERROR;
    }

    /* Check for the Timeout */
    if (Timeout != HAL_MAX_DELAY)
    {
      if ((Timeout == 0U)||((HAL_GetTick() - tickstart ) > Timeout))
      {
        hI2C->PreviousState = I2C_STATE_NONE;
        hI2C->State= HAL_I2C_STATE_READY;

        /* Process Unlocked */
        __HAL_UNLOCK(hI2C);

        return HAL_TIMEOUT;
      }
    }
  }
  return HAL_OK;
}

/**
  * @brief  This function handles I2C Communication Timeout for specific usage of TXE flag.
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *                the configuration information for the specified I2C.
  * @param  Timeout Timeout duration
  * @retval HAL status
  */
static HAL_StatusTypeDef I2C_WaitOnTXEFlagUntilTimeout(I2C_HandleTypeDef *hI2C, uint32_t Timeout)  
{  
  uint32_t tickstart = HAL_GetTick();
  
  while(__HAL_I2C_GET_FLAG(hI2C, I2C_FLAG_TXE) == RESET)
  {
    /* Check if a NACK is detected */
    if (I2C_IsAcknowledgeFailed(hI2C) != HAL_OK)
    {
      return HAL_ERROR;
    }
		
    /* Check for the Timeout */
    if (Timeout != HAL_MAX_DELAY)
    {
      if ((Timeout == 0U) || ((HAL_GetTick()-tickstart) > Timeout))
      {
        hI2C->ErrorCode |= HAL_I2C_ERROR_TIMEOUT;
        hI2C->PreviousState = I2C_STATE_NONE;
        hI2C->State= HAL_I2C_STATE_READY;

        /* Process Unlocked */
        __HAL_UNLOCK(hI2C);

        return HAL_TIMEOUT;
      }
    }
  }
  return HAL_OK;      
}

/**
  * @brief  This function handles I2C Communication Timeout for specific usage of BTF flag.
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *                the configuration information for the specified I2C.
  * @param  Timeout Timeout duration
  * @retval HAL status
  */
static HAL_StatusTypeDef I2C_WaitOnBTFFlagUntilTimeout(I2C_HandleTypeDef *hI2C, uint32_t Timeout)
{  
  uint32_t tickstart = HAL_GetTick();
  
  while(__HAL_I2C_GET_FLAG(hI2C, I2C_FLAG_BTF) == RESET)
  {
    /* Check if a NACK is detected */
    if (I2C_IsAcknowledgeFailed(hI2C) != HAL_OK)
    {
      return HAL_ERROR;
    }

    /* Check for the Timeout */
    if (Timeout != HAL_MAX_DELAY)
    {
      if ((Timeout == 0U) || ((HAL_GetTick()-tickstart) > Timeout))
      {
        hI2C->ErrorCode |= HAL_I2C_ERROR_TIMEOUT;
        hI2C->PreviousState = I2C_STATE_NONE;
        hI2C->State= HAL_I2C_STATE_READY;

        /* Process Unlocked */
        __HAL_UNLOCK(hI2C);

        return HAL_TIMEOUT;
      }
    }
  }
  return HAL_OK;
}

/**
  * @brief  This function handles I2C Communication Timeout for specific usage of STOP flag.
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *                the configuration information for the specified I2C.
  * @param  Timeout Timeout duration
  * @retval HAL status
  */
static HAL_StatusTypeDef I2C_WaitOnSTOPFlagUntilTimeout(I2C_HandleTypeDef *hI2C, uint32_t Timeout)
{  
  uint32_t tickstart = 0x00U;
  tickstart = HAL_GetTick();
  
  while(__HAL_I2C_GET_FLAG(hI2C, I2C_FLAG_STOPF) == RESET)
  {
    /* Check if a NACK is detected */
    if (I2C_IsAcknowledgeFailed(hI2C) != HAL_OK)
    {
      return HAL_ERROR;
    }

    /* Check for the Timeout */
    if ((Timeout == 0U) || ((HAL_GetTick()-tickstart) > Timeout))
    {
      hI2C->ErrorCode |= HAL_I2C_ERROR_TIMEOUT;
      hI2C->PreviousState = I2C_STATE_NONE;
      hI2C->State= HAL_I2C_STATE_READY;

      /* Process Unlocked */
      __HAL_UNLOCK(hI2C);

      return HAL_TIMEOUT;
    }
  }
  return HAL_OK;
}

/**
  * @brief  This function handles I2C Communication Timeout for specific usage of RXNE flag.
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *                the configuration information for the specified I2C.
  * @param  Timeout Timeout duration
  * @retval HAL status
  */
static HAL_StatusTypeDef I2C_WaitOnRXNEFlagUntilTimeout(I2C_HandleTypeDef *hI2C, uint32_t Timeout)
{  
  uint32_t tickstart = 0x00U;
  tickstart = HAL_GetTick();
  
  while(__HAL_I2C_GET_FLAG(hI2C, I2C_FLAG_RXNE) == RESET)
  {
    /* Check if a STOPF is detected */
    if (__HAL_I2C_GET_FLAG(hI2C, I2C_FLAG_STOPF) == SET)
    {
      /* Clear STOP Flag */
      __HAL_I2C_CLEAR_FLAG(hI2C, I2C_FLAG_STOPF);

      hI2C->ErrorCode = HAL_I2C_ERROR_NONE;
      hI2C->PreviousState = I2C_STATE_NONE;
      hI2C->State= HAL_I2C_STATE_READY;

      /* Process Unlocked */
      __HAL_UNLOCK(hI2C);

      return HAL_ERROR;
    }

    /* Check for the Timeout */
    if ((Timeout == 0U) || ((HAL_GetTick()-tickstart) > Timeout))
    {
      hI2C->ErrorCode |= HAL_I2C_ERROR_TIMEOUT;
      hI2C->State= HAL_I2C_STATE_READY;

      /* Process Unlocked */
      __HAL_UNLOCK(hI2C);

      return HAL_TIMEOUT;
    }
  }
  return HAL_OK;
}

/**
  * @brief  This function handles Acknowledge failed detection during an I2C Communication.
  * @param  hI2C Pointer to a I2C_HandleTypeDef structure that contains
  *                the configuration information for the specified I2C.
  * @retval HAL status
  */
static HAL_StatusTypeDef I2C_IsAcknowledgeFailed(I2C_HandleTypeDef *hI2C)
{
  if (__HAL_I2C_GET_FLAG(hI2C, I2C_FLAG_AF) == SET)
  {
    /* Clear NACKF Flag */
    __HAL_I2C_CLEAR_FLAG(hI2C, I2C_FLAG_AF);

    hI2C->ErrorCode = HAL_I2C_ERROR_AF;
    hI2C->PreviousState = I2C_STATE_NONE;
    hI2C->State= HAL_I2C_STATE_READY;

    /* Process Unlocked */
    __HAL_UNLOCK(hI2C);

    return HAL_ERROR;
  }
  return HAL_OK;
}
/**
  * @}
  */

#endif /* HAL_I2C_MODULE_ENABLED */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
