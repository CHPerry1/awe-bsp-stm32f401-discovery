/**
  ******************************************************************************
  * @file    usbd_audio.c
  * @author  MCD Application Team
  * @version V2.4.2
  * @date    11-December-2015
  * @brief   This file provides the Audio core functions.
  *
  * @verbatim
  *      
  *          ===================================================================      
  *                                AUDIO Class  Description
  *          ===================================================================
 *           This driver manages the Audio Class 1.0 following the "USB Device Class Definition for
  *           Audio Devices V1.0 Mar 18, 98".
  *           This driver implements the following aspects of the specification:
  *             - Device descriptor management
  *             - Configuration descriptor management
  *             - Standard AC Interface Descriptor management
  *             - 1 Audio Streaming Interface (with single channel, PCM, Stereo mode)
  *             - 1 Audio Streaming Endpoint
  *             - 1 Audio Terminal Input (1 channel)
  *             - Audio Class-Specific AC Interfaces
  *             - Audio Class-Specific AS Interfaces
  *             - AudioControl Requests: only SET_CUR and GET_CUR requests are supported (for Mute)
  *             - Audio Feature Unit (limited to Mute control)
  *             - Audio Synchronization type: Asynchronous
  *             - Single fixed audio sampling rate (configurable in usbd_conf.h file)
  *          The current audio class version supports the following audio features:
  *             - Pulse Coded Modulation (PCM) format
  *             - sampling rate: 48KHz. 
  *             - Bit resolution: 16
  *             - Number of channels: 2
  *             - No volume control
  *             - Mute/Unmute capability
  *             - Asynchronous Endpoints 
  *          
  * @note     In HS mode and when the DMA is used, all variables and data structures
  *           dealing with the DMA during the transaction process should be 32-bit aligned.
  *           
  *      
  *  @endverbatim
  *
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2015 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/
#include <string.h>
#include "usbd_audio.h"
#include "usbd_desc.h"
#include "usbd_ctlreq.h"
#include "cs43l22.h"
#include "TargetInfo.h"
#include "Platform.h"


/** @addtogroup STM32_USB_DEVICE_LIBRARY
  * @{
  */


/** @defgroup USBD_AUDIO 
  * @brief usbd core module
  * @{
  */ 

/** @defgroup USBD_AUDIO_Private_TypesDefinitions
  * @{
  */ 
/**
  * @}
  */ 


/** @defgroup USBD_AUDIO_Private_Defines
  * @{
  */ 
  
#define DEV_SPK_MAX_VOL		0x0000 
#define DEV_SPK_MIN_VOL		0xDB10
#define DEV_SPK_RES_VOL		0x0030
#define DEV_SPK_CUR_VOL		0xFFF0  

extern volatile fract32 g_nNewVolGain;

//void ProcessWriteASRC_IRQHandler(void);

static VEC_INLINE fract32 float_to_fract32(float x)
{
      // Clip to the allowable range
      float temp;
      if (x < -1.0f)
      {
          return((fract32)0x80000000);
      }

      if (x >= 1.0f)
      {
          return(0x7FFFFFFF);
      }

      temp = x * 2147483648.0f;
      temp+= temp> 0.0f? 0.5f : -0.5f;

      // Multiply by 2^31
      return (fract32)(temp);

}   // End float_to_fract32


/** @defgroup USBD_AUDIO_Private_Macros
  * @{
  */ 
#define AUDIO_SAMPLE_FREQ(frq)      (uint8_t)(frq), (uint8_t)((frq >> 8)), (uint8_t)((frq >> 16))

#define AUDIO_PACKET_SZE(frq)          (uint8_t)(((frq * 2 * 2)/1000) & 0xFF), \
                                       (uint8_t)((((frq * 2 * 2)/1000) >> 8) & 0xFF)
                                       
                                         
/**
  * @}
  */ 


/** @defgroup USBD_AUDIO_Private_FunctionPrototypes
  * @{
  */


static uint8_t USBD_AUDIO_Init(USBD_HandleTypeDef *pdev, 
                              uint8_t cfgidx);

static uint8_t USBD_AUDIO_DeInit(USBD_HandleTypeDef *pdev, 
                                uint8_t cfgidx);

static uint8_t USBD_AUDIO_Setup(USBD_HandleTypeDef *pdev, 
                               USBD_SetupReqTypedef *req);

static uint8_t *USBD_AUDIO_GetCfgDesc(uint16_t *length);

static uint8_t *USBD_AUDIO_GetDeviceQualifierDesc(uint16_t *length);

static uint8_t USBD_AUDIO_DataIn(USBD_HandleTypeDef *pdev, uint8_t epnum);

static uint8_t USBD_AUDIO_DataOut(USBD_HandleTypeDef *pdev, uint8_t epnum);

static uint8_t USBD_AUDIO_EP0_RxReady(USBD_HandleTypeDef *pdev);

static uint8_t USBD_AUDIO_EP0_TxReady(USBD_HandleTypeDef *pdev);

static uint8_t USBD_AUDIO_SOF(USBD_HandleTypeDef *pdev);

static uint8_t USBD_AUDIO_IsoINIncomplete(USBD_HandleTypeDef *pdev, uint8_t epnum);

static uint8_t USBD_AUDIO_IsoOutIncomplete(USBD_HandleTypeDef *pdev, uint8_t epnum);

static void AUDIO_REQ_GetCurrent(USBD_HandleTypeDef *pdev, USBD_SetupReqTypedef *req);

static void AUDIO_REQ_SetCurrent(USBD_HandleTypeDef *pdev, USBD_SetupReqTypedef *req);


/**
  * @}
  */ 

/** @defgroup USBD_AUDIO_Private_Variables
  * @{
  */ 
  
USBD_AUDIO_HandleTypeDef g_Audio;  

USBD_ClassTypeDef USBD_AUDIO = 
{
  USBD_AUDIO_Init,
  USBD_AUDIO_DeInit,
  USBD_AUDIO_Setup,
  USBD_AUDIO_EP0_TxReady,  
  USBD_AUDIO_EP0_RxReady,
  USBD_AUDIO_DataIn,
  USBD_AUDIO_DataOut,
  USBD_AUDIO_SOF,
  USBD_AUDIO_IsoINIncomplete,
  USBD_AUDIO_IsoOutIncomplete,      
  USBD_AUDIO_GetCfgDesc,
  USBD_AUDIO_GetCfgDesc, 
  USBD_AUDIO_GetCfgDesc,
  USBD_AUDIO_GetDeviceQualifierDesc
};

// -90.5 dB
int16_t nMinVol = DEV_SPK_MIN_VOL;

// 0 dB 
int16_t nMaxVol = DEV_SPK_MAX_VOL;

// 1/256 dB step
int16_t nResVol = DEV_SPK_RES_VOL; 

// -22.5 dB
volatile int16_t g_nCurrentVolume = DEV_SPK_CUR_VOL;

/* USB AUDIO device Configuration Descriptor */
__ALIGN_BEGIN static uint8_t USBD_AUDIO_CfgDesc[USB_AUDIO_CONFIG_DESC_SIZ] __ALIGN_END =
{
  /* Configuration 1 */
  0x09,                                 /* bLength */
  USB_DESC_TYPE_CONFIGURATION,          /* bDescriptorType */
  LOBYTE(USB_AUDIO_CONFIG_DESC_SIZ),    /* wTotalLength  141 bytes*/
  HIBYTE(USB_AUDIO_CONFIG_DESC_SIZ),      
  USBD_MAX_NUM_INTERFACES,              /* bNumInterfaces */
  0x01,                                 /* bConfigurationValue */
  0x00,                                 /* iConfiguration */
  0xC0,                                 /* bmAttributes  BUS Powred*/
  0x32,                                 /* bMaxPower = 100 mA*/
  /* 09 byte*/
  
  /* USB Speaker Standard interface descriptor */
  AUDIO_INTERFACE_DESC_SIZE,            /* bLength */
  USB_DESC_TYPE_INTERFACE,              /* bDescriptorType */
  0x00,                                 /* bInterfaceNumber */
  0x00,                                 /* bAlternateSetting */
  0x00,                                 /* bNumEndpoints */
  USB_DEVICE_CLASS_AUDIO,               /* bInterfaceClass */
  AUDIO_SUBCLASS_AUDIOCONTROL,          /* bInterfaceSubClass */
  AUDIO_PROTOCOL_UNDEFINED,             /* bInterfaceProtocol */
  0x00,                                 /* iInterface */
  /* 09 byte*/
  
  /* USB Speaker Class-specific AC Interface Descriptor */
  AUDIO_INTERFACE_DESC_SIZE,            /* bLength */
  AUDIO_INTERFACE_DESCRIPTOR_TYPE,      /* bDescriptorType */
  AUDIO_CONTROL_HEADER,                 /* bDescriptorSubtype */
  0x00,          /* 1.00 */             /* bcdADC */
  0x01,
  0x27,                                 /* wTotalLength = 39 */
  0x00,
  0x01,                                 /* bInCollection */
  0x01,                                 /* baInterfaceNr */
  /* 09 byte*/
  
  /* USB Speaker Input Terminal Descriptor */
  AUDIO_INPUT_TERMINAL_DESC_SIZE,       /* bLength */
  AUDIO_INTERFACE_DESCRIPTOR_TYPE,      /* bDescriptorType */
  AUDIO_CONTROL_INPUT_TERMINAL,         /* bDescriptorSubtype */
  0x01,                                 /* bTerminalID */
  0x01,                                 /* wTerminalType AUDIO_TERMINAL_USB_STREAMING   0x0101 */
  0x01,
  0x00,                                 /* bAssocTerminal */
  0x01,                                 /* bNrChannels */
  0x00,                                 /* wChannelConfig 0x0000  Mono */
  0x00,
  0x00,                                 /* iChannelNames */
  0x00,                                 /* iTerminal */
  /* 12 byte*/
  
  /* USB Speaker Audio Feature Unit Descriptor */
  0x09,                                 /* bLength */
  AUDIO_INTERFACE_DESCRIPTOR_TYPE,      /* bDescriptorType */
  AUDIO_CONTROL_FEATURE_UNIT,           /* bDescriptorSubtype */
  AUDIO_OUT_STREAMING_CTRL,             /* bUnitID */
  0x01,                                 /* bSourceID */
  0x01,                                 /* bControlSize */
  AUDIO_CONTROL_MUTE | AUDIO_CONTROL_VOLUME, /* bmaControls(0) */
  0x00,                                 /* bmaControls(1) */  
  0x00,                                 /* iTerminal */
  /* 09 byte*/
  
  /*USB Speaker Output Terminal Descriptor */
  0x09,                                 /* bLength */
  AUDIO_INTERFACE_DESCRIPTOR_TYPE,      /* bDescriptorType */
  AUDIO_CONTROL_OUTPUT_TERMINAL,        /* bDescriptorSubtype */
  0x03,                                 /* bTerminalID */
  0x01,                                 /* wTerminalType  0x0301*/
  0x03,
  0x00,                                 /* bAssocTerminal */
  0x02,                                 /* bSourceID */
  0x00,                                 /* iTerminal */
  /* 09 byte*/
  
  /* USB Speaker Standard AS Interface Descriptor - Audio Streaming Zero Bandwith */
  /* Interface 1, Alternate Setting 0                                             */
  AUDIO_INTERFACE_DESC_SIZE,            /* bLength */
  USB_DESC_TYPE_INTERFACE,              /* bDescriptorType */
  0x01,                                 /* bInterfaceNumber */
  0x00,                                 /* bAlternateSetting */
  0x00,                                 /* bNumEndpoints */
  USB_DEVICE_CLASS_AUDIO,               /* bInterfaceClass */
  AUDIO_SUBCLASS_AUDIOSTREAMING,        /* bInterfaceSubClass */
  AUDIO_PROTOCOL_UNDEFINED,             /* bInterfaceProtocol */
  0x00,                                 /* iInterface */
  /* 09 byte*/
  
  /* USB Speaker Standard AS Interface Descriptor - Audio Streaming Operational */
  /* Interface 1, Alternate Setting 1                                           */
  AUDIO_INTERFACE_DESC_SIZE,            /* bLength */
  USB_DESC_TYPE_INTERFACE,              /* bDescriptorType */
  0x01,                                 /* bInterfaceNumber */
  0x01,                                 /* bAlternateSetting */
  0x01,                                 /* bNumEndpoints */
  USB_DEVICE_CLASS_AUDIO,               /* bInterfaceClass */
  AUDIO_SUBCLASS_AUDIOSTREAMING,        /* bInterfaceSubClass */
  AUDIO_PROTOCOL_UNDEFINED,             /* bInterfaceProtocol */
  0x00,                                 /* iInterface */
  /* 09 byte*/
  
  /* USB Speaker Audio Streaming Interface Descriptor */
  AUDIO_STREAMING_INTERFACE_DESC_SIZE,  /* bLength */
  AUDIO_INTERFACE_DESCRIPTOR_TYPE,      /* bDescriptorType */
  AUDIO_STREAMING_GENERAL,              /* bDescriptorSubtype */
  0x01,                                 /* bTerminalLink */
  0x01,                                 /* bDelay */
  0x01,                                 /* wFormatTag AUDIO_FORMAT_PCM  0x0001*/
  0x00,
  /* 07 byte*/
  
  /* USB Speaker Audio Type III Format Interface Descriptor */
  0x0B,                                 /* bLength */
  AUDIO_INTERFACE_DESCRIPTOR_TYPE,      /* bDescriptorType */
  AUDIO_STREAMING_FORMAT_TYPE,          /* bDescriptorSubtype */
  AUDIO_FORMAT_TYPE_III,                /* bFormatType */ 
  0x02,                                 /* bNrChannels */
  0x02,                                 /* bSubFrameSize :  2 Bytes per frame (16bits) */
  16,                                   /* bBitResolution (16-bits per sample) */ 
  0x01,                                 /* bSamFreqType only one frequency supported */ 
  AUDIO_SAMPLE_FREQ(USBD_AUDIO_FREQ),         /* Audio sampling frequency coded on 3 bytes */
  /* 11 byte*/
  
  /* Endpoint 1 - Standard Descriptor */
  AUDIO_STANDARD_ENDPOINT_DESC_SIZE,    /* bLength */
  USB_DESC_TYPE_ENDPOINT,               /* bDescriptorType */
  AUDIO_OUT_EP,                         /* bEndpointAddress 1 out endpoint*/
  USBD_EP_TYPE_ISOC,                    /* bmAttributes */
  AUDIO_PACKET_SZE(USBD_AUDIO_FREQ),    /* wMaxPacketSize in Bytes (Freq(Samples)*2(Stereo)*2(HalfWord)) */
  0x01,                                 /* bInterval */
  0x00,                                 /* bRefresh */
  0x00,                                 /* bSynchAddress */
  /* 09 byte*/
  
  /* Endpoint - Audio Streaming Descriptor*/
  AUDIO_STREAMING_ENDPOINT_DESC_SIZE,   /* bLength */
  AUDIO_ENDPOINT_DESCRIPTOR_TYPE,       /* bDescriptorType */
  AUDIO_ENDPOINT_GENERAL,               /* bDescriptor */
  0x00,                                 /* bmAttributes */
  0x00,                                 /* bLockDelayUnits */
  0x00,                                 /* wLockDelay */
  0x00,
  /* 07 byte*/
  
    //************* Descriptor of Custom HID interface ***************
    USB_INTERFACE_DESC_SIZE,          // bLength            : Interface Descriptor size              
    USB_INTERFACE_DESCRIPTOR_TYPE,    // bDescriptorType    : Interface descriptor type              
    HID_INTERFACE_NUMBER,             // bInterfaceNumber   : Number of Interface                    
    0x00,                             // bAlternateSetting  : Alternate setting                      
    0x02,                             // bNumEndpoints      : 2 endpoints                            
    USB_DEVICE_CLASS_HID,             // bInterfaceClass    : HID                                    
    HID_SUBCLASS_NONE,                // bInterfaceSubClass : 1=BOOT, 0=no boot                      
    HID_PROTOCOL_NONE,                // nInterfaceProtocol : 0=none, 1=keyboard, 2=mouse            
    0,                                // iInterface         : Index of string descriptor             
    //******************* Descriptor of Custom HID HID *******************
    USB_HID_DESC_SIZE,                // bLength            : HID Descriptor size                    
    HID_DESCRIPTOR_TYPE,              // bDescriptorType    : HID                                    
    WBVAL(0x0110),                    // bcdHID             : HID Class Spec release number          
    0x00,                             // bCountryCode       : Hardware target country                
    0x01,                             // bNumDescriptors    : Nbr of HID class descriptors to follow 
    HID_REPORT_DESCRIPTOR_TYPE,       // bDescriptorType    : HID report descriptor                  
    WBVAL(REPORT_DESC_SIZE),          // wItemLength        : Total length of Report descriptor      
    //******************* Endpoints IN descriptor*****************
    USB_ENDPOINT_DESC_SIZE,           // bLength            : Endpoint Descriptor size               
    USB_ENDPOINT_DESCRIPTOR_TYPE,     // bDescriptorType    : Endpoint                               
    HID_IN_EP,                        // bEndpointAddress   : Endpoint Address (IN)                  
    USB_ENDPOINT_TYPE_INTERRUPT,      // bmAttributes       : Interrupt endpoint                     
    WBVAL(HID_EP_BUFFER_SIZE),        // wMaxPacketSize     : 64 Bytes max                           
    HID_EP_bInterval,                 // bInterval          : Polling Interval (10 ms)               
    //******************* Endpoints OUT descriptor*****************
    USB_ENDPOINT_DESC_SIZE,	          // bLength            : Endpoint Descriptor size               
    USB_ENDPOINT_DESCRIPTOR_TYPE,     // bDescriptorType    : Endpoint descriptor type               
    HID_OUT_EP,	                      // bEndpointAddress   : Endpoint Address (OUT)                 
    USB_ENDPOINT_TYPE_INTERRUPT,      // bmAttributes       : Interrupt endpoint                     
    WBVAL(HID_EP_BUFFER_SIZE),        // wMaxPacketSize     : 64 Bytes max                           
    HID_EP_bInterval                  // bInterval          : Polling Interval (10 ms)       
};

__ALIGN_BEGIN const uint8_t HID_ReportDescriptor[REPORT_DESC_SIZE] __ALIGN_END =
{
    0x06, 0x00, 0xFF,// USAGE_PAGE (Vendor Defined)
    0x09, 0x01,      // USAGE (Vendor Usage 1)
	
    0xA1, 0x01,      // COLLECTION (Application)

	0x15, 0x00,		 // LOGICAL MINIMUM (0)
	0x25, 0xFF,		 // LOGICAL MAXIMUM (255)

	// Input Report	
	0x85, 0x01,		 // Report ID 1
	0x75, 0x08,		 // REPORT SIZE (8 bits)
	0x95, 0x37, 	 // REPORT COUNT (56 bytes)
	0x09, 0x02,		 // USAGE (Vendor Usage 2)	
	0x81, 0x86,		 // (DATA, VAR, ABSOLUTE, BUFFERED BYTES)

	// Output Report
    0x85, 0x01,		 // Report ID 1	
	0x75, 0x08,		 // REPORT SIZE (8 bits)
	0x95, 0x37, 	 // REPORT COUNT (56 bytes)
	0x09, 0x02,		 // USAGE (Vendor Usage 2)	
	0x91, 0x86,		 // (DATA, VAR, ABSOLUTE, BUFFERED BYTES)	

	0xC0		 	 // END COLLECTION (Application)

};	// End HID_ReportDescriptor

__ALIGN_BEGIN static uint8_t USBD_HID_Desc[USB_HID_DESC_SIZE]  __ALIGN_END  =
{
  /* 18 */
  0x09,         /*bLength: HID Descriptor size*/
  HID_DESCRIPTOR_TYPE, /*bDescriptorType: HID*/
  0x11,         /*bcdHID: HID Class Spec release number*/
  0x01,
  0x00,         /*bCountryCode: Hardware target country*/
  0x01,         /*bNumDescriptors: Number of HID class descriptors to follow*/
  0x22,         /*bDescriptorType*/
  REPORT_DESC_SIZE, /*wItemLength: Total length of Report descriptor*/
  0x00,
};

/* USB Standard Device Descriptor */
__ALIGN_BEGIN static uint8_t USBD_AUDIO_DeviceQualifierDesc[USB_LEN_DEV_QUALIFIER_DESC] __ALIGN_END=
{
  USB_LEN_DEV_QUALIFIER_DESC,
  USB_DESC_TYPE_DEVICE_QUALIFIER,
  0x00,
  0x02,
  0x00,
  0x00,
  0x00,
  0x40,
  0x01,
  0x00,
};

/**
  * @}
  */ 

/** @defgroup USBD_AUDIO_Private_Functions
  * @{
  */ 

/**
  * @brief  USBD_AUDIO_Init
  *         Initialize the AUDIO interface
  * @param  pdev: device instance
  * @param  cfgidx: Configuration index
  * @retval status
  */
static uint8_t USBD_AUDIO_Init(USBD_HandleTypeDef *pdev, 
                               uint8_t cfgidx)
{
    USBD_AUDIO_HandleTypeDef * haudio;
    
    /* Open EP IN */
    USBD_LL_OpenEP(pdev,
                 HID_IN_EP,
                 USBD_EP_TYPE_INTR,
                 HID_IN_EP_SIZE);  


    /* Open EP OUT */
    USBD_LL_OpenEP(pdev,
                 HID_OUT_EP,
                 USBD_EP_TYPE_INTR,
                 HID_OUT_EP_SIZE);    

    /* Open EP OUT */
    USBD_LL_OpenEP(pdev,
                 AUDIO_OUT_EP,
                 USBD_EP_TYPE_ISOC,
                 AUDIO_OUT_PACKET_SIZE_BYTES);

    pdev->pClassData = &g_Audio;

    haudio = (USBD_AUDIO_HandleTypeDef *)pdev->pClassData;
    haudio->alt_setting = 0;
    haudio->offset = AUDIO_OFFSET_UNKNOWN;
    haudio->writeNdx = 0; 
    haudio->readNdx = 0;     
    haudio->rd_enable = 0;
    haudio->buffer = USBBuffer;

    /* Initialize the Audio output Hardware layer */
    if (((USBD_AUDIO_ItfTypeDef *)pdev->pUserData)->Init(USBD_AUDIO_FREQ, AUDIO_DEFAULT_VOLUME, 0) != USBD_OK)
    {
      return USBD_FAIL;
    }
    
    // Prepare Out endpoint to receive HID Report packets    
    USBD_LL_PrepareReceive(pdev, 
                           HID_OUT_EP, 
                           HIDOutBuff, 
                           HID_EP_BUFFER_SIZE);    

    // Prepare Out endpoint to receive next audio packet
    USBD_LL_PrepareReceive(pdev,
                           AUDIO_OUT_EP,
                           (PBYTE)haudio->buffer, 
                           AUDIO_OUT_PACKET_SIZE_BYTES);          
  
    return USBD_OK;
}

/**
  * @brief  USBD_AUDIO_Init
  *         DeInitialize the AUDIO layer
  * @param  pdev: device instance
  * @param  cfgidx: Configuration index
  * @retval status
  */
static uint8_t USBD_AUDIO_DeInit(USBD_HandleTypeDef *pdev, 
                                 uint8_t cfgidx)
{
    USBD_LL_CloseEP(pdev, HID_IN_EP);
    USBD_LL_CloseEP(pdev, HID_OUT_EP);
    USBD_LL_CloseEP(pdev, AUDIO_OUT_EP);

    ((USBD_AUDIO_ItfTypeDef *)pdev->pUserData)->DeInit(0);

    return USBD_OK;
}

/**
  * @brief  USBD_AUDIO_Setup
  *         Handle the AUDIO specific requests
  * @param  pdev: instance
  * @param  req: usb requests
  * @retval status
  */
static uint8_t USBD_AUDIO_Setup(USBD_HandleTypeDef *pdev, 
                                USBD_SetupReqTypedef *req)
{
    USBD_AUDIO_HandleTypeDef * haudio;
    uint16_t len;
    uint8_t *pbuf;
    uint8_t ret = USBD_OK;
    haudio = (USBD_AUDIO_HandleTypeDef*) pdev->pClassData;

    //printf("USBD_AUDIO_Setup: bmRequest = 0x%X, bRequest = 0x%X, wValue = 0x%X, wIndex = 0x%X\n\r", req->bmRequest, req->bRequest, req->wValue, req->wIndex);    
        
    switch (req->bmRequest & USB_REQ_TYPE_MASK)
    {
        case USB_REQ_TYPE_CLASS:
            
            switch (req->bRequest)
            {
                case AUDIO_REQ_GET_CUR:
                    
                    // Get volume or mute current value
                    AUDIO_REQ_GetCurrent(pdev, req);
                    break;
                  
                case AUDIO_REQ_SET_CUR:
                    
                    AUDIO_REQ_SetCurrent(pdev, req);                
                    break;
                    
                case AUDIO_REQ_GET_MIN:
                    
                    USBD_CtlSendData(pdev, (uint8_t *)&nMinVol, sizeof(nMinVol) );
                    break;

                case AUDIO_REQ_GET_MAX:
                   
                    USBD_CtlSendData(pdev, (uint8_t *)&nMaxVol, sizeof(nMaxVol) );
                    break;

                case AUDIO_REQ_GET_RES:
                         
                    USBD_CtlSendData(pdev, (uint8_t *)&nResVol, sizeof(nResVol) );
                    break;
                  
                default:
                    USBD_CtlError(pdev, req);
                    ret = USBD_FAIL; 
            }
            break;

        case USB_REQ_TYPE_STANDARD:
            
            switch (req->bRequest)
            {
                case USB_REQ_GET_DESCRIPTOR:
                    
                    if ( (req->wValue >> 8) == AUDIO_DESCRIPTOR_TYPE)
                    {
                        pbuf = USBD_AUDIO_CfgDesc + 18;
                        len = MIN(USB_AUDIO_DESC_SIZ, req->wLength);                            
                    }     
                    else if ( (req->wValue >> 8) == HID_REPORT_DESC_TYPE)
                    {
                        len = MIN(REPORT_DESC_SIZE, req->wLength);
                        pbuf = (UINT8 *)HID_ReportDescriptor;
                    }
                    else if ( (req->wValue >> 8) == HID_DESCRIPTOR_TYPE)
                    {
                        pbuf = USBD_HID_Desc;   
                        len = MIN(USB_HID_DESC_SIZE, req->wLength);
                    }
                    else
                    {
                    	break;
                    }

                    USBD_CtlSendData(pdev, 
                                     pbuf,
                                     len);
                    break;
                  
                case USB_REQ_GET_INTERFACE:
                    
                    USBD_CtlSendData(pdev, (uint8_t *)&(haudio->alt_setting), 1);
                    break;
                  
                case USB_REQ_SET_INTERFACE:
                    
                    if ((uint8_t)(req->wValue) <= USBD_MAX_NUM_INTERFACES)
                    {
                        haudio->alt_setting = (uint8_t)(req->wValue);
                    }
                    else
                    {
                        /* Call the error management function (command will be nacked */
                        USBD_CtlError(pdev, req);
                    }
                    break;      
                  
                default:
                    USBD_CtlError(pdev, req);
                    ret = USBD_FAIL; 
                
            }   // End switch
            
            break;
            
        default:
            ;
        
    }   // End switch
    
    return ret;
}


/**
  * @brief  USBD_AUDIO_GetCfgDesc 
  *         return configuration descriptor
  * @param  speed : current device speed
  * @param  length : pointer data length
  * @retval pointer to descriptor buffer
  */
static uint8_t *USBD_AUDIO_GetCfgDesc(uint16_t *length)
{
    *length = sizeof(USBD_AUDIO_CfgDesc);
    return USBD_AUDIO_CfgDesc;
}


/**
  * @brief  USBD_AUDIO_EP0_RxReady
  *         handle EP0 Rx Ready event
  * @param  pdev: device instance
  * @retval status
  */
static uint8_t USBD_AUDIO_EP0_RxReady(USBD_HandleTypeDef *pdev)
{
    USBD_AUDIO_HandleTypeDef *haudio;

    haudio = (USBD_AUDIO_HandleTypeDef *)pdev->pClassData;

    if (haudio->control.cmd == AUDIO_REQ_SET_CUR)
    {   
        if (haudio->control.unit == AUDIO_OUT_STREAMING_CTRL)
        {
            uint8_t nControlSelector = HIBYTE(pdev->request.wValue); 
            
            if (nControlSelector == AUDIO_CONTROL_MUTE)
            {            
                ((USBD_AUDIO_ItfTypeDef *)pdev->pUserData)->MuteCtl(haudio->control.data[0]);
            }
            else if (nControlSelector == AUDIO_CONTROL_VOLUME)
            {
                int16_t  nValue;
                float fValue;
                
                nValue = haudio->control.data[1]; 
                nValue = (nValue << 8) | haudio->control.data[0]; 
                
                nValue = -nValue; 
                
                nValue = nValue ? nValue : 1;

                fValue = 100.0f / nValue;
                
                g_nNewVolGain = float_to_fract32(fValue);                            
            }
            
            haudio->control.cmd = 0;
            haudio->control.len = 0;            
        }
    }

  return USBD_OK;
}


/**
  * @brief  USBD_AUDIO_EP0_TxReady
  *         handle EP0 TRx Ready event
  * @param  pdev: device instance
  * @retval status
  */
static uint8_t USBD_AUDIO_EP0_TxReady(USBD_HandleTypeDef *pdev)
{    
    return USBD_OK;
}
/**
  * @brief  USBD_AUDIO_SOF
  *         handle SOF event
  * @param  pdev: device instance
  * @retval status
  */
static uint8_t USBD_AUDIO_SOF(USBD_HandleTypeDef *pdev)
{    
    return USBD_OK;
}


/**
  * @brief  USBD_AUDIO_IsoINIncomplete
  *         handle data ISO IN Incomplete event
  * @param  pdev: device instance
  * @param  epnum: endpoint index
  * @retval status
  */
static uint8_t USBD_AUDIO_IsoINIncomplete(USBD_HandleTypeDef *pdev, uint8_t epnum)
{

  return USBD_OK;
}
/**
  * @brief  USBD_AUDIO_IsoOutIncomplete
  *         handle data ISO OUT Incomplete event
  * @param  pdev: device instance
  * @param  epnum: endpoint index
  * @retval status
  */
static uint8_t USBD_AUDIO_IsoOutIncomplete(USBD_HandleTypeDef *pdev, uint8_t epnum)
{

  return USBD_OK;
}


/**
  * @brief  USBD_AUDIO_DataIn
  *         handle data IN Stage
  * @param  pdev: device instance
  * @param  epnum: endpoint index
  * @retval status
  */
static uint8_t USBD_AUDIO_DataIn(USBD_HandleTypeDef *pdev, uint8_t epnum)
{
    if (g_bReadyToSend)
    {
        NVIC_SetPendingIRQ(ProcessUSBMsg_IRQ);  
    }           

    return USBD_OK;
}


/**
  * @brief  USBD_AUDIO_DataOut
  *         handle data OUT Stage
  * @param  pdev: device instance
  * @param  epnum: endpoint index
  * @retval status
  */
static uint8_t USBD_AUDIO_DataOut(USBD_HandleTypeDef *pdev, uint8_t epnum)
{  
    if (epnum == AUDIO_OUT_EP)
    {         
        USBD_AUDIO_HandleTypeDef * haudio = (USBD_AUDIO_HandleTypeDef*)pdev->pClassData; 
        
        // Prepare Out endpoint to receive next audio packet
        USBD_LL_PrepareReceive(pdev,
                               AUDIO_OUT_EP,
                               (PBYTE)&(haudio->buffer[nUSBWriteBufferNdx]), 
                               AUDIO_OUT_PACKET_SIZE_BYTES);       
        
        nUSBWriteBufferNdx = (nUSBWriteBufferNdx + NEW_USB_SAMPLES) % AUDIO_BUFFER_SIZE_IN_SAMPLES;        

        // Issue software interrupt to place received data into ASRC buffer
        NVIC_SetPendingIRQ(ProcessWriteASRC_IRQ);     
    }  
    else if (epnum == HID_OUT_EP)
    { 
        uint8_t nPayloadByteCount;
        uint32_t nPayloadWordCount;
        uint32_t nBufferEndingNdx;
        static uint32_t s_nPacketBufferNdx = 0;
        static uint32_t s_nPacketLenInDWords = 0;    
     
        // Prepare Out endpoint to receive next HID packet
        USBD_LL_PrepareReceive(pdev, 
                               HID_OUT_EP, 
                               HIDOutBuff, 
                               HID_EP_BUFFER_SIZE);        

        nPayloadByteCount = HIDOutBuff[3];
        
        nPayloadWordCount = nPayloadByteCount >> 2;
        
        nBufferEndingNdx = s_nPacketBufferNdx + nPayloadWordCount;
        
        if (nBufferEndingNdx >= MAX_COMMAND_BUFFER_LEN)
        {
            s_nPacketBufferNdx = 0;  
            return USBD_OK;            
        }
      
        // Copy the data into the AWE packet buffer
        memcpy(&s_PacketBuffer[s_nPacketBufferNdx], &HIDOutBuff[4], nPayloadByteCount);
        
        if (s_nPacketBufferNdx == 0)
        {
            uint32_t * pPacketLen = (uint32_t *)&HIDOutBuff[4];
            s_nPacketLenInDWords = *pPacketLen >> 16;
        }  

        s_nPacketBufferNdx += nPayloadWordCount;					
        
        if (s_nPacketBufferNdx >= s_nPacketLenInDWords)
        {
            g_bUSBPacketReceived = TRUE;
            
            s_nPacketBufferNdx = 0;            
        }        
    }  
  
    return USBD_OK;
    
}   // End USBD_AUDIO_DataOut


/**
  * @brief  AUDIO_Req_GetCurrent
  *         Handles the GET_CUR Audio control request.
  * @param  pdev: instance
  * @param  req: setup class request
  * @retval status
  */
static void AUDIO_REQ_GetCurrent(USBD_HandleTypeDef *pdev, USBD_SetupReqTypedef *req)
{  
    USBD_AUDIO_HandleTypeDef *haudio;

    haudio = (USBD_AUDIO_HandleTypeDef*)pdev->pClassData;
    
    if ( (req->wValue >> 8) == AUDIO_CONTROL_MUTE)
    {
        memset(haudio->control.data, 0, 64);
        
        // Send the current mute state
        USBD_CtlSendData(pdev, 
                        haudio->control.data,
                        req->wLength);        
    }
    else
    {
        // Send the current volume
        USBD_CtlSendData(pdev, 
                        (uint8_t *)&g_nCurrentVolume,
                        sizeof(g_nCurrentVolume) );
    }
}


/**
  * @brief  AUDIO_Req_SetCurrent
  *         Handles the SET_CUR Audio control request.
  * @param  pdev: instance
  * @param  req: setup class request
  * @retval status
  */
static void AUDIO_REQ_SetCurrent(USBD_HandleTypeDef *pdev, USBD_SetupReqTypedef *req)
{ 
    USBD_AUDIO_HandleTypeDef *haudio;

    haudio = (USBD_AUDIO_HandleTypeDef*)pdev->pClassData;

    if (req->wLength)
    {
        /* Prepare the reception of the buffer over EP0 */
        USBD_CtlPrepareRx(pdev,
                          haudio->control.data,                                  
                          req->wLength);    

        haudio->control.cmd = AUDIO_REQ_SET_CUR;     /* Set the request value */
        haudio->control.len = req->wLength;          /* Set the request data length */
        haudio->control.unit = HIBYTE(req->wIndex);  /* Set the request target unit */
    }
  
}


/**
* @brief  DeviceQualifierDescriptor 
*         return Device Qualifier descriptor
* @param  length : pointer data length
* @retval pointer to descriptor buffer
*/
static uint8_t *USBD_AUDIO_GetDeviceQualifierDesc(uint16_t *length)
{
    *length = sizeof(USBD_AUDIO_DeviceQualifierDesc);
    
    return USBD_AUDIO_DeviceQualifierDesc;
}


/**
* @brief  USBD_AUDIO_RegisterInterface
* @param  fops: Audio interface callback
* @retval status
*/
uint8_t USBD_AUDIO_RegisterInterface(USBD_HandleTypeDef *pdev, 
                                     USBD_AUDIO_ItfTypeDef *fops)
{
    if (fops != NULL)
    {
        pdev->pUserData= fops;
    }
    return 0;
} 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
