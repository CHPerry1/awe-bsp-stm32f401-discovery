This project currently supports three different tool chains as requested by ST Micro.

EWARM - IAR Workbench version 7.80.4 and shared components version 7.5.1.4405
MDK-ARM - Keil uVision version 5.15 with STM32F4 softpack version 2.9.0
SW4STM32 - Ac6 STM32 MCU GCC from openstm32.org
		ARM Compiler version 1.7.0
                Eclipse IDE version 4.5.2
                External Tools for Windows 1.3.0
                OpenOCD 1.10.0
                OpenSTM32 IDE 1.10.1

This build links to Audio Weaver libraries which need to be built with this same tool chain. 
In addition this project links to the ASCRLib and vendor provided libPDMfilter_CM4 library.

Project folders to build the required libraries:

ASRCLib - SVN\trunk\AudioWeaver\Source\ASRCLib\Projects

ModuleLibs - SVN\trunk\AudioWeaver\Source\Projects


