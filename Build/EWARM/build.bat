@ECHO OFF

REM IAR Headless Builds:https://www.iar.com/support/tech-notes/general/build-from-the-command-line/
REM headless-build.bat is missing an import switch...so need to start at a lower level: https://community.nxp.com/thread/388962

REM Checkout svn\trunk\AudioWeaver\Source\Targets\STM32F407_Discovery
REM cd Build\EWARM

ECHO ON
"C:\Program Files (x86)\IAR Systems\Embedded Workbench 7.5\common\bin\IarBuild.exe" STM32F407_Discovery.ewp -build Release -log all
