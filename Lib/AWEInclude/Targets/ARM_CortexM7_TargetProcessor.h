/* ----------------------------------------------------------------------------
 *	 Preprocessor Definitions
 * ------------------------------------------------------------------------- */

#ifndef ARM_CORTEXM7_TARGET_PROCESSOR_H_
#define ARM_CORTEXM7_TARGET_PROCESSOR_H_

#if defined(__ARMCC_VERSION)
#define VEC_RESTRICT	__restrict
#define VEC_INLINE		__inline
#elif defined (__ICCARM__)
#define VEC_RESTRICT	restrict
#define VEC_INLINE		inline
#else
#error "Unsupported compiler"
#endif

#define DISABLE_INTERRUPTS()  __disable_irq()
#define ENABLE_INTERRUPTS()  __enable_irq()

/**
 * @def FW_RESTRICT
 * @brief Compiler-specific C99 restrict keyword.
 */

#define FW_RESTRICT

#include "StandardDefs.h"

#define awe_fwIsSIMDAllowed(ADDR)    0

/* ----------------------------------------------------------------------
** Cycle counting macros
** ------------------------------------------------------------------- */

static VEC_INLINE void awe_fwCycleInit(void)
{
}

UINT32 awe_pltGetCycleCount(void);
UINT32 awe_pltElapsedCycles(UINT32 start_time, UINT32 end_time);

static VEC_INLINE UINT32 awe_fwGetCycles(void)
{
  UINT32 cycles = 0;
  cycles = awe_pltGetCycleCount();
  return (cycles);
}


static VEC_INLINE UINT32 awe_fwElapsedCycles(UINT32 start_time, UINT32 end_time)
{
  UINT elapsedTime;
    
  elapsedTime = awe_pltElapsedCycles(start_time, end_time);
  return elapsedTime;
}

#endif	  /* !defined  ARM_CORTEXM7_TARGET_PROCESSOR_H_ */
