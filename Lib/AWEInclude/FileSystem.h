/*******************************************************************************
*
*               Audio Framework
*               ---------------
*
********************************************************************************
*     FileSystem.h
********************************************************************************
*
*     Description:  Prototypes of AudioWeaver File System
*
*     Copyright:    (c) 2007 - 2016 DSP Concepts Inc., All rights reserved.
*                   1800 Wyatt Drive, Suite 14
*                   Sunnyvale, CA 95054
*
*******************************************************************************/
#ifndef _FILESYSTEM_H
#define _FILESYSTEM_H

#ifdef	__cplusplus
extern "C" {
#endif

#include "Framework.h"

#define AT25F2048

// Definitions for file attribute byte
#define LOAD_IMAGE				0x01
#define STARTUP_FILE			0x02
#define DATA_FILE				0x04	// Any file of type "Other"
#define COMPILED_SCRIPT			0x08
#define COMMAND_SCRIPT			0x10
#define PRESET_SCRIPT			0x20
#define COMPILED_PRESET_SCRIPT	0x28
#define LOADER_FILE				0x40
#define FILE_DELETED			0x80

#define ALLOCATION_BLOCKSIZE_DWORDS 16
#define MAX_FILENAME_LENGTH 55
#define MAX_FILENAME_LENGTH_IN_DWORDS 14

#ifndef DIRECTORY_ENTRY_DEFINED
#define DIRECTORY_ENTRY_DEFINED

extern const DWORD FLASH_MEMORY_SIZE_IN_BYTES;
extern const DWORD FLASH_MEMORY_SIZE_IN_WORDS;
extern const DWORD ERASEABLE_BLOCKSIZE;
extern const DWORD FILE_SYSTEM_START_OFFSET;    
extern const DWORD FLASH_START_OFFSET;

/**
 * @brief Flash file system directory entry data structure.
 *
 * All structure elements are 4 byte words to accommodate SHARC processors
 *
 * File Info DWORD layout
 *
 * Attribute bits | File Data CRC | Block Offset to Data 
 * -------------- | ------------- | --------------------
 * 1 byte         | 1 byte        |  2 bytes
 * 
 * | bit 31.................................................................................bit 0 | 
 *
 *
 * Attribute              | Value 
 * ---------------------- | -----
 * LOAD_IMAGE			  | 0x01
 * STARTUP_FILE			  | 0x02
 * DATA_FILE		      | 0x04
 * COMPILED_SCRIPT	      | 0x08
 * COMMAND_SCRIPT	      |	0x10
 * PRESET_SCRIPT	      |	0x20
 * COMPILED_PRESET_SCRIPT |	0x28
 * LOADER_FILE			  | 0x40
 * FILE_DELETED			  | 0x80
 *
 */
typedef struct _DIRECTORY_ENTRY
{
	DWORD nFileInfo;
	DWORD nDataDWordCnt;
    DWORD nFilename[MAX_FILENAME_LENGTH_IN_DWORDS];
} DIRECTORY_ENTRY, *PDIRECTORY_ENTRY;

#endif

// File system global state variables
extern PDIRECTORY_ENTRY g_pCurrentDirEntry;

// Count of number of files
extern INT32 g_nFileCnt;

extern BOOL g_bProcessingCommandFile;
extern BOOL g_bProcessingStartupCommandFile;

extern INT32 g_nCurrentDataOffset;

extern DWORD g_nAllocBlockSizeInDWords;
extern DWORD g_nAllocBlockSizeInBytes;
extern INT32 g_nFileCnt;
extern DWORD g_nFileDirectoryFlashAddr;
extern DWORD g_nStartOfFileDataFlashAddr;

extern DWORD g_nFlashBlockSize;
extern INT32 g_nFileCnt;
extern PDWORD g_pFileData;

extern DIRECTORY_ENTRY FileDir[];
extern DWORD *FileData;

//-----------------------------------------------------------------------------
// METHOD:	awe_fwGetFileAttribute
// PURPOSE: Get the file attribute byte for the indicated file entry
//-----------------------------------------------------------------------------
DWORD awe_fwGetFileAttribute(PDIRECTORY_ENTRY pDirectoryEntry);

//-----------------------------------------------------------------------------
// METHOD:	awe_fwResetFileSystem
// PURPOSE: Reset the file system
//-----------------------------------------------------------------------------
DWORD awe_fwResetFileSystem(void);

//-----------------------------------------------------------------------------
// METHOD:	awe_fwGetFirstFile
// PURPOSE: Get the first file directory entry
//-----------------------------------------------------------------------------
DWORD awe_fwGetFirstFile(PDIRECTORY_ENTRY * pDirEntry);

//-----------------------------------------------------------------------------
// METHOD:	awe_fwGetNextFile
// PURPOSE: Get the next file directory entry
//-----------------------------------------------------------------------------
DWORD awe_fwGetNextFile(PDIRECTORY_ENTRY * pDirEntry);

//-----------------------------------------------------------------------------
// METHOD:	awe_fwExecuteFile
// PURPOSE: Execute compiled file saved in the flash file system
//-----------------------------------------------------------------------------
INT32 awe_fwExecuteFile(DWORD *mFilename);

//-----------------------------------------------------------------------------
// METHOD:	awe_fwOpenFile
// PURPOSE: Open a file for read or write
//-----------------------------------------------------------------------------
DWORD awe_fwOpenFile(DWORD nFileAttribute, PDWORD pFileNameInDWords, PDWORD nFileLenInDWords);

//-----------------------------------------------------------------------------
// METHOD:	awe_fwCloseFile
// PURPOSE: Close an open file
//-----------------------------------------------------------------------------
DWORD awe_fwCloseFile(void);

//-----------------------------------------------------------------------------
// METHOD:	awe_fwReadFile
// PURPOSE: Read words from file
//-----------------------------------------------------------------------------
DWORD awe_fwReadFile(DWORD nWordsToRead, PDWORD pBuffer, PDWORD pDWordsRead);

//-----------------------------------------------------------------------------
// METHOD:	awe_fwFlasReadFile
// PURPOSE: Read words from Flash
//-----------------------------------------------------------------------------
DWORD awe_fwFlasReadFile(DWORD nWordsToRead, PDWORD pBuffer, PDWORD pDWordsRead);

//-----------------------------------------------------------------------------
// METHOD:	awe_fwPrepareFullFlashRead
// PURPOSE: Initializes file system to read complete flash content
//-----------------------------------------------------------------------------
void awe_fwPrepareFullFlashRead(PDWORD pBuffer);

//-----------------------------------------------------------------------------
// METHOD:	awe_fwFullFlashRead
// PURPOSE: Read complete flash content
//-----------------------------------------------------------------------------
DWORD awe_fwFullFlashRead(DWORD nWordsToRead, PDWORD pBuffer, PDWORD pDWordsRead);

//-----------------------------------------------------------------------------
// METHOD:	awe_fwUpdateFirmware
// PURPOSE: It initializes the file system to update the firmware
//-----------------------------------------------------------------------------
DWORD awe_fwUpdateFirmware(void);

//-----------------------------------------------------------------------------
// METHOD:	awe_fwWriteFile
// PURPOSE: Write words to file
//-----------------------------------------------------------------------------
DWORD awe_fwWriteFile(DWORD nWordsToWrite, PDWORD pBuffer, PDWORD pDWordsWritten);

//-----------------------------------------------------------------------------
// METHOD:	awe_fwFindFile
// PURPOSE: Find the directory entry for the named file
//-----------------------------------------------------------------------------
PDIRECTORY_ENTRY awe_fwFindFile(PDWORD pFileNameInDWords);

//-----------------------------------------------------------------------------
// METHOD:  awe_fwEraseFlash
// PURPOSE: Erase the Flash File System
//-----------------------------------------------------------------------------
DWORD awe_fwEraseFlash(void);

//-----------------------------------------------------------------------------
// METHOD:  awe_fwDeleteFile
// PURPOSE: Marks the file deleted
//-----------------------------------------------------------------------------
DWORD awe_fwDeleteFile(PDWORD pFileNameInDWords);

#ifdef	__cplusplus
}
#endif

#endif	// _FILESYSTEM_H
