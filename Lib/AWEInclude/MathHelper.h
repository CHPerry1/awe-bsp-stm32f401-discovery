#ifndef _math_helper_h
#define _math_helper_h

#include <math.h>
#include "FractMath.h"

#ifndef PI
#define PI 3.141592653589793
#endif

#define TWO_PI 6.283185307179586
#define PIDIV4 .785398163397448

#if (defined(WIN32) || defined(__linux__) || defined(DSPC_CORTEX_M) || defined(OMAP_DSP) || defined(IMX25))

#ifndef fmaxf
#define fmaxf(x, y)	(((x) > (y)) ? (x): (y))
#endif

#ifndef fminf
#define fminf(x, y)	(((x) < (y)) ? (x) : (y))
#endif

#endif // WIN32 || DSPC_CORTEX_M || OMAP_DSP || IMX25

#if defined(_TMS320C6700)

#ifndef fminf
static VEC_INLINE float fminf(float x, float y)
{
  return (x < y) ? x : y;
}
#endif

#ifndef fmaxf
static VEC_INLINE float fmaxf(float x, float y)
{
  return (x > y) ? x : y;
}
#endif

#ifndef min
#define min(x, y) (((x) < (y)) ? (x) : (y))
#endif

#ifndef max
#define max(x, y) (((x) > (y)) ? (x) : (y))
#endif

#define divf_approx(num, den) ((num) * _rcpsp(den))

#else

#define divf_approx(num, den) ((num) / (den))

#endif // !defined(_TMS320C6700)

#if defined(WIN32)

#ifndef fabsf
static VEC_INLINE float fabsf(float x)
{
  return (x < 0.0f) ? -x : x;
}
#endif

#ifndef expf
static VEC_INLINE float expf(float v)
{
	return (float)exp(v);
}
#endif

#ifndef sqrtf
static VEC_INLINE float sqrtf(float v)
{
  return (float)sqrt(v);
}
#endif

#ifndef sinf
static VEC_INLINE float sinf(float v)
{
  return((float) sin(v));
}
#endif

#ifndef cosf
static VEC_INLINE float cosf(float v)
{
  return((float) cos(v));
}
#endif

#ifndef tanf
static VEC_INLINE float tanf(float v)
{
  return((float) tan(v));
}
#endif

#ifndef sinhf
static VEC_INLINE float sinhf(float v)
{
  return((float) sinh(v));
}
#endif

#ifndef floorf
static VEC_INLINE float floorf(float X)
{
  return((float) floor(X));
}
#endif

#ifndef logf
static VEC_INLINE float logf(float X)
{
  return((float) log(X));
}
#endif

#ifndef coshf
static VEC_INLINE float coshf(float X)
{
  return((float) cosh(X));
}
#endif

#ifndef ceilf
static VEC_INLINE float ceilf(float X)
{
  return((float) ceil(X));
}
#endif

#ifndef atanf
static VEC_INLINE float atanf(float X)
{
  return((float) atan(X));
}
#endif

#ifndef atan2f
static VEC_INLINE float atan2f(float Y, float X)
{
  return((float) atan2(Y, X));
}
#endif

#ifndef asinf
static VEC_INLINE float asinf(float X)
{
  return((float) asin(X));
}
#endif

#ifndef acosf
static VEC_INLINE float acosf(float X)
{
  return((float) acos(X));
}
#endif

#ifndef fmodf
static VEC_INLINE float fmodf(float X, float Y)
{
  return((float) fmod(X, Y));
}
#endif

#ifndef tanhf
static VEC_INLINE float tanhf(float X)
{
  return((float) tanh(X));
}
#endif

#ifndef powf
static VEC_INLINE float powf(float X, float Y)
{
  return((float) pow(X, Y));
}
#endif

#ifndef frexpf
static VEC_INLINE float frexpf(float X, int *E)
{
  return ((float) frexp(X, E));
}
#endif

#ifndef ldexpf
static VEC_INLINE float ldexpf(float X, int E)
{
  return((float) ldexp(X, E));
}
#endif

#ifndef modff
static VEC_INLINE float modff(float X, float *N)
{
  float fval;
  double dummy;

  fval=(float) modf(X, &dummy);

  // For some reason, this call always returns nval=0.  So, compute
  // it again here.

  *N = X-fval;

  return((float) fval);
}
#endif

#endif	//WIN32

/* ----------------------------------------------------------------------
** Fast approximation to the log2() function.  It uses a two step
** process.  First, it decomposes the floating-point number into
** a fractional component F and an exponent E.  The fraction component
** is used in a polynomial approximation and then the exponent added
** to the result.  A 3rd order polynomial is used and the result
** when computing db20() is accurate to 7.984884e-003 dB.
** ------------------------------------------------------------------- */

extern float log2f_approx_coeff[4];

static VEC_INLINE float log2f_approx(float X)
{
  float *C = &log2f_approx_coeff[0];
  float Y;
  float F;
  INT32 E;

  // This is the approximation to log2()
  F = frexpf(fabsf(X), &E);

  //  Y = C[0]*F*F*F + C[1]*F*F + C[2]*F + C[3] + E;

  Y = *C++;
  Y *= F;
  Y += (*C++);
  Y *= F;
  Y += (*C++);
  Y *= F;
  Y += (*C++);
  Y += E;

  return(Y);
}

/* ----------------------------------------------------------------------
** Computes the log2 of a fractional value and returns the result in
** Q6.26 format.  The function is accurate to 1.326392691030e-3 (when
** viewed as a floating-point number.
** ------------------------------------------------------------------- */

extern fract32 log2_coeff_fract32[4];
extern int signbits(fract32 x);

static VEC_INLINE fract32 log2_fract32(fract32 X)
{

  fract32 *C = log2_coeff_fract32;
  fract32 y;
  int E;
  fract32 M;

  if (X == 0)
	  // Special case handling for 0.  Return the smallest possible number
	  return((fract32)0x80000000);
  else if (X < 0)
	  X = -X;

  // Turn into exponent and mantissa.
  E = signbits(X);
  M = shl_fr1x32(X, (short) E);

  y = *C++;
  y = mult_fr1x32x32NS(y, M);
  y += (*C++);
  y = mult_fr1x32x32NS(y, M);
  y += (*C++);
  y = mult_fr1x32x32NS(y, M);
  y += (*C++);

  return(y - (E << 26));
}

/* ----------------------------------------------------------------------
** Computes the db20 of a fractional Q1.31 value and returns the result
** in Q6.26 format.  The function is accurate to 8e-3 (when
** viewed as a floating-point number.
** ------------------------------------------------------------------- */

extern fract32 LinearToDecibels_coeff_fract32[5];

static VEC_INLINE fract32 db20_fract32(fract32 X)
{
	fract32 y;
	// input is Q16.16
	// the relayed function expects a value in format Q1.31 and produces a result in Q6.26
	// add offset to compensate the difference between the actual input in Q16.16
	y = log2_fract32(X) + ((31 - 16) << 26);
	// Shift 3 spaces to allow values of up to 190dB plus 3 more to prepare the function for the next multiplication
	y >>= 3;
	// Shift 3 more bits to the right to pre-multiply by 8 for the following operation
	// Handling for 0. Return the gretest negative number
	if (X == 0)
		y = (fract32)0x80000000;
	else
		// Multiply by db20(2) / 8
		// round((db20(2)/8)) * 2^31
		y = mult_fr1x32x32NS(y, 1616142483);

	return y;
}

/* ----------------------------------------------------------------------
** Fast approximation to the pow2() function.  It uses a two step
** process.  First, it decomposes the floating-point number into
** an integer I and fractional component F.  The fraction component
** is used in a polynomial approximation and then the exponent raised to
** the power of 2 multiplies the result.  The function uses the ldexpf
** function for speed.  A 6th order polynomial is used and the result
** when computing undb20() is accurate to 3.228661e-002 dB
** ------------------------------------------------------------------- */

extern float pow2f_approx_coeff[6];

static VEC_INLINE float pow2f_approx(float X)
{
  float *C = &pow2f_approx_coeff[0];
  FLOAT32 I;
  FLOAT32 F;
  FLOAT32 Y;

  // This is the integer component
  I = floorf(X);

  // This is the fractional component
  F = X - I;

  Y = *C++;
  Y *= F;
  Y += (*C++);
  Y *= F;
  Y += (*C++);
  Y *= F;
  Y += (*C++);
  Y *= F;
  Y += (*C++);
  Y *= F;
  Y += (*C++);

  return(ldexpf(Y, (INT32) I));
}



/* ----------------------------------------------------------------------
** Computes the pow2 of a fractional value. The input is in Q6.26 format
** and the output in Q16.16 format.  You have to ensure that the inputs 
** are in the proper range.  That is the integer part of the input must
** be in the range [-15 15] or there will be numerical problems.  
** (This isn't checked here because it is often easier to do in the 
** higher level calling function.)
** ------------------------------------------------------------------- */

extern fract32 pow2_coeff_fract32[7];

static VEC_INLINE fract32 pow2_fract32(fract32 x)
{
  fract32 *C = pow2_coeff_fract32;
  fract32 y;
  int I;
  fract32 F;
  fract32 val;
  int E;

  // Compute the integer and fractional portions of the Q6.26 input
  I = x >> 26;
  F = (0x03FFFFFF & x) << 5;

  // Evaluate the 6th order polynomial

  y = *C++;
  y = mult_fr1x32x32NS(y, F);
  y += (*C++);
  y = mult_fr1x32x32NS(y, F);
  y += (*C++);
  y = mult_fr1x32x32NS(y, F);
  y += (*C++);
  y = mult_fr1x32x32NS(y, F);
  y += (*C++);
  y = mult_fr1x32x32NS(y, F);
  y += (*C++);
  y = mult_fr1x32x32NS(y, F);
  y += (*C++);

  // Now keep track of the final number of bits to shift.
  // We have to shift:
  //    15 bits right because we want to convert from Q1.31 to Q16.16
  //    3 bits left because the coefficients were scaled down by 1/8
  //    Then the integer number of bits to the right

  E = 15 - 3 - I;
  if (E < 0)
    val = y << (-E);
  else
    val = y >> E;

  return(val);
}

#ifndef db10f
static VEC_INLINE float db10f(float _X)
{
  _X = fabsf(_X);
  if (_X < 1.0e-12f)
    {
      return(-120.0f);
    } else
      return(logf(_X) * 4.34294481903252f);
}
#endif

#ifndef db10f_approx
static VEC_INLINE float db10f_approx(float X)
{
  // Y = 10 * log2f_approx(X) / log2(10);
  return(log2f_approx(X) * 3.010299956639812f);
}
#endif

#ifndef undb10f
static VEC_INLINE float undb10f(float X)
{
  return((float) expf(X * 0.230258509299405f));
}
#endif

#ifndef undb10f_approx
static VEC_INLINE float undb10f_approx(float X)
{
  // Y = pow2_approx((X/10)*log2(10), ORDER);
  return (pow2f_approx(X * 0.332192809488736f));
}
#endif

#ifndef db20f
static VEC_INLINE float db20f(float _X)
{
  _X = fabsf(_X);
  if (_X < 1.0e-6f)
    {
      return(-120.0f);
    } else
      return(logf(_X) * 8.68588963806504f);
}
#endif

#ifndef db20f_approx
static VEC_INLINE float db20f_approx(float X)
{
  // Y = 20 * log2f_approx(X) / log2(10);
  return(log2f_approx(X) * 6.020599913279624f);
}
#endif

#ifndef undb20f
static VEC_INLINE float undb20f(float X)
{
  return((float) expf(X * 0.115129254649702f));
}
#endif

#ifndef undb20f_approx
static VEC_INLINE float undb20f_approx(float X)
{
  // Y = pow2_approx((X/20)*log2(10), ORDER);
  return (pow2f_approx(X * 0.166096404744368f));
}
#endif

  /* ----------------------------------------------------------------------
  ** Converts floating-point values in the range [-1 +1) to fractional
  ** values
  ** ------------------------------------------------------------------- */

#ifndef float_to_fract32
static VEC_INLINE fract32 float_to_fract32(float x)
  {
    // Clip to the allowable range
	  float temp;
    if (x < -1.0f)
      return((fract32)0x80000000);

    if (x >= 1.0f)
      return(0x7FFFFFFF);

	temp = x * 2147483648.0f;
	temp+= temp> 0.0f? 0.5f : -0.5f;
    // Multiply by 2^31
    return((fract32) (temp));
  }
#endif

  /* ----------------------------------------------------------------------
  ** Converts fractional values to their floating-point equivalents in the
  ** range [-1 +1)
  ** ------------------------------------------------------------------- */

#ifndef fract32_to_float
static VEC_INLINE float fract32_to_float(fract32 x)
  {
    // Multiply by 2^(-31)
    return((float) x * 4.6566128731e-010f);
  }
#endif

//// SHARC intrinsic functions
//
//#if defined(WIN32) || defined(__ADSPBLACKFIN__) || (__CC_ARM || defined(__GNUC__))
//fract32 float_to_fract32(float x);
//float fract32_to_float(fract32 x);
//#endif /* WIN32 or Blackfin */
//





#endif  // _math_helper_h
