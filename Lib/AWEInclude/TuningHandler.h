/*******************************************************************************
*
*               Audio Framework
*               ---------------
*
********************************************************************************
*     TuningHandler.h
********************************************************************************
*
*     Description:  Prototypes for AudioWeaver communication handler
*
*     Copyright:    DSP Concepts, LLC, 2007 - 2016
*                   1800 Wyatt Drive, Suite 14
*                   Sunnyvale, CA 95054
*
*******************************************************************************/


/* Protect the file from multiple inclusion. */
#ifndef AWETUNINGHANDLER_H_INCLUDED
#define AWETUNINGHANDLER_H_INCLUDED


/* ----------------------------------------------------------------------------
 *   Included files
 * ------------------------------------------------------------------------- */
#include <stddef.h>
#include "Framework.h"


/* Enter C naming convention */
#ifdef __cplusplus
extern "C" {
#endif /*__cplusplus*/

/* ----------------------------------------------------------------------------
 *   Types
 * ------------------------------------------------------------------------- */
#define REPLY_READY			0
#define NEW_PACKET			1
#define REPEATED_PACKET		2
#define IDLE_PACKET			3	/* Indicate to Target BSP that packet processing is idle (may be packet rx is in progress) */

#define MAX_MSG_SIZE 0x3fff
 
/* ----------------------------------------------------------------------------
 *   Function Prototypes
 * ------------------------------------------------------------------------- */

/* Pack received byte in to packet processing buffer */
void awe_fwTuningRxByte(unsigned char ch);

/* Get the byte from packet buffer to be sent as reply packet */
INT32 awe_fwTuningTxByte(unsigned char *ch);

/* Function to pack 32-bit message words in to packet buffer */
INT32 awe_fwTuningRxWord(DWORD word);

/* Get the word from packet buffer to be sent as reply message */
INT32 awe_fwTuningTxWord(DWORD *word);

/* Signal packet has been received */
void awe_fwTuningRxPacket(void);

/* Foreground tuning handler */
INT32 awe_fwTuningTick(void);

/* API to initialize AWE Tuning Handler */
void awe_fwTuningInit(DWORD *packet_buf, INT32 buf_length);

/* Leave C naming convention */
#ifdef __cplusplus
}
#endif /*__cplusplus*/

#endif /* !AWETUNINGHANDLER_H_INCLUDED */
