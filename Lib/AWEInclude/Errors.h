/*******************************************************************************
*
*               Audio Framework
*               ---------------
*
********************************************************************************
*     Errors.h
********************************************************************************
*
*     Description:  AudioWeaver Framework Error Codes
*
*     Copyright:    (c) 2007 - 2016 DSP Concepts, Inc. All rights reserved.
*                   1800 Wyatt Drive, Suite 14
*                   Sunnyvale, CA 95054
*
*******************************************************************************/
#ifndef _ERRORS_H
#define _ERRORS_H

/** OK result */
#define E_SUCCESS (0)

/** Attempt to reference a non-existent heap. */
#define E_HEAP_INDEX_RANGE								(-1)

/** Attempt to allocate more storage with awe_fwMalloc() than exists. */
#define E_MALLOC_SIZE_TOO_BIG							(-2)

/** Attempt to allocate more storage with awe_fwMallocScratch() than exists. */
#define E_SCRATCH_ALLOC_SIZE_TOO_BIG					(-3)

/** A constructor was called with the wrong number of arguments. */
#define E_CONSTRUCTOR_ARGUMENT_COUNT					(-4)

/** Attempt to reference a non-existent class. */
#define E_CLASS_INDEX_RANGE								(-5)

/** Can't find the specified class. */
#define E_CLASS_NOT_FOUND								(-6)

/** Attempt to bind a module instance to a layout that is alroeady owned by a layout. */
#define E_MODULE_ALREADY_OWNED							(-7)

/** Attempt to assign outside the bounds of the heap. */
#define E_ASSIGN_ADDRESS_OUT_OF_RANGE					(-8)

/** A wire input given to a module constructor is not a wire. */
#define E_MODULE_NOT_WIRE								(-9)

/** Many modules require the number of inputs and outputs to be the same. */
#define E_INPUTS_MUST_MATCH_OUTPUTS						(-10)

/** Many modules require that all inputs be of the same pintype. */
#define E_INPUTS_MUST_BE_SAME_PINTYPE					(-11)

/** Many modules require at leat one input. */
#define E_MUST_HAVE_INPUTS								(-12)

/** Many modules require at leat one output. */
#define E_MUST_HAVE_OUTPUTS								(-13)

/** Many modules require the input and output types to match in pairs. */
#define E_INPUTS_MUST_MATCH_CORRESPONDING_OUTPUTS		(-14)

/** The module given is not a module. */
#define E_NOT_MODULE									(-15)

/** Module with fixed inputs/outputs has wrong count. */
#define E_INPUT_OUTPUT_COUNT							(-16)

/** Module arguments have an error. */
#define E_PARAMETER_ERROR								(-17)

/** awe_fwGetFirstObject() or awe_fwGetNextObject() reached end of object chain. */
#define E_NO_MORE_OBJECTS								(-18)

/** Object pointer given to awe_fwGetNextObject() does not point to a module instance. */
#define E_NOT_OBJECT_POINTER							(-19)

/** Object pointer given to ClassInputWire_Constructor() is not an input pin. */
#define E_NOT_INPUT_PIN									(-20)

/** The I/O pin given to ClassXXWire_Constructor() is already in use. */
#define E_IOPIN_IN_USE									(-21)

/** The wire and I/O pin type are not compatible. */
#define E_PIN_TYPES_NOT_COMPATIBLE						(-22)

/** The wire and I/O pin sizes are not compatible. */
#define E_PIN_SIZES_NOT_COMPATIBLE						(-23)

/** Object pointer given to ClassOutpuWire_Constructor() is not an output pin. */
#define E_NOT_OUTPUT_PIN								(-24)

/** awe_fwGetFirstIO() or awe_fwGetNextIO() reached end of object chain. */
#define E_NO_MORE_IOPINS								(-25)

/** Master pump function found no layouts to pump. */
#define E_NO_LAYOUTS									(-26)

/** For modules with a hardwired single output (like RMS). */
#define E_MUST_HAVE_ONE_OUTPUT							(-27)

/** For modules that need an output with 1 single output value (like RMS). */
#define E_OUTPUT_MUST_BE_SINGLE_VALUE					(-28)

/** For containers, the contained module required sizes don't match. */
#define E_INCOMPATIBLE_BLOCK_SIZES						(-29)

/** A write index in a vector is out of range. */
#define E_WIRE_INDEX_RANGE								(-30)

/** There is no event handler for this module. */
#define E_NO_EVENT_HANDLER								(-31)

/** Audio_Start() called with the audio already running. */
#define E_AUDIO_ALREADY_STARTED							(-32)

/** Audio_Stop() called with the audio already stopped. */
#define E_AUDIO_ALREADY_STOPPED							(-33)

/** Framework communications failure. */
#define E_COMMUNICATIONS_ERROR							(-34)

/** Standalone tuning definitions */
#define E_SALT_OBJECT_NOTFOUND							(-35)

/** Salt field range error */
#define E_SALT_FIELD_RANGE								(-36)

/** Salt state range error  */
#define E_SALT_STATE_RANGE								(-37)

/** Attempt to pump for RS232. */
#define E_NOT_IMPLEMENTED_IN_RS232						(-38)

/** Bad packet - invalid argument */
#define E_BADPACKET										(-39)

/** Filename NULL */
#define E_BADFILE										(-40)

/** Filename length > 23. */
#define E_FILENAMELENGTH								(-41)

/** Can't create file. */
#define E_CANTCREATE									(-42)

/** Can't open the specified file. */
#define E_CANTOPEN										(-43)

/** The specified file does not exist. */
#define E_NOSUCHFILE									(-44)

/** An error occurred accessing resource */
#define E_IOERROR   									(-45)

/** Find First File must be called First */
#define E_FIND_FIRST_FILE_NOT_CALLED 					(-46)

/** No more files found */
#define	E_NO_MORE_FILES  								(-47)

/** The filename is not valid */
#define	E_BAD_FILENAME 									(-48)

/** Cannot perform operation while a file is open */
#define	E_FILE_ALREADY_OPEN 							(-49)

/** File was not found */
#define	E_FILE_NOT_FOUND 								(-50)

/** File attribut byte is invalid */
#define	E_ILLEGAL_FILE_ATTRIBUTE 						(-51)

/** File already exists */
#define	E_FILE_ALREADY_EXISTS 							(-52)

/** There is no file open */
#define	E_NO_OPEN_FILE 									(-53)

/** Out of file system space */
#define	E_OUT_OF_SPACE 									(-54)

/** End of file */
#define	E_END_OF_FILE 									(-55)

/** Read flash memory failed */
#define	E_ERROR_READING_FLASH_MEMORY 					(-56)

/** Write flash memory failed */
#define	E_ERROR_WRITING_FLASH_MEMORY 					(-57)

/** Erase flash memory failed */
#define	E_ERROR_ERASING_FLASH_MEMORY 					(-58)

/** Command not implemented on this platform */
#define E_COMMAND_NOT_IMPLEMENTED 						(-59)

/** Internal subsystem allocation failure */
#define E_INTERNAL_MODULE_ALLOCATION_FAILURE			(-60)

/** General hardware related failure */
#define E_HARDWARE_FAILURE								(-61)

/** Invalid register to read */
#define E_REGISTER_INVALID								(-62)

/** Register cannot be accessed at this time */
#define E_REGISTER_BUSY									(-63)

/** Register function not implemented */
#define E_REGISTER_NOT_IMPLEMENTED						(-64)

/** Trying to write a read-only register */
#define E_REGISTER_READ_ONLY							(-65)

/** Attampt to create heap failed. */
#define E_NO_HEAP_MEMORY								(-66)

/** Argument value invalid. */
#define E_ARGUMENT_ERROR								(-67)

/** Attempt to set duplicate ID with SetID. */
#define E_DUPLICATE_ID									(-68)

/** Attempt to use ID outside 1..32767 with SetID. */
#define E_ID_OUT_OF_RANGE								(-69)

/** Attempt to modify read-only object header. */
#define E_READ_ONLY										(-70)

/** Pointer to heap is NULL */
#define E_BAD_HEAP_POINTER								(-71)

/** Heaps already initialized */
#define E_HEAPS_ALREADY_INITIALIZED						(-72)

/** Heaps not yet initialized */
#define E_HEAPS_NOT_INITIALIZED							(-73)

/** CFramework exception (null pointer?). */
#define E_EXCEPTION										(-74)

/** Bad packet - message length is too long */
#define E_MESSAGE_LENGTH_TOO_LONG						(-75)

/** Bad packet - CRC error */
#define E_CRC_ERROR										(-76)

/** Bad packet - invalid command ID */
#define E_UNKNOWN_MESSAGE								(-77)

/** Message timed out (C6 only). */
#define E_MSG_TIMEOUT									(-78)

/** Object ID not found */
#define E_OBJECT_ID_NOT_FOUND                           (-79)

// In order to keep all the information about errors in one place the error description
// table is declared in this header. However, only one C file should actually instantiate
// this. All the other files that include this header should ignore.

#ifdef DEFINE_ERROR_STRINGS
/** Table of error descriptions */
const char *s_error_strings[] =
{
	"Success",											//#define E_SUCCESS (0)
	"No such heap",										//#define E_HEAP_INDEX_RANGE (-1)
	"Heap allocation request too large",				//#define E_MALLOC_SIZE_TOO_BIG (-2)
	"Scratch allocation request too large",				//#define E_SCRATCH_ALLOC_SIZE_TOO_BIG (-3)
	"Constructor argument count wrong",					//#define E_CONSTRUCTOR_ARGUMENT_COUNT (-4)
	"No such class index",								//#define E_CLASS_INDEX_RANGE (-5)
	"Class name not found",								//#define E_CLASS_NOT_FOUND (-6)
	"Module already ownde by layout",					//#define E_MODULE_ALREADY_OWNED (-7)
	"Address out of range",								//#define E_ASSIGN_ADDRESS_OUT_OF_RANGE (-8)
	"Module used, wire expected",						//#define E_MODULE_NOT_WIRE (-9)
	"Input must match outputs",							//#define E_INPUTS_MUST_MATCH_OUTPUTS (-10)
	"Input pintypes don't match",						//#define E_INPUTS_MUST_BE_SAME_PINTYPE (-11)
	"Input(s) must be specified",						//#define E_MUST_HAVE_INPUTS (-12)
	"Output(s) must be specified",						//#define E_MUST_HAVE_OUTPUTS (-13)
	"Inputs must match corresponding outputs",			//#define E_INPUTS_MUST_MATCH_CORRESPONDING_OUTPUTS (-14)
	"Not a module",										//#define E_NOT_MODULE (-15)
	"Input/output count wrong",							//#define E_INPUT_OUTPUT_COUNT (-16)
	"Parameter error",									//#define E_PARAMETER_ERROR (-17)
	"No more objects found",							//#define E_NO_MORE_OBJECTS (-18)
	"Pointer value is invalid",							//#define E_NOT_OBJECT_POINTER (-19)
	"Not an input pin",									//#define E_NOT_INPUT_PIN (-20)
	"I/O pin is in use",								//#define E_IOPIN_IN_USE (-21)
	"Pin types not compatible",							//#define E_PIN_TYPES_NOT_COMPATIBLE (-22)
	"Pin sizes not compatible",							//#define E_PIN_SIZES_NOT_COMPATIBLE (-23)
	"Not an output pin",								//#define E_NOT_OUTPUT_PIN (-24)
	"No more pins found",								//#define E_NO_MORE_IOPINS (-25)
	"No layout(s) to pump",								//#define E_NO_LAYOUTS (-26)
	"Must have one output",								//#define E_MUST_HAVE_ONE_OUTPUT (-27)
	"Output must be single value",						//#define E_OUTPUT_MUST_BE_SINGLE_VALUE (-28)
	"Incompatible block sizes",							//#define E_INCOMPATIBLE_BLOCK_SIZES (-29)
	"Wire index out of range",							//#define E_WIRE_INDEX_RANGE (-30)
	"Module has no event handler",						//#define E_NO_EVENT_HANDLER (-31)
	"Audio already started",							//#define E_AUDIO_ALREADY_STARTED (-32)
	"Audio already stopped",							//#define E_AUDIO_ALREADY_STOPPED (-33)
	"Communications error",								//#define E_COMMUNICATIONS_ERROR (-34)
	"SALT object not found",							//#define E_SALT_OBJECT_NOTFOUND (-35)
	"SALT field range error",							//#define E_SALT_FIELD_RANGE (-36)
	"SALT state range error",							//#define E_SALT_STATE_RANGE (-37)
	"Not implemented in RS232",							//#define E_NOT_IMPLEMENTED_IN_RS232 (-38)
	"Bad packet received",								//#define E_BADPACKET (-39)
	"Invalid filename",									//#define E_BADFILE (-40)
	"Filename too long",								//#define E_FILENAMELENGTH (-41)
	"File create failed",								//#define E_CANTCREATE (-42)
	"File open failed",									//#define E_CANTOPEN (-43)
	"No such file",										//#define E_NOSUCHFILE (-44)
	"File I/O error",									//#define E_IOERROR (-45)
	"FindNext called without FindFirst",				//#define E_FIND_FIRST_FILE_NOT_CALLED (-46)
	"No more files found",								//#define E_NO_MORE_FILES (-47)
	"Bad filename",										//#define E_BAD_FILENAME (-48)
	"File already open",								//#define E_FILE_ALREADY_OPEN (-49)
	"No such file",										//#define E_FILE_NOT_FOUND (-50)
	"Illegal file attribute",							//#define E_ILLEGAL_FILE_ATTRIBUTE (-51)
	"File already exists",								//#define E_FILE_ALREADY_EXISTS (-52)
	"No open file",										//#define E_NO_OPEN_FILE (-53)
	"File system full",									//#define E_OUT_OF_SPACE (-54)
	"End of file",										//#define E_END_OF_FILE (-55)
	"FLASH read error",									//#define E_ERROR_READING_FLASH_MEMORY (-56)
	"FLASH write error",								//#define E_ERROR_WRITING_FLASH_MEMORY (-57)
	"FLASH erase error",								//#define E_ERROR_ERASING_FLASH_MEMORY (-58)
	"No such command",									//#define E_COMMAND_NOT_IMPLEMENTED (-59)
	"Internal module allocation error",					//#define E_INTERNAL_MODULE_ALLOCATION_FAILURE (-60)
	"Hardware failure",									//#define E_HARDWARE_FAILURE (-61)
	"Register invalid",									//#define E_REGISTER_INVALID (-62)
	"Register busy",									//#define E_REGISTER_BUSY (-63)
	"Register not implemented",							//#define E_REGISTER_NOT_IMPLEMENTED (-64)
	"Register read only",								//#define E_REGISTER_READ_ONLY (-65)
	"Create heap failed",								//#define E_NO_HEAP_MEMORY (-66)
	"Invalid argument",									//#define E_ARGUMENT_ERROR (-67)
	"Duplicate ID",										//#define E_DUPLICATE_ID (-68)
	"ID out of range",									//#define E_ID_OUT_OF_RANGE (-69)
	"Read only",										//#define E_READ_ONLY (-70)
	"Bad heap pointer",									//#define E_BAD_HEAP_POINTER (-71)
	"Heaps already initialized",						//#define E_HEAPS_ALREADY_INITIALIZED (-72)
	"Heaps not initialized",							//#define E_HEAPS_NOT_INITIALIZED (-73)
	"Framework exception",								//#define E_EXCEPTION (-74)
	"Message too long",								    //#define E_MESSAGE_LENGTH_TOO_LONG (-75)
	"CRC error",										//#define E_CRC_ERROR (-76)
	"Unimplemented or invalid command ID",				//#define E_UNKNOWN_MESSAGE (-77)
	"Message timed out",								//#define E_MSG_TIMEOUT (-78)
	"Object ID not found"								//#define E_OBJECT_ID_NOT_FOUND (-79)
};

#endif

#endif	// _ERRORS_H
