/**
*	@addtogroup CFramework
*	@{
*/

/****************************************************************************
*
*		Audio Framework
*		---------------
*
****************************************************************************
*	StandaloneSupport.h
****************************************************************************
*
*	Description:	Definitions for running Audio Weaver generated
*						code in standalone mode (without the server)
*
*	Copyright:
*	Owner:
*
****************************************************************************
*
*	$Revision: 1.7 $
*
*	$Log: StandaloneSupport.h,v $
*	Revision 1.7  2007/11/11 21:28:51  dnixon
*	Fixed illegal #defines
*	
*	Revision 1.6  2007/10/31 16:56:33  dnixon
*	CONSTRUCT_WIRE* builds a non-complex wire
*
*	Revision 1.5  2007/10/15 22:21:56  pbeckmann
*	Memory segment information added to wire allocation
*
*	Revision 1.4  2007/09/28 01:00:03  scostello
*	Checking in changes for memory segment allocation.
*
*	Revision 1.3  2007/08/31 21:08:18  pbeckmann
*	Hierarchical SALT_ support.
*
*	Revision 1.2  2007/07/31 13:07:52  pbeckmann
*	Fixed minor typo in comment.
*
*	Revision 1.1  2007/07/28 02:06:02  pbeckmann
*	Changes for standalong tuning support (SALT_)
*
*
***************************************************************************/

/*
* @file StandaloneSupport.h
*/


#ifndef _STANDALONESUPPORT_H_
#define _STANDALONESUPPORT_H_

#if defined(__APPLE__) || defined(LINUX) || defined (WIN32)
#define ALIGNBUF16 __declspec(align(16))
#else
#define ALIGNBUF16
#endif

#define CONSTRUCT_WIRE(__NAME__, __NUMCHANNELS__, __BLOCKSIZE__, __SAMPLERATE__, __ISCOMPLEX__) \
ALIGNBUF16 float __NAME__ ## _BUFFER[__NUMCHANNELS__*__BLOCKSIZE__*(__ISCOMPLEX__ + 1)]={0.0f}; \
PinInstanceDescriptor __NAME__ ## _PIN={{0, NULL, NULL}, PINPROPS(__BLOCKSIZE__*(__ISCOMPLEX__ + 1), __NUMCHANNELS__, 4, __ISCOMPLEX__), __SAMPLERATE__}; \
WireInstance __NAME__ = {{0, NULL, NULL}, &__NAME__ ## _PIN, (Sample *) &(__NAME__ ## _BUFFER[0])};

#define CONSTRUCT_WIRE_NO_ALLOC(__NAME__, __NUMCHANNELS__, __BLOCKSIZE__, __SAMPLERATE__, __ISCOMPLEX__) \
PinInstanceDescriptor __NAME__ ## _PIN={{0, NULL, NULL}, PINPROPS(__BLOCKSIZE__, __NUMCHANNELS__, 4, __ISCOMPLEX__), __SAMPLERATE__}; \
WireInstance __NAME__ = {{0, NULL, NULL}, &__NAME__ ## _PIN, NULL};

#define CONSTRUCT_WIRE_SEG(__SEG__, __NAME__, __NUMCHANNELS__, __BLOCKSIZE__, __SAMPLERATE__, __ISCOMPLEX__) \
__SEG__ ALIGNBUF16 float __NAME__ ## _BUFFER[__NUMCHANNELS__*__BLOCKSIZE__*(__ISCOMPLEX__+1)]={0.0f}; \
__SEG__ PinInstanceDescriptor __NAME__ ## _PIN={{0, NULL, NULL}, PINPROPS(__BLOCKSIZE__*(__ISCOMPLEX__ + 1), __NUMCHANNELS__, 4, __ISCOMPLEX__), __SAMPLERATE__}; \
__SEG__ WireInstance __NAME__ = {{0, NULL, NULL}, &__NAME__ ## _PIN, (Sample *) &(__NAME__ ## _BUFFER[0])};

#define CONSTRUCT_WIRE_NO_ALLOC_SEG(__SEG__, __NAME__, __NUMCHANNELS__, __BLOCKSIZE__, __SAMPLERATE__, __ISCOMPLEX__) \
__SEG__ PinInstanceDescriptor __NAME__ ## _PIN={{0, NULL, NULL}, PINPROPS(__BLOCKSIZE__, __NUMCHANNELS__, 4, __ISCOMPLEX__), __SAMPLERATE__}; \
__SEG__ WireInstance __NAME__ = {{0, NULL, NULL}, &__NAME__ ## _PIN, NULL};


typedef struct _SALT_entry
{
  UINT idHigh;
  UINT idLow;
  void *module;
} SALT_entry;

int SALT_SetModuleVariable(UINT IDHIGH, UINT IDLOW, UINT FIELDOFFSET, void *DATA, UINT NUMWORDS);
int SALT_GetModuleVariable(UINT IDHIGH, UINT IDLOW, UINT FIELDOFFSET, void *DATA, UINT NUMWORDS);

int SALT_SetModuleArray(UINT IDHIGH, UINT IDLOW, UINT FIELDOFFSET, UINT ARRAYOFFSET, void *DATA, UINT NUMWORDS);
int SALT_GetModuleArray(UINT IDHIGH, UINT IDLOW, UINT FIELDOFFSET, UINT ARRAYOFFSET, void *DATA, UINT NUMWORDS);

int SALT_SetModuleState(UINT IDHIGH, UINT IDLOW, int STATE);
int SALT_GetModuleState(UINT IDHIGH, UINT IDLOW);

// This is the macro which translate the individual module ID's into high and low object IDs
// The same macro can be used for both the high and low object IDs since it is only doing
// bit packing.

#define SALT_PackID(ID1, ID2, ID3, ID4) \
		( (ID1 << 24) | (ID2 << 16) | (ID3 << 8) | ID4)

#endif // _STANDALONESUPPORT_H_
