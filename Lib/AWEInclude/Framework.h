/*******************************************************************************
*
*               Audio Framework
*               ---------------
*
********************************************************************************
*     Framework.h
********************************************************************************
*
*     Description:  AudioWeaver Framework main header file
*
*     Copyright:    DSP Concepts, LLC, 2007 - 2016
*                   1800 Wyatt Drive, Suite 14
*                   Sunnyvale, CA 95054
*
*******************************************************************************/
#ifndef _FRAMEWORK_H
#define _FRAMEWORK_H

#include "TargetProcessor.h"

#include <stdlib.h>
#include <string.h>

#ifdef	__cplusplus
extern "C" {
#endif

#if defined(_MSC_VER) && defined(USEFRAMEWORKDLL)
#	define DLLSYMBOL			__declspec(dllimport)
#	define CXX_NOWARN_DLLCLASSIF		__pragma(warning(push))\
		__pragma(warning(disable: 4251))
#	define CXX_RESTORE_DLLCLASSIF		__pragma(warning(pop))
#else
#	define DLLSYMBOL
#define CXX_NOWARN_DLLCLASSIF
#define CXX_RESTORE_DLLCLASSIF
#endif

/** Number of samples in a DMA buffer - bound wires must be an integer multiple of this. */
#ifndef DMA_NUM_SAMPLES
#define DMA_NUM_SAMPLES 32
#endif

#define MPS_25MHZ_COUNT (*((volatile unsigned long *)(0x40004014)))

/* --------- Class identifiers. */
#define CLASS_ID_BASE							0xBEEF0000
#define CLASS_ID_END							0xBEEFFFFF
#define CLASS_ID_MASK							0xFFFF0000

#define CLASS_ID_INPUTPIN						(CLASS_ID_BASE + 1)
#define CLASS_ID_OUTPUTPIN						(CLASS_ID_BASE + 2)
#define CLASS_ID_PIN							(CLASS_ID_BASE + 3)
#define CLASS_ID_LAYOUT							(CLASS_ID_BASE + 4)

/*	Class IDs for wires */
#define CLASS_ID_WIREBASE						(CLASS_ID_BASE + 0x80)
#define CLASS_ID_WIREEND						(CLASS_ID_BASE + 0x7FF)

#define CLASS_ID_WIRE							(CLASS_ID_WIREBASE + 0)
#define CLASS_ID_INPUTWIRE						(CLASS_ID_WIREBASE + 1)
#define CLASS_ID_OUTPUTWIRE						(CLASS_ID_WIREBASE + 2)

/* Class IDs for modules */
#define CLASS_ID_MODBASE						(CLASS_ID_BASE + 0x800)
#define CLASS_ID_MODEND							(CLASS_ID_END)

#define CLASS_ID_MODULE_CONTAINER				(CLASS_ID_MODBASE + 0)


/* Instance name IDs */
#define NAME_BASE								0x7EEDB000

#define NAME_INPUT_INTERLEAVED					(NAME_BASE + 1)
#define NAME_OUTPUT_INTERLEAVED					(NAME_BASE + 2)

/** States for ModuleFileStreaming. */
enum FileIOStatus
{
	/* ModuleFileStreaming always writes this (zero) to handshake. */
	FIOS_Null,

	/* These states are in the high byte of buffer[buffer_size]. */
	FIOS_NewStream,
	FIOS_NextBlock,
	FIOS_LastBlock,

	/* These states are reported asynchronously in asyncStatus. */
	FIOS_Stopped,
	FIOS_Paused,
	FIOS_Error,

	/* Used internally by the file streaming module to keep track of the current state */
	FIOS_Playing,
	FIOS_PlayingLast
};

/** Test if a given class is valid. */
#define IsClassValid(classID) \
		(((classID) >= CLASS_ID_BASE) && ((classID) <= CLASS_ID_END))

/** Test if a given class ID is that of a wire. */
#define IsClassWire(classID) \
	(((classID) >= CLASS_ID_WIREBASE) && ((classID) <= CLASS_ID_WIREEND))

/** Test if a given class ID is that of module. */
#define IsClassModule(classID) \
	(((classID) >= CLASS_ID_MODBASE) && ((classID) <= CLASS_ID_MODEND))

#define WORDSOF(x)					(sizeof(x) / sizeof(INT32))

/* Handle packed wire counts. */

#define GetInputPinCount(x)			((x) & 0xff)

#define GetOutputPinCount(x)		(((x) >> 8) & 0xff)

#define GetScratchPinCount(x)		(((x) >> 16) & 0xff)

#define GetFeedbackPinCount(x)		(((x) >> 24) & 0xff)

#define GetWireCount(x)				( (((x) >> 24) & 0xff) + (((x) >> 16) & 0xff) + (((x) >> 8) & 0xff) + ((x) & 0xff))


#define MakeIOSize(nInputs, nOutputs, blockSize) \
	(((blockSize) << 16) | ((nOutputs) << 8) | (nInputs))


/*------------------ Heap declarations ------------------*/

/* For the PC, all heaps are the same. No point doing anything else really. */
/** Items accessed from the inner loop, such as Wire Buffers. */
#define AWE_HEAP_FAST		1

/** Infrequently used items which could be placed in external memory. */
#define AWE_HEAP_SLOW		2

/** another fast heap which is distinct from AWE_HEAP_FAST. This is used to separate
state and coefficients, as in FIR filters. */
#define AWE_HEAP_FASTB		3

/** Fast Heap -> FastB Heap -> Slow Heap */
#define AWE_HEAP_FAST2SLOW ((AWE_HEAP_FAST) | ((AWE_HEAP_FASTB) << (4)) | ((AWE_HEAP_SLOW) << (8)))

/** Fast Heap -> FastB Heap */
#define AWE_HEAP_FAST2FASTB ((AWE_HEAP_FAST) | ((AWE_HEAP_FASTB) << (4)))

/** Fast B Heap -> Fast Heap */
#define AWE_HEAP_FASTB2FAST ((AWE_HEAP_FASTB) | ((AWE_HEAP_FAST) << (4)))

/** This performs the allocation in the fast B heap, and if this fails, in the slow heap */
#define AWE_HEAP_FASTB2SLOW ((AWE_HEAP_FASTB) | ((AWE_HEAP_SLOW) << (4)))

/** Return the number of heaps. Constant at compile time. */
size_t awe_fwGetHeapCount(void);

/** Return the number of times that the heap was destroyed */
UINT32 awe_fwGetHeapDestroyCount(void);

/** Allocate a block of storage from the heap. If zero returned, the reason is returned
in retVal. The storage is zeroed. */
void *awe_fwMalloc(size_t size, UINT32 heapIndex, INT32 *retVal);

/* Allocate a block of scratch storage from the heap. The storage is zeroed. */
void *awe_fwMallocScratch(size_t size, UINT32 heapIndex, INT32 *retVal);

/** Return the size of the specified heap. */
INT32 awe_fwGetHeapSize(DWORD *pHeaps);

/** Fetch the value at the given address. */
INT32 awe_fwFetchValue(UINT32 address);

/** Return sizeof INT32 for the target. */
INT32 awe_fwGetSizeofInt(void);

/** Fetch the FLOAT32 value at the given address. */
//FLOAT32 awe_fwFetchFloatValue(UINT32 address);

/** Set the value at the given address. */
INT32 awe_fwSetValue(UINT32 address, INT32 value);

/** Set the value at the given address. */
//INT32 awe_fwSetFloatValue(UINT32 address, FLOAT32 value);

/** Reset the framework to its initial state. */
void awe_fwDestroy(void);

/**
 * @brief Create the AWE heaps.
 * @param [in] master_heap_size		requested size of master heap
 * @param [in] slow_heap_size		requested size of slow heap
 * @param [in] fastb_heap_size		requested size of other heap
 * @return							error code
 */
INT32 awe_fwAllocateHeaps(UINT32 master_heap_size, UINT32 slow_heap_size, UINT32 fastb_heap_size);

/** Destroy the heaps allocated by awe_fwAllocateHeaps. */
void awe_fwDestroyHeaps(void);


/*------------------ Union declarations ------------------*/
typedef union _Sample
{
	INT32 iVal;
	UINT32 uiVal;
	FLOAT32 fVal;
#ifndef __ADSP21000__
	INT16 sVal[2];
	INT8 bVal[4];
#endif
}
Sample;

#define MAKE_TARGET_INFO_PACKED(sizeof_int, num_inputs, num_outputs, is_floating_point, is_flash_supported, processor_type) \
	( ((sizeof_int) & 0xf) | (((is_flash_supported) & 1) << 4) | (((is_floating_point) & 1) << 5) \
	| (((num_inputs) & 0xff) << 8) | (((num_outputs) & 0xff) << 16) | (((processor_type) & 0xff) << 24) )

#define TARGET_INFO_SIZOF_INT(packed) \
	((packed) & 0xf)

#define TARGET_INFO_IS_FLASH_SUPPORTED(packed) \
	(((packed) & 0x10) != 0)

#define TARGET_INFO_IS_FLOATING_POINT(packed) \
	(((packed) & 0x20) != 0)

#define TARGET_INFO_NUM_INPUTS(packed) \
	(((packed) >> 8) & 0xff)

#define TARGET_INFO_NUM_OUTPUTS(packed) \
	(((packed) >> 16) & 0xff)

#define TARGET_INFO_PROCESSOR_TYPE(packed) \
	(((packed) >> 24) & 0xff)

#define PROCESSOR_TYPE_NATIVE		1
#define PROCESSOR_TYPE_SHARC		2
#define PROCESSOR_TYPE_BLACKFIN		3
#define PROCESSOR_TYPE_CORTEXM4		4
#define PROCESSOR_TYPE_OMAP			5
#define PROCESSOR_TYPE_DSK6713		6
#define PROCESSOR_TYPE_HIFI2DSP		7
#define PROCESSOR_TYPE_CORTEXM3		8
#define PROCESSOR_TYPE_IMX25		9
#define PROCESSOR_TYPE_CORTEXM7		10
#define PROCESSOR_TYPE_C674x		11
#define PROCESSOR_TYPE_CortexA5		12
#define PROCESSOR_TYPE_CortexA7		13
#define PROCESSOR_TYPE_CortexA8		14
#define PROCESSOR_TYPE_CortexA9		15
#define PROCESSOR_TYPE_CortexA12	16
#define PROCESSOR_TYPE_CortexA15	17
#define PROCESSOR_TYPE_CortexA53	18
#define PROCESSOR_TYPE_CortexA57	19
#define PROCESSOR_TYPE_ARM9			20
#define PROCESSOR_TYPE_ARM11		21
#define PROCESSOR_TYPE_HEXAGON		22
#define PROCESSOR_TYPE_XTENSA		23

#define FILESYSTEM_TYPE_NONE			0
#define FILESYSTEM_TYPE_COMPILED_IN		1
#define FILESYSTEM_TYPE_FLASH			2


#define MAKE_TARGET_VERSION(v1, v2, v3, v4) \
	( (((v1) & 0xff) << 24) | (((v2) & 0xff) << 16) | (((v3) & 0xff) << 8) | ((v4) & 0xff) )

#define MAKE_PACKED_STRING(c1, c2, c3, c4) \
	( (((c4) & 0xff) << 24) | (((c3) & 0xff) << 16) | (((c2) & 0xff) << 8) | ((c1) & 0xff) )


/** This structure defines the target info. */
typedef struct _TargetInfo
{
	FLOAT32 m_sampleRate;				/* 0 */
	FLOAT32 m_profileClockSpeed;		/* 4 */
	UINT32 m_base_block_size;			/* 8 */
	UINT32 m_packedData;				/* 12 */
	UINT32 m_version;					/* 16 */
	UINT32 m_packedName[2];				/* 20 */
	UINT32 m_proxy_buffer_size;			/* 28 */
}
TargetInfo;		/* size=32 (8 words) */

/** Set in m_base_block_size when target supports ID based relative. */
#define TARGET_SUPPORTS_IDREL			0x00010000

/** Set in m_base_block_size when target supports SetID call. */
#define TARGET_SUPPORTS_SETID			0x00020000

/** Set in m_base_block_size when target supports setcall/getcall merge calls. */
#define TARGET_SUPPORTS_SETMERGE		0x00040000

/** Set when the compile to AWB requires relative. */
#define COMPILE_REQUIRES_RELATIVE		0x00080000


/** This structure defines the file system info. */
typedef struct _FileSystemInfo
{
	UINT32 m_FileSystemType;				/* 0 */
	UINT32 m_FlashDeviceDWords;				/* 4 */
	UINT32 m_FileSystemDWords;				/* 8 */
	UINT32 m_DataStructOverheadDWords;		/* 12 */
	UINT32 m_DeletedOrCorruptedDWords;		/* 16 */
	UINT32 m_DWordsInUse;					/* 20 */
	UINT32 m_DWordsAvailable;				/* 24 */
	UINT32 m_BlkSize_MaxFilename_Len;		/* 28 */
}
FileSystemInfo;		/* size=32 (8 words) */


/** The one and only target info object. */
extern TargetInfo g_target_info;

/** Copy the target info to the caller. */
UINT32 GetTargetInfo(TargetInfo *pTarget);

/** The one and only file system info object. */
extern FileSystemInfo g_filesystem_info;

/** Copy the file system info to the caller. */
INT32 GetFileSystemInfo(FileSystemInfo *pFileSystemInfo);


/*------------------ Instance declarations ------------------*/
/** Initialized to 0 on program start or restart, incremented each time an
instance of anything is created. */
extern UINT32 g_unique_instance_ID_counter;

/** Get the next instance number. */
UINT32 GetNextInstanceID(void);



/** A class descriptor. Common to all classes. */
typedef struct _ClassDescriptor
{
	/** Unique ID of the class - set at compile time. */
	UINT32 classID;

	/** All constructors look the same. To make debugging easy, we pass a generic
	array of words to the constructor, which needs to figure out what to do with
	them.

	On return, we have a pointer to an initialized instance ready for use.
	*/
	struct _InstanceDescriptor *(*Constructor)(INT32 *retVal, size_t argCount, const Sample *args);
}
ClassDescriptor;

extern const ClassDescriptor ClassInputPin;

extern const ClassDescriptor ClassOutputPin;

/** The table of classes known to the framework. */
extern const ClassDescriptor *g_class_descriptor_table[3];

/** Get the number of classes known to the framework. */
size_t awe_fwGetClassCount(void);


/** An instance descriptor. Common to all instances. */
typedef struct _InstanceDescriptor
{
	/** The unique instance ID of this instance. */
	UINT32 nUniqueInstanceID;

	/** Chain instances together in a linked list. */
	struct _InstanceDescriptor *pNextInstance;

	/** Pointer back to the static descriptor from which this instance was
	created. */
	const ClassDescriptor *pClassDescriptor;
}
InstanceDescriptor;

/** Head of chain of class instances. */
extern InstanceDescriptor *g_pInstanceHead;

/** Tail of chain of class instances. */
extern InstanceDescriptor *g_pInstanceTail;

/** Head of chain of I/O pin instances.
Must be initialized to point at the last item in the above. */
extern InstanceDescriptor *g_pIOPinHead;

/** Get the first object in the object chain. */
INT32 awe_fwGetFirstObject(InstanceDescriptor **pObject, UINT32 *pClassID);

/** Get the next object in the object chain. */
INT32 awe_fwGetNextObject(InstanceDescriptor *currentObject, InstanceDescriptor **pObject, UINT32 *pClassID);

/** Get the index'th object on the chain of objects. */
INT32 awe_fwGetObjectByIndex(UINT32 index, InstanceDescriptor **pObject, UINT32 *pClassID);

/** Get an object based on its unique ID. */
INT32 awe_fwGetObjectByID(UINT32 ID, InstanceDescriptor **pObject, UINT32 *pClassID);

/** Get the first I/O object in the object chain. */
INT32 awe_fwGetFirstIO(InstanceDescriptor **pObject, UINT32 *pClassID);

/** Get the next I/O object in the object chain. */
INT32 awe_fwGetNextIO(InstanceDescriptor *currentObject, InstanceDescriptor **pObject, UINT32 *pClassID);

/** Return the type of the class this instance was created from (effectively a member function). */
UINT32 awe_fwGetClassType(const InstanceDescriptor *pClass);

/** A pin instance. */
typedef struct _PinInstanceDescriptor
{
	/** The basic instance data. */
	InstanceDescriptor instanceDescriptor;

	/** Packed size description information. See
	ClassPin_GetNumSamples, ClassPin_GetBlockSize. */
	UINT32 pinSizeDescriptor;

	/** Sample rate of this pin. */
	UINT32 sampleRate;
}
PinInstanceDescriptor;

/** An I/O pin instance. These objects are defined at compile
time to match the physical I/O. */
typedef struct _IOPinDescriptor
{
	/** Basic pin description. */
	PinInstanceDescriptor basePinType;

	/** Pointer to the wire instance this pin is connected to. */
	struct _WireInstance *pWire;

	/** Pointer to the buffer allocated by the wire - copied from the wire at bind time. */
	Sample *pOrigBuffer;

	/** Pointer to the double buffer allcoated at bind time. */
	Sample *pSecondBuffer;

	/** Flags to control buffer flipping. */
	UINT32 ctrlFlags;
}
IOPinDescriptor;

#define PINPROPS(nSamples, nInterleaved, nSizeBytes, complex) \
	((((nSizeBytes) & 0xf) << 24) | (((nInterleaved) & 0xff) << 16) | (((complex) & 0x01) << 15) | ((nSamples) & 0x7fff))

/** Find out whether the pin is defined as complex. */
#define ClassPin_GetComplex(pPinInstanceDescriptor) \
	((((pPinInstanceDescriptor)->pinSizeDescriptor) & (0x8000)) >> (15))

/** Get the buffer size for the pin. */
#define ClassPin_GetNumSamples(pPinInstanceDescriptor) \
	((ClassPin_GetBlockSize(pPinInstanceDescriptor)) * (ClassPin_GetChannelCount(pPinInstanceDescriptor)))

/** Get the number of samples for the pin. */
#define ClassPin_GetBlockSize(pPinInstanceDescriptor) \
	(((pPinInstanceDescriptor)->pinSizeDescriptor) & (0x7fff))

/** Get the number of interleaved samples (channels) for the pin. */
#define ClassPin_GetChannelCount(pPinInstanceDescriptor) \
	((((pPinInstanceDescriptor)->pinSizeDescriptor) >> (16)) & (0xff))

/** Get the size in bytes of each sample. */
#define ClassPin_GetNSampleSize(pPinInstanceDescriptor) \
	((((pPinInstanceDescriptor)->pinSizeDescriptor) >> (24)) & (0x0f))

/** Get the same rate of the pin. */
#define ClassPin_GetSampleRate(pPinInstanceDescriptor) \
	((pPinInstanceDescriptor)->sampleRate)

/** Test if a pin is compatible with an I/O pin. */
#define IsCompatiblePintype(pinDesc, IOdesc) \
	(((pinDesc) & 0xffff0000) == ((IOdesc) & 0xffff0000))

/** Test if an output pin is compatible with an I/O pin. */
#define IsCompatibleOutputPintype(pinDesc, IOdesc) \
	(((pinDesc) & 0xff000000) == ((IOdesc) & 0xff000000))

/** Test if a pin's size is compatible with an I/O pin's size. */
#define IsCompatiblePinSize(pinDesc, IOdesc) \
	(((pinDesc) & 0xffff) % ((IOdesc) & 0xffff) == 0)


/** Pin instance constructor. The constructor takes five values: pinType, pinSizeDescriptor(3)
and sampleRate. If the return is zero, there is an error, and retVal will be assigned the
reason for the error. */
InstanceDescriptor *ClassPin_Constructor(INT32 *retVal, size_t argCount, const Sample *args);

/** Query a given pin instance for its properties. Return value is pin type. All return
arguments must point to good values. */
INT32 awe_fwGetPinType(InstanceDescriptor *pInstance, INT32 *numChannels, INT32 *numSamples, INT32 *size, INT32 *SR);

/*------------------ Dclarations of I/O ------------------*/

/** The input pin. */
extern DLLSYMBOL IOPinDescriptor InterleavedInputPin;

/** The output pin. */
extern DLLSYMBOL IOPinDescriptor InterleavedOutputPin;


/*------------------ Wire declarations ------------------*/
typedef struct _WireInstance
{
	/** The basic instance data. */
	InstanceDescriptor instanceDescriptor;

	/** The pin descriptor describing this wire. */
	PinInstanceDescriptor *pPinDescriptor;

	/** The wire buffer. */
	Sample *buffer;
}
WireInstance;


/** Generic wire instance constructor used by all wire constructors. */
InstanceDescriptor *GenericWire_Constructor(INT32 *retVal, UINT32 bufferSize, PinInstanceDescriptor *pPin, const ClassDescriptor *pClass);

/** Wire instance constructor. If the return is zero, there is an error, and retVal
will be assigned the reason for the error.

The arguments have meaning as follows:

0:	pPinDescriptor - pointer to a pin descriptor instance
*/
InstanceDescriptor *ClassWire_Constructor(INT32 *retVal, size_t argCount, const Sample *args);

/** Bind a wire to an I/O object.

	The arguments have meaning as follows:

	0:	pWire - pointer to a wire instance
	1:	ioPin - pointer to an I/O pin
*/
INT32 BindIOToWire(size_t argCount, const Sample *args);




/*------------------ Module declarations ------------------*/

typedef struct _ModClassDescriptor
{
	/** Unique ID of the class - set at compile time. */
	UINT32 classID;

	/** All constructors look the same. To make debugging easy, we pass a generic
	array of words to the constructor, which needs to figure out what to do with
	them.

	On return, we have a pointer to an initialized instance ready for use.
	*/
	struct _ModInstanceDescriptor *(*Constructor)(
			INT32 * FW_RESTRICT retVal,
			UINT32 nIO,
			WireInstance ** FW_RESTRICT pWires,
			size_t argCount,
			const Sample * FW_RESTRICT args);
}
ModClassDescriptor;


/** Class descriptor for audio modules. */
typedef struct _ModClassModule
{
	/** The basic class data. */
	ModClassDescriptor modClassDescriptor;

	/** The number of construction module parameters. */
	UINT32 nPackedParameters;

	/** Pump function for the module. */
	void (*pProcessFunc)(void *pInstance);

	/** Bypass function for the module. */
	void (*pBypassFunc)(void *pInstance);

	/** Set function. */
	UINT32 (*pSet)(void *pInstance, UINT32 mask);

	/** Get function. */
	UINT32 (*pGet)(void *pInstance, UINT32 mask);

	void (*pSetState)(void *pInstance, UINT32 state);
}
ModClassModule;

/** A module instance descriptor. Common to all module instances. */
typedef struct _ModInstanceDescriptor
{
	/** The unique instance ID of this instance. */
	UINT32 nUniqueInstanceID;

	/** Chain instances together in a linked list. */
	struct _ModInstanceDescriptor *pNextInstance;

	/** Pointer back to the static descriptor from which this instance was
	created. */
	const ModClassModule *pModClassDescriptor;
}
ModInstanceDescriptor;


/** Set the packed argument count. */
#define ClassModule_PackArgCounts(nPublicArgs, nPrivateArgs) \
	(((nPrivateArgs) << 16) | ((nPublicArgs) & 0xffff))

/** Get the number of public arguments. */
#define ClassMod_GetNPublicArgs(packed) \
	((packed) & 0xffff)

/** Get the number of private arguments. */
#define ClassMod_GetNPrivateArgs(packed) \
	(((packed) >> 16) & 0xffff)

#define ClassWire_GetNumSamples(W) (ClassPin_GetNumSamples((W)->pPinDescriptor))
#define ClassWire_GetBlockSize(W) (ClassPin_GetBlockSize((W)->pPinDescriptor))
#define ClassWire_GetChannelCount(W) (ClassPin_GetChannelCount((W)->pPinDescriptor))
#define ClassWire_GetSampleRate(W) (ClassPin_GetSampleRate((W)->pPinDescriptor))
#define ClassWire_GetComplex(W) (ClassPin_GetComplex((W)->pPinDescriptor))

/** The table of module descriptions. */
extern const ModClassModule *g_module_descriptor_table[];

/** The number of elements in g_module_descriptor_table. */
extern UINT32 g_module_descriptor_table_size;

/** Lookup a module class by ID. */
extern const ModClassModule *LookupModuleClass(UINT32 classID);

/** Returns the number of modules in the table. */
extern UINT32 awe_fwGetCIModuleCount(void);

/** Get the information for the specified module. Returns the classID on success and
all requested values, otherwise returns a negative error code. The argument pointers must
all be valid. */
INT32 awe_fwGetCIModuleInfo(size_t index, const ModClassModule **pDescr, UINT32 *classID, size_t *numParameters);


/** The base class from which all module instance parameters are derived.
Derived classes must be declared for each module.
*/
typedef struct _ModuleInstanceParms
{
	INT32 dummy;
}
ModuleInstanceParms;

/** Low order 2 bits of flags high byte are the module active state. */
#define MODULE_ACTIVE_MASK			0x03000000
#define MODULE_ACTIVE				0x00000000
#define MODULE_BYPASS				0x01000000
#define MODULE_MUTE					0x02000000
#define MODULE_INACTIVE				0x03000000


/** This bit allows for deferred execution of the module's set function in the main thread. */
#define MODULE_GENERAL_DEFERRED_SET			0x08000000
#define awe_modIsDeferredSet(S)				(((S->instance.packedFlags) & (MODULE_GENERAL_DEFERRED_SET)) != (0))
#define awe_modSetDeferredSet(S, v)			S->instance.packedFlags &= ~MODULE_GENERAL_DEFERRED_SET; S->instance.packedFlags |= ((v & 1) << 27)



/** High 4 bits of flags high byte are available for any use. */
#define MODULE_GENERAL_MASK			0xf0000000
#define GetGeneralFlagBits(S)		(((S->instance.packedFlags) >> (28)) & (0x1f))
#define SetGeneralFlagBits(S, v)	S->instance.packedFlags &= ~MODULE_GENERAL_MASK; S->instance.packedFlags |= ((v & 0xf) << 28)

/** A module instance. The instance is variable length on the heap, the size
being determinind by the constructor arguments. */
typedef struct _ModuleInstanceDescriptor
{
	/** The basic instance data. */
	ModInstanceDescriptor instanceDescriptor;

	/** Pointer to owning layout instance (type is a forward reference). */
	struct _LayoutInstance *pOwner;

	/** Control flags. */
	UINT32 packedFlags;

	/** Module specific profiling time.	 This is number of cycles times 256. */
	UINT32 profileTime;

	/** Array of input and output wires. There are nInwires + nOutWires + nScratchWires elements. */
	WireInstance **pWires;

	/** Pump function for the module. May be empty, pProcessFunc, or, pBypassFunc. */
	void (*pProcessFunc)(void *pInstance);
}
ModuleInstanceDescriptor;

/** Fetch the value at the given address. */
INT32 awe_fwGetCallFetchValue(UINT32 address, ModuleInstanceDescriptor *pMod, UINT32 mask);

/** Set the value at the given address, then do set call. */
INT32 awe_fwSetValueSetCall(UINT32 address, INT32 value, ModuleInstanceDescriptor *pMod, UINT32 mask);

/** Set the value at the given address, then do set call. */
//INT32 awe_fwSetFloatValueSetCall(UINT32 address, FLOAT32 value, ModuleInstanceDescriptor *pMod, UINT32 mask);


#define ClassModule_Set(pInstance, mask) \
{ \
UINT32 (*pSet)(void *a, UINT32 b); \
pSet=(pInstance)->instance.instanceDescriptor.pModClassDescriptor->pSet; \
if (pSet) \
pSet(pInstance, mask); \
}

// This is the function code version which includes profiling
void ClassModule_Execute(ModuleInstanceDescriptor *pInstance);

// This is the inline "non-profiling" NP version
static VEC_INLINE void ClassModule_ExecuteNP(ModuleInstanceDescriptor *pModule)
{
  pModule->pProcessFunc(pModule);
}

#define ClassModule_Get(pInstance, mask) \
{ \
UINT32 (*pGet)(void *a, UINT32 b); \
pGet=(pInstance)->instance.instanceDescriptor.pModClassDescriptor->pGet; \
if (pGet) \
pGet(pInstance, mask); \
}

/** Pack the sizes into the flags word. */
#define ClassModule_PackFlags(nInWires, nOutWires, nScratchWires)\
	((((nScratchWires) & 0xff) << 16) | (((nOutWires) & 0xff) << 8) | ((nInWires) & 0xff))

/** Get the input wire count 0..255 */
#define ClassMod_GetNInWires(packed)\
	((packed) & 0xff)

/** Get the output wire count 0..255 */
#define ClassMod_GetNOutWires(packed)\
	(((packed) >> 8) & 0xff)

/** Get the scratch wire count. */
#define ClassMod_GetNScratchWires(packed)\
	(((packed) >> 16) & 0xff)

/** Get the total number of wires */
#define ClassMod_GetWireCount(packed)\
	(((packed) & 0xff) + (((packed) >> 8) & 0xff) + (((packed) >> 16) & 0xff))

/** Get the module state. */
#define ClassMod_GetModuleState(packed)\
	(((packed) >> 24) & 0x03)


/** These macros operate on high-level module objects. The goal is to hide the
	details of the implementation from the module writer. */

/** Get the wires that are attached to the module */
#define ClassModule_GetWires(S) ((S)->instance.pWires)

/** Get the input wire count 0..255 */
#define ClassModule_GetNInWires(S)\
	(ClassMod_GetNInWires((S)->instance.packedFlags))

/** Get the output wire count 0..255 */
#define ClassModule_GetNOutWires(S)\
	(ClassMod_GetNOutWires((S)->instance.packedFlags))

/** Get the scratch wire count. */
#define ClassModule_GetNScratchWires(S)\
	(ClassMod_GetNScratchWires((S)->instance.packedFlags))

/** Get the module state. */
#define ClassModule_GetModuleState(S)\
	(ClassMod_GetModuleState((S)->instance.packedFlags))


/** Base class module instance constructor. If the return is zero, there is an error, and retVal will
be assigned the reason for the error.

This function must be called first to construct the base module, then derived classes
may operate on the constructed object further.
*/
ModInstanceDescriptor *BaseClassModule_Constructor(
		const ModClassModule * FW_RESTRICT pClass,
		INT32 * FW_RESTRICT retVal,
		UINT32 nIO,
		WireInstance ** FW_RESTRICT pWires,
		size_t argCount,
		const Sample * FW_RESTRICT args);


/** Generic module instance constructor. If the return is zero, there is an error, and retVal will
be assigned the reason for the error.

The arguments have meaning as follows:
*/
ModInstanceDescriptor *ClassModule_Constructor(
		UINT32 classID,
		INT32 * FW_RESTRICT retVal,
		UINT32 nIO,
		WireInstance ** FW_RESTRICT pWires,
		size_t argCount,
		const Sample * FW_RESTRICT args);

/** Set the given module to active=0, bypass=1, mute=2, inactive=other. */
INT32 awe_fwSetModuleState(ModuleInstanceDescriptor *pModule, INT32 state);

/** Get the given modules state: active=0, bypass=1, mute=2, inactive=3. */
INT32 awe_fwGetModuleState(ModuleInstanceDescriptor *pModule);

/** Pump the specified module. */
INT32 awe_fwPumpModule(ModuleInstanceDescriptor *pModule);

/** Deliver an event to the specified module. */
INT32 awe_fwModuleEvent(ModuleInstanceDescriptor *pModule, INT32 eventCode);

/* Returns the number of channels in the Layout's input and output pins. */
void awe_fwGetChannelCount(INT32 *inCount, INT32 *outCount);

/** Returns a pointer to where the platform should write input data */
INT32 *awe_fwGetInputChannelPtr(INT32 chan, INT32 *stride);

/** Returns a pointer to where the platform should read output data from */
INT32 *awe_fwGetOutputChannelPtr(INT32 chan, INT32 *stride);

/** Handles double buffering of the Layout's I/O pins. */
UINT32 awe_fwAudioDMAComplete(INT32 samplesPerTick);

/* Returns the block size of the Layout. */
INT32 awe_fwGetInputBlockSize(void);

/* Returns the sample rate of the Layout. */
INT32 awe_fwGetInputSampleRate(void);

/* Initialize the Layout IO wire buffers once layout is created */
void awe_fwAudioInitIO(void);

/** This must be called before doing anything else with AWE. */
void awe_fwInit_io_pins(void);

/** Static non-thread safe instance pointer. */
extern InstanceDescriptor *g_pTickObject;


/*------------------ Layout declarations ------------------*/
/** A layout instance. The instance is variable length on the heap, the size
being determined by the constructor arguments. */
typedef struct _LayoutInstance
{
	/** The basic instance data. */
	InstanceDescriptor instanceDescriptor;

	struct _LayoutInstance *pNextLayout;

	/** When 1, called on every iteration, otherwise called less frequently. */
	UINT32 nDivider;

	/** When zero, will execute, and be set to nDivider-1, otherwise will be decremeneted. */
	UINT32 nCurrentDivide;

	/** Average cycles per process. Times 256. */
	UINT32 averageCycles;

	/** Peak cycles per process. Times 256. */
	UINT32 peakCycles;

	/** Number of processes. */
	INT32 processCount;

	/** Instantaneous cycles for the entire layout that executed.  Times 256. */
	UINT32 instCycles;

	/** Averaged time in cycles between calls to the processing function.  Times 256. */
	UINT32 timePerProcess;

	/** Percentage CPU time */
	FLOAT32 percentCPU;

	/** Number of modules in the layout. */
	UINT32 nModules;

	/** Variable length array of module pointers. */
	ModuleInstanceDescriptor *pModuleInstances[1];
}
LayoutInstance;

/** Head of list of layouts. */
extern LayoutInstance *g_pLayoutHead;

/** Tail of list of layouts. */
extern LayoutInstance *g_pLayoutTail;

/* Layout instance constructor. If the return is zero, there is an error, and retVal will
be assigned the reason for the error.

The arguments have meaning as follows:

0:	nDivider = clock divider, default is 1
1:	scratchSize - amount of scratch needed in words
2:	nModules - number of modules in layout
3+	array of nModules pointers (names) of module instances
*/
InstanceDescriptor *ClassLayout_Constructor(INT32 *retVal, INT32 nModules, INT32 nDivider);

/* Adds one or module modules to a layout */
INT32 awe_fwAddModuleToLayout(size_t argCount, const Sample *args);

/** Generic layout pump function. */
void ClassLayout_Process(LayoutInstance *pInstance, UINT32 *pInstCycles);

/** Master framework pump function. */
INT32 awe_fwPump(UINT32 layout_no);

/** Master framework tick function. */
INT32 awe_fwTick(void);

/** Call a module's set function if it has one. */
INT32 awe_fwSetCall(ModuleInstanceDescriptor *pModule, UINT32 mask);

/** Call a module's get function if it has one. */
INT32 awe_fwGetCall(ModuleInstanceDescriptor *pModule, UINT32 mask);

/** Write the arguments to the given wire. */
INT32 awe_fwSetWire(InstanceDescriptor *pWireInst, size_t offset, size_t argCount, const Sample *args);

/** Read the wire into the arguments. */
INT32 awe_fwGetWire(InstanceDescriptor *pWireInst, size_t offset, size_t argSize, Sample *args);

/** Fetch the values at the given address. */
INT32 awe_fwFetchValues(UINT32 address, size_t offset, size_t argSize, Sample *args);

/** Fetch the values at the given address. */
INT32 awe_fwGetCallFetchValues(UINT32 address, size_t offset, ModuleInstanceDescriptor *pMod, UINT32 mask, size_t argSize, Sample *args);

/** Write the arguments to the given address. */
INT32 awe_fwSetValues(UINT32 address, size_t offset, size_t argCount, const Sample *args);

/** Write the arguments to the given address. */
INT32 awe_fwSetValuesSetCall(UINT32 address, size_t offset, ModuleInstanceDescriptor *pMod, UINT32 mask, size_t argCount, const Sample *args);

/* Returns the profiling information for a specified layout */
/* pAverageCycles is the number of cycles times 128. */
/* pTimePerProcess is the total number of cycles available per block process times 128. */
INT32 awe_fwGetProfileValues(INT32 layoutNumber, UINT32 *pAverageCycles, UINT32 *pTimePerProcess);

/* Clears the profiling information for a specified layout */
INT32 awe_fwClearProfileValues(INT32 layoutNumber);

/**
 * @brief Set the object by ID and call the set function of the object to set the object values.
 * @param g_PacketBuffer			command packet buffer
 * @return							E_SUCCESS or error code
 *
 * Packet format is:
 * packet[0]		object ID
 * packet[1]		variable address
 * packet[2]		if != -1 array dereference offset
 * packet[3]		array size
 * packet[4]		set mask
 * packet[5] through packet[5 + array_size - 1] values to write
 */
INT32 awe_fwSetObjectValueCall(DWORD *g_PacketBuffer);

/**
 * @brief Get the object by ID and call the get function of the object to fetch the object values.
 * @param g_PacketBuffer			command packet buffer
 * @return							E_SUCCESS or error code
 *
 * Packet format is:
 * packet[0]		object ID
 * packet[1]		variable address
 * packet[2]		if != -1 array dereference offset
 * packet[3]		array size
 * packet[4]		get mask
 *
 * array size words are written back to &packet[1]
 */
INT32 awe_fwFetchObjectValueCall(DWORD *g_PacketBuffer);


/**
 * @brief Load an AWB file.
 * @param binaryFile				the AWB file to load
 * @return							zero or error code
 */
INT32 BinaryLoad(const char *binaryFile);

/** Translate a real address to relative. */
DWORD translateRealToRelative(DWORD wrd);

/** Translate a rlative address to real. */
UINT32 translateAddr(INT32 *retVal, UINT32 RADDR);



/** The maximim ID supported by the lookup table. */
extern UINT32 g_maxID;

/** The pointer to the lookup table - initially NULL. */
extern UINT32 *g_lookup_table;

/** Create a table for O(1) object lookup. */
UINT32 awe_fwcreateLookupTable(UINT32 maxID);

/** Set an object pointer in the lookup table. */
void awe_fwSetIDPointer(UINT32 id, InstanceDescriptor *pObject);

/** Lookup an object by ID. */
UINT32 awe_fwLookupID(UINT32 id);

/** Update the object lookup table. */
UINT32 awe_fwSUpdateLookupTable(void);


extern DWORD g_target_control_flags;

#ifndef NULL
#define NULL 0
#endif

#ifndef SUCCESS
#define SUCCESS 1
#endif

#ifndef FAILURE
#define FAILURE 0
#endif

#ifdef __GNUC__
  #if __GNUC__ >= 4
	#define DLL_PUBLIC __attribute__ ((visibility("default")))
  #else
	#define DLL_PUBLIC
  #endif
#else
  #define DLL_PUBLIC
#endif


#ifdef	__cplusplus
}
#endif

#ifdef WIN32
#define FIXNAME(x)		x
#else
#define PASTE(x, y)		x ## y
#define EVAL(x, y)		PASTE(x, y)
#define FIXNAME(x)		EVAL(MODULE_PREFIX, x)
#endif

#define AWE_UNUSED_VARIABLE(x)	((void)(x))

#endif	/* _FRAMEWORK_H */

