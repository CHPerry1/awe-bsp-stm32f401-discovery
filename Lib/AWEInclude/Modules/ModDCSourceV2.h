/****************************************************************************
*
*               Audio Framework
*               ---------------
*
****************************************************************************
*     ModDCSourceV2.h
****************************************************************************
*
*     Description:  Outputs a constant DC value with optional input control pin to scale DC value.
*
*     Copyright:    DSP Concepts, Inc, 2007 - 2015
*                   1800 Wyatt Drive, Suite 14
*                   Santa Clara, CA 95054
*
***************************************************************************/

/**
 * @addtogroup Modules
 * @{
 */

/**
 * @file
 * @brief Outputs a constant DC value with optional input control pin to scale DC value.
 */

#ifndef _MOD_DCSOURCEV2_H
#define _MOD_DCSOURCEV2_H

#include "ModCommon.h"
#include "MathHelper.h"

#define MASK_DCSourceV2_value 0x00000100
#define OFFSET_DCSourceV2_value 0x00000008

#define CLASSID_DCSOURCEV2 (CLASS_ID_MODBASE + 1242)

#ifdef __cplusplus
extern "C" {
#endif

// ----------------------------------------------------------------------
// Overall instance class
// ----------------------------------------------------------------------

typedef struct _awe_modDCSourceV2Instance
{
    ModuleInstanceDescriptor instance;
    float              value;               // Data Value
    
} awe_modDCSourceV2Instance;

/* By default the class object is included and this is required for dynamic
 * instantiation.  When doing static code (define AWE_STATIC_CODE) you can save
 * a little bit of memory by eliminating class objects.  However, you lose the
 * ability to change the module status. */

#if (!defined(AWE_NO_CLASS_OBJECTS) || !defined(AWE_STATIC_CODE))
extern const ModClassModule awe_modDCSourceV2Class;
#endif // #ifndef AWE_NO_CLASS_OBJECTS

/* Dynamic instantiation is used by default.  When building for static
** code, define AWE_STATIC_CODE to eliminate the constructor function. */

#ifndef AWE_STATIC_CODE
// This points the constructor for this class to the base constructor
#define awe_modDCSourceV2Constructor(ARG1, ARG2, ARG3, ARG4, ARG5) ClassModule_Constructor(CLASSID_DCSOURCEV2, ARG1, ARG2, ARG3, ARG4, ARG5)
#endif // #ifndef AWE_STATIC_CODE


void awe_modDCSourceV2Process(void *pInstance);

 



#ifdef __cplusplus
}
#endif


#endif // _MOD_DCSOURCEV2_H

/**
 * @}
 *
 * End of file.
 */
