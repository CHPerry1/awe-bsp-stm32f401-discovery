/****************************************************************************
*
*               Audio Framework
*               ---------------
*
****************************************************************************
*     ModRawStreamingSource.h
****************************************************************************
*
*     Description:  Reads raw data from a streaming buffer module
*
*     Copyright:    DSP Concepts, Inc, 2007 - 2015
*                   1800 Wyatt Drive, Suite 14
*                   Santa Clara, CA 95054
*
***************************************************************************/

/**
 * @addtogroup Modules
 * @{
 */

/**
 * @file
 * @brief Reads raw data from a streaming buffer module
 */

#ifndef _MOD_RAWSTREAMINGSOURCE_H
#define _MOD_RAWSTREAMINGSOURCE_H

#include "ModCommon.h"
#include "MathHelper.h"

#define MASK_RawStreamingSource_underflowCount 0x00000100
#define OFFSET_RawStreamingSource_underflowCount 0x00000008

#define CLASSID_RAWSTREAMINGSOURCE (CLASS_ID_MODBASE + 27)

#ifdef __cplusplus
extern "C" {
#endif

// ----------------------------------------------------------------------
// Overall instance class
// ----------------------------------------------------------------------

typedef struct _awe_modRawStreamingSourceInstance
{
    ModuleInstanceDescriptor instance;
    int                underflowCount;      // Number of output samples that could not be serviced.
    
} awe_modRawStreamingSourceInstance;

/* By default the class object is included and this is required for dynamic
 * instantiation.  When doing static code (define AWE_STATIC_CODE) you can save
 * a little bit of memory by eliminating class objects.  However, you lose the
 * ability to change the module status. */

#if (!defined(AWE_NO_CLASS_OBJECTS) || !defined(AWE_STATIC_CODE))
extern const ModClassModule awe_modRawStreamingSourceClass;
#endif // #ifndef AWE_NO_CLASS_OBJECTS

/* Dynamic instantiation is used by default.  When building for static
** code, define AWE_STATIC_CODE to eliminate the constructor function. */

#ifndef AWE_STATIC_CODE
// This points the constructor for this class to the base constructor
#define awe_modRawStreamingSourceConstructor(ARG1, ARG2, ARG3, ARG4, ARG5) ClassModule_Constructor(CLASSID_RAWSTREAMINGSOURCE, ARG1, ARG2, ARG3, ARG4, ARG5)
#endif // #ifndef AWE_STATIC_CODE


void awe_modRawStreamingSourceProcess(void *pInstance);

 



#ifdef __cplusplus
}
#endif


#endif // _MOD_RAWSTREAMINGSOURCE_H

/**
 * @}
 *
 * End of file.
 */
