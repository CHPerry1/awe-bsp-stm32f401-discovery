/****************************************************************************
*
*               Audio Framework
*               ---------------
*
****************************************************************************
*     ModWithinRange.h
****************************************************************************
*
*     Description:  Checks if the input signal levels are within a specified upper and lower range.
*
*     Copyright:    DSP Concepts, Inc, 2007 - 2015
*                   1800 Wyatt Drive, Suite 14
*                   Santa Clara, CA 95054
*
***************************************************************************/

/**
 * @addtogroup Modules
 * @{
 */

/**
 * @file
 * @brief Checks if the input signal levels are within a specified upper and lower range.
 */

#ifndef _MOD_WITHINRANGE_H
#define _MOD_WITHINRANGE_H

#include "ModCommon.h"
#include "MathHelper.h"

#define MASK_WithinRange_lower 0x00000100
#define MASK_WithinRange_upper 0x00000200
#define MASK_WithinRange_withinRange 0x00000400
#define MASK_WithinRange_outputValue 0x00000800
#define OFFSET_WithinRange_lower 0x00000008
#define OFFSET_WithinRange_upper 0x00000009
#define OFFSET_WithinRange_withinRange 0x0000000A
#define OFFSET_WithinRange_outputValue 0x0000000B

#define CLASSID_WITHINRANGE (CLASS_ID_MODBASE + 1073)

#ifdef __cplusplus
extern "C" {
#endif

// ----------------------------------------------------------------------
// Overall instance class
// ----------------------------------------------------------------------

typedef struct _awe_modWithinRangeInstance
{
    ModuleInstanceDescriptor instance;
    float              lower;               // Lower edge of the allowable range.
    float              upper;               // Upper edge of the allowable range.
    int                withinRange;         // Equals 1 if all input samples in the block are within range. Otherwise equals 0.
    int                outputValue;         // Specifies the behavior of the output pin.
    
} awe_modWithinRangeInstance;

/* By default the class object is included and this is required for dynamic
 * instantiation.  When doing static code (define AWE_STATIC_CODE) you can save
 * a little bit of memory by eliminating class objects.  However, you lose the
 * ability to change the module status. */

#if (!defined(AWE_NO_CLASS_OBJECTS) || !defined(AWE_STATIC_CODE))
extern const ModClassModule awe_modWithinRangeClass;
#endif // #ifndef AWE_NO_CLASS_OBJECTS

/* Dynamic instantiation is used by default.  When building for static
** code, define AWE_STATIC_CODE to eliminate the constructor function. */

#ifndef AWE_STATIC_CODE
// This points the constructor for this class to the base constructor
#define awe_modWithinRangeConstructor(ARG1, ARG2, ARG3, ARG4, ARG5) ClassModule_Constructor(CLASSID_WITHINRANGE, ARG1, ARG2, ARG3, ARG4, ARG5)
#endif // #ifndef AWE_STATIC_CODE


void awe_modWithinRangeProcess(void *pInstance);

 

void awe_modWithinRangeBypass(void *pInstance);


#ifdef __cplusplus
}
#endif


#endif // _MOD_WITHINRANGE_H

/**
 * @}
 *
 * End of file.
 */
