/****************************************************************************
*
*               Audio Framework
*               ---------------
*
****************************************************************************
*     ModRepWinOverlap.h
****************************************************************************
*
*     Description:  Replicates, windows, and overlap adds signals
*
*     Copyright:    DSP Concepts, Inc, 2007 - 2015
*                   1800 Wyatt Drive, Suite 14
*                   Santa Clara, CA 95054
*
***************************************************************************/

/**
 * @addtogroup Modules
 * @{
 */

/**
 * @file
 * @brief Replicates, windows, and overlap adds signals
 */

#ifndef _MOD_REPWINOVERLAP_H
#define _MOD_REPWINOVERLAP_H

#include "ModCommon.h"
#include "MathHelper.h"

#define MASK_RepWinOverlap_winLen 0x00000100
#define MASK_RepWinOverlap_repCount 0x00000200
#define MASK_RepWinOverlap_window 0x00000400
#define MASK_RepWinOverlap_state 0x00000800
#define OFFSET_RepWinOverlap_winLen 0x00000008
#define OFFSET_RepWinOverlap_repCount 0x00000009
#define OFFSET_RepWinOverlap_window 0x0000000A
#define OFFSET_RepWinOverlap_state 0x0000000B

#define CLASSID_REPWINOVERLAP (CLASS_ID_MODBASE + 1414)

#ifdef __cplusplus
extern "C" {
#endif

// ----------------------------------------------------------------------
// Overall instance class
// ----------------------------------------------------------------------

typedef struct _awe_modRepWinOverlapInstance
{
    ModuleInstanceDescriptor instance;
    int                winLen;              // Length of the window
    int                repCount;            // Number of times to replicate the input prior to applying the window
    float*             window;              // Window coefficients
    float*             state;               // State variable array
} awe_modRepWinOverlapInstance;

/* By default the class object is included and this is required for dynamic
 * instantiation.  When doing static code (define AWE_STATIC_CODE) you can save
 * a little bit of memory by eliminating class objects.  However, you lose the
 * ability to change the module status. */

#if (!defined(AWE_NO_CLASS_OBJECTS) || !defined(AWE_STATIC_CODE))
extern const ModClassModule awe_modRepWinOverlapClass;
#endif // #ifndef AWE_NO_CLASS_OBJECTS

/* Dynamic instantiation is used by default.  When building for static
** code, define AWE_STATIC_CODE to eliminate the constructor function. */

#ifndef AWE_STATIC_CODE
ModInstanceDescriptor * awe_modRepWinOverlapConstructor(INT32 * FW_RESTRICT retVal, UINT32 nIO, WireInstance ** FW_RESTRICT pWires, size_t argCount, const Sample * FW_RESTRICT args);
#endif // #ifndef AWE_STATIC_CODE


void awe_modRepWinOverlapProcess(void *pInstance);

 



#ifdef __cplusplus
}
#endif


#endif // _MOD_REPWINOVERLAP_H

/**
 * @}
 *
 * End of file.
 */
