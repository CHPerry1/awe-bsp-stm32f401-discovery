/****************************************************************************
*
*               Audio Framework
*               ---------------
*
****************************************************************************
*     ModMeasurement.h
****************************************************************************
*
*     Description:  Performs transfer function measurements
*
*     Copyright:    DSP Concepts, Inc, 2007 - 2015
*                   1800 Wyatt Drive, Suite 14
*                   Santa Clara, CA 95054
*
***************************************************************************/

/**
 * @addtogroup Modules
 * @{
 */

/**
 * @file
 * @brief Performs transfer function measurements
 */

#ifndef _MOD_MEASUREMENT_H
#define _MOD_MEASUREMENT_H

#include "ModCommon.h"
#include "MathHelper.h"

#define MASK_Measurement_L 0x00000100
#define MASK_Measurement_numReps 0x00000200
#define MASK_Measurement_trigger 0x00000400
#define MASK_Measurement_readIndex 0x00000800
#define MASK_Measurement_stateIndex 0x00001000
#define MASK_Measurement_cnt 0x00002000
#define MASK_Measurement_stimulus 0x00004000
#define MASK_Measurement_response 0x00008000
#define OFFSET_Measurement_L 0x00000008
#define OFFSET_Measurement_numReps 0x00000009
#define OFFSET_Measurement_trigger 0x0000000A
#define OFFSET_Measurement_readIndex 0x0000000B
#define OFFSET_Measurement_stateIndex 0x0000000C
#define OFFSET_Measurement_cnt 0x0000000D
#define OFFSET_Measurement_stimulus 0x0000000E
#define OFFSET_Measurement_response 0x0000000F

#define CLASSID_MEASUREMENT (CLASS_ID_MODBASE + 1216)

#ifdef __cplusplus
extern "C" {
#endif

// ----------------------------------------------------------------------
// Overall instance class
// ----------------------------------------------------------------------

typedef struct _awe_modMeasurementInstance
{
    ModuleInstanceDescriptor instance;
    int                L;                   // Length of the stimulus signal.
    int                numReps;             // Number of times to repeat the stimulus and average the result
    int                trigger;             // state variable/parameter which the PC sets to trigger the measurement.
    int                readIndex;           // Index used for circular reading
    int                stateIndex;          // Index used for state
    int                cnt;                 // Index used to measure the response
    float*             stimulus;            // Column vector of length L holding the stimulus signal
    float*             response;            // Holds the time average measured signal
} awe_modMeasurementInstance;

/* By default the class object is included and this is required for dynamic
 * instantiation.  When doing static code (define AWE_STATIC_CODE) you can save
 * a little bit of memory by eliminating class objects.  However, you lose the
 * ability to change the module status. */

#if (!defined(AWE_NO_CLASS_OBJECTS) || !defined(AWE_STATIC_CODE))
extern const ModClassModule awe_modMeasurementClass;
#endif // #ifndef AWE_NO_CLASS_OBJECTS

/* Dynamic instantiation is used by default.  When building for static
** code, define AWE_STATIC_CODE to eliminate the constructor function. */

#ifndef AWE_STATIC_CODE
ModInstanceDescriptor * awe_modMeasurementConstructor(INT32 * FW_RESTRICT retVal, UINT32 nIO, WireInstance ** FW_RESTRICT pWires, size_t argCount, const Sample * FW_RESTRICT args);
#endif // #ifndef AWE_STATIC_CODE


void awe_modMeasurementProcess(void *pInstance);

 



#ifdef __cplusplus
}
#endif


#endif // _MOD_MEASUREMENT_H

/**
 * @}
 *
 * End of file.
 */
