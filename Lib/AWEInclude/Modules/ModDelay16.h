/****************************************************************************
*
*               Audio Framework
*               ---------------
*
****************************************************************************
*     ModDelay16.h
****************************************************************************
*
*     Description:  Time delay with 16-bit samples
*
*     Copyright:    DSP Concepts, Inc, 2007 - 2015
*                   1800 Wyatt Drive, Suite 14
*                   Santa Clara, CA 95054
*
***************************************************************************/

/**
 * @addtogroup Modules
 * @{
 */

/**
 * @file
 * @brief Time delay with 16-bit samples
 */

#ifndef _MOD_DELAY16_H
#define _MOD_DELAY16_H

#include "ModCommon.h"
#include "MathHelper.h"

#define MASK_Delay16_maxDelay 0x00000100
#define MASK_Delay16_currentDelay 0x00000200
#define MASK_Delay16_stateIndex 0x00000400
#define MASK_Delay16_stateHeap 0x00000800
#define MASK_Delay16_isFloat 0x00001000
#define MASK_Delay16_state 0x00002000
#define OFFSET_Delay16_maxDelay 0x00000008
#define OFFSET_Delay16_currentDelay 0x00000009
#define OFFSET_Delay16_stateIndex 0x0000000A
#define OFFSET_Delay16_stateHeap 0x0000000B
#define OFFSET_Delay16_isFloat 0x0000000C
#define OFFSET_Delay16_state 0x0000000D

#define CLASSID_DELAY16 (CLASS_ID_MODBASE + 36)

#ifdef __cplusplus
extern "C" {
#endif

// ----------------------------------------------------------------------
// Overall instance class
// ----------------------------------------------------------------------

typedef struct _awe_modDelay16Instance
{
    ModuleInstanceDescriptor instance;
    int                maxDelay;            // Maximum delay, in samples.
    int                currentDelay;        // Current delay.
    int                stateIndex;          // Index of the oldest state variable in the array of state variables.
    int                stateHeap;           // Heap in which to allocate memory.
    int                isFloat;             // Boolean that indicates whether the data is floating-point and needs to be converted.
    int*               state;               // State variable array.
} awe_modDelay16Instance;

/* By default the class object is included and this is required for dynamic
 * instantiation.  When doing static code (define AWE_STATIC_CODE) you can save
 * a little bit of memory by eliminating class objects.  However, you lose the
 * ability to change the module status. */

#if (!defined(AWE_NO_CLASS_OBJECTS) || !defined(AWE_STATIC_CODE))
extern const ModClassModule awe_modDelay16Class;
#endif // #ifndef AWE_NO_CLASS_OBJECTS

/* Dynamic instantiation is used by default.  When building for static
** code, define AWE_STATIC_CODE to eliminate the constructor function. */

#ifndef AWE_STATIC_CODE
ModInstanceDescriptor * awe_modDelay16Constructor(INT32 * FW_RESTRICT retVal, UINT32 nIO, WireInstance ** FW_RESTRICT pWires, size_t argCount, const Sample * FW_RESTRICT args);
#endif // #ifndef AWE_STATIC_CODE


void awe_modDelay16Process(void *pInstance);

 



#ifdef __cplusplus
}
#endif


#endif // _MOD_DELAY16_H

/**
 * @}
 *
 * End of file.
 */
