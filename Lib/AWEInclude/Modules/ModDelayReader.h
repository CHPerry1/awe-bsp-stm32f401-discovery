/****************************************************************************
*
*               Audio Framework
*               ---------------
*
****************************************************************************
*     ModDelayReader.h
****************************************************************************
*
*     Description:  Reads the data from a delay_state_writer_module with a given delay
*
*     Copyright:    DSP Concepts, Inc, 2007 - 2015
*                   1800 Wyatt Drive, Suite 14
*                   Santa Clara, CA 95054
*
***************************************************************************/

/**
 * @addtogroup Modules
 * @{
 */

/**
 * @file
 * @brief Reads the data from a delay_state_writer_module with a given delay
 */

#ifndef _MOD_DELAYREADER_H
#define _MOD_DELAYREADER_H

#include "ModCommon.h"
#include "MathHelper.h"

#define MASK_DelayReader_maxDelay 0x00000100
#define MASK_DelayReader_currentDelay 0x00000200
#define MASK_DelayReader_interpolationType 0x00000400
#define OFFSET_DelayReader_maxDelay 0x00000008
#define OFFSET_DelayReader_currentDelay 0x00000009
#define OFFSET_DelayReader_interpolationType 0x0000000A

#define CLASSID_DELAYREADER (CLASS_ID_MODBASE + 1238)

#ifdef __cplusplus
extern "C" {
#endif

// ----------------------------------------------------------------------
// Overall instance class
// ----------------------------------------------------------------------

typedef struct _awe_modDelayReaderInstance
{
    ModuleInstanceDescriptor instance;
    int                maxDelay;            // Max Delay of the writer module (per channel)
    float              currentDelay;        // Current delay
    int                interpolationType;   // Interpolation type - linear, cubic, etc
    
} awe_modDelayReaderInstance;

/* By default the class object is included and this is required for dynamic
 * instantiation.  When doing static code (define AWE_STATIC_CODE) you can save
 * a little bit of memory by eliminating class objects.  However, you lose the
 * ability to change the module status. */

#if (!defined(AWE_NO_CLASS_OBJECTS) || !defined(AWE_STATIC_CODE))
extern const ModClassModule awe_modDelayReaderClass;
#endif // #ifndef AWE_NO_CLASS_OBJECTS

/* Dynamic instantiation is used by default.  When building for static
** code, define AWE_STATIC_CODE to eliminate the constructor function. */

#ifndef AWE_STATIC_CODE
// This points the constructor for this class to the base constructor
#define awe_modDelayReaderConstructor(ARG1, ARG2, ARG3, ARG4, ARG5) ClassModule_Constructor(CLASSID_DELAYREADER, ARG1, ARG2, ARG3, ARG4, ARG5)
#endif // #ifndef AWE_STATIC_CODE


void awe_modDelayReaderProcess(void *pInstance);

 

void awe_modDelayReaderBypass(void *pInstance);


#ifdef __cplusplus
}
#endif


#endif // _MOD_DELAYREADER_H

/**
 * @}
 *
 * End of file.
 */
