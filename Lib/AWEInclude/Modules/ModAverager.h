/****************************************************************************
*
*               Audio Framework
*               ---------------
*
****************************************************************************
*     ModAverager.h
****************************************************************************
*
*     Description:  Advanced multichannel averager
*
*     Copyright:    DSP Concepts, Inc, 2007 - 2015
*                   1800 Wyatt Drive, Suite 14
*                   Santa Clara, CA 95054
*
***************************************************************************/

/**
 * @addtogroup Modules
 * @{
 */

/**
 * @file
 * @brief Advanced multichannel averager
 */

#ifndef _MOD_AVERAGER_H
#define _MOD_AVERAGER_H

#include "ModCommon.h"
#include "MathHelper.h"

#define MASK_Averager_smoothingTime 0x00000100
#define MASK_Averager_smoothingCoeff 0x00000200
#define MASK_Averager_sampleCounter 0x00000400
#define MASK_Averager_switchCount 0x00000800
#define MASK_Averager_stateSize 0x00001000
#define MASK_Averager_state 0x00002000
#define OFFSET_Averager_smoothingTime 0x00000008
#define OFFSET_Averager_smoothingCoeff 0x00000009
#define OFFSET_Averager_sampleCounter 0x0000000A
#define OFFSET_Averager_switchCount 0x0000000B
#define OFFSET_Averager_stateSize 0x0000000C
#define OFFSET_Averager_state 0x0000000D

#define CLASSID_AVERAGER (CLASS_ID_MODBASE + 1233)

#ifdef __cplusplus
extern "C" {
#endif

// ----------------------------------------------------------------------
// Overall instance class
// ----------------------------------------------------------------------

typedef struct _awe_modAveragerInstance
{
    ModuleInstanceDescriptor instance;
    float              smoothingTime;       // Time constant of the smoothing process.
    float              smoothingCoeff;      // Smoothing coefficient.
    int                sampleCounter;       // Number of samples processed since last reset.
    int                switchCount;         // When to switch from cumulative to exponential averaging.
    int                stateSize;           // Size of the state array derived from input pin.
    float*             state;               // Instantaneous average value.
} awe_modAveragerInstance;

/* By default the class object is included and this is required for dynamic
 * instantiation.  When doing static code (define AWE_STATIC_CODE) you can save
 * a little bit of memory by eliminating class objects.  However, you lose the
 * ability to change the module status. */

#if (!defined(AWE_NO_CLASS_OBJECTS) || !defined(AWE_STATIC_CODE))
extern const ModClassModule awe_modAveragerClass;
#endif // #ifndef AWE_NO_CLASS_OBJECTS

/* Dynamic instantiation is used by default.  When building for static
** code, define AWE_STATIC_CODE to eliminate the constructor function. */

#ifndef AWE_STATIC_CODE
ModInstanceDescriptor * awe_modAveragerConstructor(INT32 * FW_RESTRICT retVal, UINT32 nIO, WireInstance ** FW_RESTRICT pWires, size_t argCount, const Sample * FW_RESTRICT args);
#endif // #ifndef AWE_STATIC_CODE


void awe_modAveragerProcess(void *pInstance);

UINT32 awe_modAveragerSet(void *pInstance, UINT32 mask);
 

void awe_modAveragerBypass(void *pInstance);


#ifdef __cplusplus
}
#endif


#endif // _MOD_AVERAGER_H

/**
 * @}
 *
 * End of file.
 */
