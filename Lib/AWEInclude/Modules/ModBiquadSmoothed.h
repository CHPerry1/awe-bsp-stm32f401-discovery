/****************************************************************************
*
*               Audio Framework
*               ---------------
*
****************************************************************************
*     ModBiquadSmoothed.h
****************************************************************************
*
*     Description:  2nd order smoothly updating IIR filter
*
*     Copyright:    DSP Concepts, Inc, 2007 - 2015
*                   1800 Wyatt Drive, Suite 14
*                   Santa Clara, CA 95054
*
***************************************************************************/

/**
 * @addtogroup Modules
 * @{
 */

/**
 * @file
 * @brief 2nd order smoothly updating IIR filter
 */

#ifndef _MOD_BIQUADSMOOTHED_H
#define _MOD_BIQUADSMOOTHED_H

#include "ModCommon.h"
#include "MathHelper.h"

#define MASK_BiquadSmoothed_updateActive 0x00000100
#define MASK_BiquadSmoothed_smoothingTime 0x00000200
#define MASK_BiquadSmoothed_b0 0x00000400
#define MASK_BiquadSmoothed_b1 0x00000800
#define MASK_BiquadSmoothed_b2 0x00001000
#define MASK_BiquadSmoothed_a1 0x00002000
#define MASK_BiquadSmoothed_a2 0x00004000
#define MASK_BiquadSmoothed_current_b0 0x00008000
#define MASK_BiquadSmoothed_current_b1 0x00010000
#define MASK_BiquadSmoothed_current_b2 0x00020000
#define MASK_BiquadSmoothed_current_a1 0x00040000
#define MASK_BiquadSmoothed_current_a2 0x00080000
#define MASK_BiquadSmoothed_smoothingCoeff 0x00100000
#define MASK_BiquadSmoothed_state 0x00200000
#define OFFSET_BiquadSmoothed_updateActive 0x00000008
#define OFFSET_BiquadSmoothed_smoothingTime 0x00000009
#define OFFSET_BiquadSmoothed_b0 0x0000000A
#define OFFSET_BiquadSmoothed_b1 0x0000000B
#define OFFSET_BiquadSmoothed_b2 0x0000000C
#define OFFSET_BiquadSmoothed_a1 0x0000000D
#define OFFSET_BiquadSmoothed_a2 0x0000000E
#define OFFSET_BiquadSmoothed_current_b0 0x0000000F
#define OFFSET_BiquadSmoothed_current_b1 0x00000010
#define OFFSET_BiquadSmoothed_current_b2 0x00000011
#define OFFSET_BiquadSmoothed_current_a1 0x00000012
#define OFFSET_BiquadSmoothed_current_a2 0x00000013
#define OFFSET_BiquadSmoothed_smoothingCoeff 0x00000014
#define OFFSET_BiquadSmoothed_state 0x00000015

#define CLASSID_BIQUADSMOOTHED (CLASS_ID_MODBASE + 1011)

#ifdef __cplusplus
extern "C" {
#endif

// ----------------------------------------------------------------------
// Overall instance class
// ----------------------------------------------------------------------

typedef struct _awe_modBiquadSmoothedInstance
{
    ModuleInstanceDescriptor instance;
    int                updateActive;        // Specifies whether the filter coefficients are updating (=1) or fixed (=0).
    float              smoothingTime;       // Time constant of the smoothing process.
    float              b0;                  // Desired first numerator coefficient.
    float              b1;                  // Desired second numerator coefficient.
    float              b2;                  // Desired third numerator coefficient.
    float              a1;                  // Desired second denominator coefficient.
    float              a2;                  // Desired third denominator coefficient.
    float              current_b0;          // Instantaneous first numerator coefficient.
    float              current_b1;          // Instantaneous second numerator coefficient.
    float              current_b2;          // Instantaneous third numerator coefficient.
    float              current_a1;          // Instantaneous second denominator coefficient.
    float              current_a2;          // Instantaneous third denominator coefficient.
    float              smoothingCoeff;      // Smoothing coefficient. This is computed based on the smoothingTime, sample rate, and block size of the module.
    float*             state;               // State variables. 2 per channel.
} awe_modBiquadSmoothedInstance;

/* By default the class object is included and this is required for dynamic
 * instantiation.  When doing static code (define AWE_STATIC_CODE) you can save
 * a little bit of memory by eliminating class objects.  However, you lose the
 * ability to change the module status. */

#if (!defined(AWE_NO_CLASS_OBJECTS) || !defined(AWE_STATIC_CODE))
extern const ModClassModule awe_modBiquadSmoothedClass;
#endif // #ifndef AWE_NO_CLASS_OBJECTS

/* Dynamic instantiation is used by default.  When building for static
** code, define AWE_STATIC_CODE to eliminate the constructor function. */

#ifndef AWE_STATIC_CODE
ModInstanceDescriptor * awe_modBiquadSmoothedConstructor(INT32 * FW_RESTRICT retVal, UINT32 nIO, WireInstance ** FW_RESTRICT pWires, size_t argCount, const Sample * FW_RESTRICT args);
#endif // #ifndef AWE_STATIC_CODE


void awe_modBiquadSmoothedProcess(void *pInstance);

UINT32 awe_modBiquadSmoothedSet(void *pInstance, UINT32 mask);
 



#ifdef __cplusplus
}
#endif


#endif // _MOD_BIQUADSMOOTHED_H

/**
 * @}
 *
 * End of file.
 */
