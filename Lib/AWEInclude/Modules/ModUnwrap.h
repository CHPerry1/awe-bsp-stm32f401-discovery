/****************************************************************************
*
*               Audio Framework
*               ---------------
*
****************************************************************************
*     ModUnwrap.h
****************************************************************************
*
*     Description:  Unwrap phase angle in radians
*
*     Copyright:    DSP Concepts, Inc, 2007 - 2015
*                   1800 Wyatt Drive, Suite 14
*                   Santa Clara, CA 95054
*
***************************************************************************/

/**
 * @addtogroup Modules
 * @{
 */

/**
 * @file
 * @brief Unwrap phase angle in radians
 */

#ifndef _MOD_UNWRAP_H
#define _MOD_UNWRAP_H

#include "ModCommon.h"
#include "MathHelper.h"

#define MASK_Unwrap_startup 0x00000100
#define MASK_Unwrap_lastValue 0x00000200
#define OFFSET_Unwrap_startup 0x00000008
#define OFFSET_Unwrap_lastValue 0x00000009

#define CLASSID_UNWRAP (CLASS_ID_MODBASE + 1429)

#ifdef __cplusplus
extern "C" {
#endif

// ----------------------------------------------------------------------
// Overall instance class
// ----------------------------------------------------------------------

typedef struct _awe_modUnwrapInstance
{
    ModuleInstanceDescriptor instance;
    int                startup;             // Internal boolean flag to start module with known input.
    float*             lastValue;           // State array which holds last sample of the previous frame per channel
} awe_modUnwrapInstance;

/* By default the class object is included and this is required for dynamic
 * instantiation.  When doing static code (define AWE_STATIC_CODE) you can save
 * a little bit of memory by eliminating class objects.  However, you lose the
 * ability to change the module status. */

#if (!defined(AWE_NO_CLASS_OBJECTS) || !defined(AWE_STATIC_CODE))
extern const ModClassModule awe_modUnwrapClass;
#endif // #ifndef AWE_NO_CLASS_OBJECTS

/* Dynamic instantiation is used by default.  When building for static
** code, define AWE_STATIC_CODE to eliminate the constructor function. */

#ifndef AWE_STATIC_CODE
ModInstanceDescriptor * awe_modUnwrapConstructor(INT32 * FW_RESTRICT retVal, UINT32 nIO, WireInstance ** FW_RESTRICT pWires, size_t argCount, const Sample * FW_RESTRICT args);
#endif // #ifndef AWE_STATIC_CODE


void awe_modUnwrapProcess(void *pInstance);

 



#ifdef __cplusplus
}
#endif


#endif // _MOD_UNWRAP_H

/**
 * @}
 *
 * End of file.
 */
