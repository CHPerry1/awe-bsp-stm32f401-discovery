/****************************************************************************
*
*               Audio Framework
*               ---------------
*
****************************************************************************
*     ModTableLookup.h
****************************************************************************
*
*     Description:  Evenly spaced table lookup
*
*     Copyright:    DSP Concepts, Inc, 2007 - 2015
*                   1800 Wyatt Drive, Suite 14
*                   Santa Clara, CA 95054
*
***************************************************************************/

/**
 * @addtogroup Modules
 * @{
 */

/**
 * @file
 * @brief Evenly spaced table lookup
 */

#ifndef _MOD_TABLELOOKUP_H
#define _MOD_TABLELOOKUP_H

#include "ModCommon.h"
#include "MathHelper.h"

#define MASK_TableLookup_L 0x00000100
#define MASK_TableLookup_interpolationType 0x00000200
#define MASK_TableLookup_minX 0x00000400
#define MASK_TableLookup_maxX 0x00000800
#define MASK_TableLookup_divisor 0x00001000
#define MASK_TableLookup_table 0x00002000
#define OFFSET_TableLookup_L 0x00000008
#define OFFSET_TableLookup_interpolationType 0x00000009
#define OFFSET_TableLookup_minX 0x0000000A
#define OFFSET_TableLookup_maxX 0x0000000B
#define OFFSET_TableLookup_divisor 0x0000000C
#define OFFSET_TableLookup_table 0x0000000D

#define CLASSID_TABLELOOKUP (CLASS_ID_MODBASE + 1067)

#ifdef __cplusplus
extern "C" {
#endif

// ----------------------------------------------------------------------
// Overall instance class
// ----------------------------------------------------------------------

typedef struct _awe_modTableLookupInstance
{
    ModuleInstanceDescriptor instance;
    int                L;                   // Number of entries in the table.
    int                interpolationType;   // Selects the type of interpolation: 0=nearest, 1=linear.
    float              minX;                // X value corresponding to the first table entry.
    float              maxX;                // X value corresponding to the last table entry.
    float              divisor;             // Precomputed constant = 1/(L-1) to eliminate division on the target.
    float*             table;               // Table of evenly spaced values.
} awe_modTableLookupInstance;

/* By default the class object is included and this is required for dynamic
 * instantiation.  When doing static code (define AWE_STATIC_CODE) you can save
 * a little bit of memory by eliminating class objects.  However, you lose the
 * ability to change the module status. */

#if (!defined(AWE_NO_CLASS_OBJECTS) || !defined(AWE_STATIC_CODE))
extern const ModClassModule awe_modTableLookupClass;
#endif // #ifndef AWE_NO_CLASS_OBJECTS

/* Dynamic instantiation is used by default.  When building for static
** code, define AWE_STATIC_CODE to eliminate the constructor function. */

#ifndef AWE_STATIC_CODE
ModInstanceDescriptor * awe_modTableLookupConstructor(INT32 * FW_RESTRICT retVal, UINT32 nIO, WireInstance ** FW_RESTRICT pWires, size_t argCount, const Sample * FW_RESTRICT args);
#endif // #ifndef AWE_STATIC_CODE


void awe_modTableLookupProcess(void *pInstance);

 



#ifdef __cplusplus
}
#endif


#endif // _MOD_TABLELOOKUP_H

/**
 * @}
 *
 * End of file.
 */
