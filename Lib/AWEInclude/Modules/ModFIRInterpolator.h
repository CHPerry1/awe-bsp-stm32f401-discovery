/****************************************************************************
*
*               Audio Framework
*               ---------------
*
****************************************************************************
*     ModFIRInterpolator.h
****************************************************************************
*
*     Description:  Interpolator combining an upsampler and an FIR filter
*
*     Copyright:    DSP Concepts, Inc, 2007 - 2015
*                   1800 Wyatt Drive, Suite 14
*                   Santa Clara, CA 95054
*
***************************************************************************/

/**
 * @addtogroup Modules
 * @{
 */

/**
 * @file
 * @brief Interpolator combining an upsampler and an FIR filter
 */

#ifndef _MOD_FIRINTERPOLATOR_H
#define _MOD_FIRINTERPOLATOR_H

#include "ModCommon.h"
#include "MathHelper.h"

#define MASK_FIRInterpolator_L 0x00000100
#define MASK_FIRInterpolator_N 0x00000200
#define MASK_FIRInterpolator_polyphaseLen 0x00000400
#define MASK_FIRInterpolator_stateIndex 0x00000800
#define MASK_FIRInterpolator_stateLen 0x00001000
#define MASK_FIRInterpolator_coeffs 0x00002000
#define MASK_FIRInterpolator_state 0x00004000
#define OFFSET_FIRInterpolator_L 0x00000008
#define OFFSET_FIRInterpolator_N 0x00000009
#define OFFSET_FIRInterpolator_polyphaseLen 0x0000000A
#define OFFSET_FIRInterpolator_stateIndex 0x0000000B
#define OFFSET_FIRInterpolator_stateLen 0x0000000C
#define OFFSET_FIRInterpolator_coeffs 0x0000000D
#define OFFSET_FIRInterpolator_state 0x0000000E

#define CLASSID_FIRINTERPOLATOR (CLASS_ID_MODBASE + 1209)

#ifdef __cplusplus
extern "C" {
#endif

// ----------------------------------------------------------------------
// Overall instance class
// ----------------------------------------------------------------------

typedef struct _awe_modFIRInterpolatorInstance
{
    ModuleInstanceDescriptor instance;
    int                L;                   // Upsampling factor.
    int                N;                   // Length of the filter.
    int                polyphaseLen;        // Length of each polyphase filter component.
    int                stateIndex;          // Index of the oldest state variable in the array of state variables.
    int                stateLen;            // Length of the circular state buffer.
    float*             coeffs;              // Filter coefficient array in normal order.
    float*             state;               // State variable array. The size of the array equals stateLen*numChannels.
} awe_modFIRInterpolatorInstance;

/* By default the class object is included and this is required for dynamic
 * instantiation.  When doing static code (define AWE_STATIC_CODE) you can save
 * a little bit of memory by eliminating class objects.  However, you lose the
 * ability to change the module status. */

#if (!defined(AWE_NO_CLASS_OBJECTS) || !defined(AWE_STATIC_CODE))
extern const ModClassModule awe_modFIRInterpolatorClass;
#endif // #ifndef AWE_NO_CLASS_OBJECTS

/* Dynamic instantiation is used by default.  When building for static
** code, define AWE_STATIC_CODE to eliminate the constructor function. */

#ifndef AWE_STATIC_CODE
ModInstanceDescriptor * awe_modFIRInterpolatorConstructor(INT32 * FW_RESTRICT retVal, UINT32 nIO, WireInstance ** FW_RESTRICT pWires, size_t argCount, const Sample * FW_RESTRICT args);
#endif // #ifndef AWE_STATIC_CODE


void awe_modFIRInterpolatorProcess(void *pInstance);

UINT32 awe_modFIRInterpolatorSet(void *pInstance, UINT32 mask);
 



#ifdef __cplusplus
}
#endif


#endif // _MOD_FIRINTERPOLATOR_H

/**
 * @}
 *
 * End of file.
 */
