/****************************************************************************
*
*               Audio Framework
*               ---------------
*
****************************************************************************
*     ModMixerSmoothedV2b.h
****************************************************************************
*
*     Description:  M-input x N-output smoothly varying mixer
*
*     Copyright:    DSP Concepts, Inc, 2007 - 2015
*                   1800 Wyatt Drive, Suite 14
*                   Santa Clara, CA 95054
*
***************************************************************************/

/**
 * @addtogroup Modules
 * @{
 */

/**
 * @file
 * @brief M-input x N-output smoothly varying mixer
 */

#ifndef _MOD_MIXERSMOOTHEDV2B_H
#define _MOD_MIXERSMOOTHEDV2B_H

#include "ModCommon.h"
#include "MathHelper.h"

#define MASK_MixerSmoothedV2b_smoothingTime 0x00000100
#define MASK_MixerSmoothedV2b_smoothingCoeff 0x00000200
#define MASK_MixerSmoothedV2b_numIn 0x00000400
#define MASK_MixerSmoothedV2b_gain 0x00000800
#define MASK_MixerSmoothedV2b_currentGain 0x00001000
#define OFFSET_MixerSmoothedV2b_smoothingTime 0x00000008
#define OFFSET_MixerSmoothedV2b_smoothingCoeff 0x00000009
#define OFFSET_MixerSmoothedV2b_numIn 0x0000000A
#define OFFSET_MixerSmoothedV2b_gain 0x0000000B
#define OFFSET_MixerSmoothedV2b_currentGain 0x0000000C

#define CLASSID_MIXERSMOOTHEDV2B (CLASS_ID_MODBASE + 1079)

#ifdef __cplusplus
extern "C" {
#endif

// ----------------------------------------------------------------------
// Overall instance class
// ----------------------------------------------------------------------

typedef struct _awe_modMixerSmoothedV2bInstance
{
    ModuleInstanceDescriptor instance;
    float              smoothingTime;       // Time constant of the smoothing process, in msec.
    float              smoothingCoeff;      // Smoothing coefficient.
    int                numIn;               // Number of input channels.
    float*             gain;                // Linear gain.
    float*             currentGain;         // Instanteous gain being applied.  This is an internal variable used in the smoothing process
} awe_modMixerSmoothedV2bInstance;

/* By default the class object is included and this is required for dynamic
 * instantiation.  When doing static code (define AWE_STATIC_CODE) you can save
 * a little bit of memory by eliminating class objects.  However, you lose the
 * ability to change the module status. */

#if (!defined(AWE_NO_CLASS_OBJECTS) || !defined(AWE_STATIC_CODE))
extern const ModClassModule awe_modMixerSmoothedV2bClass;
#endif // #ifndef AWE_NO_CLASS_OBJECTS

/* Dynamic instantiation is used by default.  When building for static
** code, define AWE_STATIC_CODE to eliminate the constructor function. */

#ifndef AWE_STATIC_CODE
ModInstanceDescriptor * awe_modMixerSmoothedV2bConstructor(INT32 * FW_RESTRICT retVal, UINT32 nIO, WireInstance ** FW_RESTRICT pWires, size_t argCount, const Sample * FW_RESTRICT args);
#endif // #ifndef AWE_STATIC_CODE


void awe_modMixerSmoothedV2bProcess(void *pInstance);

UINT32 awe_modMixerSmoothedV2bSet(void *pInstance, UINT32 mask);
 

void awe_modMixerSmoothedV2bBypass(void *pInstance);


#ifdef __cplusplus
}
#endif


#endif // _MOD_MIXERSMOOTHEDV2B_H

/**
 * @}
 *
 * End of file.
 */
