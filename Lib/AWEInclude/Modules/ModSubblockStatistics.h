/****************************************************************************
*
*               Audio Framework
*               ---------------
*
****************************************************************************
*     ModSubblockStatistics.h
****************************************************************************
*
*     Description:  Computes statistics at several points in a block of samples
*
*     Copyright:    DSP Concepts, Inc, 2007 - 2015
*                   1800 Wyatt Drive, Suite 14
*                   Santa Clara, CA 95054
*
***************************************************************************/

/**
 * @addtogroup Modules
 * @{
 */

/**
 * @file
 * @brief Computes statistics at several points in a block of samples
 */

#ifndef _MOD_SUBBLOCKSTATISTICS_H
#define _MOD_SUBBLOCKSTATISTICS_H

#include "ModCommon.h"
#include "MathHelper.h"

#define MASK_SubblockStatistics_subblockSize 0x00000100
#define MASK_SubblockStatistics_statisticsType 0x00000200
#define MASK_SubblockStatistics_nSubblocks 0x00000400
#define OFFSET_SubblockStatistics_subblockSize 0x00000008
#define OFFSET_SubblockStatistics_statisticsType 0x00000009
#define OFFSET_SubblockStatistics_nSubblocks 0x0000000A

#define CLASSID_SUBBLOCKSTATISTICS (CLASS_ID_MODBASE + 1232)

#ifdef __cplusplus
extern "C" {
#endif

// ----------------------------------------------------------------------
// Overall instance class
// ----------------------------------------------------------------------

typedef struct _awe_modSubblockStatisticsInstance
{
    ModuleInstanceDescriptor instance;
    int                subblockSize;        // Number of samples per subblock to run statistics on.
    int                statisticsType;      // Type of statistics calculated.
    int                nSubblocks;          // Number of subblocks per input block.
    
} awe_modSubblockStatisticsInstance;

/* By default the class object is included and this is required for dynamic
 * instantiation.  When doing static code (define AWE_STATIC_CODE) you can save
 * a little bit of memory by eliminating class objects.  However, you lose the
 * ability to change the module status. */

#if (!defined(AWE_NO_CLASS_OBJECTS) || !defined(AWE_STATIC_CODE))
extern const ModClassModule awe_modSubblockStatisticsClass;
#endif // #ifndef AWE_NO_CLASS_OBJECTS

/* Dynamic instantiation is used by default.  When building for static
** code, define AWE_STATIC_CODE to eliminate the constructor function. */

#ifndef AWE_STATIC_CODE
// This points the constructor for this class to the base constructor
#define awe_modSubblockStatisticsConstructor(ARG1, ARG2, ARG3, ARG4, ARG5) ClassModule_Constructor(CLASSID_SUBBLOCKSTATISTICS, ARG1, ARG2, ARG3, ARG4, ARG5)
#endif // #ifndef AWE_STATIC_CODE


void awe_modSubblockStatisticsProcess(void *pInstance);

 



#ifdef __cplusplus
}
#endif


#endif // _MOD_SUBBLOCKSTATISTICS_H

/**
 * @}
 *
 * End of file.
 */
