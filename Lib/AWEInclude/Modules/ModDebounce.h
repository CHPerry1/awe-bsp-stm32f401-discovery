/****************************************************************************
*
*               Audio Framework
*               ---------------
*
****************************************************************************
*     ModDebounce.h
****************************************************************************
*
*     Description:  Debounces signals
*
*     Copyright:    DSP Concepts, Inc, 2007 - 2015
*                   1800 Wyatt Drive, Suite 14
*                   Santa Clara, CA 95054
*
***************************************************************************/

/**
 * @addtogroup Modules
 * @{
 */

/**
 * @file
 * @brief Debounces signals
 */

#ifndef _MOD_DEBOUNCE_H
#define _MOD_DEBOUNCE_H

#include "ModCommon.h"
#include "MathHelper.h"

#define MASK_Debounce_waitTime 0x00000100
#define MASK_Debounce_count 0x00000200
#define MASK_Debounce_waitCount 0x00000400
#define MASK_Debounce_firstTime 0x00000800
#define MASK_Debounce_previousBlock 0x00001000
#define OFFSET_Debounce_waitTime 0x00000008
#define OFFSET_Debounce_count 0x00000009
#define OFFSET_Debounce_waitCount 0x0000000A
#define OFFSET_Debounce_firstTime 0x0000000B
#define OFFSET_Debounce_previousBlock 0x0000000C

#define CLASSID_DEBOUNCE (CLASS_ID_MODBASE + 40)

#ifdef __cplusplus
extern "C" {
#endif

// ----------------------------------------------------------------------
// Overall instance class
// ----------------------------------------------------------------------

typedef struct _awe_modDebounceInstance
{
    ModuleInstanceDescriptor instance;
    float              waitTime;            // Waiting time before switching
    int                count;               // Counts down to zero
    int                waitCount;           // waitTime as a number of blocks
    int                firstTime;           // Indicates first block processed
    int*               previousBlock;       // Held block
} awe_modDebounceInstance;

/* By default the class object is included and this is required for dynamic
 * instantiation.  When doing static code (define AWE_STATIC_CODE) you can save
 * a little bit of memory by eliminating class objects.  However, you lose the
 * ability to change the module status. */

#if (!defined(AWE_NO_CLASS_OBJECTS) || !defined(AWE_STATIC_CODE))
extern const ModClassModule awe_modDebounceClass;
#endif // #ifndef AWE_NO_CLASS_OBJECTS

/* Dynamic instantiation is used by default.  When building for static
** code, define AWE_STATIC_CODE to eliminate the constructor function. */

#ifndef AWE_STATIC_CODE
ModInstanceDescriptor * awe_modDebounceConstructor(INT32 * FW_RESTRICT retVal, UINT32 nIO, WireInstance ** FW_RESTRICT pWires, size_t argCount, const Sample * FW_RESTRICT args);
#endif // #ifndef AWE_STATIC_CODE


void awe_modDebounceProcess(void *pInstance);

UINT32 awe_modDebounceSet(void *pInstance, UINT32 mask);
 



#ifdef __cplusplus
}
#endif


#endif // _MOD_DEBOUNCE_H

/**
 * @}
 *
 * End of file.
 */
