/****************************************************************************
*
*               Audio Framework
*               ---------------
*
****************************************************************************
*     ModLogicAndConstFract32.h
****************************************************************************
*
*     Description:  Ands a signal with a constant
*
*     Copyright:    DSP Concepts, Inc, 2007 - 2015
*                   1800 Wyatt Drive, Suite 14
*                   Santa Clara, CA 95054
*
***************************************************************************/

/**
 * @addtogroup Modules
 * @{
 */

/**
 * @file
 * @brief Ands a signal with a constant
 */

#ifndef _MOD_LOGICANDCONSTFRACT32_H
#define _MOD_LOGICANDCONSTFRACT32_H

#include "ModCommon.h"
#include "MathHelper.h"

#define MASK_LogicAndConstFract32_mask 0x00000100
#define OFFSET_LogicAndConstFract32_mask 0x00000008

#define CLASSID_LOGICANDCONSTFRACT32 (CLASS_ID_MODBASE + 2018)

#ifdef __cplusplus
extern "C" {
#endif

// ----------------------------------------------------------------------
// Overall instance class
// ----------------------------------------------------------------------

typedef struct _awe_modLogicAndConstFract32Instance
{
    ModuleInstanceDescriptor instance;
    unsigned int       mask;                // Bit mask used when ANDing
    
} awe_modLogicAndConstFract32Instance;

/* By default the class object is included and this is required for dynamic
 * instantiation.  When doing static code (define AWE_STATIC_CODE) you can save
 * a little bit of memory by eliminating class objects.  However, you lose the
 * ability to change the module status. */

#if (!defined(AWE_NO_CLASS_OBJECTS) || !defined(AWE_STATIC_CODE))
extern const ModClassModule awe_modLogicAndConstFract32Class;
#endif // #ifndef AWE_NO_CLASS_OBJECTS

/* Dynamic instantiation is used by default.  When building for static
** code, define AWE_STATIC_CODE to eliminate the constructor function. */

#ifndef AWE_STATIC_CODE
// This points the constructor for this class to the base constructor
#define awe_modLogicAndConstFract32Constructor(ARG1, ARG2, ARG3, ARG4, ARG5) ClassModule_Constructor(CLASSID_LOGICANDCONSTFRACT32, ARG1, ARG2, ARG3, ARG4, ARG5)
#endif // #ifndef AWE_STATIC_CODE


void awe_modLogicAndConstFract32Process(void *pInstance);

 



#ifdef __cplusplus
}
#endif


#endif // _MOD_LOGICANDCONSTFRACT32_H

/**
 * @}
 *
 * End of file.
 */
