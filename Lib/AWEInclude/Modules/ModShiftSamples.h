/****************************************************************************
*
*               Audio Framework
*               ---------------
*
****************************************************************************
*     ModShiftSamples.h
****************************************************************************
*
*     Description:  Shifts the samples in a block of data left or right a specified number of samples
*
*     Copyright:    DSP Concepts, Inc, 2007 - 2015
*                   1800 Wyatt Drive, Suite 14
*                   Santa Clara, CA 95054
*
***************************************************************************/

/**
 * @addtogroup Modules
 * @{
 */

/**
 * @file
 * @brief Shifts the samples in a block of data left or right a specified number of samples
 */

#ifndef _MOD_SHIFTSAMPLES_H
#define _MOD_SHIFTSAMPLES_H

#include "ModCommon.h"
#include "MathHelper.h"

#define MASK_ShiftSamples_index 0x00000100
#define OFFSET_ShiftSamples_index 0x00000008

#define CLASSID_SHIFTSAMPLES (CLASS_ID_MODBASE + 32)

#ifdef __cplusplus
extern "C" {
#endif

// ----------------------------------------------------------------------
// Overall instance class
// ----------------------------------------------------------------------

typedef struct _awe_modShiftSamplesInstance
{
    ModuleInstanceDescriptor instance;
    int                index;               // Specifies the number of values to be shifted.
    
} awe_modShiftSamplesInstance;

/* By default the class object is included and this is required for dynamic
 * instantiation.  When doing static code (define AWE_STATIC_CODE) you can save
 * a little bit of memory by eliminating class objects.  However, you lose the
 * ability to change the module status. */

#if (!defined(AWE_NO_CLASS_OBJECTS) || !defined(AWE_STATIC_CODE))
extern const ModClassModule awe_modShiftSamplesClass;
#endif // #ifndef AWE_NO_CLASS_OBJECTS

/* Dynamic instantiation is used by default.  When building for static
** code, define AWE_STATIC_CODE to eliminate the constructor function. */

#ifndef AWE_STATIC_CODE
// This points the constructor for this class to the base constructor
#define awe_modShiftSamplesConstructor(ARG1, ARG2, ARG3, ARG4, ARG5) ClassModule_Constructor(CLASSID_SHIFTSAMPLES, ARG1, ARG2, ARG3, ARG4, ARG5)
#endif // #ifndef AWE_STATIC_CODE


void awe_modShiftSamplesProcess(void *pInstance);

UINT32 awe_modShiftSamplesSet(void *pInstance, UINT32 mask);
 



#ifdef __cplusplus
}
#endif


#endif // _MOD_SHIFTSAMPLES_H

/**
 * @}
 *
 * End of file.
 */
