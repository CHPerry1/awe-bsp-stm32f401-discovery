/****************************************************************************
*
*               Audio Framework
*               ---------------
*
****************************************************************************
*     ModAGCAttackRelease.h
****************************************************************************
*
*     Description:  Multi-channel envelope detector with programmable attack and release times
*
*     Copyright:    DSP Concepts, Inc, 2007 - 2015
*                   1800 Wyatt Drive, Suite 14
*                   Santa Clara, CA 95054
*
***************************************************************************/

/**
 * @addtogroup Modules
 * @{
 */

/**
 * @file
 * @brief Multi-channel envelope detector with programmable attack and release times
 */

#ifndef _MOD_AGCATTACKRELEASE_H
#define _MOD_AGCATTACKRELEASE_H

#include "ModCommon.h"
#include "MathHelper.h"

#define MASK_AGCAttackRelease_attackTime 0x00000100
#define MASK_AGCAttackRelease_releaseTime 0x00000200
#define MASK_AGCAttackRelease_attackCoeff 0x00000400
#define MASK_AGCAttackRelease_releaseCoeff 0x00000800
#define MASK_AGCAttackRelease_envStates 0x00001000
#define OFFSET_AGCAttackRelease_attackTime 0x00000008
#define OFFSET_AGCAttackRelease_releaseTime 0x00000009
#define OFFSET_AGCAttackRelease_attackCoeff 0x0000000A
#define OFFSET_AGCAttackRelease_releaseCoeff 0x0000000B
#define OFFSET_AGCAttackRelease_envStates 0x0000000C

#define CLASSID_AGCATTACKRELEASE (CLASS_ID_MODBASE + 1002)

#ifdef __cplusplus
extern "C" {
#endif

// ----------------------------------------------------------------------
// Overall instance class
// ----------------------------------------------------------------------

typedef struct _awe_modAGCAttackReleaseInstance
{
    ModuleInstanceDescriptor instance;
    float              attackTime;          // Speed at which the detector reacts to increasing levels.
    float              releaseTime;         // Speed at which the detector reacts to decreasing levels.
    float              attackCoeff;         // Internal coefficient realizing the attack time.
    float              releaseCoeff;        // Internal coefficient realizing the release time.
    float*             envStates;           // Vector of sample-by-sample states of the envelope detectors; each column is the state for a channel.
} awe_modAGCAttackReleaseInstance;

/* By default the class object is included and this is required for dynamic
 * instantiation.  When doing static code (define AWE_STATIC_CODE) you can save
 * a little bit of memory by eliminating class objects.  However, you lose the
 * ability to change the module status. */

#if (!defined(AWE_NO_CLASS_OBJECTS) || !defined(AWE_STATIC_CODE))
extern const ModClassModule awe_modAGCAttackReleaseClass;
#endif // #ifndef AWE_NO_CLASS_OBJECTS

/* Dynamic instantiation is used by default.  When building for static
** code, define AWE_STATIC_CODE to eliminate the constructor function. */

#ifndef AWE_STATIC_CODE
ModInstanceDescriptor * awe_modAGCAttackReleaseConstructor(INT32 * FW_RESTRICT retVal, UINT32 nIO, WireInstance ** FW_RESTRICT pWires, size_t argCount, const Sample * FW_RESTRICT args);
#endif // #ifndef AWE_STATIC_CODE


void awe_modAGCAttackReleaseProcess(void *pInstance);

UINT32 awe_modAGCAttackReleaseSet(void *pInstance, UINT32 mask);
 

void awe_modAGCAttackReleaseBypass(void *pInstance);


#ifdef __cplusplus
}
#endif


#endif // _MOD_AGCATTACKRELEASE_H

/**
 * @}
 *
 * End of file.
 */
