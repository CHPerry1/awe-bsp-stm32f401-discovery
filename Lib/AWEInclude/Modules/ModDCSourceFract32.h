/****************************************************************************
*
*               Audio Framework
*               ---------------
*
****************************************************************************
*     ModDCSourceFract32.h
****************************************************************************
*
*     Description:  Source buffer holding 1 wire of constant data
*
*     Copyright:    DSP Concepts, Inc, 2007 - 2015
*                   1800 Wyatt Drive, Suite 14
*                   Santa Clara, CA 95054
*
***************************************************************************/

/**
 * @addtogroup Modules
 * @{
 */

/**
 * @file
 * @brief Source buffer holding 1 wire of constant data
 */

#ifndef _MOD_DCSOURCEFRACT32_H
#define _MOD_DCSOURCEFRACT32_H

#include "ModCommon.h"
#include "MathHelper.h"

#define MASK_DCSourceFract32_value 0x00000100
#define MASK_DCSourceFract32_valuefract32 0x00000200
#define OFFSET_DCSourceFract32_value 0x00000008
#define OFFSET_DCSourceFract32_valuefract32 0x00000009

#define CLASSID_DCSOURCEFRACT32 (CLASS_ID_MODBASE + 2014)

#ifdef __cplusplus
extern "C" {
#endif

// ----------------------------------------------------------------------
// Overall instance class
// ----------------------------------------------------------------------

typedef struct _awe_modDCSourceFract32Instance
{
    ModuleInstanceDescriptor instance;
    float              value;               // Data Value
    fract32            valuefract32;        // Data Value
    
} awe_modDCSourceFract32Instance;

/* By default the class object is included and this is required for dynamic
 * instantiation.  When doing static code (define AWE_STATIC_CODE) you can save
 * a little bit of memory by eliminating class objects.  However, you lose the
 * ability to change the module status. */

#if (!defined(AWE_NO_CLASS_OBJECTS) || !defined(AWE_STATIC_CODE))
extern const ModClassModule awe_modDCSourceFract32Class;
#endif // #ifndef AWE_NO_CLASS_OBJECTS

/* Dynamic instantiation is used by default.  When building for static
** code, define AWE_STATIC_CODE to eliminate the constructor function. */

#ifndef AWE_STATIC_CODE
// This points the constructor for this class to the base constructor
#define awe_modDCSourceFract32Constructor(ARG1, ARG2, ARG3, ARG4, ARG5) ClassModule_Constructor(CLASSID_DCSOURCEFRACT32, ARG1, ARG2, ARG3, ARG4, ARG5)
#endif // #ifndef AWE_STATIC_CODE


void awe_modDCSourceFract32Process(void *pInstance);

UINT32 awe_modDCSourceFract32Set(void *pInstance, UINT32 mask);
 



#ifdef __cplusplus
}
#endif


#endif // _MOD_DCSOURCEFRACT32_H

/**
 * @}
 *
 * End of file.
 */
