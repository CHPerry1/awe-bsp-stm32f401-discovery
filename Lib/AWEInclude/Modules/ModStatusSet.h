/****************************************************************************
*
*               Audio Framework
*               ---------------
*
****************************************************************************
*     ModStatusSet.h
****************************************************************************
*
*     Description:  Sets status in other modules
*
*     Copyright:    DSP Concepts, Inc, 2007 - 2015
*                   1800 Wyatt Drive, Suite 14
*                   Santa Clara, CA 95054
*
***************************************************************************/

/**
 * @addtogroup Modules
 * @{
 */

/**
 * @file
 * @brief Sets status in other modules
 */

#ifndef _MOD_STATUSSET_H
#define _MOD_STATUSSET_H

#include "ModCommon.h"
#include "MathHelper.h"

#define MASK_StatusSet_setBehavior 0x00000100
#define MASK_StatusSet_prevStatus 0x00000200
#define MASK_StatusSet_numModules 0x00000400
#define MASK_StatusSet_initSet 0x00000800
#define MASK_StatusSet_modPtr 0x00001000
#define OFFSET_StatusSet_setBehavior 0x00000008
#define OFFSET_StatusSet_prevStatus 0x00000009
#define OFFSET_StatusSet_numModules 0x0000000A
#define OFFSET_StatusSet_initSet 0x0000000B
#define OFFSET_StatusSet_modPtr 0x0000000C

#define CLASSID_STATUSSET (CLASS_ID_MODBASE + 45)

#ifdef __cplusplus
extern "C" {
#endif

// ----------------------------------------------------------------------
// Overall instance class
// ----------------------------------------------------------------------

typedef struct _awe_modStatusSetInstance
{
    ModuleInstanceDescriptor instance;
    int                setBehavior;         // Controls the behavior of setting status
    int                prevStatus;          // Previous status
    int                numModules;          // Total number of modules this module controls
    int                initSet;             // Initial int to call the set at start up no matter what
    int*               modPtr;              // Points to the module to set
} awe_modStatusSetInstance;

/* By default the class object is included and this is required for dynamic
 * instantiation.  When doing static code (define AWE_STATIC_CODE) you can save
 * a little bit of memory by eliminating class objects.  However, you lose the
 * ability to change the module status. */

#if (!defined(AWE_NO_CLASS_OBJECTS) || !defined(AWE_STATIC_CODE))
extern const ModClassModule awe_modStatusSetClass;
#endif // #ifndef AWE_NO_CLASS_OBJECTS

/* Dynamic instantiation is used by default.  When building for static
** code, define AWE_STATIC_CODE to eliminate the constructor function. */

#ifndef AWE_STATIC_CODE
ModInstanceDescriptor * awe_modStatusSetConstructor(INT32 * FW_RESTRICT retVal, UINT32 nIO, WireInstance ** FW_RESTRICT pWires, size_t argCount, const Sample * FW_RESTRICT args);
#endif // #ifndef AWE_STATIC_CODE


void awe_modStatusSetProcess(void *pInstance);

 



#ifdef __cplusplus
}
#endif


#endif // _MOD_STATUSSET_H

/**
 * @}
 *
 * End of file.
 */
