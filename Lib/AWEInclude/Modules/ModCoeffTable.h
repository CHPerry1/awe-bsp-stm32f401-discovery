/****************************************************************************
*
*               Audio Framework
*               ---------------
*
****************************************************************************
*     ModCoeffTable.h
****************************************************************************
*
*     Description:  Sets filter coefficients in other modules
*
*     Copyright:    DSP Concepts, Inc, 2007 - 2015
*                   1800 Wyatt Drive, Suite 14
*                   Santa Clara, CA 95054
*
***************************************************************************/

/**
 * @addtogroup Modules
 * @{
 */

/**
 * @file
 * @brief Sets filter coefficients in other modules
 */

#ifndef _MOD_COEFFTABLE_H
#define _MOD_COEFFTABLE_H

#include "ModCommon.h"
#include "MathHelper.h"

#define MASK_CoeffTable_index 0x00000100
#define MASK_CoeffTable_numRows 0x00000200
#define MASK_CoeffTable_numCols 0x00000400
#define MASK_CoeffTable_setBehavior 0x00000800
#define MASK_CoeffTable_modPtr 0x00001000
#define MASK_CoeffTable_varPtr 0x00002000
#define MASK_CoeffTable_coeff 0x00004000
#define OFFSET_CoeffTable_index 0x00000008
#define OFFSET_CoeffTable_numRows 0x00000009
#define OFFSET_CoeffTable_numCols 0x0000000A
#define OFFSET_CoeffTable_setBehavior 0x0000000B
#define OFFSET_CoeffTable_modPtr 0x0000000C
#define OFFSET_CoeffTable_varPtr 0x0000000D
#define OFFSET_CoeffTable_coeff 0x0000000E

#define CLASSID_COEFFTABLE (CLASS_ID_MODBASE + 42)

#ifdef __cplusplus
extern "C" {
#endif

// ----------------------------------------------------------------------
// Overall instance class
// ----------------------------------------------------------------------

typedef struct _awe_modCoeffTableInstance
{
    ModuleInstanceDescriptor instance;
    int                index;               // current index to coeff table
    int                numRows;             // the size of each vector
    int                numCols;             // the number of vectors
    int                setBehavior;         // Controls the behavior of the module
    int                modPtr;              // Points to the module to set
    int                varPtr;              // Points to the variable to set within the module instance structure
    float*             coeff;               // 2-D coeff table.
} awe_modCoeffTableInstance;

/* By default the class object is included and this is required for dynamic
 * instantiation.  When doing static code (define AWE_STATIC_CODE) you can save
 * a little bit of memory by eliminating class objects.  However, you lose the
 * ability to change the module status. */

#if (!defined(AWE_NO_CLASS_OBJECTS) || !defined(AWE_STATIC_CODE))
extern const ModClassModule awe_modCoeffTableClass;
#endif // #ifndef AWE_NO_CLASS_OBJECTS

/* Dynamic instantiation is used by default.  When building for static
** code, define AWE_STATIC_CODE to eliminate the constructor function. */

#ifndef AWE_STATIC_CODE
ModInstanceDescriptor * awe_modCoeffTableConstructor(INT32 * FW_RESTRICT retVal, UINT32 nIO, WireInstance ** FW_RESTRICT pWires, size_t argCount, const Sample * FW_RESTRICT args);
#endif // #ifndef AWE_STATIC_CODE


void awe_modCoeffTableProcess(void *pInstance);

 



#ifdef __cplusplus
}
#endif


#endif // _MOD_COEFFTABLE_H

/**
 * @}
 *
 * End of file.
 */
