/****************************************************************************
*
*               Audio Framework
*               ---------------
*
****************************************************************************
*     ModSampleMultiplexorControl.h
****************************************************************************
*
*     Description:  N to 1 multiplexor module with a control input operating sample-by-sample
*
*     Copyright:    DSP Concepts, Inc, 2007 - 2015
*                   1800 Wyatt Drive, Suite 14
*                   Santa Clara, CA 95054
*
***************************************************************************/

/**
 * @addtogroup Modules
 * @{
 */

/**
 * @file
 * @brief N to 1 multiplexor module with a control input operating sample-by-sample
 */

#ifndef _MOD_SAMPLEMULTIPLEXORCONTROL_H
#define _MOD_SAMPLEMULTIPLEXORCONTROL_H

#include "ModCommon.h"
#include "MathHelper.h"

#define MASK_SampleMultiplexorControl_pinPtrs 0x00000100
#define OFFSET_SampleMultiplexorControl_pinPtrs 0x00000008

#define CLASSID_SAMPLEMULTIPLEXORCONTROL (CLASS_ID_MODBASE + 44)

#ifdef __cplusplus
extern "C" {
#endif

// ----------------------------------------------------------------------
// Overall instance class
// ----------------------------------------------------------------------

typedef struct _awe_modSampleMultiplexorControlInstance
{
    ModuleInstanceDescriptor instance;
    
    int*               pinPtrs;             // Array of pointers to all the pins
} awe_modSampleMultiplexorControlInstance;

/* By default the class object is included and this is required for dynamic
 * instantiation.  When doing static code (define AWE_STATIC_CODE) you can save
 * a little bit of memory by eliminating class objects.  However, you lose the
 * ability to change the module status. */

#if (!defined(AWE_NO_CLASS_OBJECTS) || !defined(AWE_STATIC_CODE))
extern const ModClassModule awe_modSampleMultiplexorControlClass;
#endif // #ifndef AWE_NO_CLASS_OBJECTS

/* Dynamic instantiation is used by default.  When building for static
** code, define AWE_STATIC_CODE to eliminate the constructor function. */

#ifndef AWE_STATIC_CODE
ModInstanceDescriptor * awe_modSampleMultiplexorControlConstructor(INT32 * FW_RESTRICT retVal, UINT32 nIO, WireInstance ** FW_RESTRICT pWires, size_t argCount, const Sample * FW_RESTRICT args);
#endif // #ifndef AWE_STATIC_CODE


void awe_modSampleMultiplexorControlProcess(void *pInstance);

 

void awe_modSampleMultiplexorControlBypass(void *pInstance);


#ifdef __cplusplus
}
#endif


#endif // _MOD_SAMPLEMULTIPLEXORCONTROL_H

/**
 * @}
 *
 * End of file.
 */
