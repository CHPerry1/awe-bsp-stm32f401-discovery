/****************************************************************************
*
*               Audio Framework
*               ---------------
*
****************************************************************************
*     ModUpdateSampleRate.h
****************************************************************************
*
*     Description:  Updates the Sample Rate of each module wire in the system
*
*     Copyright:    DSP Concepts, Inc, 2007 - 2015
*                   1800 Wyatt Drive, Suite 14
*                   Santa Clara, CA 95054
*
***************************************************************************/

/**
 * @addtogroup Modules
 * @{
 */

/**
 * @file
 * @brief Updates the Sample Rate of each module wire in the system
 */

#ifndef _MOD_UPDATESAMPLERATE_H
#define _MOD_UPDATESAMPLERATE_H

#include "ModCommon.h"
#include "MathHelper.h"

#define MASK_UpdateSampleRate_newSampleRate 0x00000100
#define MASK_UpdateSampleRate_updateActive 0x00000200
#define MASK_UpdateSampleRate_allModWireInfo 0x00000400
#define OFFSET_UpdateSampleRate_newSampleRate 0x00000008
#define OFFSET_UpdateSampleRate_updateActive 0x00000009
#define OFFSET_UpdateSampleRate_allModWireInfo 0x0000000A

#define CLASSID_UPDATESAMPLERATE (CLASS_ID_MODBASE + 3010)

#ifdef __cplusplus
extern "C" {
#endif

// ----------------------------------------------------------------------
// Overall instance class
// ----------------------------------------------------------------------

typedef struct _awe_modUpdateSampleRateInstance
{
    ModuleInstanceDescriptor instance;
    int                newSampleRate;       // Default sample rate of the system. Updated during the construction time.
    int                updateActive;        // Flag to control the state of teh boolean output pin.
    float*             allModWireInfo;      // Array of all modules wire sample rate info
} awe_modUpdateSampleRateInstance;

/* By default the class object is included and this is required for dynamic
 * instantiation.  When doing static code (define AWE_STATIC_CODE) you can save
 * a little bit of memory by eliminating class objects.  However, you lose the
 * ability to change the module status. */

#if (!defined(AWE_NO_CLASS_OBJECTS) || !defined(AWE_STATIC_CODE))
extern const ModClassModule awe_modUpdateSampleRateClass;
#endif // #ifndef AWE_NO_CLASS_OBJECTS

/* Dynamic instantiation is used by default.  When building for static
** code, define AWE_STATIC_CODE to eliminate the constructor function. */

#ifndef AWE_STATIC_CODE
ModInstanceDescriptor * awe_modUpdateSampleRateConstructor(INT32 * FW_RESTRICT retVal, UINT32 nIO, WireInstance ** FW_RESTRICT pWires, size_t argCount, const Sample * FW_RESTRICT args);
#endif // #ifndef AWE_STATIC_CODE


void awe_modUpdateSampleRateProcess(void *pInstance);

UINT32 awe_modUpdateSampleRateSet(void *pInstance, UINT32 mask);
 



#ifdef __cplusplus
}
#endif


#endif // _MOD_UPDATESAMPLERATE_H

/**
 * @}
 *
 * End of file.
 */
