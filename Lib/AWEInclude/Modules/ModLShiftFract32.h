/****************************************************************************
*
*               Audio Framework
*               ---------------
*
****************************************************************************
*  ModLShiftFract32.h
****************************************************************************
*
*         Description:  Arithmetic shift of integer or fractional data
*
*         Copyright:	DSP Concepts, LLC, 2007 - 2012
*                       568 E. Weddell Drive, Suite 3
*                       Sunnyvale, CA 94089
*
***************************************************************************/

/**
 * @addtogroup Modules
 * @{
 */

/**
 * @file
 * @brief Arithmetic shift of integer or fractional data
 */

#ifndef _MOD_LSHIFTFRACT32_H
#define _MOD_LSHIFTFRACT32_H

#include "ModCommon.h"
#include "MathHelper.h"

#define MASK_LShiftFract32_numBits 0x00000100
#define OFFSET_LShiftFract32_numBits 0x00000008

#define CLASSID_LSHIFTFRACT32 (CLASS_ID_MODBASE + 5019)

#ifdef __cplusplus
extern "C" {
#endif

// ----------------------------------------------------------------------
// Overall instance class
// ----------------------------------------------------------------------

typedef struct _awe_modLShiftFract32Instance
{
    ModuleInstanceDescriptor instance;
    int                numBits;             // Integer value that specifies the number of bits to shift the input.
    
} awe_modLShiftFract32Instance;

/* By default the class object is included and this is required for dynamic
 * instantiation.  When doing static code (define AWE_STATIC_CODE) you can save
 * a little bit of memory by eliminating class objects.  However, you lose the
 * ability to change the module status. */

#if (!defined(AWE_NO_CLASS_OBJECTS) || !defined(AWE_STATIC_CODE))
extern const ModClassModule awe_modLShiftFract32Class;
#endif // #ifndef AWE_NO_CLASS_OBJECTS

/* Dynamic instantiation is used by default.  When building for static
** code, define AWE_STATIC_CODE to eliminate the constructor function. */

#ifndef AWE_STATIC_CODE
// This points the constructor for this class to the base constructor
#define awe_modLShiftFract32Constructor(ARG1, ARG2, ARG3, ARG4, ARG5) ClassModule_Constructor(CLASSID_LSHIFTFRACT32, ARG1, ARG2, ARG3, ARG4, ARG5)
#endif // #ifndef AWE_STATIC_CODE


void awe_modLShiftFract32Process(void *pInstance);

 



#ifdef __cplusplus
}
#endif


#endif // _MOD_LSHIFTFRACT32_H

/**
 * @}
 *
 * End of file.
 */
