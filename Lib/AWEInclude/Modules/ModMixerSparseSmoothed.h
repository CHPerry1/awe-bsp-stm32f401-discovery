/****************************************************************************
*
*               Audio Framework
*               ---------------
*
****************************************************************************
*     ModMixerSparseSmoothed.h
****************************************************************************
*
*     Description:  M-input x N-output smoothly varying mixer
*
*     Copyright:    DSP Concepts, Inc, 2007 - 2015
*                   1800 Wyatt Drive, Suite 14
*                   Santa Clara, CA 95054
*
***************************************************************************/

/**
 * @addtogroup Modules
 * @{
 */

/**
 * @file
 * @brief M-input x N-output smoothly varying mixer
 */

#ifndef _MOD_MIXERSPARSESMOOTHED_H
#define _MOD_MIXERSPARSESMOOTHED_H

#include "ModCommon.h"
#include "MathHelper.h"

#define MASK_MixerSparseSmoothed_smoothingTime 0x00000100
#define MASK_MixerSparseSmoothed_smoothingCoeff 0x00000200
#define MASK_MixerSparseSmoothed_maxNZC 0x00000400
#define MASK_MixerSparseSmoothed_numIn 0x00000800
#define MASK_MixerSparseSmoothed_gain 0x00001000
#define MASK_MixerSparseSmoothed_currentGain 0x00002000
#define MASK_MixerSparseSmoothed_nonZeroGain 0x00004000
#define MASK_MixerSparseSmoothed_table 0x00008000
#define OFFSET_MixerSparseSmoothed_smoothingTime 0x00000008
#define OFFSET_MixerSparseSmoothed_smoothingCoeff 0x00000009
#define OFFSET_MixerSparseSmoothed_maxNZC 0x0000000A
#define OFFSET_MixerSparseSmoothed_numIn 0x0000000B
#define OFFSET_MixerSparseSmoothed_gain 0x0000000C
#define OFFSET_MixerSparseSmoothed_currentGain 0x0000000D
#define OFFSET_MixerSparseSmoothed_nonZeroGain 0x0000000E
#define OFFSET_MixerSparseSmoothed_table 0x0000000F

#define CLASSID_MIXERSPARSESMOOTHED (CLASS_ID_MODBASE + 1080)

#ifdef __cplusplus
extern "C" {
#endif

// ----------------------------------------------------------------------
// Overall instance class
// ----------------------------------------------------------------------

typedef struct _awe_modMixerSparseSmoothedInstance
{
    ModuleInstanceDescriptor instance;
    float              smoothingTime;       // Time constant of the smoothing process, in msec.
    float              smoothingCoeff;      // Smoothing coefficient.
    int                maxNZC;              // Maximum number of non zero coefficients.
    int                numIn;               // Number of input channels.
    float*             gain;                // Linear gain.
    float*             currentGain;         // Instanteous gain being applied.  This is an internal variable used in the smoothing process
    float*             nonZeroGain;         // Instanteous gain being applied.  This is an internal variable used in the smoothing process
    int*               table;               // Internal table to hold src pointer, src stride and dst pointer of corresponding non zero coefficient.
} awe_modMixerSparseSmoothedInstance;

/* By default the class object is included and this is required for dynamic
 * instantiation.  When doing static code (define AWE_STATIC_CODE) you can save
 * a little bit of memory by eliminating class objects.  However, you lose the
 * ability to change the module status. */

#if (!defined(AWE_NO_CLASS_OBJECTS) || !defined(AWE_STATIC_CODE))
extern const ModClassModule awe_modMixerSparseSmoothedClass;
#endif // #ifndef AWE_NO_CLASS_OBJECTS

/* Dynamic instantiation is used by default.  When building for static
** code, define AWE_STATIC_CODE to eliminate the constructor function. */

#ifndef AWE_STATIC_CODE
ModInstanceDescriptor * awe_modMixerSparseSmoothedConstructor(INT32 * FW_RESTRICT retVal, UINT32 nIO, WireInstance ** FW_RESTRICT pWires, size_t argCount, const Sample * FW_RESTRICT args);
#endif // #ifndef AWE_STATIC_CODE


void awe_modMixerSparseSmoothedProcess(void *pInstance);

UINT32 awe_modMixerSparseSmoothedSet(void *pInstance, UINT32 mask);
 

void awe_modMixerSparseSmoothedBypass(void *pInstance);


#ifdef __cplusplus
}
#endif


#endif // _MOD_MIXERSPARSESMOOTHED_H

/**
 * @}
 *
 * End of file.
 */
