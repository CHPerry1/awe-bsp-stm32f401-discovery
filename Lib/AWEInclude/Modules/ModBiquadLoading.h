/****************************************************************************
*
*               Audio Framework
*               ---------------
*
****************************************************************************
*     ModBiquadLoading.h
****************************************************************************
*
*     Description:  Simulates loading of biquad filters
*
*     Copyright:    DSP Concepts, Inc, 2007 - 2015
*                   1800 Wyatt Drive, Suite 14
*                   Santa Clara, CA 95054
*
***************************************************************************/

/**
 * @addtogroup Modules
 * @{
 */

/**
 * @file
 * @brief Simulates loading of biquad filters
 */

#ifndef _MOD_BIQUADLOADING_H
#define _MOD_BIQUADLOADING_H

#include "ModCommon.h"
#include "MathHelper.h"
#include "ModBiquadCascade.h"

#define MASK_BiquadLoading_maxStages 0x00000100
#define MASK_BiquadLoading_numStages 0x00000200
#define OFFSET_BiquadLoading_maxStages 0x00000008
#define OFFSET_BiquadLoading_numStages 0x00000009

#define CLASSID_BIQUADLOADING (CLASS_ID_MODBASE + 3000)

#ifdef __cplusplus
extern "C" {
#endif

// ----------------------------------------------------------------------
// Overall instance class
// ----------------------------------------------------------------------

typedef struct _awe_modBiquadLoadingInstance
{
    ModuleInstanceDescriptor instance;
    int                maxStages;           // Maximum Biquad stages that can be run
    int                numStages;           // Number of Biquad stages currently running
        awe_modBiquadCascadeInstance    *filt;               // Cascade of second order Biquad filters
} awe_modBiquadLoadingInstance;

/* By default the class object is included and this is required for dynamic
 * instantiation.  When doing static code (define AWE_STATIC_CODE) you can save
 * a little bit of memory by eliminating class objects.  However, you lose the
 * ability to change the module status. */

#if (!defined(AWE_NO_CLASS_OBJECTS) || !defined(AWE_STATIC_CODE))
extern const ModClassModule awe_modBiquadLoadingClass;
#endif // #ifndef AWE_NO_CLASS_OBJECTS

/* Dynamic instantiation is used by default.  When building for static
** code, define AWE_STATIC_CODE to eliminate the constructor function. */

#ifndef AWE_STATIC_CODE
ModInstanceDescriptor * awe_modBiquadLoadingConstructor(INT32 * FW_RESTRICT retVal, UINT32 nIO, WireInstance ** FW_RESTRICT pWires, size_t argCount, const Sample * FW_RESTRICT args);
#endif // #ifndef AWE_STATIC_CODE


void awe_modBiquadLoadingProcess(void *pInstance);

UINT32 awe_modBiquadLoadingSet(void *pInstance, UINT32 mask);
 



#ifdef __cplusplus
}
#endif


#endif // _MOD_BIQUADLOADING_H

/**
 * @}
 *
 * End of file.
 */
