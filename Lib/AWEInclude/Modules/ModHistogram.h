/****************************************************************************
*
*               Audio Framework
*               ---------------
*
****************************************************************************
*     ModHistogram.h
****************************************************************************
*
*     Description:  Histogram of data
*
*     Copyright:    DSP Concepts, Inc, 2007 - 2015
*                   1800 Wyatt Drive, Suite 14
*                   Santa Clara, CA 95054
*
***************************************************************************/

/**
 * @addtogroup Modules
 * @{
 */

/**
 * @file
 * @brief Histogram of data
 */

#ifndef _MOD_HISTOGRAM_H
#define _MOD_HISTOGRAM_H

#include "ModCommon.h"
#include "MathHelper.h"

#define MASK_Histogram_numBins 0x00000100
#define MASK_Histogram_lowerBinCenter 0x00000200
#define MASK_Histogram_binSpacing 0x00000400
#define MASK_Histogram_reset 0x00000800
#define MASK_Histogram_numHistSamples 0x00001000
#define MASK_Histogram_numUnderSamples 0x00002000
#define MASK_Histogram_numOverSamples 0x00004000
#define MASK_Histogram_hist 0x00008000
#define OFFSET_Histogram_numBins 0x00000008
#define OFFSET_Histogram_lowerBinCenter 0x00000009
#define OFFSET_Histogram_binSpacing 0x0000000A
#define OFFSET_Histogram_reset 0x0000000B
#define OFFSET_Histogram_numHistSamples 0x0000000C
#define OFFSET_Histogram_numUnderSamples 0x0000000D
#define OFFSET_Histogram_numOverSamples 0x0000000E
#define OFFSET_Histogram_hist 0x0000000F

#define CLASSID_HISTOGRAM (CLASS_ID_MODBASE + 1212)

#ifdef __cplusplus
extern "C" {
#endif

// ----------------------------------------------------------------------
// Overall instance class
// ----------------------------------------------------------------------

typedef struct _awe_modHistogramInstance
{
    ModuleInstanceDescriptor instance;
    int                numBins;             // Number of bins in the histogram
    float              lowerBinCenter;      // Specifies the center of the first (lowest) bin in the histogram
    float              binSpacing;          // Specifies the spacing between bins
    int                reset;               // Boolean parameter to reset the histogram.
    int                numHistSamples;      // Number of input samples processed.
    int                numUnderSamples;     // Total number of input samples that were outside of the histogram range on the low end.
    int                numOverSamples;      // Total number of input samples that were outside of the histogram range on the high end.
    int*               hist;                // Histogram array.
} awe_modHistogramInstance;

/* By default the class object is included and this is required for dynamic
 * instantiation.  When doing static code (define AWE_STATIC_CODE) you can save
 * a little bit of memory by eliminating class objects.  However, you lose the
 * ability to change the module status. */

#if (!defined(AWE_NO_CLASS_OBJECTS) || !defined(AWE_STATIC_CODE))
extern const ModClassModule awe_modHistogramClass;
#endif // #ifndef AWE_NO_CLASS_OBJECTS

/* Dynamic instantiation is used by default.  When building for static
** code, define AWE_STATIC_CODE to eliminate the constructor function. */

#ifndef AWE_STATIC_CODE
ModInstanceDescriptor * awe_modHistogramConstructor(INT32 * FW_RESTRICT retVal, UINT32 nIO, WireInstance ** FW_RESTRICT pWires, size_t argCount, const Sample * FW_RESTRICT args);
#endif // #ifndef AWE_STATIC_CODE


void awe_modHistogramProcess(void *pInstance);

 



#ifdef __cplusplus
}
#endif


#endif // _MOD_HISTOGRAM_H

/**
 * @}
 *
 * End of file.
 */
