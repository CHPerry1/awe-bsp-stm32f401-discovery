/****************************************************************************
*
*               Audio Framework
*               ---------------
*
****************************************************************************
*     ModFifoIn.h
****************************************************************************
*
*     Description:  First-in first-out buffering
*
*     Copyright:    DSP Concepts, Inc, 2007 - 2015
*                   1800 Wyatt Drive, Suite 14
*                   Santa Clara, CA 95054
*
***************************************************************************/

/**
 * @addtogroup Modules
 * @{
 */

/**
 * @file
 * @brief First-in first-out buffering
 */

#ifndef _MOD_FIFOIN_H
#define _MOD_FIFOIN_H

#include "ModCommon.h"
#include "MathHelper.h"

#define MASK_FifoIn_bufferSize 0x00000100
#define MASK_FifoIn_fillLevel 0x00000200
#define MASK_FifoIn_underflowCount 0x00000400
#define MASK_FifoIn_overflowCount 0x00000800
#define MASK_FifoIn_blockCounter 0x00001000
#define MASK_FifoIn_stateIndex 0x00002000
#define MASK_FifoIn_state 0x00004000
#define OFFSET_FifoIn_bufferSize 0x00000008
#define OFFSET_FifoIn_fillLevel 0x00000009
#define OFFSET_FifoIn_underflowCount 0x0000000A
#define OFFSET_FifoIn_overflowCount 0x0000000B
#define OFFSET_FifoIn_blockCounter 0x0000000C
#define OFFSET_FifoIn_stateIndex 0x0000000D
#define OFFSET_FifoIn_state 0x0000000E

#define CLASSID_FIFOIN (CLASS_ID_MODBASE + 11)

#ifdef __cplusplus
extern "C" {
#endif

// ----------------------------------------------------------------------
// Overall instance class
// ----------------------------------------------------------------------

typedef struct _awe_modFifoInInstance
{
    ModuleInstanceDescriptor instance;
    int                bufferSize;          // Number of samples buffer per channel. The size of the array is BUFFERSIZE*numChannels.
    int                fillLevel;           // Current number of samples available in the buffer. Can be set prior to building in order to prefill the buffer.
    int                underflowCount;      // Number of samples that could not be output because the FIFO buffer was out of data.
    int                overflowCount;       // Number of samples that could not be input because the FIFO buffer was full.
    int                blockCounter;        // Counts down to 0.
    int                stateIndex;          // Index of the oldest variable in the buffer.
    float*             state;               // State variable array.
} awe_modFifoInInstance;

/* By default the class object is included and this is required for dynamic
 * instantiation.  When doing static code (define AWE_STATIC_CODE) you can save
 * a little bit of memory by eliminating class objects.  However, you lose the
 * ability to change the module status. */

#if (!defined(AWE_NO_CLASS_OBJECTS) || !defined(AWE_STATIC_CODE))
extern const ModClassModule awe_modFifoInClass;
#endif // #ifndef AWE_NO_CLASS_OBJECTS

/* Dynamic instantiation is used by default.  When building for static
** code, define AWE_STATIC_CODE to eliminate the constructor function. */

#ifndef AWE_STATIC_CODE
ModInstanceDescriptor * awe_modFifoInConstructor(INT32 * FW_RESTRICT retVal, UINT32 nIO, WireInstance ** FW_RESTRICT pWires, size_t argCount, const Sample * FW_RESTRICT args);
#endif // #ifndef AWE_STATIC_CODE


void awe_modFifoInProcess(void *pInstance);

 



#ifdef __cplusplus
}
#endif


#endif // _MOD_FIFOIN_H

/**
 * @}
 *
 * End of file.
 */
