/****************************************************************************
*
*               Audio Framework
*               ---------------
*
****************************************************************************
*     ModSMixer2x1.h
****************************************************************************
*
*     Description:  2-input x 1-output smoothly varying mixer module
*
*     Copyright:    DSP Concepts, Inc, 2007 - 2015
*                   1800 Wyatt Drive, Suite 14
*                   Santa Clara, CA 95054
*
***************************************************************************/

/**
 * @addtogroup Modules
 * @{
 */

/**
 * @file
 * @brief 2-input x 1-output smoothly varying mixer module
 */

#ifndef _MOD_SMIXER2X1_H
#define _MOD_SMIXER2X1_H

#include "ModCommon.h"
#include "MathHelper.h"

#define MASK_SMixer2x1_gain1 0x00000100
#define MASK_SMixer2x1_gain2 0x00000200
#define MASK_SMixer2x1_smoothingTime 0x00000400
#define MASK_SMixer2x1_currentGain1 0x00000800
#define MASK_SMixer2x1_currentGain2 0x00001000
#define MASK_SMixer2x1_smoothingCoeff 0x00002000
#define OFFSET_SMixer2x1_gain1 0x00000008
#define OFFSET_SMixer2x1_gain2 0x00000009
#define OFFSET_SMixer2x1_smoothingTime 0x0000000A
#define OFFSET_SMixer2x1_currentGain1 0x0000000B
#define OFFSET_SMixer2x1_currentGain2 0x0000000C
#define OFFSET_SMixer2x1_smoothingCoeff 0x0000000D

#define CLASSID_SMIXER2X1 (CLASS_ID_MODBASE + 1059)

#ifdef __cplusplus
extern "C" {
#endif

// ----------------------------------------------------------------------
// Overall instance class
// ----------------------------------------------------------------------

typedef struct _awe_modSMixer2x1Instance
{
    ModuleInstanceDescriptor instance;
    float              gain1;               // Desired gain applied to first input channel.
    float              gain2;               // Desired gain applied to second input channel.
    float              smoothingTime;       // Time constant of the smoothing process.
    float              currentGain1;        // Instantaneous gain for the first input channel.
    float              currentGain2;        // Instantaneous gain for the second input channel.
    float              smoothingCoeff;      // Smoothing coefficient.
    
} awe_modSMixer2x1Instance;

/* By default the class object is included and this is required for dynamic
 * instantiation.  When doing static code (define AWE_STATIC_CODE) you can save
 * a little bit of memory by eliminating class objects.  However, you lose the
 * ability to change the module status. */

#if (!defined(AWE_NO_CLASS_OBJECTS) || !defined(AWE_STATIC_CODE))
extern const ModClassModule awe_modSMixer2x1Class;
#endif // #ifndef AWE_NO_CLASS_OBJECTS

/* Dynamic instantiation is used by default.  When building for static
** code, define AWE_STATIC_CODE to eliminate the constructor function. */

#ifndef AWE_STATIC_CODE
// This points the constructor for this class to the base constructor
#define awe_modSMixer2x1Constructor(ARG1, ARG2, ARG3, ARG4, ARG5) ClassModule_Constructor(CLASSID_SMIXER2X1, ARG1, ARG2, ARG3, ARG4, ARG5)
#endif // #ifndef AWE_STATIC_CODE


void awe_modSMixer2x1Process(void *pInstance);

UINT32 awe_modSMixer2x1Set(void *pInstance, UINT32 mask);
 



#ifdef __cplusplus
}
#endif


#endif // _MOD_SMIXER2X1_H

/**
 * @}
 *
 * End of file.
 */
