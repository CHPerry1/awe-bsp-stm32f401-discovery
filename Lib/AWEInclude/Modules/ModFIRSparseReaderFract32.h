/****************************************************************************
*
*               Audio Framework
*               ---------------
*
****************************************************************************
*     ModFIRSparseReaderFract32.h
****************************************************************************
*
*     Description:  Sparse FIR filter which works together with the DelayStateWriter module
*
*     Copyright:    DSP Concepts, Inc, 2007 - 2015
*                   1800 Wyatt Drive, Suite 14
*                   Santa Clara, CA 95054
*
***************************************************************************/

/**
 * @addtogroup Modules
 * @{
 */

/**
 * @file
 * @brief Sparse FIR filter which works together with the DelayStateWriter module
 */

#ifndef _MOD_FIRSPARSEREADERFRACT32_H
#define _MOD_FIRSPARSEREADERFRACT32_H

#include "ModCommon.h"
#include "MathHelper.h"

#define MASK_FIRSparseReaderFract32_numTaps 0x00000100
#define MASK_FIRSparseReaderFract32_postShift 0x00000200
#define MASK_FIRSparseReaderFract32_tapDelay 0x00000400
#define MASK_FIRSparseReaderFract32_tapDelayTarget 0x00000800
#define MASK_FIRSparseReaderFract32_tapCoeffs 0x00001000
#define MASK_FIRSparseReaderFract32_tapCoeffsFract32 0x00002000
#define OFFSET_FIRSparseReaderFract32_numTaps 0x00000008
#define OFFSET_FIRSparseReaderFract32_postShift 0x00000009
#define OFFSET_FIRSparseReaderFract32_tapDelay 0x0000000A
#define OFFSET_FIRSparseReaderFract32_tapDelayTarget 0x0000000B
#define OFFSET_FIRSparseReaderFract32_tapCoeffs 0x0000000C
#define OFFSET_FIRSparseReaderFract32_tapCoeffsFract32 0x0000000D

#define CLASSID_FIRSPARSEREADERFRACT32 (CLASS_ID_MODBASE + 2226)

#ifdef __cplusplus
extern "C" {
#endif

// ----------------------------------------------------------------------
// Overall instance class
// ----------------------------------------------------------------------

typedef struct _awe_modFIRSparseReaderFract32Instance
{
    ModuleInstanceDescriptor instance;
    int                numTaps;             // Number of non-zero coefficients (taps) in the filter
    int                postShift;           // Number of bits to shift
    int*               tapDelay;            // Current delay per tap
    int*               tapDelayTarget;      // Current delay per tap
    float*             tapCoeffs;           // Coefficient applied to each tap output
    fract32*           tapCoeffsFract32;    // Fract32 coefficient applied to each tap output
} awe_modFIRSparseReaderFract32Instance;

/* By default the class object is included and this is required for dynamic
 * instantiation.  When doing static code (define AWE_STATIC_CODE) you can save
 * a little bit of memory by eliminating class objects.  However, you lose the
 * ability to change the module status. */

#if (!defined(AWE_NO_CLASS_OBJECTS) || !defined(AWE_STATIC_CODE))
extern const ModClassModule awe_modFIRSparseReaderFract32Class;
#endif // #ifndef AWE_NO_CLASS_OBJECTS

/* Dynamic instantiation is used by default.  When building for static
** code, define AWE_STATIC_CODE to eliminate the constructor function. */

#ifndef AWE_STATIC_CODE
ModInstanceDescriptor * awe_modFIRSparseReaderFract32Constructor(INT32 * FW_RESTRICT retVal, UINT32 nIO, WireInstance ** FW_RESTRICT pWires, size_t argCount, const Sample * FW_RESTRICT args);
#endif // #ifndef AWE_STATIC_CODE


void awe_modFIRSparseReaderFract32Process(void *pInstance);

UINT32 awe_modFIRSparseReaderFract32Set(void *pInstance, UINT32 mask);
 



#ifdef __cplusplus
}
#endif


#endif // _MOD_FIRSPARSEREADERFRACT32_H

/**
 * @}
 *
 * End of file.
 */
