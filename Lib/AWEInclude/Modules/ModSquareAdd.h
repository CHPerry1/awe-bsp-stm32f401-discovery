/****************************************************************************
*
*               Audio Framework
*               ---------------
*
****************************************************************************
*     ModSquareAdd.h
****************************************************************************
*
*     Description:  Squares all inputs and then sums them together
*
*     Copyright:    DSP Concepts, Inc, 2007 - 2015
*                   1800 Wyatt Drive, Suite 14
*                   Santa Clara, CA 95054
*
***************************************************************************/

/**
 * @addtogroup Modules
 * @{
 */

/**
 * @file
 * @brief Squares all inputs and then sums them together
 */

#ifndef _MOD_SQUAREADD_H
#define _MOD_SQUAREADD_H

#include "ModCommon.h"
#include "MathHelper.h"

#define MASK_SquareAdd_oneChannelOutput 0x00000100
#define OFFSET_SquareAdd_oneChannelOutput 0x00000008

#define CLASSID_SQUAREADD (CLASS_ID_MODBASE + 1062)

#ifdef __cplusplus
extern "C" {
#endif

// ----------------------------------------------------------------------
// Overall instance class
// ----------------------------------------------------------------------

typedef struct _awe_modSquareAddInstance
{
    ModuleInstanceDescriptor instance;
    int                oneChannelOutput;    // Boolean value to determine if multichannel output or mono output.
    
} awe_modSquareAddInstance;

/* By default the class object is included and this is required for dynamic
 * instantiation.  When doing static code (define AWE_STATIC_CODE) you can save
 * a little bit of memory by eliminating class objects.  However, you lose the
 * ability to change the module status. */

#if (!defined(AWE_NO_CLASS_OBJECTS) || !defined(AWE_STATIC_CODE))
extern const ModClassModule awe_modSquareAddClass;
#endif // #ifndef AWE_NO_CLASS_OBJECTS

/* Dynamic instantiation is used by default.  When building for static
** code, define AWE_STATIC_CODE to eliminate the constructor function. */

#ifndef AWE_STATIC_CODE
// This points the constructor for this class to the base constructor
#define awe_modSquareAddConstructor(ARG1, ARG2, ARG3, ARG4, ARG5) ClassModule_Constructor(CLASSID_SQUAREADD, ARG1, ARG2, ARG3, ARG4, ARG5)
#endif // #ifndef AWE_STATIC_CODE


void awe_modSquareAddProcess(void *pInstance);

 



#ifdef __cplusplus
}
#endif


#endif // _MOD_SQUAREADD_H

/**
 * @}
 *
 * End of file.
 */
