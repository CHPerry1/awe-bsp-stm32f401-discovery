/****************************************************************************
*
*               Audio Framework
*               ---------------
*
****************************************************************************
*     ModComplexMultiplier.h
****************************************************************************
*
*     Description:  Multi-input complex multiplier
*
*     Copyright:    DSP Concepts, Inc, 2007 - 2015
*                   1800 Wyatt Drive, Suite 14
*                   Santa Clara, CA 95054
*
***************************************************************************/

/**
 * @addtogroup Modules
 * @{
 */

/**
 * @file
 * @brief Multi-input complex multiplier
 */

#ifndef _MOD_COMPLEXMULTIPLIER_H
#define _MOD_COMPLEXMULTIPLIER_H

#include "ModCommon.h"
#include "MathHelper.h"

#define MASK_ComplexMultiplier_oneChannelOutput 0x00000100
#define OFFSET_ComplexMultiplier_oneChannelOutput 0x00000008

#define CLASSID_COMPLEXMULTIPLIER (CLASS_ID_MODBASE + 1405)

#ifdef __cplusplus
extern "C" {
#endif

// ----------------------------------------------------------------------
// Overall instance class
// ----------------------------------------------------------------------

typedef struct _awe_modComplexMultiplierInstance
{
    ModuleInstanceDescriptor instance;
    int                oneChannelOutput;    // Boolean value that determines whether all channels are summed to form a single output
    
} awe_modComplexMultiplierInstance;

/* By default the class object is included and this is required for dynamic
 * instantiation.  When doing static code (define AWE_STATIC_CODE) you can save
 * a little bit of memory by eliminating class objects.  However, you lose the
 * ability to change the module status. */

#if (!defined(AWE_NO_CLASS_OBJECTS) || !defined(AWE_STATIC_CODE))
extern const ModClassModule awe_modComplexMultiplierClass;
#endif // #ifndef AWE_NO_CLASS_OBJECTS

/* Dynamic instantiation is used by default.  When building for static
** code, define AWE_STATIC_CODE to eliminate the constructor function. */

#ifndef AWE_STATIC_CODE
// This points the constructor for this class to the base constructor
#define awe_modComplexMultiplierConstructor(ARG1, ARG2, ARG3, ARG4, ARG5) ClassModule_Constructor(CLASSID_COMPLEXMULTIPLIER, ARG1, ARG2, ARG3, ARG4, ARG5)
#endif // #ifndef AWE_STATIC_CODE


void awe_modComplexMultiplierProcess(void *pInstance);

 

void awe_modComplexMultiplierBypass(void *pInstance);


#ifdef __cplusplus
}
#endif


#endif // _MOD_COMPLEXMULTIPLIER_H

/**
 * @}
 *
 * End of file.
 */
