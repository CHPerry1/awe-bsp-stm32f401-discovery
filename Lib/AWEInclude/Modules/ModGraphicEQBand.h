/****************************************************************************
*
*               Audio Framework
*               ---------------
*
****************************************************************************
*     ModGraphicEQBand.h
****************************************************************************
*
*     Description:  Single band of a cascade graphic equalizer
*
*     Copyright:    DSP Concepts, Inc, 2007 - 2015
*                   1800 Wyatt Drive, Suite 14
*                   Santa Clara, CA 95054
*
***************************************************************************/

/**
 * @addtogroup Modules
 * @{
 */

/**
 * @file
 * @brief Single band of a cascade graphic equalizer
 */

#ifndef _MOD_GRAPHICEQBAND_H
#define _MOD_GRAPHICEQBAND_H

#include "ModCommon.h"
#include "MathHelper.h"

#define MASK_GraphicEQBand_gain 0x00000100
#define MASK_GraphicEQBand_lowerEdge 0x00000200
#define MASK_GraphicEQBand_upperEdge 0x00000400
#define MASK_GraphicEQBand_smoothingTime 0x00000800
#define MASK_GraphicEQBand_numStages 0x00001000
#define MASK_GraphicEQBand_smoothingCoeff 0x00002000
#define MASK_GraphicEQBand_targetCoeffs 0x00004000
#define MASK_GraphicEQBand_currentCoeffs 0x00008000
#define MASK_GraphicEQBand_state 0x00010000
#define OFFSET_GraphicEQBand_gain 0x00000008
#define OFFSET_GraphicEQBand_lowerEdge 0x00000009
#define OFFSET_GraphicEQBand_upperEdge 0x0000000A
#define OFFSET_GraphicEQBand_smoothingTime 0x0000000B
#define OFFSET_GraphicEQBand_numStages 0x0000000C
#define OFFSET_GraphicEQBand_smoothingCoeff 0x0000000D
#define OFFSET_GraphicEQBand_targetCoeffs 0x0000000E
#define OFFSET_GraphicEQBand_currentCoeffs 0x0000000F
#define OFFSET_GraphicEQBand_state 0x00000010

#define CLASSID_GRAPHICEQBAND (CLASS_ID_MODBASE + 1211)

#ifdef __cplusplus
extern "C" {
#endif

// ----------------------------------------------------------------------
// Overall instance class
// ----------------------------------------------------------------------

typedef struct _awe_modGraphicEQBandInstance
{
    ModuleInstanceDescriptor instance;
    float              gain;                // Gain in the center of the band, in dB.
    float              lowerEdge;           // Lower frequency edge of the band, in Hz.
    float              upperEdge;           // Upper frequency edge of the band, in Hz.
    float              smoothingTime;       // Time constant of the smoothing process.
    int                numStages;           // Number of 2nd order stages in the filter.
    float              smoothingCoeff;      // Smoothing coefficient.
    float*             targetCoeffs;        // Target coefficient array.
    float*             currentCoeffs;       // Current coefficient array after smoothing.
    float*             state;               // State variable array with 4 values per stage per channel.
} awe_modGraphicEQBandInstance;

/* By default the class object is included and this is required for dynamic
 * instantiation.  When doing static code (define AWE_STATIC_CODE) you can save
 * a little bit of memory by eliminating class objects.  However, you lose the
 * ability to change the module status. */

#if (!defined(AWE_NO_CLASS_OBJECTS) || !defined(AWE_STATIC_CODE))
extern const ModClassModule awe_modGraphicEQBandClass;
#endif // #ifndef AWE_NO_CLASS_OBJECTS

/* Dynamic instantiation is used by default.  When building for static
** code, define AWE_STATIC_CODE to eliminate the constructor function. */

#ifndef AWE_STATIC_CODE
ModInstanceDescriptor * awe_modGraphicEQBandConstructor(INT32 * FW_RESTRICT retVal, UINT32 nIO, WireInstance ** FW_RESTRICT pWires, size_t argCount, const Sample * FW_RESTRICT args);
#endif // #ifndef AWE_STATIC_CODE


void awe_modGraphicEQBandProcess(void *pInstance);

UINT32 awe_modGraphicEQBandSet(void *pInstance, UINT32 mask);
 



#ifdef __cplusplus
}
#endif


#endif // _MOD_GRAPHICEQBAND_H

/**
 * @}
 *
 * End of file.
 */
