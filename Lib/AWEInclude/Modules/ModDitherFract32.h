/****************************************************************************
*
*               Audio Framework
*               ---------------
*
****************************************************************************
*     ModDitherFract32.h
****************************************************************************
*
*     Description:  Adds dither to a fract32 signal
*
*     Copyright:    DSP Concepts, Inc, 2007 - 2015
*                   1800 Wyatt Drive, Suite 14
*                   Santa Clara, CA 95054
*
***************************************************************************/

/**
 * @addtogroup Modules
 * @{
 */

/**
 * @file
 * @brief Adds dither to a fract32 signal
 */

#ifndef _MOD_DITHERFRACT32_H
#define _MOD_DITHERFRACT32_H

#include "ModCommon.h"
#include "MathHelper.h"

#define MASK_DitherFract32_ditherType 0x00000100
#define MASK_DitherFract32_shiftBits 0x00000200
#define MASK_DitherFract32_seed 0x00000400
#define OFFSET_DitherFract32_ditherType 0x00000008
#define OFFSET_DitherFract32_shiftBits 0x00000009
#define OFFSET_DitherFract32_seed 0x0000000A

#define CLASSID_DITHERFRACT32 (CLASS_ID_MODBASE + 2210)

#ifdef __cplusplus
extern "C" {
#endif

// ----------------------------------------------------------------------
// Overall instance class
// ----------------------------------------------------------------------

typedef struct _awe_modDitherFract32Instance
{
    ModuleInstanceDescriptor instance;
    int                ditherType;          // Controls the type of dither used by the module.  0 = no dither.  1 = rectangular distribution.  2 = triangular distribution
    int                shiftBits;           // Number of bits to shift random noise
    unsigned int       seed;                // Initial seed value for the random number generator
    
} awe_modDitherFract32Instance;

/* By default the class object is included and this is required for dynamic
 * instantiation.  When doing static code (define AWE_STATIC_CODE) you can save
 * a little bit of memory by eliminating class objects.  However, you lose the
 * ability to change the module status. */

#if (!defined(AWE_NO_CLASS_OBJECTS) || !defined(AWE_STATIC_CODE))
extern const ModClassModule awe_modDitherFract32Class;
#endif // #ifndef AWE_NO_CLASS_OBJECTS

/* Dynamic instantiation is used by default.  When building for static
** code, define AWE_STATIC_CODE to eliminate the constructor function. */

#ifndef AWE_STATIC_CODE
// This points the constructor for this class to the base constructor
#define awe_modDitherFract32Constructor(ARG1, ARG2, ARG3, ARG4, ARG5) ClassModule_Constructor(CLASSID_DITHERFRACT32, ARG1, ARG2, ARG3, ARG4, ARG5)
#endif // #ifndef AWE_STATIC_CODE


void awe_modDitherFract32Process(void *pInstance);

 



#ifdef __cplusplus
}
#endif


#endif // _MOD_DITHERFRACT32_H

/**
 * @}
 *
 * End of file.
 */
