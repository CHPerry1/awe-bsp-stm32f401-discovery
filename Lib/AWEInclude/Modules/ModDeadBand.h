/****************************************************************************
*
*               Audio Framework
*               ---------------
*
****************************************************************************
*     ModDeadBand.h
****************************************************************************
*
*     Description:  Dead band module
*
*     Copyright:    DSP Concepts, Inc, 2007 - 2015
*                   1800 Wyatt Drive, Suite 14
*                   Santa Clara, CA 95054
*
***************************************************************************/

/**
 * @addtogroup Modules
 * @{
 */

/**
 * @file
 * @brief Dead band module
 */

#ifndef _MOD_DEADBAND_H
#define _MOD_DEADBAND_H

#include "ModCommon.h"
#include "MathHelper.h"

#define MASK_DeadBand_posDelta 0x00000100
#define MASK_DeadBand_negDelta 0x00000200
#define MASK_DeadBand_reset 0x00000400
#define MASK_DeadBand_state 0x00000800
#define OFFSET_DeadBand_posDelta 0x00000008
#define OFFSET_DeadBand_negDelta 0x00000009
#define OFFSET_DeadBand_reset 0x0000000A
#define OFFSET_DeadBand_state 0x0000000B

#define CLASSID_DEADBAND (CLASS_ID_MODBASE + 1229)

#ifdef __cplusplus
extern "C" {
#endif

// ----------------------------------------------------------------------
// Overall instance class
// ----------------------------------------------------------------------

typedef struct _awe_modDeadBandInstance
{
    ModuleInstanceDescriptor instance;
    float              posDelta;            // Positive change that needs to occur before the output signal is affected
    float              negDelta;            // Negative change that needs to occur before the output signal is affected
    int                reset;               // Forces the output to equal the input
    float*             state;               // State variables.  One per channel
} awe_modDeadBandInstance;

/* By default the class object is included and this is required for dynamic
 * instantiation.  When doing static code (define AWE_STATIC_CODE) you can save
 * a little bit of memory by eliminating class objects.  However, you lose the
 * ability to change the module status. */

#if (!defined(AWE_NO_CLASS_OBJECTS) || !defined(AWE_STATIC_CODE))
extern const ModClassModule awe_modDeadBandClass;
#endif // #ifndef AWE_NO_CLASS_OBJECTS

/* Dynamic instantiation is used by default.  When building for static
** code, define AWE_STATIC_CODE to eliminate the constructor function. */

#ifndef AWE_STATIC_CODE
ModInstanceDescriptor * awe_modDeadBandConstructor(INT32 * FW_RESTRICT retVal, UINT32 nIO, WireInstance ** FW_RESTRICT pWires, size_t argCount, const Sample * FW_RESTRICT args);
#endif // #ifndef AWE_STATIC_CODE


void awe_modDeadBandProcess(void *pInstance);

 



#ifdef __cplusplus
}
#endif


#endif // _MOD_DEADBAND_H

/**
 * @}
 *
 * End of file.
 */
