/****************************************************************************
*
*               Audio Framework
*               ---------------
*
****************************************************************************
*     ModWindowOverlap.h
****************************************************************************
*
*     Description:  Applies a time domain window and optionally aliases the result to a shorter length
*
*     Copyright:    DSP Concepts, Inc, 2007 - 2015
*                   1800 Wyatt Drive, Suite 14
*                   Santa Clara, CA 95054
*
***************************************************************************/

/**
 * @addtogroup Modules
 * @{
 */

/**
 * @file
 * @brief Applies a time domain window and optionally aliases the result to a shorter length
 */

#ifndef _MOD_WINDOWOVERLAP_H
#define _MOD_WINDOWOVERLAP_H

#include "ModCommon.h"
#include "MathHelper.h"

#define MASK_WindowOverlap_winLen 0x00000100
#define MASK_WindowOverlap_outLen 0x00000200
#define MASK_WindowOverlap_stateIndex 0x00000400
#define MASK_WindowOverlap_numOverlapSections 0x00000800
#define MASK_WindowOverlap_window 0x00001000
#define MASK_WindowOverlap_state 0x00002000
#define OFFSET_WindowOverlap_winLen 0x00000008
#define OFFSET_WindowOverlap_outLen 0x00000009
#define OFFSET_WindowOverlap_stateIndex 0x0000000A
#define OFFSET_WindowOverlap_numOverlapSections 0x0000000B
#define OFFSET_WindowOverlap_window 0x0000000C
#define OFFSET_WindowOverlap_state 0x0000000D

#define CLASSID_WINDOWOVERLAP (CLASS_ID_MODBASE + 1424)

#ifdef __cplusplus
extern "C" {
#endif

// ----------------------------------------------------------------------
// Overall instance class
// ----------------------------------------------------------------------

typedef struct _awe_modWindowOverlapInstance
{
    ModuleInstanceDescriptor instance;
    int                winLen;              // Length of the window
    int                outLen;              // Length of the output
    int                stateIndex;          // Index of the oldest state variable in the array of state variables
    int                numOverlapSections;  // Number of sections that are overlaped to form the output.  Equals WLEN/OLEN
    float*             window;              // Window coefficients
    float*             state;               // State variable array
} awe_modWindowOverlapInstance;

/* By default the class object is included and this is required for dynamic
 * instantiation.  When doing static code (define AWE_STATIC_CODE) you can save
 * a little bit of memory by eliminating class objects.  However, you lose the
 * ability to change the module status. */

#if (!defined(AWE_NO_CLASS_OBJECTS) || !defined(AWE_STATIC_CODE))
extern const ModClassModule awe_modWindowOverlapClass;
#endif // #ifndef AWE_NO_CLASS_OBJECTS

/* Dynamic instantiation is used by default.  When building for static
** code, define AWE_STATIC_CODE to eliminate the constructor function. */

#ifndef AWE_STATIC_CODE
ModInstanceDescriptor * awe_modWindowOverlapConstructor(INT32 * FW_RESTRICT retVal, UINT32 nIO, WireInstance ** FW_RESTRICT pWires, size_t argCount, const Sample * FW_RESTRICT args);
#endif // #ifndef AWE_STATIC_CODE


void awe_modWindowOverlapProcess(void *pInstance);

 



#ifdef __cplusplus
}
#endif


#endif // _MOD_WINDOWOVERLAP_H

/**
 * @}
 *
 * End of file.
 */
