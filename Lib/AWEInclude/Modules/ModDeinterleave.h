/****************************************************************************
*
*               Audio Framework
*               ---------------
*
****************************************************************************
*     ModDeinterleave.h
****************************************************************************
*
*     Description:  Deinterleaves a multichannel audio signal into separate mono signals
*
*     Copyright:    DSP Concepts, Inc, 2007 - 2015
*                   1800 Wyatt Drive, Suite 14
*                   Santa Clara, CA 95054
*
***************************************************************************/

/**
 * @addtogroup Modules
 * @{
 */

/**
 * @file
 * @brief Deinterleaves a multichannel audio signal into separate mono signals
 */

#ifndef _MOD_DEINTERLEAVE_H
#define _MOD_DEINTERLEAVE_H

#include "ModCommon.h"
#include "MathHelper.h"


#define CLASSID_DEINTERLEAVE (CLASS_ID_MODBASE + 5)

#ifdef __cplusplus
extern "C" {
#endif

// ----------------------------------------------------------------------
// Overall instance class
// ----------------------------------------------------------------------

typedef struct _awe_modDeinterleaveInstance
{
    ModuleInstanceDescriptor instance;
    
    
} awe_modDeinterleaveInstance;

/* By default the class object is included and this is required for dynamic
 * instantiation.  When doing static code (define AWE_STATIC_CODE) you can save
 * a little bit of memory by eliminating class objects.  However, you lose the
 * ability to change the module status. */

#if (!defined(AWE_NO_CLASS_OBJECTS) || !defined(AWE_STATIC_CODE))
extern const ModClassModule awe_modDeinterleaveClass;
#endif // #ifndef AWE_NO_CLASS_OBJECTS

/* Dynamic instantiation is used by default.  When building for static
** code, define AWE_STATIC_CODE to eliminate the constructor function. */

#ifndef AWE_STATIC_CODE
// This points the constructor for this class to the base constructor
#define awe_modDeinterleaveConstructor(ARG1, ARG2, ARG3, ARG4, ARG5) ClassModule_Constructor(CLASSID_DEINTERLEAVE, ARG1, ARG2, ARG3, ARG4, ARG5)
#endif // #ifndef AWE_STATIC_CODE


void awe_modDeinterleaveProcess(void *pInstance);

 



#ifdef __cplusplus
}
#endif


#endif // _MOD_DEINTERLEAVE_H

/**
 * @}
 *
 * End of file.
 */
