/****************************************************************************
*
*               Audio Framework
*               ---------------
*
****************************************************************************
*     ModRunningMinMax.h
****************************************************************************
*
*     Description:  Computes long term min/max/max abs
*
*     Copyright:    DSP Concepts, Inc, 2007 - 2015
*                   1800 Wyatt Drive, Suite 14
*                   Santa Clara, CA 95054
*
***************************************************************************/

/**
 * @addtogroup Modules
 * @{
 */

/**
 * @file
 * @brief Computes long term min/max/max abs
 */

#ifndef _MOD_RUNNINGMINMAX_H
#define _MOD_RUNNINGMINMAX_H

#include "ModCommon.h"
#include "MathHelper.h"

#define MASK_RunningMinMax_statisticsType 0x00000100
#define MASK_RunningMinMax_value 0x00000200
#define MASK_RunningMinMax_reset 0x00000400
#define OFFSET_RunningMinMax_statisticsType 0x00000008
#define OFFSET_RunningMinMax_value 0x00000009
#define OFFSET_RunningMinMax_reset 0x0000000A

#define CLASSID_RUNNINGMINMAX (CLASS_ID_MODBASE + 1042)

#ifdef __cplusplus
extern "C" {
#endif

// ----------------------------------------------------------------------
// Overall instance class
// ----------------------------------------------------------------------

typedef struct _awe_modRunningMinMaxInstance
{
    ModuleInstanceDescriptor instance;
    int                statisticsType;      // Type of statistics needed.
    float              value;               // Instantaneous output value.
    int                reset;               // Trigger pin to reset monitoring.
    
} awe_modRunningMinMaxInstance;

/* By default the class object is included and this is required for dynamic
 * instantiation.  When doing static code (define AWE_STATIC_CODE) you can save
 * a little bit of memory by eliminating class objects.  However, you lose the
 * ability to change the module status. */

#if (!defined(AWE_NO_CLASS_OBJECTS) || !defined(AWE_STATIC_CODE))
extern const ModClassModule awe_modRunningMinMaxClass;
#endif // #ifndef AWE_NO_CLASS_OBJECTS

/* Dynamic instantiation is used by default.  When building for static
** code, define AWE_STATIC_CODE to eliminate the constructor function. */

#ifndef AWE_STATIC_CODE
// This points the constructor for this class to the base constructor
#define awe_modRunningMinMaxConstructor(ARG1, ARG2, ARG3, ARG4, ARG5) ClassModule_Constructor(CLASSID_RUNNINGMINMAX, ARG1, ARG2, ARG3, ARG4, ARG5)
#endif // #ifndef AWE_STATIC_CODE


void awe_modRunningMinMaxProcess(void *pInstance);

 



#ifdef __cplusplus
}
#endif


#endif // _MOD_RUNNINGMINMAX_H

/**
 * @}
 *
 * End of file.
 */
