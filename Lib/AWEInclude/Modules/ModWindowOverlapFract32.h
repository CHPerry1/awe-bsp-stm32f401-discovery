/****************************************************************************
*
*               Audio Framework
*               ---------------
*
****************************************************************************
*     ModWindowOverlapFract32.h
****************************************************************************
*
*     Description:  Applies a time domain window and optionally aliases the result to a shorter length
*
*     Copyright:    DSP Concepts, Inc, 2007 - 2015
*                   1800 Wyatt Drive, Suite 14
*                   Santa Clara, CA 95054
*
***************************************************************************/

/**
 * @addtogroup Modules
 * @{
 */

/**
 * @file
 * @brief Applies a time domain window and optionally aliases the result to a shorter length
 */

#ifndef _MOD_WINDOWOVERLAPFRACT32_H
#define _MOD_WINDOWOVERLAPFRACT32_H

#include "ModCommon.h"
#include "MathHelper.h"

#define MASK_WindowOverlapFract32_winLen 0x00000100
#define MASK_WindowOverlapFract32_outLen 0x00000200
#define MASK_WindowOverlapFract32_stateIndex 0x00000400
#define MASK_WindowOverlapFract32_numOverlapSections 0x00000800
#define MASK_WindowOverlapFract32_postShift 0x00001000
#define MASK_WindowOverlapFract32_window 0x00002000
#define MASK_WindowOverlapFract32_windowFract32 0x00004000
#define MASK_WindowOverlapFract32_state 0x00008000
#define OFFSET_WindowOverlapFract32_winLen 0x00000008
#define OFFSET_WindowOverlapFract32_outLen 0x00000009
#define OFFSET_WindowOverlapFract32_stateIndex 0x0000000A
#define OFFSET_WindowOverlapFract32_numOverlapSections 0x0000000B
#define OFFSET_WindowOverlapFract32_postShift 0x0000000C
#define OFFSET_WindowOverlapFract32_window 0x0000000D
#define OFFSET_WindowOverlapFract32_windowFract32 0x0000000E
#define OFFSET_WindowOverlapFract32_state 0x0000000F

#define CLASSID_WINDOWOVERLAPFRACT32 (CLASS_ID_MODBASE + 2409)

#ifdef __cplusplus
extern "C" {
#endif

// ----------------------------------------------------------------------
// Overall instance class
// ----------------------------------------------------------------------

typedef struct _awe_modWindowOverlapFract32Instance
{
    ModuleInstanceDescriptor instance;
    int                winLen;              // Length of the window
    int                outLen;              // Length of the output
    int                stateIndex;          // Index of the oldest state variable in the array of state variables
    int                numOverlapSections;  // Number of sections that are overlaped to form the output.  Equals WLEN/OLEN
    int                postShift;           // Number of bits to shift
    float*             window;              // Window coefficients
    fract32*           windowFract32;       // Window coefficients in fractional representation
    fract32*           state;               // State variable array
} awe_modWindowOverlapFract32Instance;

/* By default the class object is included and this is required for dynamic
 * instantiation.  When doing static code (define AWE_STATIC_CODE) you can save
 * a little bit of memory by eliminating class objects.  However, you lose the
 * ability to change the module status. */

#if (!defined(AWE_NO_CLASS_OBJECTS) || !defined(AWE_STATIC_CODE))
extern const ModClassModule awe_modWindowOverlapFract32Class;
#endif // #ifndef AWE_NO_CLASS_OBJECTS

/* Dynamic instantiation is used by default.  When building for static
** code, define AWE_STATIC_CODE to eliminate the constructor function. */

#ifndef AWE_STATIC_CODE
ModInstanceDescriptor * awe_modWindowOverlapFract32Constructor(INT32 * FW_RESTRICT retVal, UINT32 nIO, WireInstance ** FW_RESTRICT pWires, size_t argCount, const Sample * FW_RESTRICT args);
#endif // #ifndef AWE_STATIC_CODE


void awe_modWindowOverlapFract32Process(void *pInstance);

UINT32 awe_modWindowOverlapFract32Set(void *pInstance, UINT32 mask);
 



#ifdef __cplusplus
}
#endif


#endif // _MOD_WINDOWOVERLAPFRACT32_H

/**
 * @}
 *
 * End of file.
 */
