/****************************************************************************
*
*               Audio Framework
*               ---------------
*
****************************************************************************
*     ModBiquadCascadeHP.h
****************************************************************************
*
*     Description:  Cascade of second order Biquad filters with high precision implementation
*
*     Copyright:    DSP Concepts, Inc, 2007 - 2015
*                   1800 Wyatt Drive, Suite 14
*                   Santa Clara, CA 95054
*
***************************************************************************/

/**
 * @addtogroup Modules
 * @{
 */

/**
 * @file
 * @brief Cascade of second order Biquad filters with high precision implementation
 */

#ifndef _MOD_BIQUADCASCADEHP_H
#define _MOD_BIQUADCASCADEHP_H

#include "ModCommon.h"
#include "MathHelper.h"

#define MASK_BiquadCascadeHP_numStages 0x00000100
#define MASK_BiquadCascadeHP_coeffs 0x00000200
#define MASK_BiquadCascadeHP_targetCoeffs 0x00000400
#define MASK_BiquadCascadeHP_state 0x00000800
#define OFFSET_BiquadCascadeHP_numStages 0x00000008
#define OFFSET_BiquadCascadeHP_coeffs 0x00000009
#define OFFSET_BiquadCascadeHP_targetCoeffs 0x0000000A
#define OFFSET_BiquadCascadeHP_state 0x0000000B

#define CLASSID_BIQUADCASCADEHP (CLASS_ID_MODBASE + 4003)

#ifdef __cplusplus
extern "C" {
#endif

// ----------------------------------------------------------------------
// Overall instance class
// ----------------------------------------------------------------------

typedef struct _awe_modBiquadCascadeHPInstance
{
    ModuleInstanceDescriptor instance;
    int                numStages;           // Number of stages in the filter. The filter order = 2*numStages.
    float*             coeffs;              // Matrix of filter coefficients. The size of the matrix is 5 x numStages. Each column contains the variables for a biquad arranged as [b0; b1; b2; a1; a2].
    float*             targetCoeffs;        // Matrix of filter coefficients in the High Precision format. The size of the matrix is 5 x numStages. Each column contains the variables for a biquad arranged as [Fb; Gaa; Gab; K; Fa].
    float*             state;               // State variables. 2 per channel.
} awe_modBiquadCascadeHPInstance;

/* By default the class object is included and this is required for dynamic
 * instantiation.  When doing static code (define AWE_STATIC_CODE) you can save
 * a little bit of memory by eliminating class objects.  However, you lose the
 * ability to change the module status. */

#if (!defined(AWE_NO_CLASS_OBJECTS) || !defined(AWE_STATIC_CODE))
extern const ModClassModule awe_modBiquadCascadeHPClass;
#endif // #ifndef AWE_NO_CLASS_OBJECTS

/* Dynamic instantiation is used by default.  When building for static
** code, define AWE_STATIC_CODE to eliminate the constructor function. */

#ifndef AWE_STATIC_CODE
ModInstanceDescriptor * awe_modBiquadCascadeHPConstructor(INT32 * FW_RESTRICT retVal, UINT32 nIO, WireInstance ** FW_RESTRICT pWires, size_t argCount, const Sample * FW_RESTRICT args);
#endif // #ifndef AWE_STATIC_CODE


void awe_modBiquadCascadeHPProcess(void *pInstance);

UINT32 awe_modBiquadCascadeHPSet(void *pInstance, UINT32 mask);
 



#ifdef __cplusplus
}
#endif


#endif // _MOD_BIQUADCASCADEHP_H

/**
 * @}
 *
 * End of file.
 */
