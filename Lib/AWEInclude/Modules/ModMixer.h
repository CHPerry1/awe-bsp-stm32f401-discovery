/****************************************************************************
*
*               Audio Framework
*               ---------------
*
****************************************************************************
*     ModMixer.h
****************************************************************************
*
*     Description:  M-input x N-output mixer
*
*     Copyright:    DSP Concepts, Inc, 2007 - 2015
*                   1800 Wyatt Drive, Suite 14
*                   Santa Clara, CA 95054
*
***************************************************************************/

/**
 * @addtogroup Modules
 * @{
 */

/**
 * @file
 * @brief M-input x N-output mixer
 */

#ifndef _MOD_MIXER_H
#define _MOD_MIXER_H

#include "ModCommon.h"
#include "MathHelper.h"

#define MASK_Mixer_gain 0x00000100
#define OFFSET_Mixer_gain 0x00000008

#define CLASSID_MIXER (CLASS_ID_MODBASE + 1029)

#ifdef __cplusplus
extern "C" {
#endif

// ----------------------------------------------------------------------
// Overall instance class
// ----------------------------------------------------------------------

typedef struct _awe_modMixerInstance
{
    ModuleInstanceDescriptor instance;
    
    float*             gain;                // Array of linear gain factors. The size of the array is numIn * numOut.
} awe_modMixerInstance;

/* By default the class object is included and this is required for dynamic
 * instantiation.  When doing static code (define AWE_STATIC_CODE) you can save
 * a little bit of memory by eliminating class objects.  However, you lose the
 * ability to change the module status. */

#if (!defined(AWE_NO_CLASS_OBJECTS) || !defined(AWE_STATIC_CODE))
extern const ModClassModule awe_modMixerClass;
#endif // #ifndef AWE_NO_CLASS_OBJECTS

/* Dynamic instantiation is used by default.  When building for static
** code, define AWE_STATIC_CODE to eliminate the constructor function. */

#ifndef AWE_STATIC_CODE
ModInstanceDescriptor * awe_modMixerConstructor(INT32 * FW_RESTRICT retVal, UINT32 nIO, WireInstance ** FW_RESTRICT pWires, size_t argCount, const Sample * FW_RESTRICT args);
#endif // #ifndef AWE_STATIC_CODE


void awe_modMixerProcess(void *pInstance);

 



#ifdef __cplusplus
}
#endif


#endif // _MOD_MIXER_H

/**
 * @}
 *
 * End of file.
 */
