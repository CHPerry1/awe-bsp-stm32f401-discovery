/****************************************************************************
*
*               Audio Framework
*               ---------------
*
****************************************************************************
*     ModWAVInterp16OneShotSourceFract32.h
****************************************************************************
*
*     Description:  Periodic one shot WAV playback with linear interpolation
*
*     Copyright:    DSP Concepts, Inc, 2007 - 2015
*                   1800 Wyatt Drive, Suite 14
*                   Santa Clara, CA 95054
*
***************************************************************************/

/**
 * @addtogroup Modules
 * @{
 */

/**
 * @file
 * @brief Periodic one shot WAV playback with linear interpolation
 */

#ifndef _MOD_WAVINTERP16ONESHOTSOURCEFRACT32_H
#define _MOD_WAVINTERP16ONESHOTSOURCEFRACT32_H

#include "ModCommon.h"
#include "MathHelper.h"

#define MASK_WAVInterp16OneShotSourceFract32_smoothingTime 0x00000100
#define MASK_WAVInterp16OneShotSourceFract32_circularBufferSize 0x00000200
#define MASK_WAVInterp16OneShotSourceFract32_extensionSize 0x00000400
#define MASK_WAVInterp16OneShotSourceFract32_currentFRatio 0x00000800
#define MASK_WAVInterp16OneShotSourceFract32_smoothingCoeff 0x00001000
#define MASK_WAVInterp16OneShotSourceFract32_fIndex 0x00002000
#define OFFSET_WAVInterp16OneShotSourceFract32_smoothingTime 0x00000008
#define OFFSET_WAVInterp16OneShotSourceFract32_circularBufferSize 0x00000009
#define OFFSET_WAVInterp16OneShotSourceFract32_extensionSize 0x0000000A
#define OFFSET_WAVInterp16OneShotSourceFract32_currentFRatio 0x0000000B
#define OFFSET_WAVInterp16OneShotSourceFract32_smoothingCoeff 0x0000000C
#define OFFSET_WAVInterp16OneShotSourceFract32_fIndex 0x0000000D

#define CLASSID_WAVINTERP16ONESHOTSOURCEFRACT32 (CLASS_ID_MODBASE + 2224)

#ifdef __cplusplus
extern "C" {
#endif

// ----------------------------------------------------------------------
// Overall instance class
// ----------------------------------------------------------------------

typedef struct _awe_modWAVInterp16OneShotSourceFract32Instance
{
    ModuleInstanceDescriptor instance;
    float              smoothingTime;       // Time constant of the smoothing process
    int                circularBufferSize;  // Size of the upstream circular buffer, in 16-bit words.
    int                extensionSize;       // Size of the extension region of the circular buffer,  in 16-bit words
    float              currentFRatio;       // Specifies rate at which audio is read from the input buffer
    float              smoothingCoeff;      // Smoothing coefficient
    int                fIndex;              // 32-bit fractional state index
    
} awe_modWAVInterp16OneShotSourceFract32Instance;

/* By default the class object is included and this is required for dynamic
 * instantiation.  When doing static code (define AWE_STATIC_CODE) you can save
 * a little bit of memory by eliminating class objects.  However, you lose the
 * ability to change the module status. */

#if (!defined(AWE_NO_CLASS_OBJECTS) || !defined(AWE_STATIC_CODE))
extern const ModClassModule awe_modWAVInterp16OneShotSourceFract32Class;
#endif // #ifndef AWE_NO_CLASS_OBJECTS

/* Dynamic instantiation is used by default.  When building for static
** code, define AWE_STATIC_CODE to eliminate the constructor function. */

#ifndef AWE_STATIC_CODE
// This points the constructor for this class to the base constructor
#define awe_modWAVInterp16OneShotSourceFract32Constructor(ARG1, ARG2, ARG3, ARG4, ARG5) ClassModule_Constructor(CLASSID_WAVINTERP16ONESHOTSOURCEFRACT32, ARG1, ARG2, ARG3, ARG4, ARG5)
#endif // #ifndef AWE_STATIC_CODE


void awe_modWAVInterp16OneShotSourceFract32Process(void *pInstance);

UINT32 awe_modWAVInterp16OneShotSourceFract32Set(void *pInstance, UINT32 mask);
 



#ifdef __cplusplus
}
#endif


#endif // _MOD_WAVINTERP16ONESHOTSOURCEFRACT32_H

/**
 * @}
 *
 * End of file.
 */
