/****************************************************************************
*
*               Audio Framework
*               ---------------
*
****************************************************************************
*     ModComplexToRealImag.h
****************************************************************************
*
*     Description:  Converts a complex signal into separate real and imaginary components
*
*     Copyright:    DSP Concepts, Inc, 2007 - 2015
*                   1800 Wyatt Drive, Suite 14
*                   Santa Clara, CA 95054
*
***************************************************************************/

/**
 * @addtogroup Modules
 * @{
 */

/**
 * @file
 * @brief Converts a complex signal into separate real and imaginary components
 */

#ifndef _MOD_COMPLEXTOREALIMAG_H
#define _MOD_COMPLEXTOREALIMAG_H

#include "ModCommon.h"
#include "MathHelper.h"


#define CLASSID_COMPLEXTOREALIMAG (CLASS_ID_MODBASE + 1407)

#ifdef __cplusplus
extern "C" {
#endif

// ----------------------------------------------------------------------
// Overall instance class
// ----------------------------------------------------------------------

typedef struct _awe_modComplexToRealImagInstance
{
    ModuleInstanceDescriptor instance;
    
    
} awe_modComplexToRealImagInstance;

/* By default the class object is included and this is required for dynamic
 * instantiation.  When doing static code (define AWE_STATIC_CODE) you can save
 * a little bit of memory by eliminating class objects.  However, you lose the
 * ability to change the module status. */

#if (!defined(AWE_NO_CLASS_OBJECTS) || !defined(AWE_STATIC_CODE))
extern const ModClassModule awe_modComplexToRealImagClass;
#endif // #ifndef AWE_NO_CLASS_OBJECTS

/* Dynamic instantiation is used by default.  When building for static
** code, define AWE_STATIC_CODE to eliminate the constructor function. */

#ifndef AWE_STATIC_CODE
// This points the constructor for this class to the base constructor
#define awe_modComplexToRealImagConstructor(ARG1, ARG2, ARG3, ARG4, ARG5) ClassModule_Constructor(CLASSID_COMPLEXTOREALIMAG, ARG1, ARG2, ARG3, ARG4, ARG5)
#endif // #ifndef AWE_STATIC_CODE


void awe_modComplexToRealImagProcess(void *pInstance);

 



#ifdef __cplusplus
}
#endif


#endif // _MOD_COMPLEXTOREALIMAG_H

/**
 * @}
 *
 * End of file.
 */
