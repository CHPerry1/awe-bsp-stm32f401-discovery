/****************************************************************************
*
*               Audio Framework
*               ---------------
*
****************************************************************************
*     ModFft.h
****************************************************************************
*
*     Description:  Forward FFT of real data
*
*     Copyright:    DSP Concepts, Inc, 2007 - 2015
*                   1800 Wyatt Drive, Suite 14
*                   Santa Clara, CA 95054
*
***************************************************************************/

/**
 * @addtogroup Modules
 * @{
 */

/**
 * @file
 * @brief Forward FFT of real data
 */

#ifndef _MOD_FFT_H
#define _MOD_FFT_H

#include "ModCommon.h"
#include "MathHelper.h"

#define MASK_Fft_hardware_specific_struct_pointer 0x00000100
#define OFFSET_Fft_hardware_specific_struct_pointer 0x00000008

#define CLASSID_FFT (CLASS_ID_MODBASE + 1408)

#ifdef __cplusplus
extern "C" {
#endif

// ----------------------------------------------------------------------
// Overall instance class
// ----------------------------------------------------------------------

typedef struct _awe_modFftInstance
{
    ModuleInstanceDescriptor instance;
    int                hardware_specific_struct_pointer; // This may point to a struct that varies based on the target platform
    
} awe_modFftInstance;

/* By default the class object is included and this is required for dynamic
 * instantiation.  When doing static code (define AWE_STATIC_CODE) you can save
 * a little bit of memory by eliminating class objects.  However, you lose the
 * ability to change the module status. */

#if (!defined(AWE_NO_CLASS_OBJECTS) || !defined(AWE_STATIC_CODE))
extern const ModClassModule awe_modFftClass;
#endif // #ifndef AWE_NO_CLASS_OBJECTS

/* Dynamic instantiation is used by default.  When building for static
** code, define AWE_STATIC_CODE to eliminate the constructor function. */

#ifndef AWE_STATIC_CODE
ModInstanceDescriptor * awe_modFftConstructor(INT32 * FW_RESTRICT retVal, UINT32 nIO, WireInstance ** FW_RESTRICT pWires, size_t argCount, const Sample * FW_RESTRICT args);
#endif // #ifndef AWE_STATIC_CODE


void awe_modFftProcess(void *pInstance);

UINT32 awe_modFftSet(void *pInstance, UINT32 mask);
 



#ifdef __cplusplus
}
#endif


#endif // _MOD_FFT_H

/**
 * @}
 *
 * End of file.
 */
