/****************************************************************************
*
*               Audio Framework
*               ---------------
*
****************************************************************************
*     ModSampleRate.h
****************************************************************************
*
*     Description:  Measures the sample rate of the input signal
*
*     Copyright:    DSP Concepts, Inc, 2007 - 2015
*                   1800 Wyatt Drive, Suite 14
*                   Santa Clara, CA 95054
*
***************************************************************************/

/**
 * @addtogroup Modules
 * @{
 */

/**
 * @file
 * @brief Measures the sample rate of the input signal
 */

#ifndef _MOD_SAMPLERATE_H
#define _MOD_SAMPLERATE_H

#include "ModCommon.h"
#include "MathHelper.h"

#define MASK_SampleRate_procSpeed 0x00000100
#define MASK_SampleRate_numBlocksToAverage 0x00000200
#define MASK_SampleRate_lastCount 0x00000400
#define MASK_SampleRate_averageDeltaCount 0x00000800
#define MASK_SampleRate_SRArrayLength 0x00001000
#define MASK_SampleRate_initialized 0x00002000
#define MASK_SampleRate_dataValid 0x00004000
#define MASK_SampleRate_measuredSR 0x00008000
#define MASK_SampleRate_prevMeasuredSR 0x00010000
#define MASK_SampleRate_prevPrevMeasuredSR 0x00020000
#define MASK_SampleRate_SROutPin 0x00040000
#define MASK_SampleRate_blockCounter 0x00080000
#define MASK_SampleRate_allowableSR 0x00100000
#define OFFSET_SampleRate_procSpeed 0x00000008
#define OFFSET_SampleRate_numBlocksToAverage 0x00000009
#define OFFSET_SampleRate_lastCount 0x0000000A
#define OFFSET_SampleRate_averageDeltaCount 0x0000000B
#define OFFSET_SampleRate_SRArrayLength 0x0000000C
#define OFFSET_SampleRate_initialized 0x0000000D
#define OFFSET_SampleRate_dataValid 0x0000000E
#define OFFSET_SampleRate_measuredSR 0x0000000F
#define OFFSET_SampleRate_prevMeasuredSR 0x00000010
#define OFFSET_SampleRate_prevPrevMeasuredSR 0x00000011
#define OFFSET_SampleRate_SROutPin 0x00000012
#define OFFSET_SampleRate_blockCounter 0x00000013
#define OFFSET_SampleRate_allowableSR 0x00000014

#define CLASSID_SAMPLERATE (CLASS_ID_MODBASE + 3003)

#ifdef __cplusplus
extern "C" {
#endif

// ----------------------------------------------------------------------
// Overall instance class
// ----------------------------------------------------------------------

typedef struct _awe_modSampleRateInstance
{
    ModuleInstanceDescriptor instance;
    float              procSpeed;           // Processor clock speed in MHz
    int                numBlocksToAverage;  // Number of blocks to process before the sample rate is retured
    int                lastCount;           // Holds the last call cycle count
    int                averageDeltaCount;   // Holds the average delta count of numBlocksToAverage calls
    int                SRArrayLength;       // Number of sample rates that can be estimated
    int                initialized;         // Specifies whether the last cycle counter has been initialized
    int                dataValid;           // Indicates whether the sample rate measurement is valid
    float              measuredSR;          // Readback variable.  Displays the measured sample rate on the inspector
    float              prevMeasuredSR;      // Last measured sample rate
    float              prevPrevMeasuredSR;  // Previous to last measured sample rate
    int                SROutPin;            // Boolean specifies whether module will have output pin
    int                blockCounter;        // Counts up to numBlocksToAverage
    float*             allowableSR;         // Array of possible sample rates
} awe_modSampleRateInstance;

/* By default the class object is included and this is required for dynamic
 * instantiation.  When doing static code (define AWE_STATIC_CODE) you can save
 * a little bit of memory by eliminating class objects.  However, you lose the
 * ability to change the module status. */

#if (!defined(AWE_NO_CLASS_OBJECTS) || !defined(AWE_STATIC_CODE))
extern const ModClassModule awe_modSampleRateClass;
#endif // #ifndef AWE_NO_CLASS_OBJECTS

/* Dynamic instantiation is used by default.  When building for static
** code, define AWE_STATIC_CODE to eliminate the constructor function. */

#ifndef AWE_STATIC_CODE
ModInstanceDescriptor * awe_modSampleRateConstructor(INT32 * FW_RESTRICT retVal, UINT32 nIO, WireInstance ** FW_RESTRICT pWires, size_t argCount, const Sample * FW_RESTRICT args);
#endif // #ifndef AWE_STATIC_CODE


void awe_modSampleRateProcess(void *pInstance);

 



#ifdef __cplusplus
}
#endif


#endif // _MOD_SAMPLERATE_H

/**
 * @}
 *
 * End of file.
 */
