/****************************************************************************
*
*               Audio Framework
*               ---------------
*
****************************************************************************
*     ModScalerDBSmoothed.h
****************************************************************************
*
*     Description:  Gain control with dB units and smoothing
*
*     Copyright:    DSP Concepts, Inc, 2007 - 2015
*                   1800 Wyatt Drive, Suite 14
*                   Santa Clara, CA 95054
*
***************************************************************************/

/**
 * @addtogroup Modules
 * @{
 */

/**
 * @file
 * @brief Gain control with dB units and smoothing
 */

#ifndef _MOD_SCALERDBSMOOTHED_H
#define _MOD_SCALERDBSMOOTHED_H

#include "ModCommon.h"
#include "MathHelper.h"

#define MASK_ScalerDBSmoothed_gainDB 0x00000100
#define MASK_ScalerDBSmoothed_currentGain 0x00000200
#define MASK_ScalerDBSmoothed_smoothingTime 0x00000400
#define MASK_ScalerDBSmoothed_smoothingCoeff 0x00000800
#define MASK_ScalerDBSmoothed_gain 0x00001000
#define OFFSET_ScalerDBSmoothed_gainDB 0x00000008
#define OFFSET_ScalerDBSmoothed_currentGain 0x00000009
#define OFFSET_ScalerDBSmoothed_smoothingTime 0x0000000A
#define OFFSET_ScalerDBSmoothed_smoothingCoeff 0x0000000B
#define OFFSET_ScalerDBSmoothed_gain 0x0000000C

#define CLASSID_SCALERDBSMOOTHED (CLASS_ID_MODBASE + 1048)

#ifdef __cplusplus
extern "C" {
#endif

// ----------------------------------------------------------------------
// Overall instance class
// ----------------------------------------------------------------------

typedef struct _awe_modScalerDBSmoothedInstance
{
    ModuleInstanceDescriptor instance;
    float              gainDB;              // Gain in dB units.
    float              currentGain;         // Instantaneous gain applied by the module. This is also the starting gain of the module.
    float              smoothingTime;       // Time constant of the smoothing process.
    float              smoothingCoeff;      // Smoothing coefficient.
    float              gain;                // Gain in linear units.
    
} awe_modScalerDBSmoothedInstance;

/* By default the class object is included and this is required for dynamic
 * instantiation.  When doing static code (define AWE_STATIC_CODE) you can save
 * a little bit of memory by eliminating class objects.  However, you lose the
 * ability to change the module status. */

#if (!defined(AWE_NO_CLASS_OBJECTS) || !defined(AWE_STATIC_CODE))
extern const ModClassModule awe_modScalerDBSmoothedClass;
#endif // #ifndef AWE_NO_CLASS_OBJECTS

/* Dynamic instantiation is used by default.  When building for static
** code, define AWE_STATIC_CODE to eliminate the constructor function. */

#ifndef AWE_STATIC_CODE
// This points the constructor for this class to the base constructor
#define awe_modScalerDBSmoothedConstructor(ARG1, ARG2, ARG3, ARG4, ARG5) ClassModule_Constructor(CLASSID_SCALERDBSMOOTHED, ARG1, ARG2, ARG3, ARG4, ARG5)
#endif // #ifndef AWE_STATIC_CODE


void awe_modScalerDBSmoothedProcess(void *pInstance);

UINT32 awe_modScalerDBSmoothedSet(void *pInstance, UINT32 mask);
 



#ifdef __cplusplus
}
#endif


#endif // _MOD_SCALERDBSMOOTHED_H

/**
 * @}
 *
 * End of file.
 */
