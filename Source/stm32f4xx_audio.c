/*******************************************************************************
*
*               Audio Framework
*               ---------------
*
********************************************************************************
*     stm32f4xx_audio.c
********************************************************************************
*
*     Description:  Audio Methods
*
*     Copyright:    (c) DSP Concepts, Inc. 2007 - 2016
*                   1800 Wyatt Drive, Suite 14
*                   Sunnyvale, CA 95054
*
*******************************************************************************/

#include "stm32f4xx_audio.h"
#include "platform.h"

#define IO_MODE_OUTPUT 1

// These numbers may not be correct for anything below 48KHz
const uint32_t I2SFreq[8] = {8000, 11025, 16000, 22050, 32000, 44100, 48000, 96000};
const uint32_t I2SPLLN[8] = {256, 429, 213, 429, 426, 271, 258, 344};
const uint32_t I2SPLLR[8] = {5, 4, 4, 4, 4, 6, 3, 1};

AUDIO_DrvTypeDef * audio_drv;
extern I2S_HandleTypeDef hI2S3;

static void CODEC_Reset(void);
static void I2Sx_Init(void);


/**
  * @brief  Configures the audio peripherals.
  * @param  OutputDevice: OUTPUT_DEVICE_SPEAKER, OUTPUT_DEVICE_HEADPHONE,
  *                       OUTPUT_DEVICE_BOTH or OUTPUT_DEVICE_AUTO .
  * @param  Volume: Initial volume level (from 0 (Mute) to 100 (Max))
  * @param  AudioFreq: Audio frequency used to play the audio stream.
  * @note   This function configure also that the I2S PLL input clock.    
  * @retval 0 if correct communication, else wrong communication
  */
uint8_t BSP_AUDIO_OUT_Init(uint16_t OutputDevice, uint8_t Volume, uint32_t AudioFreq)
{ 
    RCC_PeriphCLKInitTypeDef RCC_ExCLKInitStruct;    
    uint32_t nDeviceID = 0x00;
    uint8_t index = 0, freqindex = 0xFF;
  
    for (index = 0; index < 8; index++)
    {
        if (I2SFreq[index] == AudioFreq)
        {
          freqindex = index;
        }
    }
  
    HAL_RCCEx_GetPeriphCLKConfig(&RCC_ExCLKInitStruct); 

    if (freqindex != 0xFF)
    {
        /* I2S clock config 
        PLLI2S_VCO = f(VCO clock) = f(PLLI2S clock input) � (PLLI2SN/PLLM)
        I2SCLK = f(PLLI2S clock output) = f(VCO clock) / PLLI2SR */
        RCC_ExCLKInitStruct.PeriphClockSelection = RCC_PERIPHCLK_I2S;
        RCC_ExCLKInitStruct.PLLI2S.PLLI2SN = I2SPLLN[freqindex];
        RCC_ExCLKInitStruct.PLLI2S.PLLI2SR = I2SPLLR[freqindex];
        HAL_RCCEx_PeriphCLKConfig(&RCC_ExCLKInitStruct);     
    } 
    else /* Default PLL I2S configuration */
    {
        /* I2S clock config 
        PLLI2S_VCO = f(VCO clock) = f(PLLI2S clock input) � (PLLI2SN/PLLM)
        I2SCLK = f(PLLI2S clock output) = f(VCO clock) / PLLI2SR */
        RCC_ExCLKInitStruct.PeriphClockSelection = RCC_PERIPHCLK_I2S;
        RCC_ExCLKInitStruct.PLLI2S.PLLI2SN = 258;
        RCC_ExCLKInitStruct.PLLI2S.PLLI2SR = 3;      
        HAL_RCCEx_PeriphCLKConfig(&RCC_ExCLKInitStruct); 
    }
  
    /* Reset the Codec Registers */
    CODEC_Reset();

    nDeviceID = cs43l22_drv.ReadID(AUDIO_I2C_ADDRESS);

    if ( (nDeviceID & CS43L22_ID_MASK) == CS43L22_ID)
    {  
        /* Initialize the audio driver structure */
        audio_drv = &cs43l22_drv; 
        
        audio_drv->Init(AUDIO_I2C_ADDRESS, OutputDevice, Volume, AudioFreq);
        
        /* I2S data transfer preparation:
        Prepare the Media to be used for the audio transfer from memory to I2S peripheral */
        /* Configure the I2S peripheral */
        I2Sx_Init();  
        
        cs43l22_drv.Play(AUDIO_I2C_ADDRESS);
        
        return AUDIO_OK;
    }

    return AUDIO_ERROR;

}


/**
  * @brief  Starts playing audio stream from a data buffer for a determined size. 
  * @param  pBuffer: Pointer to the buffer 
  * @param  Size: Number of audio data BYTES.
  * @retval AUDIO_OK if correct communication, else wrong communication
  */
uint8_t BSP_AUDIO_OUT_Play(uint16_t* pBuffer, uint32_t Size)
{    
    //printf("BSP_AUDIO_OUT_Play\n");
    
    if (audio_drv->Play(AUDIO_I2C_ADDRESS) != 0)
    {
        return AUDIO_ERROR;
    }
    else
    {
        HAL_I2S_Transmit_DMA(&hI2S3, pBuffer, Size);
    
        return AUDIO_OK;
    }
}


/**
  * @brief  Sends n-Bytes on the I2S interface.
  * @param  pData: Pointer to data address 
  * @param  Size: Number of data to be written.
  */
void BSP_AUDIO_OUT_ChangeBuffer(uint16_t *pData, uint16_t Size)
{
    HAL_I2S_Transmit_DMA(&hI2S3, pData, Size); 
}


/**
  * @brief   Pauses the audio file stream. 
  *          In case of using DMA, the DMA Pause feature is used.
  * WARNING: When calling BSP_AUDIO_OUT_Pause() function for pause, only
  *          BSP_AUDIO_OUT_Resume() function should be called for resume (use of BSP_AUDIO_OUT_Play() 
  *          function for resume could lead to unexpected behavior).
  * @retval  AUDIO_OK if correct communication, else wrong communication
  */
uint8_t BSP_AUDIO_OUT_Pause(void)
{    
  /* Call the Audio Codec Pause/Resume function */
  if (audio_drv->Pause(AUDIO_I2C_ADDRESS) != 0)
  {
    return AUDIO_ERROR;
  }
  else
  {
    /* Call the Media layer pause function */
    HAL_I2S_DMAPause(&hI2S3);
    
    /* Return AUDIO_OK when all operations are correctly done */
    return AUDIO_OK;
  }
}


/**
  * @brief   Resumes the audio file stream.  
  * WARNING: When calling BSP_AUDIO_OUT_Pause() function for pause, only
  *          BSP_AUDIO_OUT_Resume() function should be called for resume (use of BSP_AUDIO_OUT_Play() 
  *          function for resume could lead to unexpected behavior).
  * @retval  AUDIO_OK if correct communication, else wrong communication
  */
uint8_t BSP_AUDIO_OUT_Resume(void)
{    
  /* Call the Audio Codec Pause/Resume function */
  if (audio_drv->Resume(AUDIO_I2C_ADDRESS) != 0)
  {
    return AUDIO_ERROR;
  }
  else
  {
    /* Call the Media layer pause/resume function */
    HAL_I2S_DMAResume(&hI2S3);
    
    /* Return AUDIO_OK when all operations are correctly done */
    return AUDIO_OK;
  }
}


/**
  * @brief  Stops audio playing and Power down the Audio Codec. 
  * @param  Option: could be one of the following parameters 
  *           - CODEC_PDWN_SW: for software power off (by writing registers). 
  *                            Then no need to reconfigure the Codec after power on.
  *           - CODEC_PDWN_HW: completely shut down the codec (physically). 
  *                            Then need to reconfigure the Codec after power on.  
  * @retval AUDIO_OK if correct communication, else wrong communication
  */
uint8_t BSP_AUDIO_OUT_Stop(uint32_t Option)
{
  /* Call the Media layer stop function */
  HAL_I2S_DMAStop(&hI2S3);
  
  /* Call Audio Codec Stop function */
  if (audio_drv->Stop(AUDIO_I2C_ADDRESS, Option) != 0)
  {
    return AUDIO_ERROR;
  }
  else
  {
    if (Option == CODEC_PDWN_HW)
    { 
      /* Wait at least 1ms */
      HAL_Delay(1);
      
      /* Reset the pin */
        HAL_GPIO_WritePin(AUDIO_RESET_GPIO, AUDIO_RESET_PIN, GPIO_PIN_RESET);
    }
    
    /* Return AUDIO_OK when all operations are correctly done */
    return AUDIO_OK;
  }
}


/**
  * @brief  Controls the current audio volume level. 
  * @param  Volume: Volume level to be set in percentage from 0% to 100% (0 for 
  *         Mute and 100 for Max volume level).
  * @retval AUDIO_OK if correct communication, else wrong communication
  */
uint8_t BSP_AUDIO_OUT_SetVolume(uint8_t Volume)
{
  /* Call the codec volume control function with converted volume value */
  if (audio_drv->SetVolume(AUDIO_I2C_ADDRESS, Volume) != 0)
  {
    return AUDIO_ERROR;
  }
  else
  {
    /* Return AUDIO_OK when all operations are correctly done */
    return AUDIO_OK;
  }
}


/**
  * @brief  Enables or disables the MUTE mode by software 
  * @param  Cmd: could be AUDIO_MUTE_ON to mute sound or AUDIO_MUTE_OFF to 
  *         unmute the codec and restore previous volume level.
  * @retval AUDIO_OK if correct communication, else wrong communication
  */
uint8_t BSP_AUDIO_OUT_SetMute(uint32_t Cmd)
{ 
  /* Call the Codec Mute function */
  if (audio_drv->SetMute(AUDIO_I2C_ADDRESS, Cmd) != 0)
  {
    return AUDIO_ERROR;
  }
  else
  {
    /* Return AUDIO_OK when all operations are correctly done */
    return AUDIO_OK;
  }
}


/**
  * @brief  Switch dynamically (while audio file is played) the output target 
  *         (speaker or headphone).
  * @note   This function modifies a global variable of the audio codec driver: OutputDev.
  * @param  Output: specifies the audio output target: OUTPUT_DEVICE_SPEAKER,
  *         OUTPUT_DEVICE_HEADPHONE, OUTPUT_DEVICE_BOTH or OUTPUT_DEVICE_AUTO 
  * @retval AUDIO_OK if correct communication, else wrong communication
  */
uint8_t BSP_AUDIO_OUT_SetOutputMode(uint8_t Output)
{
  /* Call the Codec output Device function */
  if (audio_drv->SetOutputMode(AUDIO_I2C_ADDRESS, Output) != 0)
  {
    return AUDIO_ERROR;
  }
  else
  {
    /* Return AUDIO_OK when all operations are correctly done */
    return AUDIO_OK;
  }
}


/**
  * @brief  Updates the audio frequency.
  * @param  AudioFreq: Audio frequency used to play the audio stream.
  * @retval AUDIO_OK if correct communication, else wrong communication
  */
void BSP_AUDIO_OUT_SetFrequency(uint32_t AudioFreq)
{
    RCC_PeriphCLKInitTypeDef RCC_ExCLKInitStruct;
    uint8_t index = 0, freqindex = 0xFF;

    for (index = 0; index < 8; index++)
    {
        if (I2SFreq[index] == AudioFreq)
        {
          freqindex = index;
        }
    }

    HAL_RCCEx_GetPeriphCLKConfig(&RCC_ExCLKInitStruct);

    if (freqindex != 0xFF)
    {
        /* I2S clock config 
        PLLI2S_VCO = f(VCO clock) = f(PLLI2S clock input) � (PLLI2SN/PLLM)
        I2SCLK = f(PLLI2S clock output) = f(VCO clock) / PLLI2SR */
        RCC_ExCLKInitStruct.PeriphClockSelection = RCC_PERIPHCLK_I2S;
        RCC_ExCLKInitStruct.PLLI2S.PLLI2SN = I2SPLLN[freqindex];
        RCC_ExCLKInitStruct.PLLI2S.PLLI2SR = I2SPLLR[freqindex];
        HAL_RCCEx_PeriphCLKConfig(&RCC_ExCLKInitStruct);     
    } 
    else /* Default PLL I2S configuration */
    {
        /* I2S clock config 
        PLLI2S_VCO = f(VCO clock) = f(PLLI2S clock input) � (PLLI2SN/PLLM)
        I2SCLK = f(PLLI2S clock output) = f(VCO clock) / PLLI2SR */
        RCC_ExCLKInitStruct.PeriphClockSelection = RCC_PERIPHCLK_I2S;
        RCC_ExCLKInitStruct.PLLI2S.PLLI2SN = 258;
        RCC_ExCLKInitStruct.PLLI2S.PLLI2SR = 1;      
        HAL_RCCEx_PeriphCLKConfig(&RCC_ExCLKInitStruct); 
    }

    I2Sx_Init();
}


/**
  * @brief Rx Transfer completed callbacks
  * @param hI2S: I2S handle
  */
void HAL_I2S_RxCpltCallback(I2S_HandleTypeDef *hI2S)
{
#ifdef ENABLE_MIC
    I2S2_DMA_Complete_CallBack();  
#endif    
}


/**
  * @brief Rx Transfer Half completed callbacks
  * @param hI2S: I2S handle
  */
void HAL_I2S_RxHalfCpltCallback(I2S_HandleTypeDef *hI2S)
{
#ifdef ENABLE_MIC
    I2S2_DMA_Complete_CallBack(); 
#endif
}

/**
  * @brief Tx Transfer completed callbacks
  * @param hI2S: I2S handle
  */
void HAL_I2S_TxCpltCallback(I2S_HandleTypeDef *hI2S)
{
    I2S3_DMA_Complete_CallBack();       
}


/**
  * @brief Tx Transfer Half completed callbacks
  * @param hI2S: I2S handle
  */
void HAL_I2S_TxHalfCpltCallback(I2S_HandleTypeDef *hI2S)
{
    I2S3_DMA_Complete_CallBack();   
}


/**
  * @brief  I2S error callbacks.
  * @param  hI2S: I2S handle
  */
void HAL_I2S_ErrorCallback(I2S_HandleTypeDef *hI2S) 
{
    BSP_AUDIO_OUT_Error_CallBack();
}


/**
  * @brief  Manages the DMA FIFO error event.
  */
__weak void BSP_AUDIO_OUT_Error_CallBack(void)
{
    
}



/**
  * @brief  Initializes the Audio Codec audio interface (I2S).
  */
static void I2Sx_Init(void)
{        
    HAL_I2S_StateTypeDef nI2SState;
    
    nI2SState = HAL_I2S_GetState(&hI2S2);
    
    if (nI2SState == HAL_I2S_STATE_RESET)
    {        
        HAL_I2S_Init(&hI2S2); 
    }
    
    nI2SState = HAL_I2S_GetState(&hI2S3);
    
    if (nI2SState == HAL_I2S_STATE_RESET)
    {        
        HAL_I2S_Init(&hI2S3); 
    }
}


/**
  * @brief  Resets the audio codec. It restores the default configuration of the 
  *         codec (this function shall be called before initializing the codec).
  */
static void CODEC_Reset(void)
{
    /* Power Down the codec */
    HAL_GPIO_WritePin(GPIOD, GPIO_PIN_4, GPIO_PIN_RESET);

    HAL_Delay(CODEC_RESET_DELAY); 

    /* Power on the codec */
    HAL_GPIO_WritePin(GPIOD, GPIO_PIN_4, GPIO_PIN_SET);

    HAL_Delay(CODEC_RESET_DELAY); 
}

 
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
