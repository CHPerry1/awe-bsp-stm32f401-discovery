/*******************************************************************************
*
*               Audio Framework
*               ---------------
*
********************************************************************************
*     AWE_Wrapper.h
********************************************************************************
*
*     Description:  AudioWeaver Wrapper Header File
*
*     Copyright:    (c) DSP Concepts, Inc, 2007 - 2016
*                   1800 Wyatt Drive, Suite 14
*                   Sunnyvale, CA 95054
*
*******************************************************************************/
  
 #ifndef __PLATFORM_H__
 #define __PLATFORM_H__
 
#include "usb_device.h" 
#include "usbd_audio.h"
#include "PlatformAPI.h"
#include "AWEFlash.h"

#include "dspc_asrc_48000_to_48000_32m_080db.h"
 
#define INPUT_CHANNEL_COUNT 2

#define OUTPUT_CHANNEL_COUNT 2

#define STEREO_CHANNEL_COUNT 2

#define MONO_CHANNEL_COUNT 1

#define INPUT_BLOCKSIZE 48
#define OUTPUT_BLOCKSIZE 32

#define AWE_FRAME_SIZE 32

#define AWE_OUTPUT_BUFFER_SIZE_IN_SAMPLES (AWE_FRAME_SIZE * OUTPUT_CHANNEL_COUNT)

#define USB_BUFFER_SIZE_IN_SAMPLES 48

#define STEREO_ASRC_BUFFER_SIZE_IN_SAMPLES DSPC_ASRC_BUFFER_LEN(ASRC_FS_IN_48000, ASRC_FS_OUT_48000, ASRC_PHASELEN_28, INPUT_BLOCKSIZE, OUTPUT_BLOCKSIZE, STEREO_CHANNEL_COUNT)

// Double buffer of 32 stereo samples
#define I2S_OUT_BUFFER_SIZE 128

// 32 stereo samples
#define NEW_CODEC_SAMPLES 64

#define I2S_IN_BUFFER_SIZE 96

#define HID_EP_BUFFER_SIZE 64

#define AWE_COMMAND_BUFFER_LEN 264

// 48000/1000 * 64/8 = 384 (384 / 8 = 48)
#define PDM_BUFFER_SIZE 384

#define PCM_SAMPLE_SIZE_IN_BYTES 2
#define PCM_SAMPLE_SIZE_IN_BITS 16

#define HID_REPORT_PACKET_SIZE 56
#define HID_REPORT_DATA_SIZE 52
#define DATA_SIZE_IN_DWORDS 13

#define MAX_PUMP_COUNT 100

#define DEFAULT_AUDIO_IN_VOLUME 64

#define SMOOTHING_COEFF 894598

extern INT16 I2SBuffer[I2S_OUT_BUFFER_SIZE];
extern volatile INT32 nI2SBufferNdx;

extern UINT8 HIDInBuff[HID_EP_BUFFER_SIZE];
extern UINT8 HIDOutBuff[HID_EP_BUFFER_SIZE];

extern UINT32 s_PacketBuffer[];

extern INT16 USBBuffer[AUDIO_BUFFER_SIZE_IN_SAMPLES];
extern volatile UINT32 nUSBReadBufferNdx;
extern volatile UINT32 nUSBWriteBufferNdx;

extern volatile BOOL g_bUSBPacketReceived;
extern volatile BOOL g_bReadyToSend;
extern AWE_FW_SLOW_ANY_DATA volatile INT32 s_AudioRunning;

extern volatile INT32 g_nReceivedPacketCount;
extern volatile BOOL g_bBlinkLED4ForBoardAlive;

extern volatile UINT32 g_nPumpCount;

extern USBD_HandleTypeDef hUsbDeviceFS;
extern I2S_HandleTypeDef hI2S2;
extern I2S_HandleTypeDef hI2S3;

extern DSPC_ASRC USB_ASRC;

extern DWORD g_target_control_flags;

extern void BoardSetup(void);

#define ProcessWriteASRC_IRQHandler EXTI1_IRQHandler
#define ProcessWriteASRC_IRQ EXTI1_IRQn

#define AudioWeaverPump_IRQHandler1 EXTI2_IRQHandler
#define AudioWeaverPump_IRQ1 EXTI2_IRQn

#define ProcessUSBMsg_IRQHandler EXTI3_IRQHandler
#define ProcessUSBMsg_IRQ EXTI3_IRQn

#define AudioWeaverPump_IRQHandler2 EXTI4_IRQHandler
#define AudioWeaverPump_IRQ2 EXTI4_IRQn

void CoreInit(void);
void BoardInit(void);
void AudioInit(void);
void USBMsgInit(void);
void CheckForUSBPacketReady(void);
void USBSendReply(void);

INT32 awe_pltAudioStart(void);

INT32 awe_pltAudioStop(void);

void AWEProcessing(fract16 * pUSBSamples, fract16 * pProcessedSamples);

void I2S2_DMA_Complete_CallBack(void);

void I2S3_DMA_Complete_CallBack(void);

void awe_pltGPIOSetPin(UINT32 nPinNo, UINT32 nValue);
void awe_pltGPIOTogglePin(UINT32 nPinNo);

void awe_vecScaleSmoothFract32(
		     fract32 *              src,
		     int                    srcInc,
		     fract32 *              dst,
		     int                    dstInc,
		     fract32 * VEC_RESTRICT currentGainFract,
		     fract32                targetGainFract,
		     int                    postShift,
		     fract32                smoothingCoeff,
		     int                    blockSize);

#endif
