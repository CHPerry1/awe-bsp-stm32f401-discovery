/*******************************************************************************
*
*               Audio Framework
*               ---------------
*
********************************************************************************
*     TargetInfo.h
********************************************************************************
*
*     Description:  All target specific information
*
*     Copyright:    DSP Concepts, LLC, 2007 - 2016
*                   1800 Wyatt Drive, Suite 14
*                   Sunnyvale, CA 95054
*
*******************************************************************************/


#ifndef _TARGETINFO_H
#define _TARGETINFO_H

#include "Framework.h"

/* ----------------------------------------------------------------------
** Default audio I/O mode
** ------------------------------------------------------------------- */

#define DEFAULT_AUDIO_IO_MODE AudioIOMode_ADC


/* ----------------------------------------------------------------------
** Specifies the sizes of each of the heaps on the target
** ------------------------------------------------------------------- */

#define MASTER_HEAP_SIZE		(1024*1)
#define SLOW_HEAP_SIZE			(1024*1)
#define FASTB_HEAP_SIZE			(1024*1)

#define MAX_COMMAND_BUFFER_LEN  272

extern const ModClassModule awe_modAGCAttackReleaseClass;
extern const ModClassModule awe_modAGCCompressorCoreClass;
extern const ModClassModule awe_modAGCCoreClass;
extern const ModClassModule awe_modAGCLimiterCoreClass;
extern const ModClassModule awe_modAGCMultiplierClass;
extern const ModClassModule awe_modAGCNoiseGateCoreClass;
extern const ModClassModule awe_modAGCVariableAttackReleaseClass;
extern const ModClassModule awe_modAbsClass;
extern const ModClassModule awe_modAcosClass;
extern const ModClassModule awe_modAdderClass;
extern const ModClassModule awe_modAdderInt32Class;
extern const ModClassModule awe_modAllpassDelayClass;
extern const ModClassModule awe_modAllpassDelay16Class;
extern const ModClassModule awe_modAllpassDelayciClass;
extern const ModClassModule awe_modAllpassDelayiClass;
extern const ModClassModule awe_modApplyPresetClass;
extern const ModClassModule awe_modAsinClass;
extern const ModClassModule awe_modAtanClass;
extern const ModClassModule awe_modAtan2Class;
extern const ModClassModule awe_modAttackDecaySustainReleaseClass;
extern const ModClassModule awe_modAudioWeightingClass;
extern const ModClassModule awe_modAveragerClass;
extern const ModClassModule awe_modBalanceClass;
extern const ModClassModule awe_modBiquadClass;
extern const ModClassModule awe_modBiquadCascadeClass;
extern const ModClassModule awe_modBiquadCascadeFract32Class;
extern const ModClassModule awe_modBiquadCascadeHPClass;
extern const ModClassModule awe_modBiquadLoadingClass;
extern const ModClassModule awe_modBiquadLoadingFract32Class;
extern const ModClassModule awe_modBiquadNCascadeClass;
extern const ModClassModule awe_modBiquadSmoothedClass;
extern const ModClassModule awe_modBiquadSmoothedHPClass;
extern const ModClassModule awe_modBitsToIntClass;
extern const ModClassModule awe_modBlockConcatenateClass;
extern const ModClassModule awe_modBlockCounterClass;
extern const ModClassModule awe_modBlockDelayClass;
extern const ModClassModule awe_modBlockExtractClass;
extern const ModClassModule awe_modBlockFlipClass;
extern const ModClassModule awe_modBlockMedianClass;
extern const ModClassModule awe_modBlockStatisticsClass;
extern const ModClassModule awe_modBooleanInvertClass;
extern const ModClassModule awe_modBooleanSinkClass;
extern const ModClassModule awe_modBooleanSourceClass;
extern const ModClassModule awe_modButterworthFilterClass;
extern const ModClassModule awe_modButterworthFilterHPClass;
extern const ModClassModule awe_modCeilClass;
extern const ModClassModule awe_modCfftClass;
extern const ModClassModule awe_modClipAsymClass;
extern const ModClassModule awe_modClipIndicatorClass;
extern const ModClassModule awe_modCoeffTableClass;
extern const ModClassModule awe_modComplexAngleClass;
extern const ModClassModule awe_modComplexConjugateClass;
extern const ModClassModule awe_modComplexMagnitudeClass;
extern const ModClassModule awe_modComplexMagnitudeSquaredClass;
extern const ModClassModule awe_modComplexModulateClass;
extern const ModClassModule awe_modComplexMultiplierClass;
extern const ModClassModule awe_modComplexToPolarClass;
extern const ModClassModule awe_modComplexToRealImagClass;
extern const ModClassModule awe_modConsecutiveCountIntClass;
extern const ModClassModule awe_modConvolveClass;
extern const ModClassModule awe_modCopierClass;
extern const ModClassModule awe_modCorrelateClass;
extern const ModClassModule awe_modCosClass;
extern const ModClassModule awe_modCoshClass;
extern const ModClassModule awe_modCounterClass;
extern const ModClassModule awe_modCrossFaderClass;
extern const ModClassModule awe_modCycleBurnerClass;
extern const ModClassModule awe_modDCSourceClass;
extern const ModClassModule awe_modDCSourceControlClass;
extern const ModClassModule awe_modDCSourceIntClass;
extern const ModClassModule awe_modDCSourceV2Class;
extern const ModClassModule awe_modDateTimeClass;
extern const ModClassModule awe_modDb10Class;
extern const ModClassModule awe_modDb10ApproxClass;
extern const ModClassModule awe_modDb20Class;
extern const ModClassModule awe_modDb20ApproxClass;
extern const ModClassModule awe_modDeMultiplexorClass;
extern const ModClassModule awe_modDeadBandClass;
extern const ModClassModule awe_modDebounceClass;
extern const ModClassModule awe_modDeinterleaveClass;
extern const ModClassModule awe_modDelayClass;
extern const ModClassModule awe_modDelay16Class;
extern const ModClassModule awe_modDelayInterpClass;
extern const ModClassModule awe_modDelayMsecClass;
extern const ModClassModule awe_modDelayNTapClass;
extern const ModClassModule awe_modDelayReaderClass;
extern const ModClassModule awe_modDelayStateWriterClass;
extern const ModClassModule awe_modDelayStateWriter16Class;
extern const ModClassModule awe_modDelayciClass;
extern const ModClassModule awe_modDelayiClass;
extern const ModClassModule awe_modDerivativeClass;
extern const ModClassModule awe_modDivideClass;
extern const ModClassModule awe_modDownsamplerClass;
extern const ModClassModule awe_modDownwardExpanderCoreClass;
extern const ModClassModule awe_modDuckerClass;
extern const ModClassModule awe_modEmphasisFilterClass;
extern const ModClassModule awe_modExpClass;
extern const ModClassModule awe_modFIRClass;
extern const ModClassModule awe_modFIRDecimatorClass;
extern const ModClassModule awe_modFIRFract32Class;
extern const ModClassModule awe_modFIRInterpolatorClass;
extern const ModClassModule awe_modFIRLoadingClass;
extern const ModClassModule awe_modFIRLoadingFract32Class;
extern const ModClassModule awe_modFIRSparseClass;
extern const ModClassModule awe_modFIRSparseReaderClass;
extern const ModClassModule awe_modFOFControlClass;
extern const ModClassModule awe_modFftClass;
extern const ModClassModule awe_modFifoInClass;
extern const ModClassModule awe_modFifoOutClass;
extern const ModClassModule awe_modFloatToFract32Class;
extern const ModClassModule awe_modFloatToIntClass;
extern const ModClassModule awe_modFloorClass;
extern const ModClassModule awe_modFmodClass;
extern const ModClassModule awe_modFract32ToFloatClass;
extern const ModClassModule awe_modFract32ToIntClass;
extern const ModClassModule awe_modFrexpClass;
extern const ModClassModule awe_modGPIOClass;
extern const ModClassModule awe_modGraphicEQBandClass;
extern const ModClassModule awe_modGraphicEQBandHPClass;
extern const ModClassModule awe_modHistogramClass;
extern const ModClassModule awe_modHysteresisClass;
extern const ModClassModule awe_modIfftClass;
extern const ModClassModule awe_modImpulseMsecSourceClass;
extern const ModClassModule awe_modImpulseSourceClass;
extern const ModClassModule awe_modIntToBitsClass;
extern const ModClassModule awe_modIntToFloatClass;
extern const ModClassModule awe_modIntToFract32Class;
extern const ModClassModule awe_modIntegralClass;
extern const ModClassModule awe_modInterleaveClass;
extern const ModClassModule awe_modInterleavedSinkClass;
extern const ModClassModule awe_modInvertClass;
extern const ModClassModule awe_modLMSClass;
extern const ModClassModule awe_modLPF1ControlClass;
extern const ModClassModule awe_modLdexpClass;
extern const ModClassModule awe_modLogClass;
extern const ModClassModule awe_modLog10Class;
extern const ModClassModule awe_modLog2Class;
extern const ModClassModule awe_modLogicAllClass;
extern const ModClassModule awe_modLogicAnyClass;
extern const ModClassModule awe_modLogicBinaryOpClass;
extern const ModClassModule awe_modLogicCompareClass;
extern const ModClassModule awe_modLogicCompareConstClass;
extern const ModClassModule awe_modLogicCompareConstInt32Class;
extern const ModClassModule awe_modLogicCompareInt32Class;
extern const ModClassModule awe_modMathExceptionClass;
extern const ModClassModule awe_modMaxAbsClass;
extern const ModClassModule awe_modMeasurementClass;
extern const ModClassModule awe_modMeterClass;
extern const ModClassModule awe_modMixerClass;
extern const ModClassModule awe_modMixerDenseClass;
extern const ModClassModule awe_modMixerSmoothedClass;
extern const ModClassModule awe_modMixerSmoothedV2bClass;
extern const ModClassModule awe_modMixerSparseSmoothedClass;
extern const ModClassModule awe_modMixerV3Class;
extern const ModClassModule awe_modModfClass;
extern const ModClassModule awe_modMultiplexorClass;
extern const ModClassModule awe_modMultiplexorControlClass;
extern const ModClassModule awe_modMultiplexorFadeClass;
extern const ModClassModule awe_modMultiplexorSmoothedClass;
extern const ModClassModule awe_modMultiplexorV2Class;
extern const ModClassModule awe_modMultiplierClass;
extern const ModClassModule awe_modMultiplierV2Class;
extern const ModClassModule awe_modMuteNSmoothedClass;
extern const ModClassModule awe_modMuteSmoothedClass;
extern const ModClassModule awe_modMuteSyncClass;
extern const ModClassModule awe_modMuteUnmuteClass;
extern const ModClassModule awe_modNullSinkClass;
extern const ModClassModule awe_modOscillatorClass;
extern const ModClassModule awe_modOverlapAddClass;
extern const ModClassModule awe_modParamGetClass;
extern const ModClassModule awe_modParamSetClass;
extern const ModClassModule awe_modPeriodicFunctionGenClass;
extern const ModClassModule awe_modPeriodicSourceClass;
extern const ModClassModule awe_modPeriodicStreamingClass;
extern const ModClassModule awe_modPolarToComplexClass;
extern const ModClassModule awe_modPolynomialClass;
extern const ModClassModule awe_modPowClass;
extern const ModClassModule awe_modPow10toXClass;
extern const ModClassModule awe_modProbeClass;
extern const ModClassModule awe_modPulseGenClass;
extern const ModClassModule awe_modRMSClass;
extern const ModClassModule awe_modRandClass;
extern const ModClassModule awe_modRandiClass;
extern const ModClassModule awe_modRealImagToComplexClass;
extern const ModClassModule awe_modRebufferClass;
extern const ModClassModule awe_modReciprocalClass;
extern const ModClassModule awe_modRemainderClass;
extern const ModClassModule awe_modRepWinOverlapClass;
extern const ModClassModule awe_modRoundClass;
extern const ModClassModule awe_modRouterClass;
extern const ModClassModule awe_modRouterSmoothedClass;
extern const ModClassModule awe_modRunningMinMaxClass;
extern const ModClassModule awe_modRunningStatisticsClass;
extern const ModClassModule awe_modSMixer2x1Class;
extern const ModClassModule awe_modSOFCascadeHPClass;
extern const ModClassModule awe_modSOFControlClass;
extern const ModClassModule awe_modSOFControlHPClass;
extern const ModClassModule awe_modSafetyClipClass;
extern const ModClassModule awe_modSampleAndHoldClass;
extern const ModClassModule awe_modSampleAndHoldInt32Class;
extern const ModClassModule awe_modSampleMultiplexorControlClass;
extern const ModClassModule awe_modSampleRateClass;
extern const ModClassModule awe_modSampleStatisticsClass;
extern const ModClassModule awe_modSawtoothClass;
extern const ModClassModule awe_modSbAttackReleaseClass;
extern const ModClassModule awe_modSbComplexFIRClass;
extern const ModClassModule awe_modSbComplexFIRvlClass;
extern const ModClassModule awe_modSbDerivativeClass;
extern const ModClassModule awe_modSbNLMSClass;
extern const ModClassModule awe_modSbRMSClass;
extern const ModClassModule awe_modSbSOFClass;
extern const ModClassModule awe_modSbSmoothClass;
extern const ModClassModule awe_modSbSplitterClass;
extern const ModClassModule awe_modScaleOffsetClass;
extern const ModClassModule awe_modScaleOffsetInt32Class;
extern const ModClassModule awe_modScalerClass;
extern const ModClassModule awe_modScalerControlClass;
extern const ModClassModule awe_modScalerDBClass;
extern const ModClassModule awe_modScalerDBControlClass;
extern const ModClassModule awe_modScalerDBSmoothedClass;
extern const ModClassModule awe_modScalerNClass;
extern const ModClassModule awe_modScalerNDBSmoothedClass;
extern const ModClassModule awe_modScalerNSmoothedClass;
extern const ModClassModule awe_modScalerNV2Class;
extern const ModClassModule awe_modScalerSmoothedClass;
extern const ModClassModule awe_modScalerV2Class;
extern const ModClassModule awe_modSecondOrderFilterHPClass;
extern const ModClassModule awe_modSecondOrderFilterSmoothedClass;
extern const ModClassModule awe_modSecondOrderFilterSmoothedCascadeClass;
extern const ModClassModule awe_modSetWirePropertiesClass;
extern const ModClassModule awe_modShiftSamplesClass;
extern const ModClassModule awe_modSignClass;
extern const ModClassModule awe_modSinClass;
extern const ModClassModule awe_modSineGenClass;
extern const ModClassModule awe_modSineSmoothedGenClass;
extern const ModClassModule awe_modSinhClass;
extern const ModClassModule awe_modSinkClass;
extern const ModClassModule awe_modSinkIntClass;
extern const ModClassModule awe_modSlewRateLimiterClass;
extern const ModClassModule awe_modSoftClipClass;
extern const ModClassModule awe_modSoloMuteClass;
extern const ModClassModule awe_modSourceClass;
extern const ModClassModule awe_modSourceFract32Class;
extern const ModClassModule awe_modSourceIntClass;
extern const ModClassModule awe_modSqrtClass;
extern const ModClassModule awe_modSquareClass;
extern const ModClassModule awe_modSquareAddClass;
extern const ModClassModule awe_modStatusSetClass;
extern const ModClassModule awe_modSubblockStatisticsClass;
extern const ModClassModule awe_modSubtractClass;
extern const ModClassModule awe_modSubtractInt32Class;
extern const ModClassModule awe_modSumDiffClass;
extern const ModClassModule awe_modSumDiffInt32Class;
extern const ModClassModule awe_modTableInterpClass;
extern const ModClassModule awe_modTableInterp2dClass;
extern const ModClassModule awe_modTableInterpRuntimeClass;
extern const ModClassModule awe_modTableLookupClass;
extern const ModClassModule awe_modTableLookupIntFloatClass;
extern const ModClassModule awe_modTableLookupIntIntClass;
extern const ModClassModule awe_modTanClass;
extern const ModClassModule awe_modTanhClass;
extern const ModClassModule awe_modThreeBandToneControlClass;
extern const ModClassModule awe_modToggleButtonClass;
extern const ModClassModule awe_modTriggeredSinkClass;
extern const ModClassModule awe_modTwoPieceInterpClass;
extern const ModClassModule awe_modTwoPieceInterpV2Class;
extern const ModClassModule awe_modTypeConversionClass;
extern const ModClassModule awe_modUndb10Class;
extern const ModClassModule awe_modUndb10ApproxClass;
extern const ModClassModule awe_modUndb20Class;
extern const ModClassModule awe_modUndb20ApproxClass;
extern const ModClassModule awe_modUnwrapClass;
extern const ModClassModule awe_modUpdateSampleRateClass;
extern const ModClassModule awe_modUpsamplerClass;
extern const ModClassModule awe_modVolumeControlClass;
extern const ModClassModule awe_modVolumeControlHPClass;
extern const ModClassModule awe_modWetDryClass;
extern const ModClassModule awe_modWhiteNoiseClass;
extern const ModClassModule awe_modWindowClass;
extern const ModClassModule awe_modWindowAliasClass;
extern const ModClassModule awe_modWindowOverlapClass;
extern const ModClassModule awe_modWithinRangeClass;
extern const ModClassModule awe_modZeroCrossingDetectorClass;
extern const ModClassModule awe_modZeroPaddingClass;
extern const ModClassModule awe_modZeroSourceClass;

/*
#define LISTOFCLASSOBJECTS \
&awe_modAGCAttackReleaseClass, \
&awe_modAGCCompressorCoreClass, \
&awe_modAGCCoreClass, \
&awe_modAGCLimiterCoreClass, \
&awe_modAGCMultiplierClass, \
&awe_modAGCNoiseGateCoreClass, \
&awe_modAGCVariableAttackReleaseClass, \
&awe_modAbsClass, \
&awe_modAcosClass, \
&awe_modAdderClass, \
&awe_modAdderInt32Class, \
&awe_modAllpassDelayClass, \
&awe_modAllpassDelay16Class, \
&awe_modAllpassDelayciClass, \
&awe_modAllpassDelayiClass, \
&awe_modApplyPresetClass, \
&awe_modAsinClass, \
&awe_modAtanClass, \
&awe_modAtan2Class, \
&awe_modAttackDecaySustainReleaseClass, \
&awe_modAudioWeightingClass, \
&awe_modAveragerClass, \
&awe_modBalanceClass, \
&awe_modBiquadClass, \
&awe_modBiquadCascadeClass, \
&awe_modBiquadCascadeFract32Class, \
&awe_modBiquadCascadeHPClass, \
&awe_modBiquadLoadingClass, \
&awe_modBiquadLoadingFract32Class, \
&awe_modBiquadNCascadeClass, \
&awe_modBiquadSmoothedClass, \
&awe_modBiquadSmoothedHPClass, \
&awe_modBitsToIntClass, \
&awe_modBlockConcatenateClass, \
&awe_modBlockCounterClass, \
&awe_modBlockDelayClass, \
&awe_modBlockExtractClass, \
&awe_modBlockFlipClass, \
&awe_modBlockMedianClass, \
&awe_modBlockStatisticsClass, \
&awe_modBooleanInvertClass, \
&awe_modBooleanSinkClass, \
&awe_modBooleanSourceClass, \
&awe_modButterworthFilterClass, \
&awe_modButterworthFilterHPClass, \
&awe_modCeilClass, \
&awe_modCfftClass, \
&awe_modClipAsymClass, \
&awe_modClipIndicatorClass, \
&awe_modCoeffTableClass, \
&awe_modComplexAngleClass, \
&awe_modComplexConjugateClass, \
&awe_modComplexMagnitudeClass, \
&awe_modComplexMagnitudeSquaredClass, \
&awe_modComplexModulateClass, \
&awe_modComplexMultiplierClass, \
&awe_modComplexToPolarClass, \
&awe_modComplexToRealImagClass, \
&awe_modConsecutiveCountIntClass, \
&awe_modConvolveClass, \
*/

#define LISTOFCLASSOBJECTS \
&awe_modCopierClass, \
&awe_modCorrelateClass, \
&awe_modCosClass, \
&awe_modCoshClass, \
&awe_modCounterClass, \
&awe_modCrossFaderClass, \
&awe_modCycleBurnerClass, \
&awe_modDCSourceClass, \
&awe_modDCSourceControlClass, \
&awe_modDCSourceIntClass, \
&awe_modDCSourceV2Class, \
&awe_modDateTimeClass, \
&awe_modDb10Class, \
&awe_modDb10ApproxClass, \
&awe_modDb20Class, \
&awe_modDb20ApproxClass, \
&awe_modDeMultiplexorClass, \
&awe_modDeadBandClass, \
&awe_modDebounceClass, \
&awe_modDeinterleaveClass, \
&awe_modDelayClass, \
&awe_modDelay16Class, \
&awe_modDelayInterpClass, \
&awe_modDelayMsecClass, \
&awe_modDelayNTapClass, \
&awe_modDelayReaderClass, \
&awe_modDelayStateWriterClass, \
&awe_modDelayStateWriter16Class, \
&awe_modDelayciClass, \
&awe_modDelayiClass, \
&awe_modDerivativeClass, \
&awe_modDivideClass, \
&awe_modDownsamplerClass, \
&awe_modDownwardExpanderCoreClass, \
&awe_modDuckerClass, \
&awe_modEmphasisFilterClass, \
&awe_modExpClass, \
&awe_modFIRClass, \
&awe_modFIRDecimatorClass, \
&awe_modFIRFract32Class, \
&awe_modFIRInterpolatorClass, \
&awe_modFIRLoadingClass, \
&awe_modFIRLoadingFract32Class, \
&awe_modFIRSparseClass, \
&awe_modFIRSparseReaderClass, \
&awe_modFOFControlClass, \
&awe_modFftClass, \
&awe_modFifoInClass, \
&awe_modFifoOutClass, \
&awe_modFloatToFract32Class, \
&awe_modFloatToIntClass, \
&awe_modFloorClass, \
&awe_modFmodClass, \
&awe_modFract32ToFloatClass, \
&awe_modFract32ToIntClass, \
&awe_modFrexpClass, \
&awe_modGPIOClass, \
&awe_modGraphicEQBandClass, \
&awe_modGraphicEQBandHPClass, \
&awe_modHistogramClass, \
&awe_modHysteresisClass, \
&awe_modIfftClass, \
&awe_modImpulseMsecSourceClass, \
&awe_modImpulseSourceClass, \
&awe_modIntToBitsClass, \
&awe_modIntToFloatClass, \
&awe_modIntToFract32Class, \
&awe_modIntegralClass, \
&awe_modInterleaveClass, \
&awe_modInterleavedSinkClass, \
&awe_modInvertClass, \
&awe_modLMSClass, \
&awe_modLPF1ControlClass, \
&awe_modLdexpClass, \
&awe_modLogClass, \
&awe_modLog10Class, \
&awe_modLog2Class, \
&awe_modLogicAllClass, \
&awe_modLogicAnyClass, \
&awe_modLogicBinaryOpClass, \
&awe_modLogicCompareClass, \
&awe_modLogicCompareConstClass, \
&awe_modLogicCompareConstInt32Class, \
&awe_modLogicCompareInt32Class, \
&awe_modMathExceptionClass, \
&awe_modMaxAbsClass, \
&awe_modMeasurementClass, \
&awe_modMeterClass, \
&awe_modMixerClass, \
&awe_modMixerDenseClass, \
&awe_modMixerSmoothedClass, \
&awe_modMixerSmoothedV2bClass, \
&awe_modMixerSparseSmoothedClass, \
&awe_modMixerV3Class, \
&awe_modModfClass, \
&awe_modMultiplexorClass, \
&awe_modMultiplexorControlClass, \
&awe_modMultiplexorFadeClass, \
&awe_modMultiplexorSmoothedClass, \
&awe_modMultiplexorV2Class, \
&awe_modMultiplierClass, \
&awe_modMultiplierV2Class, \
&awe_modMuteNSmoothedClass, \
&awe_modMuteSmoothedClass, \
&awe_modMuteSyncClass, \
&awe_modMuteUnmuteClass, \
&awe_modNullSinkClass, \
&awe_modOscillatorClass, \
&awe_modOverlapAddClass, \
&awe_modParamGetClass, \
&awe_modParamSetClass, \
&awe_modPeriodicFunctionGenClass, \
&awe_modPeriodicSourceClass, \
&awe_modPeriodicStreamingClass, \
&awe_modPolarToComplexClass, \
&awe_modPolynomialClass, \
&awe_modPowClass, \
&awe_modPow10toXClass, \
&awe_modProbeClass, \
&awe_modPulseGenClass, \
&awe_modRMSClass, \
&awe_modRandClass, \
&awe_modRandiClass, \
&awe_modRealImagToComplexClass, \
&awe_modRebufferClass, \
&awe_modReciprocalClass, \
&awe_modRemainderClass, \
&awe_modRepWinOverlapClass, \
&awe_modRoundClass, \
&awe_modRouterClass, \
&awe_modRouterSmoothedClass, \
&awe_modRunningMinMaxClass, \
&awe_modRunningStatisticsClass, \
&awe_modSMixer2x1Class, \
&awe_modSOFCascadeHPClass, \
&awe_modSOFControlClass, \
&awe_modSOFControlHPClass, \
&awe_modSafetyClipClass, \
&awe_modSampleAndHoldClass, \
&awe_modSampleAndHoldInt32Class, \
&awe_modSampleMultiplexorControlClass, \
&awe_modSampleRateClass, \
&awe_modSampleStatisticsClass, \
&awe_modSawtoothClass, \
&awe_modSbAttackReleaseClass, \
&awe_modSbComplexFIRClass, \
&awe_modSbComplexFIRvlClass, \
&awe_modSbDerivativeClass, \
&awe_modSbNLMSClass, \
&awe_modSbRMSClass, \
&awe_modSbSOFClass, \
&awe_modSbSmoothClass, \
&awe_modSbSplitterClass, \
&awe_modScaleOffsetClass, \
&awe_modScaleOffsetInt32Class, \
&awe_modScalerClass, \
&awe_modScalerControlClass, \
&awe_modScalerDBClass, \
&awe_modScalerDBControlClass, \
&awe_modScalerDBSmoothedClass, \
&awe_modScalerNClass, \
&awe_modScalerNDBSmoothedClass, \
&awe_modScalerNSmoothedClass, \
&awe_modScalerNV2Class, \
&awe_modScalerSmoothedClass, \
&awe_modScalerV2Class, \
&awe_modSecondOrderFilterHPClass, \
&awe_modSecondOrderFilterSmoothedClass, \
&awe_modSecondOrderFilterSmoothedCascadeClass, \
&awe_modSetWirePropertiesClass, \
&awe_modShiftSamplesClass, \
&awe_modSignClass, \
&awe_modSinClass, \
&awe_modSineGenClass, \
&awe_modSineSmoothedGenClass, \
&awe_modSinhClass, \
&awe_modSinkClass, \
&awe_modSinkIntClass, \
&awe_modSlewRateLimiterClass, \
&awe_modSoftClipClass, \
&awe_modSoloMuteClass, \
&awe_modSourceClass, \
&awe_modSourceFract32Class, \
&awe_modSourceIntClass, \
&awe_modSqrtClass, \
&awe_modSquareClass, \
&awe_modSquareAddClass, \
&awe_modStatusSetClass, \
&awe_modSubblockStatisticsClass, \
&awe_modSubtractClass, \
&awe_modSubtractInt32Class, \
&awe_modSumDiffClass, \
&awe_modSumDiffInt32Class

/*
&awe_modTableInterpClass, \
&awe_modTableInterp2dClass, \
&awe_modTableInterpRuntimeClass, \
&awe_modTableLookupClass, \
&awe_modTableLookupIntFloatClass, \
&awe_modTableLookupIntIntClass, \
&awe_modTanClass, \
&awe_modTanhClass, \
&awe_modThreeBandToneControlClass, \
&awe_modToggleButtonClass, \
&awe_modTriggeredSinkClass, \
&awe_modTwoPieceInterpClass, \
&awe_modTwoPieceInterpV2Class, \
&awe_modTypeConversionClass, \
&awe_modUndb10Class, \
&awe_modUndb10ApproxClass, \
&awe_modUndb20Class, \
&awe_modUndb20ApproxClass, \
&awe_modUnwrapClass, \
&awe_modUpdateSampleRateClass, \
&awe_modUpsamplerClass, \
&awe_modVolumeControlClass, \
&awe_modVolumeControlHPClass, \
&awe_modWetDryClass, \
&awe_modWhiteNoiseClass, \
&awe_modWindowClass, \
&awe_modWindowAliasClass, \
&awe_modWindowOverlapClass, \
&awe_modWithinRangeClass, \
&awe_modZeroCrossingDetectorClass, \
&awe_modZeroPaddingClass, \
&awe_modZeroSourceClass
*/

#endif	// _TARGETINFO_H

