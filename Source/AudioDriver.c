/*******************************************************************************
*
*               Audio Framework
*               ---------------
*
********************************************************************************
*     AudioDriver.c
********************************************************************************
*
*     Description:  AudioWeaver Audio Driver for STM Discovery board
*
*     DSP Concepts, Inc.
*     1800 Wyatt Drive, Suite 14
*     Sunnyvale, CA 95054
*
*     Copyright (c) 2007 - 2016 DSP Concepts, Inc. All rights reserved.
*******************************************************************************/
#include <string.h>

#include "usbd_audio.h"
#include "pdm_filter.h"
#include "Errors.h"
#include "TargetInfo.h"
#include "PlatformAPI.h"
#include "Platform.h"

//extern INT16 SineTable[48049];
//extern INT32 g_nTableElements;

#if defined ( __ICCARM__ ) /*!< IAR Compiler */
  #pragma data_alignment=4   
#endif
__ALIGN_BEGIN INT16 USBBuffer[AUDIO_BUFFER_SIZE_IN_SAMPLES] __ALIGN_END;

#if defined ( __ICCARM__ ) /*!< IAR Compiler */
  #pragma data_alignment=4   
#endif
__ALIGN_BEGIN INT16 I2SBuffer[I2S_OUT_BUFFER_SIZE] __ALIGN_END;
    
#if defined ( __ICCARM__ ) /*!< IAR Compiler */
  #pragma data_alignment=4   
#endif
__ALIGN_BEGIN fract16 USB_ASRCBuffer[STEREO_ASRC_BUFFER_SIZE_IN_SAMPLES] __ALIGN_END;

fract16 USB_ASRCSamples[OUTPUT_BLOCKSIZE * OUTPUT_CHANNEL_COUNT]; 
fract16 ZeroedSamples[OUTPUT_BLOCKSIZE * OUTPUT_CHANNEL_COUNT]; 

volatile INT32 g_nAudioPacketCount = 0;   
volatile INT32 g_nSamplesAvail = 0;

volatile BOOL g_bCODECReady = FALSE;

volatile UINT32 nUSBReadBufferNdx = 0;
volatile UINT32 nUSBWriteBufferNdx = 0;
volatile INT32 nI2SBufferNdx = 0;

volatile BOOL g_bAudioPump1Active = FALSE;  
volatile BOOL g_bAudioPump2Active = FALSE; 
volatile BOOL g_AudioProcessingActive = FALSE;
volatile BOOL g_bAudioPumpActive = FALSE;

volatile UINT32 g_nPumpCount = 0; 

DSPC_ASRC USB_ASRC;

static volatile fract32 nVolCurrentGain = 0;

volatile fract32 g_nNewVolGain = 0;


//-----------------------------------------------------------------------------
// METHOD:  AudioInit
// PURPOSE: Initialize AWE
//-----------------------------------------------------------------------------
AWE_OPTIMIZE_FOR_SPACE
AWE_FW_SLOW_CODE
void AudioInit(void)
{        
    // Initialize the ASRC for USB 
	dspc_asrc_init_Q15(&DSPC_ASRC_48000_TO_48000_32M_080DB_PROPS, 
                       USB_ASRCBuffer, 
                       DSPC_ASRC_STATE_LEN(INPUT_BLOCKSIZE, OUTPUT_BLOCKSIZE, ASRC_FS_IN_48000, ASRC_PHASELEN_28), 
                       DSPC_ASRC_GUARD_LEN(ASRC_FS_IN_48000, ASRC_FS_OUT_48000, ASRC_PHASELEN_28, OUTPUT_BLOCKSIZE), 
                       STEREO_CHANNEL_COUNT, 
                       &USB_ASRC);     

    // USB audio interrupt
    HAL_NVIC_SetPriority(OTG_FS_IRQn, 4, 0);
    HAL_NVIC_EnableIRQ(OTG_FS_IRQn);  
           
    // Ready to send next USB reply packet
    HAL_NVIC_SetPriority(ProcessUSBMsg_IRQ, 4, 0);
    HAL_NVIC_EnableIRQ(ProcessUSBMsg_IRQ);
    
    // CODEC I2S Processing Interrupt
    HAL_NVIC_SetPriority(DMA1_Stream7_IRQn, 3, 0);
    HAL_NVIC_EnableIRQ(DMA1_Stream7_IRQn); 
    
    // DPC to process writing to ASRC
    HAL_NVIC_SetPriority(ProcessWriteASRC_IRQ, 3, 0);
    HAL_NVIC_EnableIRQ(ProcessWriteASRC_IRQ);                     
    
    // Higher Level AWE Processing Interrupt
    HAL_NVIC_SetPriority(AudioWeaverPump_IRQ1, 6, 0);
    HAL_NVIC_EnableIRQ(AudioWeaverPump_IRQ1);  
    
    // Lower level AWE Processing Interrupt
    HAL_NVIC_SetPriority(AudioWeaverPump_IRQ2, 7, 0);
    HAL_NVIC_EnableIRQ(AudioWeaverPump_IRQ2);    
   
    // When no USB audio playing pass this buffer of silence to the CODEC
    memset(ZeroedSamples, 0, OUTPUT_BLOCKSIZE * OUTPUT_CHANNEL_COUNT * sizeof(fract16) );
    
    // CODEC double buffer
    memset(I2SBuffer, 0, I2S_OUT_BUFFER_SIZE * sizeof(INT16) );
    
    // Start CODEC continuously playing
    HAL_I2S_Transmit_DMA(&hI2S3, (UINT16 *)I2SBuffer, I2S_OUT_BUFFER_SIZE);  

    // Set pin PD0 as output to toggle for timing code
    awe_pltGPIOSetPinDir(31, GPIO_DIR_OUTPUT);    
    awe_pltGPIOSetPin(31, 0);
    
     // Set pin PD2 as output to toggle for timing code
    awe_pltGPIOSetPinDir(33, GPIO_DIR_OUTPUT);    
    awe_pltGPIOSetPin(33, 0);
    
}	// End AudioInit



//-----------------------------------------------------------------------------
// METHOD:  AWEProcessing
// PURPOSE: Pass new samples to Audio Weaver and return processed samples
//-----------------------------------------------------------------------------
void AWEProcessing(fract16 * pUSBSamples, fract16 * pProcessedSamples)
{
    INT32  fwInCount, fwOutCount;
    UINT32  used_chans, chan;        
    INT32   pinStride;	
    INT16   n16BitSample; 
    INT32   n32BitSample;
    UINT32  layoutMask;  
    INT32   nSample;  
    INT32 * dest32BitPtr;
    fract32 * src32BitPtr;
    fract16 * src16BitPtr;    
    INT16 * dest16BitPtr;      

    // If no AWE layout wired up then copy input to output
	if (!s_AudioRunning)
    { 
        //memset(pProcessedSamples, 0, AWE_OUTPUT_BUFFER_SIZE_IN_SAMPLES * PCM_SAMPLE_SIZE_IN_BYTES);
        
        // ****************** FOR DEBUG ************************************
        for (nSample = 0; nSample < AWE_FRAME_SIZE; nSample++)
        {
            // Send USB left channel to output left channel
            *pProcessedSamples++ = *pUSBSamples++;
        
            // Send USB right channel to output right channel
            *pProcessedSamples++ = *pUSBSamples++;        
        }
        // ******************************************************************
    }
    else
    {  
        // Get Current audio layout number of channels
        awe_fwGetChannelCount(&fwInCount, &fwOutCount);
        
        if (fwInCount > 0)
        {						           
            // Get the input pin channels actually used
            used_chans = (fwInCount < INPUT_CHANNEL_COUNT) ? fwInCount : INPUT_CHANNEL_COUNT;

            // For USB the block size is in units of stereo samples and there can be only two channels
            for (chan = 0; chan < used_chans; chan++)
            {                
                // Determine where the Framework wants the input data written
                dest32BitPtr = awe_fwGetInputChannelPtr(chan, &pinStride);
                                 
                src16BitPtr = pUSBSamples + chan;
                
                for (nSample = 0; nSample < AWE_FRAME_SIZE; nSample++)
                {    
                    *dest32BitPtr = *src16BitPtr; 
                    
                    *dest32BitPtr <<= 16;
                    
                    dest32BitPtr += pinStride;
                    
                    // Source channels are interleaved
                    src16BitPtr += STEREO_CHANNEL_COUNT;				
                }
            }

            // Zero any unused layout inputs
            for ( ; chan < fwInCount; chan++)
            {
                // Determine where the Framework wants the input data written
                dest32BitPtr = awe_fwGetInputChannelPtr(chan, &pinStride); 
                
                for (nSample = 0; nSample < AWE_FRAME_SIZE; nSample++)
                {
                    *dest32BitPtr = 0;
                     dest32BitPtr += pinStride;				
                }
            }									
        }
               
        for (chan = 0; chan < OUTPUT_CHANNEL_COUNT; chan++)
        {
            if (chan < fwOutCount)
            {
                // Determine where the Framework wants the input data written								
                src32BitPtr = (fract32 *)awe_fwGetOutputChannelPtr(chan, &pinStride);
                
                // Implement output volume control
                //awe_vecScaleSmoothFract32(src32BitPtr, pinStride, src32BitPtr, pinStride, 
                //                          (fract32 *)&nVolCurrentGain, g_nNewVolGain, 0, SMOOTHING_COEFF, AWE_FRAME_SIZE);
                
                //nVolCurrentGain = g_nNewVolGain;
                
                dest16BitPtr = pProcessedSamples + chan;
                
                for (nSample = 0; nSample < AWE_FRAME_SIZE; nSample++)
                {
                    n32BitSample = *src32BitPtr;
                    
                    // Audio Weaver has sample in high order bytes of 32-bit word
                    n16BitSample = n32BitSample >> PCM_SAMPLE_SIZE_IN_BITS;                  
                    
                    *dest16BitPtr = n16BitSample;

                    src32BitPtr += pinStride;

                    // Output samples are interleaved
                    dest16BitPtr += OUTPUT_CHANNEL_COUNT;					 		
                }
            } 
            else 
            {
				// The layout doesn't have enough channels; fill the remaining one with zeros
                dest16BitPtr = pProcessedSamples + chan;
                
                for (nSample = 0; nSample < AWE_FRAME_SIZE; nSample++)
                {
                    *dest16BitPtr = 0;
                    
                    // Output samples are interleaved
                    dest16BitPtr += OUTPUT_CHANNEL_COUNT;					 		
                }
            }
        } 

        layoutMask = awe_fwAudioDMAComplete(AWE_FRAME_SIZE);

        // If higher priority level processing ready pend an interrupt for it
        if (layoutMask & 1)
        {
            if (s_AudioRunning && !g_bAudioPump1Active)
            { 
                NVIC_SetPendingIRQ(AudioWeaverPump_IRQ1);                
            }            
        }
        
        // If lower priority level processing ready pend an interrupt for it
        if (layoutMask & 2)
        {
            if (s_AudioRunning && !g_bAudioPump2Active)
            {          
                NVIC_SetPendingIRQ(AudioWeaverPump_IRQ2); 
            }
        }
        
        // When this higher level priority method returns, processing will resume
        // with AudioWeaverPump_IRQ1 or AudioWeaverPump_IRQ2 if triggered.
        // If both were triggered then processing for AudioWeaverPump_IRQ2 will start
        // only when the processing for AudioWeaverPump_IRQ1 has completed
    }
	
}	// End AWEProcessing


//-----------------------------------------------------------------------------
// METHOD:  I2S3_DMA_Complete_CallBack
// PURPOSE: DMA interrupt complete for the CODEC
//-----------------------------------------------------------------------------
void I2S3_DMA_Complete_CallBack(void)
{   
    int nSamplesAvail;  
    fract16 * pUSBSamples; 

/*    
    int n;  
        
    static INT32 ndx = 0;
    
    awe_pltGPIOTogglePin(33);
    
    for (n = 0; n < OUTPUT_BLOCKSIZE; n++)
    {
        USB_ASRCSamples[n*2] = SineTable[ndx];
        USB_ASRCSamples[n*2 + 1] = SineTable[ndx];
        
        // Next Sine table entry with buffer wrap
        ndx = (ndx + 1) % g_nTableElements;
    }
    
    pUSBSamples = USB_ASRCSamples; 

*/        
    // Get the number of available USB samples in the ASRC jitter buffer
    nSamplesAvail = dspc_asrc_output_avail(&USB_ASRC);
    
    if (nSamplesAvail >= OUTPUT_BLOCKSIZE)
    {
        // Toggle Pin PD2 high for generating a timing trace on a scope
        //awe_pltGPIOSetPin(33, 1);
        
        // Read a block of samples from the ASRC jitter buffer
        dspc_asrc_read_output_linear_Q15(&USB_ASRC, USB_ASRCSamples, OUTPUT_BLOCKSIZE);     
        
        // Toggle Pin PD2 low
        //awe_pltGPIOSetPin(33, 0);
        
        pUSBSamples = USB_ASRCSamples;       
    }
    else
    {
        // Insert zeros if no audio samples available (CODEC continually runs)
        pUSBSamples = ZeroedSamples;               
    }
    
    // Place the samples in the AWE processing buffers
    AWEProcessing(pUSBSamples, &I2SBuffer[nI2SBufferNdx]); 
    
    // CODEC double buffer
    nI2SBufferNdx = (nI2SBufferNdx + NEW_CODEC_SAMPLES) % I2S_OUT_BUFFER_SIZE;
        
 }  // End I2S3_DMA_Complete_CallBack



//-----------------------------------------------------------------------------
// METHOD:  ProcessWriteASRC_IRQHandler
// PURPOSE: DPC for writing to ASRC buffer
//-----------------------------------------------------------------------------
void ProcessWriteASRC_IRQHandler(void)
{
    // Insert 48 stereo samples that just arrived over USB into the ASRC jitter buffer
    dspc_asrc_write_input_Q15(&USB_ASRC, &USBBuffer[nUSBReadBufferNdx], INPUT_BLOCKSIZE);
    
    // USB Audio double buffer
    nUSBReadBufferNdx = (nUSBReadBufferNdx + NEW_USB_SAMPLES) % AUDIO_BUFFER_SIZE_IN_SAMPLES; 
    
    // Manually clear software interrupt
    NVIC_ClearPendingIRQ(ProcessWriteASRC_IRQ);
    
}   // End ProcessWriteASRC_IRQHandler



//-----------------------------------------------------------------------------
// METHOD:  AudioWeaver Pump Interrupt Handler
// PURPOSE: Perform AudioWeaver Processing
//-----------------------------------------------------------------------------
void AudioWeaverPump_IRQHandler1(void)
{  
    g_bAudioPump1Active = TRUE;

    // If IDLE loop did not get some CPU time then skip the pump
    // g_nPumpCount gets reset in the idle loop method awe_pltTick
    if (g_nPumpCount++ < MAX_PUMP_COUNT)
    {
        // Perform highest priority AWE processing
        awe_fwPump(0);
    }
    
    g_bAudioPump1Active = FALSE; 
    
    // Manually clear software interrupt
    NVIC_ClearPendingIRQ(AudioWeaverPump_IRQ1);    

}   // End AudioWeaverPump_IRQHandler1


//-----------------------------------------------------------------------------
// METHOD:  AudioWeaver Pump Interrupt Handler
// PURPOSE: Perform AudioWeaver Processing
//-----------------------------------------------------------------------------
void AudioWeaverPump_IRQHandler2(void)
{    
    g_bAudioPump2Active = TRUE;

    // Perform medium priority AWE processing (FFT, etc)
    awe_fwPump(1);
    
    g_bAudioPump2Active = FALSE;   
    
    // Manually clear software interrupt
    NVIC_ClearPendingIRQ(AudioWeaverPump_IRQ2);      
       
}   // End AudioWeaverPump_IRQHandler2

