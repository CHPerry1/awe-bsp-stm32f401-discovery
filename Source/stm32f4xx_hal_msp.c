/*******************************************************************************
*
*               Audio Framework
*               ---------------
*
********************************************************************************
*     stm32f4xx_hal_msp.c
********************************************************************************
*
*     Description:  AudioWeaver MSP setup
*
*     Copyright:    (c) DSP Concepts, Inc. 2007 - 2016
*                   1800 Wyatt Drive, Suite 14
*                   Sunnyvale, CA 95054
*
*******************************************************************************/
#include "stm32f4xx_hal.h"

extern DMA_HandleTypeDef DMA_I2C1_TX;

extern DMA_HandleTypeDef DMA_I2C1_RX;

extern DMA_HandleTypeDef DMA_I2S2_RX;

extern DMA_HandleTypeDef DMA_I2S3_TX;


//-----------------------------------------------------------------------------
// METHOD:  HAL_MspInit
// PURPOSE: 
//-----------------------------------------------------------------------------
void HAL_MspInit(void)
{
  HAL_NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4);

  /* MemoryManagement_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(MemoryManagement_IRQn, 0, 0);
    
  /* BusFault_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(BusFault_IRQn, 0, 0);
    
  /* UsageFault_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(UsageFault_IRQn, 0, 0);
    
  /* DebugMonitor_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DebugMonitor_IRQn, 0, 0);
    
  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);

}   // End HAL_MspInit


//-----------------------------------------------------------------------------
// METHOD:  HAL_I2C_MspInit
// PURPOSE: 
//-----------------------------------------------------------------------------
void HAL_I2C_MspInit(I2C_HandleTypeDef* hI2C)
{    
    //printf("HAL_I2C_MspInit\n");
    
    if (hI2C->Instance == I2C1)
    { 
        __HAL_RCC_I2C1_CLK_ENABLE(); 
        
        __HAL_RCC_I2C1_FORCE_RESET();
        
        __HAL_RCC_I2C1_RELEASE_RESET();                
    }
    
}   // End HAL_I2C_MspInit


//-----------------------------------------------------------------------------
// METHOD:  HAL_I2C_MspDeInit
// PURPOSE: 
//-----------------------------------------------------------------------------
void HAL_I2C_MspDeInit(I2C_HandleTypeDef* hI2C)
{
  if (hI2C->Instance == I2C1)
  {
    __HAL_RCC_I2C1_CLK_DISABLE();  
  }

}   // End HAL_I2C_MspDeInit


//-----------------------------------------------------------------------------
// METHOD:  HAL_I2S_MspInit
// PURPOSE: 
//-----------------------------------------------------------------------------
void HAL_I2S_MspInit(I2S_HandleTypeDef* hI2S)
{       
    if (hI2S->Instance == SPI3)
    {   
        __HAL_RCC_DMA1_CLK_ENABLE();
      
        DMA_I2S3_TX.Instance            = DMA1_Stream7;
        DMA_I2S3_TX.Init.Channel        = DMA_CHANNEL_0;
        DMA_I2S3_TX.Init.Direction      = DMA_MEMORY_TO_PERIPH;
        DMA_I2S3_TX.Init.PeriphInc      = DMA_PINC_DISABLE;
        DMA_I2S3_TX.Init.MemInc         = DMA_MINC_ENABLE;
        DMA_I2S3_TX.Init.PeriphDataAlignment = DMA_PDATAALIGN_HALFWORD;
        DMA_I2S3_TX.Init.MemDataAlignment = DMA_MDATAALIGN_HALFWORD;
        DMA_I2S3_TX.Init.Mode          = DMA_CIRCULAR;
        DMA_I2S3_TX.Init.Priority      = DMA_PRIORITY_VERY_HIGH;
        DMA_I2S3_TX.Init.FIFOMode      = DMA_FIFOMODE_ENABLE;         
        DMA_I2S3_TX.Init.FIFOThreshold = DMA_FIFO_THRESHOLD_FULL;
        DMA_I2S3_TX.Init.MemBurst      = DMA_MBURST_SINGLE;
        DMA_I2S3_TX.Init.PeriphBurst   = DMA_PBURST_SINGLE;
        
        HAL_DMA_Init(&DMA_I2S3_TX);

        __HAL_LINKDMA(hI2S, hdmatx, DMA_I2S3_TX);

        __HAL_RCC_SPI3_CLK_ENABLE();
    }
    else if (hI2S->Instance == SPI2)
    {
        __HAL_RCC_DMA1_CLK_ENABLE();
        
        DMA_I2S2_RX.Instance                 = DMA1_Stream3;
        DMA_I2S2_RX.Init.Channel             = DMA_CHANNEL_0;
        DMA_I2S2_RX.Init.Direction           = DMA_PERIPH_TO_MEMORY;
        DMA_I2S2_RX.Init.PeriphInc           = DMA_PINC_DISABLE;
        DMA_I2S2_RX.Init.MemInc              = DMA_MINC_ENABLE;
        DMA_I2S2_RX.Init.PeriphDataAlignment = DMA_PDATAALIGN_HALFWORD;
        DMA_I2S2_RX.Init.MemDataAlignment    = DMA_MDATAALIGN_HALFWORD;
        DMA_I2S2_RX.Init.Mode                = DMA_CIRCULAR;
        DMA_I2S2_RX.Init.Priority            = DMA_PRIORITY_HIGH;
        DMA_I2S2_RX.Init.FIFOMode            = DMA_FIFOMODE_DISABLE;         
        DMA_I2S2_RX.Init.FIFOThreshold       = DMA_FIFO_THRESHOLD_FULL;
        DMA_I2S2_RX.Init.MemBurst            = DMA_MBURST_SINGLE;
        DMA_I2S2_RX.Init.PeriphBurst         = DMA_MBURST_SINGLE;  
        
        HAL_DMA_Init(&DMA_I2S2_RX);

        __HAL_LINKDMA(hI2S, hdmarx, DMA_I2S2_RX);

        __HAL_RCC_SPI2_CLK_ENABLE();
    }

}   // End HAL_I2S_MspInit


//-----------------------------------------------------------------------------
// METHOD:  HAL_I2S_MspDeInit
// PURPOSE: 
//-----------------------------------------------------------------------------
void HAL_I2S_MspDeInit(I2S_HandleTypeDef* hI2S)
{
    if (hI2S->Instance == SPI3)
    {
        /* Peripheral clock disable */
        __HAL_RCC_SPI3_CLK_DISABLE();

        /* Peripheral DMA DeInit*/
        HAL_DMA_DeInit(hI2S->hdmatx);
    }
    else if (hI2S->Instance == SPI2)
    {
        /* Peripheral clock disable */
        __HAL_RCC_SPI2_CLK_DISABLE();

        /* Peripheral DMA DeInit*/
        HAL_DMA_DeInit(hI2S->hdmarx);        
    }

}   // End HAL_I2S_MspDeInit
