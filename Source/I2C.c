/*******************************************************************************
*
*               Audio Framework
*               ---------------
*
********************************************************************************
*     I2C.c
********************************************************************************
*
*     Description:  I2C Read and Write 
*
*     Copyright:    DSP Concepts, Inc. (c) 2007 - 2016
*                   1800 Wyatt Drive, Suite 14
*                   Sunnyvale, CA 95054
*
*******************************************************************************/
#include "stm32f4xx_hal.h"

extern I2C_HandleTypeDef hI2C1;

#define I2C_TIMEOUT 100 

#define AUDIO_I2C_ADDRESS 0x94


/**
  * @brief  Initializes I2C HAL.
  */
void I2Cx_Init(void)
{
    HAL_I2C_StateTypeDef nI2CState;
    
    nI2CState = HAL_I2C_GetState(&hI2C1);
    
    if (nI2CState == HAL_I2C_STATE_RESET)
    { 
        __I2C1_FORCE_RESET(); 

        __I2C1_RELEASE_RESET(); 

        HAL_I2C_Init(&hI2C1);
    }
}


/**
  * @brief  Manages error callback by re-initializing I2C.
  * @param  Addr: I2C Address
  */
void I2Cx_Error(uint8_t Addr)
{
    HAL_I2C_DeInit(&hI2C1);

    I2Cx_Init();  
}


/**
  * @brief  Reads a single data.
  * @param  Addr: I2C address
  * @param  Reg: Reg address 
  * @retval Data to be read
  */
uint8_t I2Cx_Read(uint8_t Addr, uint8_t Reg)
{
    HAL_StatusTypeDef status = HAL_OK;
    uint8_t Value = 0;

    status = HAL_I2C_Mem_Read(&hI2C1, Addr, Reg, I2C_MEMADD_SIZE_8BIT, &Value, 1, I2C_TIMEOUT);

    /* Check the communication status */
    if (status != HAL_OK)
    {
        /* Execute user timeout callback */
        I2Cx_Error(Addr);
    }

    return Value;   
}


/**
  * @brief  Writes a single data.
  * @param  Addr: I2C address
  * @param  Reg: Reg address 
  * @param  Value: Data to be written
  */
void I2Cx_Write(uint8_t Addr, uint8_t Reg, uint8_t Value)
{
    HAL_StatusTypeDef status = HAL_OK;

    status = HAL_I2C_Mem_Write(&hI2C1, Addr, (uint16_t)Reg, I2C_MEMADD_SIZE_8BIT, &Value, 1, I2C_TIMEOUT); 

    /* Check the communication status */
    if (status != HAL_OK)
    {
        /* I2C error occured */
        I2Cx_Error(Addr);
    }
}


/**
  * @brief  Reads multiple data.
  * @param  Addr: I2C address
  * @param  Reg: Reg address 
  * @param  MemAddress Internal memory address
  * @param  Buffer: Pointer to data buffer
  * @param  Length: Length of the data
  * @retval Number of read data
  */
HAL_StatusTypeDef I2Cx_ReadMultiple(uint8_t Addr, uint16_t Reg, uint16_t MemAddress, uint8_t *Buffer, uint16_t Length)
{
    HAL_StatusTypeDef status = HAL_OK;

    status = HAL_I2C_Mem_Read(&hI2C1, Addr, (uint16_t)Reg, MemAddress, Buffer, Length, I2C_TIMEOUT);

    if (status != HAL_OK)
    {
        /* I2C error occured */
        I2Cx_Error(Addr);
    }
    
    return status;
} 
    

/**
  * @brief  Write a value in a register of the device through BUS in using DMA mode
  * @param  Addr: Device address on BUS Bus.  
  * @param  Reg: The target register address to write
  * @param  MemAddress Internal memory address
  * @param  Buffer: The target register value to be written 
  * @param  Length: buffer size to be written
  * @retval HAL status
  */
HAL_StatusTypeDef I2Cx_WriteMultiple(uint8_t Addr, uint16_t Reg, uint16_t MemAddress, uint8_t *Buffer, uint16_t Length)
{
    HAL_StatusTypeDef status = HAL_OK;

    status = HAL_I2C_Mem_Write(&hI2C1, Addr, (uint16_t)Reg, MemAddress, Buffer, Length, I2C_TIMEOUT);

    /* Check the communication status */
    if (status != HAL_OK)
    {
        /* Re-Initiaize the I2C Bus */
        I2Cx_Error(Addr);
    }
    
    return status;
}


/**
  * @brief  Checks if target device is ready for communication. 
  * @note   This function is used with Memory devices
  * @param  DevAddress: Target device address
  * @param  Trials: Number of trials
  * @retval HAL status
  */
HAL_StatusTypeDef I2Cx_IsDeviceReady(uint16_t DevAddress, uint32_t Trials)
{ 
    return HAL_I2C_IsDeviceReady(&hI2C1, DevAddress, Trials, I2C_TIMEOUT);
}
