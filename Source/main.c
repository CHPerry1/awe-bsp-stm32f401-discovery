/*******************************************************************************
*
*               Audio Framework
*               ---------------
*
********************************************************************************
*     main.c
********************************************************************************
*
*     Description:  AudioWeaver target main program
*
*     Copyright:    DSP Concepts, Inc. (c) 2007 - 2016
*                   1800 Wyatt Drive, Suite 14
*                   Sunnyvale, CA 95054
*
*******************************************************************************/
#include "Platform.h"

int main(void)
{    
    // Initialize AWE platform
	awe_pltInit();
	
    // Initialize the framework
	//awe_fwInit(); 

    while(TRUE) 
    {
        // Process any platform tasks
        awe_pltTick();
	
        // Process any framework tasks
    	awe_fwTick();
    
    }   // End while
	
}   // End main
