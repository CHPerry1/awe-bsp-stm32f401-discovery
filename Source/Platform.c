/*******************************************************************************
*
*               Audio Framework
*               ---------------
*
********************************************************************************
*     Platform.c
********************************************************************************
*
*     Description:  AWE Platform Interface for STM Discovery board
*
*     Copyright:    DSP Concepts, Inc. (c) 2007 - 2016
*                   1800 Wyatt Drive, Suite 14
*                   Sunnyvale, CA 95054
*
*******************************************************************************/
#include <string.h>

#include "usbd_audio.h"
#include "pdm_filter.h"
#include "Errors.h"
#include "TargetInfo.h"
#include "PlatformAPI.h"
#include "Platform.h"
#include "TuningHandler.h"
#include "PinDef.h"

volatile INT32 s_AudioRunning = 0;
volatile BOOL g_bReboot = FALSE;
volatile UINT32 systicks = 0;
volatile BOOL g_bBlinkLED4ForBoardAlive = TRUE;

/* ----------------------------------------------------------------------------
 *   Describes target properties to the framework
 * ------------------------------------------------------------------------- */
 TargetInfo g_target_info =
{
	48000.0f,				// float m_sampleRate;
	84e6f,					// float m_profileClockSpeed;
	#if defined(USE_V4_FEATURES)
	AWE_FRAME_SIZE | TARGET_SUPPORTS_IDREL | TARGET_SUPPORTS_SETID | TARGET_SUPPORTS_SETMERGE,
	#else	
	AWE_FRAME_SIZE,	// UINT32 m_base_block_size;
	#endif
    #if defined(USE_FLASH_FILE_SYSTEM) || defined(USE_COMPILED_FILE_SYSTEM)
	MAKE_TARGET_INFO_PACKED(sizeof(int), INPUT_CHANNEL_COUNT, OUTPUT_CHANNEL_COUNT, TRUE, TRUE, PROCESSOR_TYPE_CORTEXM4),
    #else
    MAKE_TARGET_INFO_PACKED(sizeof(int), INPUT_CHANNEL_COUNT, OUTPUT_CHANNEL_COUNT, TRUE, FALSE, PROCESSOR_TYPE_CORTEXM4),
    #endif
	MAKE_TARGET_VERSION(1, 8, 2, 17), // This is backwards (ver, day, month, year)
	{ MAKE_PACKED_STRING('S', 'T', '3', '2'), MAKE_PACKED_STRING('F', '4', '0', '1') },
	AWE_COMMAND_BUFFER_LEN
};

/* ----------------------------------------------------------------------
** Memory heaps
** ------------------------------------------------------------------- */
AWE_FW_SLOW_ANY_CONST UINT32 g_master_heap_size = MASTER_HEAP_SIZE;
AWE_FW_SLOW_ANY_CONST UINT32 g_slow_heap_size = SLOW_HEAP_SIZE;
AWE_FW_SLOW_ANY_CONST UINT32 g_fastb_heap_size = FASTB_HEAP_SIZE;

UINT32 g_master_heap[MASTER_HEAP_SIZE];
UINT32 g_slow_heap[SLOW_HEAP_SIZE];
UINT32 g_fastb_heap[FASTB_HEAP_SIZE];

/* ----------------------------------------------------------------------
** Module table
** ------------------------------------------------------------------- */

/* Array of pointers to module descriptors. This is initialized at compile time.
Each item is the address of a module descriptor that we need linked in. The
linker magic is such that only those modules referenced here will be in the
final program. */
const ModClassModule *g_module_descriptor_table[] =
{
	// The suitably cast pointers to the module descriptors.
    LISTOFCLASSOBJECTS
};

AWE_MOD_SLOW_DM_DATA UINT32 g_module_descriptor_table_size = sizeof(g_module_descriptor_table) / sizeof(g_module_descriptor_table[0]);

#ifdef USE_GPIO

//-----------------------------------------------------------------------------
// METHOD:  awe_pltGPIOSetPinDir
// PURPOSE: Set GPIO pin direction
//-----------------------------------------------------------------------------
void awe_pltGPIOSetPinDir(UINT32 nPinNo, UINT32 nPinDir)
{
    GPIO_InitTypeDef GPIO_InitStruct;
    UINT32 nPinNdx;

    if (nPinNo < 1 || nPinNo > MAX_PINS)
    {
        return;
    }
    
    // If user wants control of LED4 relinquish board alive LED toggle
    if (nPinNo == 3 && nPinDir != GPIO_DIR_INPUT)
    {
        g_bBlinkLED4ForBoardAlive = FALSE; 
    }        
    
    nPinNdx = nPinNo - 1;
        
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    GPIO_InitStruct.Pull = GPIO_NOPULL; 
   
    if (nPinDir == GPIO_DIR_INPUT)
    {
        GPIO_InitStruct.Pin = InputPinMap[nPinNdx].GPIO_Pin;        
        GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
                
        // Setup the GPIO pin
        HAL_GPIO_Init(InputPinMap[nPinNdx].GPIOx, &GPIO_InitStruct);    
    }
    else
    {
        GPIO_InitStruct.Pin = OutputPinMap[nPinNdx].GPIO_Pin; 
        GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;        
        
        // Setup the GPIO pin
        HAL_GPIO_Init(OutputPinMap[nPinNdx].GPIOx, &GPIO_InitStruct); 

        // Initialize output pin to 0
        awe_pltGPIOSetPin(nPinNo, 0);        
    }

}   // End awe_pltGPIOSetPinDir


//-----------------------------------------------------------------------------
// METHOD:  awe_pltGPIOTogglePin
// PURPOSE: Set GBPIO pin value
//-----------------------------------------------------------------------------
void awe_pltGPIOTogglePin(UINT32 nPinNo)
{
    UINT32 nPinNdx;

    if (nPinNo < 1 || nPinNo > MAX_PINS)
    {
        return;
    }  
    
    nPinNdx = nPinNo - 1;
    
    HAL_GPIO_TogglePin(OutputPinMap[nPinNdx].GPIOx, OutputPinMap[nPinNdx].GPIO_Pin);
    
}   // End awe_pltGPIOTogglePin


//-----------------------------------------------------------------------------
// METHOD:  awe_pltGPIOSetPin
// PURPOSE: Set GBPIO pin value
//-----------------------------------------------------------------------------
void awe_pltGPIOSetPin(UINT32 nPinNo, UINT32 nValue)
{
    UINT32 nPinNdx;

    if (nPinNo < 1 || nPinNo > MAX_PINS)
    {
        return;
    }  
    
    nPinNdx = nPinNo - 1;
 
    if (nValue > 0)
    {
        HAL_GPIO_WritePin(OutputPinMap[nPinNdx].GPIOx, OutputPinMap[nPinNdx].GPIO_Pin, GPIO_PIN_SET);
     }
    else
    {
        HAL_GPIO_WritePin(OutputPinMap[nPinNdx].GPIOx, OutputPinMap[nPinNdx].GPIO_Pin, GPIO_PIN_RESET);
    }             

}   // End awe_pltGPIOSetPin


//-----------------------------------------------------------------------------
// METHOD:  awe_pltGPIOGetPin
// PURPOSE: Get GPIO pin value
//-----------------------------------------------------------------------------
void awe_pltGPIOGetPin(UINT32 nPinNo, UINT32 * nValue)
{ 
    UINT32 nPinNdx;

    if (nPinNo < 1 || nPinNo > MAX_PINS)
    {
        return;
    }
    
    nPinNdx = nPinNo - 1;
    
    *nValue = HAL_GPIO_ReadPin(InputPinMap[nPinNdx].GPIOx, InputPinMap[nPinNdx].GPIO_Pin);

}   // End awe_pltGPIOGetPin

#endif


//-----------------------------------------------------------------------------
// METHOD:  awe_pltInit
// PURPOSE: Initialize AWE
//-----------------------------------------------------------------------------
AWE_OPTIMIZE_FOR_SPACE
AWE_FW_SLOW_CODE
void awe_pltInit(void)
{ 
    // Publish what this target supports
    g_target_control_flags = TARGET_SUPPORTS_IDREL | TARGET_SUPPORTS_SETID | TARGET_SUPPORTS_SETMERGE;
    
    // Setup processor clocks, signal routing, timers, etc.
    CoreInit();
    
    // Setup board peripherals (CODECs, external memory, etc.)
    BoardInit();
    
    // Setup audio DMA, interrupt priorities, etc.
    AudioInit();
    
    // Setup communication channel for monitoring and control
    USBMsgInit();	
    	
  	// Initialize the Flash File System
  	awe_pltInitFlashFileSystem();  
       
}	// End awe_pltInit


//-----------------------------------------------------------------------------
// METHOD:  awe_pltTick
// PURPOSE: Platform heartbeat tick
//-----------------------------------------------------------------------------
AWE_OPTIMIZE_FOR_SPEED
AWE_FW_SLOW_CODE
void awe_pltTick(void)
{   
    BOOL bReplyReady;
    
    // Indicate that this idle loop call is getting CPU attention
    g_nPumpCount = 0; 
    
    CheckForUSBPacketReady();
    
    bReplyReady = awe_fwTuningTick();
    
    if (bReplyReady == REPLY_READY)
    {
        USBSendReply();

        if (g_bReboot)
        {
            g_bReboot = FALSE;
            HAL_Delay(500);
            HAL_NVIC_SystemReset();
        }
    }
		
}	// End awe_pltTick


//-----------------------------------------------------------------------------
// METHOD:  awe_pltGetCycleCount
// PURPOSE: Returns the current value in the counter
//-----------------------------------------------------------------------------
UINT32 awe_pltGetCycleCount(void)
{   
	UINT32 nCycles, nElapsedMilliSecs;
    
    // This value is 84,000
    UINT32 nReloadValue = SysTick->LOAD + 1;
    
    nElapsedMilliSecs = HAL_GetTick();
    
    // Current COUNTDOWN value ( 0 - 83,999 )
	nCycles = SysTick->VAL;	
    
    nElapsedMilliSecs = (nElapsedMilliSecs * nReloadValue) + nReloadValue - nCycles;	
          	
	return nElapsedMilliSecs;
    
}   // End awe_pltGetCycleCount


//-----------------------------------------------------------------------------
// METHOD:  awe_pltElapsedCycles
// PURPOSE: Returns the cycle count between start time and end time
//-----------------------------------------------------------------------------
UINT32 awe_pltElapsedCycles(UINT32 nStartTime, UINT32 nEndTime)
{
    UINT32 nElapsedTime;
    
	if (nEndTime > nStartTime)
	{
		nElapsedTime = nEndTime - nStartTime;
	}
	else
	{
		// Wrap around occurred 
		nElapsedTime = ((((UINT32)0xFFFFFFFF) - nStartTime) + nEndTime + 1);

        // Correct for race condition reading ms tick and elapsed cycle count
        if ( (nElapsedTime > 84000) && (nElapsedTime < 168000) )
        {
          nElapsedTime -= 84000;
        }
        else if (nElapsedTime > ( (UINT32)0xFFFFFFFF - 84000) )
        {
          nElapsedTime += 84000;
        }
    }
 
	return nElapsedTime; 
    
}   // End awe_pltElapsedCycles


//----------------------------------------------------------------------------
// METHOD:  awe_pltAudioStart
// PURPOSE: Start processing audio with AWE
//----------------------------------------------------------------------------
AWE_OPTIMIZE_FOR_SPACE 
AWE_FW_SLOW_CODE
INT32 awe_pltAudioStart(void)
{
	// Mark the audio as started
	s_AudioRunning = 1;

	return 0;
    
}   // End awe_pltAudioStart


//----------------------------------------------------------------------------
// METHOD:  awe_pltAudioStop
// PURPOSE: Stop processing audio with AWE
//----------------------------------------------------------------------------
AWE_OPTIMIZE_FOR_SPACE 
AWE_FW_SLOW_CODE
INT32 awe_pltAudioStop(void)
{
	// Mark the audio as stopped
	s_AudioRunning = 0;

	return 0;
    
}   // End awe_pltAudioStop
